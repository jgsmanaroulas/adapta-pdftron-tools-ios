#include "PDF/JobRequest.hpp"
#include "PDF/Page.h"

#if TRN_HAS_FAST_HTML_REFLOW
#include "Common/Atomic.hpp"
#include "Common/Config.h"
#include "Common/Threads.hpp"
#include "Convert/Reflow/ReflowRequest.hpp"
#include <boost/filesystem/path.hpp>

namespace fs = boost::filesystem;
#endif // if (TRN_HAS_FAST_HTML_REFLOW)

namespace trn {
	namespace PDF {
/************************************************************************/
/* ReflowProcessor                                                      */
/************************************************************************/
/*
* ReflowProcessor is a processor that manages the requests of converting PDF page documents to
*     reflow documents as HTML files. The output reflowable HTML file is identified by the requested page
*     itself. For each PDF document that at least one of its page is requested for converting to reflow,
*     a folder is created in cache directory. The folder name will be set to a unique ID of the PDF document 
*     (obtained from PDFUtil::GetUniqueIdString).
*     Also, the output HTML file is obtained by the page object number. For example, the output HTML path
*     of a PDF page with document ID of 2740180447,229945,WT1NOmJhyyH9xNqwbEbbOg== and object number
*     of 123 will be ../trn_reflowcache/2740180447,229945,WT1NOmJhyyH9xNqwbEbbOg==/123.html
*
* The cache is placed in the sub-directory 'trn_reflowcache' of the PDFNet Resources folder, which can be
*    set by calling PDFNet::SetPersistentCachePath. In the case the PersistentCachePath is not set the HTML
*    files will be stored in the system temp path (PDFNet::GetSystemTempPath()). The cache will be used by any
*    PDFNet process that uses the resource path. If more than one PDFNet process is running concurrently, the
*    first process to acquire a lock on the cache will use it. Subsequent processes will not read or modify
*    the cache.
*/
class ReflowProcessor
{
public:
	/*
	* initialize reflow cache and job queues
	* NOTE: calling this method clears everything in reflow disk cache
	*/
	static void Initialize(); 
	/*
	* extract reflow from a page and callback the proc handler thereafter
	*/
	static void GetReflow(const Page & page, JobRequest::ProcHandler proc, void * custom_data);
	/*
	* cancel all job requests
	*/
	static void CancelAllRequests();
	/*
	* cancel a certain request
	*/
	static void CancelRequest(const Page & page);
	/*
	* cancel all requests belonging to a document
	*/
	//static void CancelRequest(const string & doc_id);
	/*
	* clear the reflow disk cache
	*/
	static void ClearCache();
};

#if TRN_HAS_FAST_HTML_REFLOW
/************************************************************************/
/* ReflowProcessorImpl                                                  */
/************************************************************************/
class ReflowProcessorImpl
{
public:
	static ReflowProcessorImpl & GetInstance();
	static bool HasInstance();

	void Initialize(); // NOTE: calling this method clears everything in reflow disk cache
	void GetReflow(const Page & page, JobRequest::ProcHandler proc, void * custom_data);
	void CancelAllRequests();
	void CancelRequest(const Page & page);
	void CancelRequest(const string & doc_id);
	void ClearCache();

private:
	ReflowProcessorImpl();
	ReflowProcessorImpl(const ReflowProcessorImpl & other); // no implementation (not allowed)

	UString GetReflowPath(const Page & page);
	UString GetReflowPath(const string & doc_id, UInt32 page_id);
	UString GetReflowParentPath(const string & doc_id);
	bool ClearCacheDirectory(std::list<UString> & directory_list);

	static pdfnet_thread_space::mutex st_mutex;
	static AutoPtr<ReflowProcessorImpl> st_instance;
	static const UString st_CacheDirectoryName;

	bool m_initialized;
	ReflowRequestManager m_request_manager;
	fs::path m_cache_dir;

	typedef  pdfnet_atomic_ns::atomic<bool> tAtomicBool;
	tAtomicBool m_was_cancelled_clear_cache;

	pdfnet_thread_space::thread m_clear_cache_thread;
	pdfnet_thread_space::mutex m_clear_cache_mutex;
};
#endif // if (TRN_HAS_FAST_HTML_REFLOW)
	}	// namespace PDF
}	// namespace pdftron
