//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "Eraser.h"
#import "PanTool.h"
@class PTPDFViewCtrl;

@interface Eraser ()
{
	int m_startPageNum;
}
@end

@implementation Eraser

-(void)ignorePinch:(UIGestureRecognizer *)gestureRecognizer
{
    return;
}

-(CGPoint) midPoint:(CGPoint)p1 p2:(CGPoint)p2
{
	
    return CGPointMake((p1.x + p2.x) * 0.5, (p1.y + p2.y) * 0.5);
	
}

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        pdfNetAnnotType = [PTInk class];
        self.opaque = NO;
        
		m_startPageNum = 0; // non-existant page in PDF
        
        m_eraser_half_width = 10.0;
		
		UIPinchGestureRecognizer* pgr = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(ignorePinch:)];
		
        [self addGestureRecognizer:pgr];
        
        self.frame = CGRectMake([m_pdfViewCtrl GetHScrollPos], [m_pdfViewCtrl GetVScrollPos], m_pdfViewCtrl.frame.size.width, m_pdfViewCtrl.frame.size.height);
        
    }
    
    return self;
}

-(PTAnnotType)annotType
{
	return e_ptInk;
}

- (void)drawRect:(CGRect)rect
{
    if( m_free_hand_points )
    {
        NSValue* val;
        CGPoint previousPoint1 = CGPointZero, previousPoint2 = CGPointZero, currentPoint;
        
        context = UIGraphicsGetCurrentContext();
        
		if( ! context )
			return;
		
        [self setupContextFromDefaultAnnot:context];
        
        CGContextSetLineJoin(context, kCGLineJoinRound);
        
        CGPoint firstPoint = val.CGPointValue;
        CGContextMoveToPoint(context, firstPoint.x, firstPoint.y);
        previousPoint1 = CGPointZero, previousPoint2 = CGPointZero;
        
        for (NSValue* val in m_free_hand_points)
        {
            currentPoint = val.CGPointValue;
            
            if( CGPointEqualToPoint(previousPoint1, CGPointZero))
                previousPoint1 = currentPoint;
            
            if( CGPointEqualToPoint(previousPoint2, CGPointZero))
                previousPoint2 = currentPoint;
            
            CGPoint mid1 = [self midPoint:previousPoint1 p2:previousPoint2];
            CGPoint	mid2 = [self midPoint:currentPoint p2:previousPoint1];
            
            CGContextMoveToPoint(context, mid1.x, mid1.y);
            
            CGContextAddQuadCurveToPoint(context, previousPoint1.x, previousPoint1.y, mid2.x, mid2.y);
            
            previousPoint2 = previousPoint1;
            previousPoint1 = currentPoint;
        }
        
        CGContextStrokePath(context);
    }
}

- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = touches.allObjects[0];
    
    startPoint = [touch locationInView:m_pdfViewCtrl];
    
    pageNumber = [m_pdfViewCtrl GetPageNumberFromScreenPt:startPoint.x y:startPoint.y];
    
    if (pageNumber < 1) {
        return YES;
    }
    
	if( context )
		CGContextSetLineJoin(context, kCGLineJoinRound);
    
    CGPoint pagePoint = CGPointMake(startPoint.x, startPoint.y);
    
	
	m_free_hand_points = [[NSMutableArray alloc] initWithCapacity:50];
	
	[m_free_hand_points addObject:[NSValue valueWithCGPoint:pagePoint]];
    
    m_current_point = CGPointMake(pagePoint.x, pagePoint.y);
    m_prev_point = CGPointMake(pagePoint.x, pagePoint.y);

    m_startPageNum = pageNumber;
    
    self.frame = CGRectMake([m_pdfViewCtrl GetHScrollPos], [m_pdfViewCtrl GetVScrollPos], m_pdfViewCtrl.bounds.size.width, m_pdfViewCtrl.bounds.size.height);
    
    return YES;
}

- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = touches.allObjects[0];
    CGPoint aPoint = [touch locationInView:m_pdfViewCtrl];
    pageNumber = [m_pdfViewCtrl GetPageNumberFromScreenPt:aPoint.x y:aPoint.y];
    
	if (pageNumber < 1 || m_startPageNum != pageNumber) {
        return YES;
    }
    
    CGPoint pagePoint = CGPointMake(aPoint.x, aPoint.y);
    
    [m_free_hand_points addObject:[NSValue valueWithCGPoint:pagePoint]];
    m_current_point.x = pagePoint.x;
    m_current_point.y = pagePoint.y;
    CGPoint _current = CGPointMake(m_current_point.x, m_current_point.y);
    CGPoint _prev = CGPointMake(m_prev_point.x, m_prev_point.y);
    
    [self setNeedsDisplay];
    
    // Erase
    PTPDFPoint* pdfPoint1 = [[PTPDFPoint alloc] init];
    PTPDFPoint* pdfPoint2 = [[PTPDFPoint alloc] init];
    
    [self ConvertScreenPtToPagePtX:&_prev.x Y:&_prev.y PageNumber:m_startPageNum];
    [self ConvertScreenPtToPagePtX:&_current.x Y:&_current.y PageNumber:m_startPageNum];
    
    [pdfPoint1 setX:_prev.x];
    [pdfPoint1 setY:_prev.y];
    [pdfPoint2 setX:_current.x];
    [pdfPoint2 setY:_current.y];
    
    @try {
        PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
        [m_pdfViewCtrl DocLock:YES];
        PTPage* pg = [doc GetPage:m_startPageNum];
        if( [pg IsValid] )
        {
            int annot_num = [pg GetNumAnnots];
            for(int i=annot_num-1;i>=0;--i){
                PTAnnot *annot = [pg GetAnnot:i];
                if (![annot IsValid]) continue;
                if([annot GetType] == e_ptInk) {
                    PTInk *ink = [[PTInk alloc]initWithAnn:annot];
                    
                    if ([ink Erase:pdfPoint1 pt2:pdfPoint2 width:m_eraser_half_width]) {
                        if (!m_ink_list) {
                            m_ink_list = [[NSMutableArray alloc] initWithCapacity:10];
                        }
                        if ([self canAddObjectToInkList:ink]) {
                            [m_ink_list addObject:ink];
                        }
                    }
                }
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
        m_prev_point.x = m_current_point.x;
        m_prev_point.y = m_current_point.y;
    }
    
    return YES;
}

- (BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (m_free_hand_points.count == 1) {
        // Erase
        CGPoint _prev = CGPointMake(m_prev_point.x, m_prev_point.y);
        PTPDFPoint* pdfPoint1 = [[PTPDFPoint alloc] init];
        
        [self ConvertScreenPtToPagePtX:&_prev.x Y:&_prev.y PageNumber:m_startPageNum];
        
        [pdfPoint1 setX:_prev.x];
        [pdfPoint1 setY:_prev.y];
        
        @try {
            PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
            [m_pdfViewCtrl DocLock:YES];
            PTPage* pg = [doc GetPage:m_startPageNum];
            if( [pg IsValid] )
            {
                int annot_num = [pg GetNumAnnots];
                for(int i=annot_num-1;i>=0;--i){
                    PTAnnot *annot = [pg GetAnnot:i];
                    if (![annot IsValid]) continue;
                    if([annot GetType] == e_ptInk) {
                        PTInk *ink = [[PTInk alloc]initWithAnn:annot];
                        
                        if ([ink Erase:pdfPoint1 pt2:pdfPoint1 width:m_eraser_half_width]) {
                            if (!m_ink_list) {
                                m_ink_list = [[NSMutableArray alloc] initWithCapacity:10];
                            }
                            if ([self canAddObjectToInkList:ink]) {
                                [m_ink_list addObject:ink];
                            }
                        }
                    }
                }
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@: %@",exception.name, exception.reason);
        }
        @finally {
            [m_pdfViewCtrl DocUnlock];
        }
    }
    
    [self commitAnnotation];
    
    if( self.backToPanToolAfterUse )
    {
        nextToolType = [PanTool class];
        return NO;
    }
    else
        return YES;
}

-(void)commitAnnotation
{
    if( m_startPageNum > 0 )
	{
        if (m_ink_list.count > 0) {
            [self keepToolAppearanceOnScreen];
        }
		
		PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
		
		@try
		{
			[m_pdfViewCtrl DocLock:YES];
			
			PTPage* pg = [doc GetPage:m_startPageNum];
			
			if( [pg IsValid] )
			{
				for (PTInk* ink in m_ink_list) {
                    PTPDFRect* myRec = [ink GetRect];
                    if ([ink GetPathCount] == 0) {
                        [pg AnnotRemoveWithAnnot:ink];
						[self annotationRemoved:ink onPageNumber:m_startPageNum];
                    } else {
                        [ink RefreshAppearance];
						[self annotationModified:ink onPageNumber:m_startPageNum];
                    }
                    
                    CGPoint screenPt1 = CGPointMake([myRec GetX1], [myRec GetY1]);
                    CGPoint screenPt2 = CGPointMake([myRec GetX2], [myRec GetY2]);
                    
                    [self ConvertPagePtToScreenPtX:&screenPt1.x Y:&screenPt1.y PageNumber:m_startPageNum];
                    [self ConvertPagePtToScreenPtX:&screenPt2.x Y:&screenPt2.y PageNumber:m_startPageNum];
                    
                    PTPDFRect *screenRec = [[PTPDFRect alloc]init];
                    [screenRec SetX1:screenPt1.x];
                    [screenRec SetY1:screenPt1.y];
                    [screenRec SetX2:screenPt2.x];
                    [screenRec SetY2:screenPt2.y];
                    
                    [m_pdfViewCtrl UpdateWithRect:screenRec];
                }
            }
		}
		@catch (NSException *exception) {
            NSLog(@"Exception: %@: %@",exception.name, exception.reason);
		}
		@finally {
			[m_pdfViewCtrl DocUnlock];
		}
		
		m_free_hand_points = 0;
		[m_pdfViewCtrl RequestRendering];
	}
	
	[m_free_hand_points removeAllObjects];
    [m_ink_list removeAllObjects];
	[self setNeedsDisplay];
	
}

-(double)setupContextFromDefaultAnnot:(CGContextRef)currentContext
{
    PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
    
    PTPage* pg = [doc GetPage:pageNumber];
    
    PTPDFRect* pageRect = [pg GetCropBox];
    
    CGRect cropBox = [self PDFRectPage2CGRectScreen:pageRect PageNumber:pageNumber];
    
	if( currentContext )
		CGContextClipToRect(currentContext, cropBox);
    
    @try
    {
        [m_pdfViewCtrl DocLockRead];
        
		if(! _strokeColor )
        {
            _strokeColor = [UIColor lightGrayColor];
            
            _opacity = 0.5;
            
            _thickness = m_eraser_half_width * 2;
            
            _thickness *= [m_pdfViewCtrl GetZoom];
        }
        
        CGContextSetLineWidth(currentContext, _thickness);
        CGContextSetLineCap(currentContext, kCGLineCapRound);
        CGContextSetLineJoin(currentContext, kCGLineJoinRound);
        CGContextSetStrokeColorWithColor(currentContext, _strokeColor.CGColor);
        CGContextSetFillColorWithColor(currentContext, _fillColor.CGColor);
        CGContextSetAlpha(currentContext, _opacity);
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }
    
    return _thickness;
}

-(BOOL)canAddObjectToInkList:(PTInk*)ink
{
    if (!m_ink_list) {
        return NO;
    }
    for (PTInk *i in m_ink_list) {
        if ([[ink GetSDFObj] IsEqual:[i GetSDFObj]]) {
            return NO;
        }
    }
    return YES;
}

- (BOOL)handleTap:(UITapGestureRecognizer *)sender
{
    if (m_ink_list.count > 0) {
        return YES;
    }
	return [super handleTap:sender];
}

@end
