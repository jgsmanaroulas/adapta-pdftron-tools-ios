//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//


#import "TextUnderlineCreate.h"

@implementation TextUnderlineCreate

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        annotType = e_ptUnderline;
    }
    
    return self;
}
@end
