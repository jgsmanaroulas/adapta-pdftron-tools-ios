//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "ResizeWidgetView.h"

const int circleDiameter = 12;

@implementation ResizeWidgetView

- (instancetype)initAtPoint:(CGPoint)point WithLocation:(Location)loc
{
    m_location = loc;
    int length = [ResizeWidgetView getLength];
    CGRect frame = CGRectMake(point.x, point.y, length, length);
    
    self = [super initWithFrame:frame];
    if (self) {
		self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

-(void)drawRect:(CGRect)rect
{
	CGRect myRect = CGRectMake(rect.size.width/2-circleDiameter/2, rect.size.height/2-circleDiameter/2, circleDiameter, circleDiameter);
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGContextClearRect(ctx, myRect);
    CGContextSetFillColorWithColor(ctx, [UIColor colorWithRed:0.4 green:0.4	blue:1.0 alpha:1.0].CGColor);
	CGContextFillEllipseInRect(ctx, myRect);
	CGContextStrokePath(ctx);
}

+(int)getLength
{
    return circleDiameter*2;
}



@end
