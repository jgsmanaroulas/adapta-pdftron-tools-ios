//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "FormFillTool.h"
#import "ChoiceFormViewController.h"
#import "PanTool.h"
#import "DigitalSignatureTool.h"
#import "SelectionRectContainerView.h"
#import "PDFViewCtrlToolsUtil.h"

@implementation FormFillTool

-(instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl *)in_pdfViewCtrl
{
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
    }
    
    return self;
}

-(void)executeActionWithFieldIfAvailable:(PTField*)fld Type:(PTFieldActionTriggerEvent) type
{
    PTObj* aa = [fld GetTriggerAction:type];
    if(aa)
    {
        PTAction* a =[[PTAction alloc] initWithIn_obj:aa];
        PTActionParameter* action_parameter = [[PTActionParameter alloc] initWithAction:a field:fld];
        [self executeAction:action_parameter];
    }
}

-(void)executeActionWithAnnotIfAvailable:(PTAnnot*)annot Type:(PTAnnotActionTriggerEvent) type
{
    PTObj* aa = [annot GetTriggerAction:type];
    if(aa)
    {
        PTAction* a =[[PTAction alloc] initWithIn_obj:aa];
        PTActionParameter* action_parameter = [[PTActionParameter alloc] initWithAction:a annot:annot];
        [self executeAction:action_parameter];
    }
}

-(void)executeMouseUpAction:(PTAnnot*)annot
{
    [self executeActionWithAnnotIfAvailable:annot Type:e_ptaction_trigger_activate];
    [self executeActionWithAnnotIfAvailable:annot Type:e_ptaction_trigger_annot_up];
	[self executeActionWithAnnotIfAvailable:m_moving_annotation Type:e_ptaction_trigger_annot_exit];
}

-(void)willMoveToSuperview:(UIView*)newSuperview
{
    if( newSuperview == Nil )
    {
        [self deselectAnnotation];
    }
    
    [super willMoveToSuperview:newSuperview];
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

-(void)deselectAnnotation
{
    if(responder)
    {
        [responder.subviews[0] resignFirstResponder];
    }
    [responder removeFromSuperview];
    responder = 0;
    [selectionRectContainerView setHidden:YES];
    if (m_moving_annotation) {
        m_moving_annotation = 0;
    }
    
}

-(UIColor*)getWidgetBackgroundColor:(PTAnnot*)annot
{
    @try
    {
        [m_pdfViewCtrl DocLockRead];
        
        PTObj* o =[[annot GetSDFObj] FindObj:@"MK"];
        
        if(o)
        {
            PTObj* bgc = [o FindObj:@"BG"];
            
            if( bgc && [bgc IsArray] )
            {
                int sz = (int)[bgc Size];
                
                switch (sz) {
                    case 1:
                    {
                        PTObj* n = [bgc GetAt:0];
                        if( [n IsNumber] )
                        {
                            return [UIColor colorWithRed:[n GetNumber] green:[n GetNumber] blue:[n GetNumber] alpha:1.0];
                        }
                        break;
                    }
                    case 3:
                    {
                        PTObj* r = [bgc GetAt:0];
                        PTObj* g = [bgc GetAt:1];
                        PTObj* b = [bgc GetAt:2];
                        
                        if( [r IsNumber] && [g IsNumber] && [b IsNumber])
                        {
                            return [UIColor colorWithRed:[r GetNumber] green:[g GetNumber] blue:[b GetNumber] alpha:1.0];
                        }
                        break;
                    }
                    case 4:
                    {
                        PTObj* c = [bgc GetAt:0];
                        PTObj* m = [bgc GetAt:1];
                        PTObj* y = [bgc GetAt:2];
                        PTObj* k = [bgc GetAt:3];
                        
                        if( [c IsNumber] && [m IsNumber] && [y IsNumber] && [k IsNumber])
                        {
                            PTColorPt* cp = [[PTColorPt alloc] initWithX:[c GetNumber] y:[m GetNumber] z:[y GetNumber] w:[k GetNumber]];
                            
                            PTColorSpace* cs = [PTColorSpace CreateDeviceCMYK];
                            PTColorPt* cp_rgb = [cs Convert2RGB:cp];
                            
                            return [UIColor colorWithRed:[cp_rgb Get:0] green:[cp_rgb Get:1] blue:[cp_rgb Get:2] alpha:1.0];
                        }
                        break;
                    }
                    default:
                        break;
                }
            }
            
        }
        
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }
    
    return [UIColor whiteColor];
    
    
}

-(BOOL)fillForm:(UIGestureRecognizer*)gestureRecognizer
{
    
    CGPoint down = [gestureRecognizer locationInView:m_pdfViewCtrl];
    
    
    [self deselectAnnotation];
    [selectionRectContainerView setNeedsDisplay];
    if( m_moving_annotation )
    {
        
        [self deselectAnnotation];
        [m_pdfViewCtrl setNeedsDisplay];
    }
    
    //PDFDoc* doc = [m_pdfViewCtrl GetDoc];
    
    
    @try
    {
        [m_pdfViewCtrl DocLockRead];
        
        if (m_moving_annotation) {
            m_moving_annotation = 0;
        }
        
        m_moving_annotation = [m_pdfViewCtrl GetAnnotationAt:down.x y:down.y distanceThreshold:GET_ANNOT_AT_DISTANCE_THRESHOLD minimumLineWeight:GET_ANNOT_AT_MINIMUM_LINE_WEIGHT];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }
    
    
    if( [m_moving_annotation IsValid] )
    {
        
        CGRect annnot_rect;
        @try
        {
            [m_pdfViewCtrl DocLockRead];
            
            PTPDFRect* rect = [m_moving_annotation GetRect];
            
            m_annot_page_number = [m_pdfViewCtrl GetPageNumberFromScreenPt:down.x y:down.y];
            
            annnot_rect = [self PDFRectPage2CGRectScreen:rect PageNumber:m_annot_page_number];
            annnot_rect.origin.x = annnot_rect.origin.x + [m_pdfViewCtrl GetHScrollPos];
            annnot_rect.origin.y = annnot_rect.origin.y + [m_pdfViewCtrl GetVScrollPos];
        }
        @catch (NSException* exception) {
            
        }
        @finally {
            [m_pdfViewCtrl DocUnlockRead];
        }
        @try {
            [m_pdfViewCtrl DocLock:true];
			[self executeActionWithAnnotIfAvailable:m_moving_annotation Type:e_ptaction_trigger_annot_enter];
            [self executeActionWithAnnotIfAvailable:m_moving_annotation Type:e_ptaction_trigger_annot_down];
			[self executeActionWithAnnotIfAvailable:m_moving_annotation Type:e_ptaction_trigger_annot_focus];
            // is interactive form annotation?
            if([m_moving_annotation GetType] == e_ptWidget)
            {
                PTWidget* wg4;
                PTField* f;
                wg4 = [[PTWidget alloc] initWithAnn:m_moving_annotation];
                f = [wg4 GetField];
                
                if( [f IsValid] && ![f GetFlag:e_ptread_only] )
                {
                    if( [f GetType] == e_ptsignature )
                    {
                        nextToolType = [DigitalSignatureTool class];
                        [self executeMouseUpAction:m_moving_annotation];
                        return NO;
                    }
                    else if( [f GetType] == e_ptcheck)
                    {
                        PTViewChangeCollection* view_change = [f SetValueWithBool:![f GetValueAsBool]];
                        [m_pdfViewCtrl RefreshAndUpdate:view_change];                    
                        
                        nextToolType = [PanTool class];
                        [m_pdfViewCtrl postCustomEvent:Nil];
                        [self executeMouseUpAction:m_moving_annotation];
                        return YES;
                    }
                    else if([f GetType] == e_ptradio)
                    {
                        PTViewChangeCollection* view_change = [f SetValueWithBool:YES];
                        [m_pdfViewCtrl RefreshAndUpdate:view_change];                    
                        
                        nextToolType = [PanTool class];
                        [m_pdfViewCtrl postCustomEvent:Nil];
                        [self executeMouseUpAction:m_moving_annotation];
                        return YES;
                    }
                    else if([f GetType] == e_pttext)
                    {
                        UITextView* tv;
                        UITextField* fv;
                        
                        // used to prevent iOS from scrolling the PDFView when the widget beceomes the first responder.
                        UIScrollView* sv;
                        
                        if( responder )
                            [responder removeFromSuperview];
                        
                        responder = 0;
                        
                        
                        
                        [[NSNotificationCenter defaultCenter] addObserver:self
                                                                 selector:@selector (keyboardWillShow:)
                                                                     name: UIKeyboardWillShowNotification object:nil];
                        
                        [[NSNotificationCenter defaultCenter] addObserver:self
                                                                 selector:@selector (keyboardWillHide:)
                                                                     name: UIKeyboardWillHideNotification object:nil];
                        
                        if( [f GetFlag:e_ptmultiline] )
                        {
                            tv = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, annnot_rect.size.width, annnot_rect.size.height)];
                            sv = [[UIScrollView alloc] initWithFrame:annnot_rect];
                            responder = sv;
                            tv.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
                            
                            [sv addSubview:tv];
                            [m_pdfViewCtrl->ContainerView addSubview:sv];
                            NSString* str = [f GetValueAsString];
                            tv.text = str;
                            tv.backgroundColor = [UIColor whiteColor];
                            
                            PTGState* gs = [f GetDefaultAppearance];
                            
                            int fontSize = [gs GetFontSize];
                            
                            CGFloat displayFontSize;
                            
                            if( fontSize != 0 )
                                displayFontSize = [gs GetFontSize]*[m_pdfViewCtrl GetZoom];
                            else
                                displayFontSize = 12*[m_pdfViewCtrl GetZoom];
                            
                            tv.font = [UIFont fontWithName:@"Helvetica" size:displayFontSize];
                            
                            CGRect displayFontRect = [tv.text boundingRectWithSize:tv.bounds.size options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:displayFontSize] } context:nil];
                            
                            self.fontRect = CGRectMake(sv.frame.origin.x, sv.frame.origin.y, displayFontRect.size.width, displayFontRect.size.height);
                            
                            characterLimit = [f GetMaxLen]-1;
                            
                            PTColorPt* fontColour = [gs GetFillColor];
                            
                            fontColour = [[gs GetFillColorSpace] Convert2RGB:fontColour];
                            
                            CGFloat r , g, b, a = 1;
                            
                            r = [fontColour Get:0];
                            g = [fontColour Get:1];
                            b = [fontColour Get:2];
                            
                            tv.textColor = [UIColor colorWithRed:r green:g blue:b alpha:a];
                            
                            tv.backgroundColor = [self getWidgetBackgroundColor:m_moving_annotation];
                            
                            
                            [tv becomeFirstResponder];
                            tv.delegate = self;
                            
                            if([f GetJustification] == e_ptleft_justified)
                                tv.textAlignment = NSTextAlignmentLeft;
                            if([f GetJustification] == e_ptright_justified)
                                tv.textAlignment = NSTextAlignmentRight;
                            if([f GetJustification] == e_ptcentered)
                                tv.textAlignment = NSTextAlignmentCenter;
                        }
                        else
                        {
                            fv = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, annnot_rect.size.width, annnot_rect.size.height)];
                            sv = [[UIScrollView alloc] initWithFrame:annnot_rect];
                            responder = sv;
                            
                            // Add a "textFieldDidChange" notification method to the text field control.
                            [fv addTarget:self
                                   action:@selector(textFieldDidChange:)
                         forControlEvents:UIControlEventEditingChanged];
                            
                            characterLimit = [f GetMaxLen]-1;
                            
                            
                            [sv addSubview:fv];
                            [m_pdfViewCtrl->ContainerView addSubview:sv];
                            
                            NSString* str = [f GetValueAsString];
                            fv.text = str;
                            
                            // consider setting a different colour if you wish to "highlight" the field that is being edited
                            fv.backgroundColor = [UIColor whiteColor];
                            
                            fv.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                            
                            
                            PTGState* gs = [f GetDefaultAppearance];
                            
                            int fontSize = [gs GetFontSize];
                            
                            CGFloat displayFontSize;
                            
                            if( fontSize != 0 )
                                displayFontSize = [gs GetFontSize]*[m_pdfViewCtrl GetZoom];
                            else
                                displayFontSize = annnot_rect.size.height;
                            
                            fv.font = [UIFont fontWithName:@"Helvetica" size:displayFontSize];
                            
                            CGRect displayFontRect = [fv.text boundingRectWithSize:fv.bounds.size options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:displayFontSize] } context:nil];
                            
                            self.fontRect = CGRectMake(sv.frame.origin.x, sv.frame.origin.y, displayFontRect.size.width, displayFontRect.size.height);
                            
                            
                            PTColorPt* fontColour = [gs GetFillColor];
                            
                            fontColour = [[gs GetFillColorSpace] Convert2RGB:fontColour];
                            
                            CGFloat r , g, b, a = 1;
                            
                            r = [fontColour Get:0];
                            g = [fontColour Get:1];
                            b = [fontColour Get:2];
                            
                            fv.textColor = [UIColor colorWithRed:r green:g blue:b alpha:a];
                            
                            fv.backgroundColor = [self getWidgetBackgroundColor:m_moving_annotation];
                            
                            if( [f GetFlag:e_ptpassword] )
                                fv.secureTextEntry = YES;
                            [fv becomeFirstResponder];
                            fv.delegate = self;
                            
                            if([f GetJustification] == e_ptleft_justified)
                                fv.textAlignment = NSTextAlignmentLeft;
                            if([f GetJustification] == e_ptright_justified)
                                fv.textAlignment = NSTextAlignmentRight;
                            if([f GetJustification] == e_ptcentered)
                                fv.textAlignment = NSTextAlignmentCenter;
                            
                        }
                        [self executeMouseUpAction:m_moving_annotation];
                        return YES;
                    }
                    else if([f GetType] == e_ptbutton)
                    {
                        [self executeMouseUpAction:m_moving_annotation];
                        nextToolType = [PanTool class];
                        [m_pdfViewCtrl postCustomEvent:Nil];
                        return YES;
                    }
                    else if([f GetType] == e_ptchoice)
                    {
                        ChoiceFormViewController* cfvc = [[ChoiceFormViewController alloc] init];
                        [cfvc setDelegate:self];
                        
                        if( [f GetFlag:e_ptmultiselect] )
                            [cfvc setIsMultiSelect:YES];
						
						
						cfvc.modalPresentationStyle = UIModalPresentationPopover;
						
						[[self viewController] presentViewController:cfvc animated:YES completion:Nil];
						
						UIPopoverPresentationController *popController = cfvc.popoverPresentationController;
						popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
						
						PTPDFRect* rect = [m_moving_annotation GetRect];
						
						CGRect annotRect = [self PDFRectPage2CGRectScreen:rect PageNumber:m_annot_page_number];
						
						popController.sourceRect = annotRect;
						popController.sourceView = m_pdfViewCtrl;
						popController.delegate = self;

                    }
                    [self executeMouseUpAction:m_moving_annotation];
                    return YES;
                }
				else
				{
					m_moving_annotation = 0;
				}
            }
            else
            {
                nextToolType = [PanTool class];
                return NO;
            }
        } @catch (NSException *exception) {
            
        } @finally {
            [m_pdfViewCtrl DocUnlock];
        }
        
        nextToolType = [PanTool class];
        [m_pdfViewCtrl postCustomEvent:Nil];
        return YES;
    }
    else
    {
        m_moving_annotation = 0;
    }
    
    nextToolType = [PanTool class];
    return NO;
    
}

-(void)textViewDidChange:(UITextView *)textView
{
    PTWidget* wg4 = [[PTWidget alloc] initWithAnn:m_moving_annotation];
    PTField* f = [wg4 GetField];
    PTObj* aa = [f GetTriggerAction:e_ptaction_trigger_keystroke];
    if(aa)
    {
        PTAction* a =[[PTAction alloc] initWithIn_obj:aa];
        NSString* suggestedText = textView.text;
        NSString* fieldName = [f GetName];
        UITextRange *selRange = textView.selectedTextRange;
        UITextPosition *selStart = selRange.start;
        NSInteger startPosition = [textView offsetFromPosition:textView.beginningOfDocument toPosition:selStart];
        NSInteger addedLength = startPosition - selectionStart;
        if(addedLength<0)
        {
            return;
        }
        NSRange range = NSMakeRange(selectionStart, addedLength);
        NSString* addedText = [suggestedText substringWithRange:range];
        int begin = (int) selectionStart;
        int end = (int) selectionEnd;
        PTKeyStrokeEventData* data = [[PTKeyStrokeEventData alloc]initWithField_name:fieldName current_value:originalText change:addedText selection_start:begin selection_end:end];
        PTKeyStrokeActionResult* actionResult = [a ExecuteKeyStrokeAction:data];
        if([actionResult IsValid])
        {
            NSString* addedValue = [actionResult GetText];
            if(![addedText isEqualToString:addedValue])
            {
                NSRange rangeBefore = NSMakeRange(0, selectionStart);
                NSString* textBefore = [originalText substringWithRange:rangeBefore];
                NSString* textEnd = [originalText substringFromIndex:selectionEnd];
                NSString * newText = [NSString stringWithFormat:@"%@%@%@",textBefore,addedValue,textEnd];
                textView.text = newText;
                NSInteger newLength = addedValue.length;
                NSInteger newCursorPosition = selectionStart + newLength;
                UITextPosition* finalCursorPosition = [textView positionFromPosition:textView.beginningOfDocument offset:newCursorPosition];
                UITextRange* newSelectedRange = [textView textRangeFromPosition:finalCursorPosition toPosition:finalCursorPosition];
                textView.selectedTextRange = newSelectedRange;
            }
        }
        else
        {
            textView.text = originalText;
            textView.selectedTextRange = originalRange;
        }
    }
}

-(void)textFieldDidChange:(UITextField*)textField
{
    PTWidget* wg4 = [[PTWidget alloc] initWithAnn:m_moving_annotation];
    PTField* f = [wg4 GetField];
    PTObj* aa = [f GetTriggerAction:e_ptaction_trigger_keystroke];
    if(aa)
    {
        PTAction* a =[[PTAction alloc] initWithIn_obj:aa];
        NSString* suggestedText = textField.text;
        NSString* fieldName = [f GetName];
        UITextRange *selRange = textField.selectedTextRange;
        UITextPosition *selStart = selRange.start;
        NSInteger startPosition = [textField offsetFromPosition:textField.beginningOfDocument toPosition:selStart];
        NSInteger addedLength = startPosition - selectionStart;
        if(addedLength<0)
        {
            return;
        }
        NSRange range = NSMakeRange(selectionStart, addedLength);
        NSString* addedText = [suggestedText substringWithRange:range];
        int begin = (int) selectionStart;
        int end = (int) selectionEnd;
        PTKeyStrokeEventData* data = [[PTKeyStrokeEventData alloc]initWithField_name:fieldName current_value:originalText change:addedText selection_start:begin selection_end:end];
        PTKeyStrokeActionResult* actionResult = [a ExecuteKeyStrokeAction:data];
        if([actionResult IsValid])
        {
            NSString* addedValue = [actionResult GetText];
            if(![addedText isEqualToString:addedValue])
            {
                NSRange rangeBefore = NSMakeRange(0, selectionStart);
                NSString* textBefore = [originalText substringWithRange:rangeBefore];
                NSString* textEnd = [originalText substringFromIndex:selectionEnd];
                NSString * newText = [NSString stringWithFormat:@"%@%@%@",textBefore,addedValue,textEnd];
                textField.text = newText;
                NSInteger newLength = addedValue.length;
                NSInteger newCursorPosition = selectionStart + newLength;
                UITextPosition* finalCursorPosition = [textField positionFromPosition:textField.beginningOfDocument offset:newCursorPosition];
                UITextRange* newSelectedRange = [textField textRangeFromPosition:finalCursorPosition toPosition:finalCursorPosition];
                textField.selectedTextRange = newSelectedRange;
            }
        }
        else
        {
            textField.text = originalText;
            textField.selectedTextRange = originalRange;
        }
    }
}

// used to limit number of characters that can be entered
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString* str = textField.text;
    UITextRange *selRange = textField.selectedTextRange;
    UITextPosition *selStart = selRange.start;
    UITextPosition *selEnd = selRange.end;
    originalRange = selRange;
    NSInteger start = [textField offsetFromPosition:textField.beginningOfDocument toPosition:selStart];
    NSInteger end = [textField offsetFromPosition:textField.beginningOfDocument toPosition:selEnd];
    originalText = str;
    selectionStart = start;
    selectionEnd = end;
    if( characterLimit >= 0 )
        return !((textField.text).length > characterLimit && string.length > range.length);
    else
        return true;
}

// used to limit number of characters that can be entered
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString* str = textView.text;
    UITextRange *selRange = textView.selectedTextRange;
    UITextPosition *selStart = selRange.start;
    UITextPosition *selEnd = selRange.end;
    originalRange = selRange;
    NSInteger start = [textView offsetFromPosition:textView.beginningOfDocument toPosition:selStart];
    NSInteger end = [textView offsetFromPosition:textView.beginningOfDocument toPosition:selEnd];
    originalText = str;
    selectionStart = start;
    selectionEnd = end;

    if( characterLimit >= 0 )
        return !((textView.text).length > characterLimit && text.length > range.length);
    else
        return true;
}

-(BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    nextToolType = [PanTool class];
    return NO;
}

- (BOOL)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    return [self fillForm:gestureRecognizer];
}

-(BOOL)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if( gestureRecognizer.state == UIGestureRecognizerStateBegan )
    {
        return [self fillForm:gestureRecognizer];
    }
    
    return YES;
}


-(void) onLayoutChanged
{
    @try
    {
        [m_pdfViewCtrl DocLockRead];
        
        PTAnnotType annotType = [m_moving_annotation GetType];
        
        if( ![m_moving_annotation IsValid] || annotType != e_ptWidget )
        {
            return;
        }
        
        
        PTPDFRect* rect = [m_moving_annotation GetRect];
        
        CGRect annnot_rect = [self PDFRectPage2CGRectScreen:rect PageNumber:m_annot_page_number];
        
        annnot_rect.origin.x = annnot_rect.origin.x + [m_pdfViewCtrl GetHScrollPos];
        annnot_rect.origin.y = annnot_rect.origin.y + [m_pdfViewCtrl GetVScrollPos];
        
        if( annotType == e_ptWidget )
        {
            if( responder && responder.subviews.count > 0)
            {
                responder.frame = annnot_rect;
                responder.subviews[0].frame = CGRectMake(0, 0, annnot_rect.size.width, annnot_rect.size.height);
                
                PTWidget* wg4 = [[PTWidget alloc] initWithAnn:m_moving_annotation];
                PTField* f = [wg4 GetField];
                PTGState* gs = [f GetDefaultAppearance];
                
                // responder is a UIScrollView and its only subview is the actual widget, a UITextView or UITextField
                
                int fontSize = [gs GetFontSize];
                
                if( fontSize == 0 )
                {
                    if( [responder isKindOfClass:[UITextView class]] )
                    {
                        UITextView* tempView = [[UITextView alloc] init];
                        
                        fontSize  = tempView.font.pointSize;
                    }
                    else
                    {
                        UITextField* tempView = [[UITextField alloc] init];
                        
                        fontSize  = tempView.font.pointSize;
                    }
                }
                
                [[[responder subviews] objectAtIndex:0] setFont:[UIFont fontWithName:@"Helvetica" size:fontSize*[m_pdfViewCtrl GetZoom]]];
                
            }
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }
}

- (void)pageNumberChangedFrom:(int)oldPageNumber To:(int)newPageNumber
{
    TrnPagePresentationMode mode = [m_pdfViewCtrl GetPagePresentationMode];
    if( mode == e_ptsingle_page || mode == e_ptfacing || mode == e_ptfacing_cover )
    {
        if(responder)
        {
            [responder.subviews[0] resignFirstResponder];
        }
        [responder removeFromSuperview];
        responder = 0;
    }
    [super pageNumberChangedFrom:oldPageNumber To:newPageNumber];
    
    if( mode == e_ptsingle_page || mode == e_ptfacing || mode == e_ptfacing_cover )
    {
        nextToolType = [PanTool class];
        [m_pdfViewCtrl postCustomEvent:Nil];
    }
}

- (BOOL)onCustomEvent:(id)userData
{
    return NO;
}

#pragma mark - Form Filling

-(int)GetOptionIndexStr:(PTObj*)str_val Opt:(PTObj*)opt
{
    if( ![str_val IsString] ) return -1;
    if( ![opt IsArray] ) return -1;
    
    unsigned long sz = [opt Size];
    
    for( int i =0; i < sz; ++i )
    {
        PTObj* v = [opt GetAt:i];
        
        if( [v IsString] && [str_val Size] == [v Size] )
        {
            if( !memcmp([str_val GetBuffer].bytes, [v GetBuffer].bytes, [v Size]))
            {
                return i;
            }
        }
        else if( [v IsArray] && [v Size] >=2 && [[v GetAt:1] IsString] && [str_val Size] == [[v GetAt:1] Size])
        {
            v = [v GetAt:1];
            
            if( !memcmp([str_val GetBuffer].bytes, [v GetBuffer].bytes, (int)[v Size]) )
            {
                return i;
            }
        }
    }
    
    return -1;
}

-(NSMutableArray*)getSelectedItemsInActiveListbox
{
    PTWidget* wg4 = [[PTWidget alloc] initWithAnn:m_moving_annotation];
    PTField* f = [wg4 GetField];
    
    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:10];
    
    if( ![f IsValid] )
        return arr;
    
    PTObj* val = [f GetValue];
    
    if( [val IsString] )
    {
        PTObj* o = [[m_moving_annotation GetSDFObj] FindObj:@"Opt"];
        if( !o )
            return arr;
        
        [arr addObject:@([self GetOptionIndexStr:val Opt:o])];
    }
    else if( [val IsArray] )
    {
        int sz = (int)[val Size];
        for(int i = 0;i < sz;i++)
        {
            PTObj* entry = [val GetAt:i];
            PTObj* o = [[m_moving_annotation GetSDFObj] FindObj:@"Opt"];
            if( !o )
                return arr;
            
            [arr addObject:@([self GetOptionIndexStr:entry Opt:o])];
        }
    }
    
    return arr;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger num = [indexPath indexAtPosition:1];
    
    PTWidget* wg4;
    PTField* f;
    
    @try {
        [m_pdfViewCtrl DocLockRead];
        wg4 = [[PTWidget alloc] initWithAnn:m_moving_annotation];
        f = [wg4 GetField];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }

    
    // combo box and single selection list
    if( !([self.viewController isMemberOfClass:[ChoiceFormViewController class]] && ((ChoiceFormViewController*)self.viewController).isMultiSelect) )
    {
        @try
        {
            [m_pdfViewCtrl DocLock:YES];
            PTViewChangeCollection* view_change = [f SetValueWithString:choices[num]];
            [m_pdfViewCtrl RefreshAndUpdate:view_change];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@: %@",exception.name, exception.reason);
        }
        @finally {
            [m_pdfViewCtrl DocUnlock];
        }
        
        [m_pdfViewCtrl UpdateWithField:f];
		
		[[self viewController] dismissViewControllerAnimated:YES completion:Nil]; // iOS 5
        
        //change back to default pan tool.
        nextToolType = [PanTool class];
        [m_pdfViewCtrl postCustomEvent:Nil];
        
    }
    else
    {
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionTop];
        [tableView cellForRowAtIndexPath:indexPath].selectionStyle = UITableViewCellSelectionStyleNone;
        if( [tableView cellForRowAtIndexPath:indexPath].accessoryType != UITableViewCellAccessoryCheckmark )
            [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        else
            [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    }
}

-(void)keyboardWillShow:(NSNotification *)notification
{
    [m_pdfViewCtrl keyboardWillShow:notification rectToNotOverlapWith:self.fontRect];
    
    keyboardOnScreen = true;
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [m_pdfViewCtrl keyboardWillHide:notification];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    keyboardOnScreen = false;
    
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)didEndEditing:(UIView *)view
{
    @try
    {
        [m_pdfViewCtrl DocLock:YES];
        
        PTWidget* wg4 = [[PTWidget alloc] initWithAnn:m_moving_annotation];
        PTField* f = [wg4 GetField];
        NSString* textString;
        
        //view will be a textView or a textField so warnings here are not applicable
        if( [view isKindOfClass:[UITextView class]] )
        {
            textString = ((UITextView*)view).text;
        }
        else if( [view isKindOfClass:[UITextField class]] )
        {
            textString = ((UITextField*)view).text;;
        }
        else
        {
            textString = @"";
        }
        
        // no need to refresh if string hasn't changed value
        if( ![[f GetValueAsString] isEqualToString:textString] )
        {
            PTViewChangeCollection* view_change = [f SetValueWithString:textString];
            [m_pdfViewCtrl RefreshAndUpdate:view_change];
        }
        
        [self executeActionWithAnnotIfAvailable:m_moving_annotation Type:e_ptaction_trigger_annot_blur];
        
        [responder removeFromSuperview];
        
        responder = 0;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self didEndEditing:textView];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self didEndEditing:textField];
}

-(NSInteger)numberOfChoices
{
    [choices removeAllObjects];
    
    if( choices == 0 )
        choices =  [[NSMutableArray alloc] initWithCapacity:10];
    
    PTWidget* wg4 = [[PTWidget alloc] initWithAnn:m_moving_annotation];
    PTField* f = [wg4 GetField];
    
    NSInteger total = [f GetOptCount];
    
    for(int i = 0; i < total; i++)
    {
        [choices addObject:[f GetOpt:i]];
    }
    
    return total;
}

-(NSString*)titleOfChoiceAtIndex:(NSUInteger)num
{
    return choices[num];
}


- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController;
{

								  
    // only relevant for multiselect
    if( ([popoverPresentationController.presentedViewController isMemberOfClass:[ChoiceFormViewController class]] && ((ChoiceFormViewController*)popoverPresentationController.presentedViewController).isMultiSelect) )
    {
        
        UITableView* tv = ((UITableView*)(self.viewController.presentedViewController.view));
        
        @try
        {
            [m_pdfViewCtrl DocLock:YES];
            PTWidget* wg4 = [[PTWidget alloc] initWithAnn:m_moving_annotation];
            PTField* f = [wg4 GetField];
            
            PTObj* arr = [[m_pdfViewCtrl GetDoc] CreateIndirectArray];
            
            for (int i = 0; i < [tv numberOfRowsInSection:0]; ++i)
            {
                
                if( [tv cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]].accessoryType == UITableViewCellAccessoryCheckmark )
                {
                    NSString* str = choices[i];
                    [arr PushBackText:str];
                }
            }
            
            PTViewChangeCollection* view_change = [f SetValueWithObj:arr];
            
            [m_pdfViewCtrl RefreshAndUpdate:view_change];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@: %@",exception.name, exception.reason);
        }
        @finally {
            [m_pdfViewCtrl DocUnlock];
        }
    }
    nextToolType = [PanTool class];
    [m_pdfViewCtrl postCustomEvent:Nil];
    
}

-(void)javascriptCallback:(const char*)event_type json:(const char*)json
{
    NSString* type = @(event_type);
    NSString* message = @(json);
    if([type isEqualToString:@"alert"])
    {
		NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
		
        NSData* jsonData = [message dataUsingEncoding:NSUTF8StringEncoding];
        NSError* e;
        NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&e];
        NSString* alert_message = dict[@"cMsg"];
		
		UIAlertController *alertController = [UIAlertController
											  alertControllerWithTitle:NSLocalizedStringFromTableInBundle(@"JavaScript Alert", @"PDFNet-Tools", stringBundle, @"")
											  message:alert_message
											  preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction *okAction = [UIAlertAction
								   actionWithTitle:NSLocalizedStringFromTableInBundle(@"OK", @"PDFNet-Tools", stringBundle, @"")
								   style:UIAlertActionStyleDefault
								   handler:Nil];
		
		
		[alertController addAction:okAction];
		
		[self.viewController presentViewController:alertController animated:YES completion:nil];
    }
}

@end
