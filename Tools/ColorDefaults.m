//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "ColorDefaults.h"

#define ANNOT_MARKUP				@"markup"
#define ANNOT_HIGHLIGHT				@"highlight"
#define ANNOT_TEXTUNDERLINE			@"textMarkup"
#define ANNOT_TEXTSRIKEOUT			@"textStrikeout"
#define ANNOT_TEXTSQUIGGLY			@"textSquiggly"
#define ANNOT_FREETEXT				@"freeText"

@implementation ColorDefaults

#pragma mark - Helpers




+(PTColorPt*)colorPtFromUIColor:(UIColor*)uiColor
{
    CGColorRef colorRef = [uiColor CGColor];
    int numcomps = (int) CGColorGetNumberOfComponents(colorRef);
    const CGFloat* components = CGColorGetComponents(colorRef);
    
    switch (numcomps)
    {
        case (1):
            return [[PTColorPt alloc] initWithX:components[0] y:0 z:0 w:0];
        case (2):
            return [[PTColorPt alloc] initWithX:components[0] y:components[1] z:0 w:0];
        case (3):
            return [[PTColorPt alloc] initWithX:components[0] y:components[1] z:components[2] w:0];
        case (4):
            return [[PTColorPt alloc] initWithX:components[0] y:components[1] z:components[2] w:components[3]];
        default:
            return [[PTColorPt alloc] initWithX:0 y:0 z:0 w:0];
    }
}

+(const CGFloat*)colorComponentsInUIColor:(UIColor*)uiColor
{
	CGColorRef chosenCGColor = uiColor.CGColor;
	return CGColorGetComponents(chosenCGColor);
}

+(UIColor*)inverseUIColor:(UIColor*)color
{
	//return color;
	CGFloat r, g, b, a;
	[color getRed:&r green:&g blue:&b alpha:&a];
	return [UIColor colorWithRed:1.-r green:1.-g blue:1.-b alpha:a];
}

+(PTColorPt*)inverseColorPt:(PTColorPt*)colorpt
{
	//return colorpt;
	double x, y, z, w;
	x = 1.-[colorpt Get:0];
	y = 1.-[colorpt Get:1];
	z = 1.-[colorpt Get:2];
	w = 1.-[colorpt Get:3];
	
	return [[PTColorPt alloc] initWithX:x y:y z:z w:w];
}

+(NSString*)keyForAnnotType:(PTAnnotType)type attribute:(NSString*)attribute
{
	NSString* annotType;
	if( type == e_ptUnderline ) {
		annotType = ANNOT_TEXTUNDERLINE;
	} else if( type == e_ptStrikeOut ) {
		annotType = ANNOT_TEXTSRIKEOUT;
	} else if( type == e_ptSquiggly ) {
		annotType = ANNOT_TEXTSQUIGGLY;
	} else if ( type == e_ptHighlight ) {
		annotType = ANNOT_HIGHLIGHT;
	} else if( type == e_ptFreeText) {
		annotType = ANNOT_FREETEXT;
	} else {
		annotType = ANNOT_MARKUP;
	}
	
	return [annotType stringByAppendingString:attribute];
}

#pragma mark - Colors

+(void)setDefaultColor:(UIColor*)color forAnnotType:(PTAnnotType)type attribute:(NSString*)attribute colorPostProcessMode:(PTColorPostProcessMode)mode
{
	NSString* key = [self keyForAnnotType:type attribute:attribute];
	if( mode == e_ptpostprocess_invert)
		color = [self inverseUIColor:color];
	
	NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:color];
	[[NSUserDefaults standardUserDefaults] setObject:colorData forKey:key];
}

+(UIColor*)defaultColorForAnnotType:(PTAnnotType)type attribute:(NSString*)attribute colorPostProcessMode:(PTColorPostProcessMode)mode
{
	NSString* key = [self keyForAnnotType:type attribute:attribute];
	NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:key];
	UIColor* returnColor;
	
	if( !colorData )
	{
		if( type == e_ptHighlight )
			returnColor = [UIColor yellowColor];
		else if ([attribute  isEqualToString:ATTRIBUTE_FILL_COLOR] )
			returnColor = [UIColor clearColor];
		else
			returnColor = [UIColor redColor];
		
	}
	
	if (!returnColor )
		returnColor = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
	
	if( mode == e_ptpostprocess_invert )
		returnColor = [self inverseUIColor:returnColor];
	
	return returnColor;
}

+(PTColorPt*)defaultColorPtForAnnotType:(PTAnnotType)type attribute:(NSString*)attribute colorPostProcessMode:(PTColorPostProcessMode)mode
{
	NSString* key = [self keyForAnnotType:type attribute:attribute];
	NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:key];
	PTColorPt* returnColor;
	
	if( !colorData )
	{
		if( type == e_ptHighlight )
			returnColor = [self colorPtFromUIColor:[UIColor yellowColor]];
		else if ([attribute  isEqualToString:ATTRIBUTE_FILL_COLOR] )
			returnColor =  [self colorPtFromUIColor:[UIColor clearColor]];
		else
			returnColor =  [self colorPtFromUIColor:[UIColor redColor]];
	}
	
	if( !returnColor )
		returnColor = [self colorPtFromUIColor:[NSKeyedUnarchiver unarchiveObjectWithData:colorData]];
	
	if( mode == e_ptpostprocess_invert )
		returnColor = [self inverseColorPt:returnColor];
	
	return returnColor;
}

+(int)numCompsInColorPtForAnnotType:(PTAnnotType)type attribute:(NSString*)attribute
{
	// color mode is irrelevant, so fine to pass 0
	UIColor* color = [self defaultColorForAnnotType:type attribute:attribute colorPostProcessMode:0];

    CGColorRef colorRef = [color CGColor];
    
    //PDF comp number is 0 or 3
    unsigned long numComps = CGColorGetNumberOfComponents(colorRef);
    if( numComps > 3 )
        return 3;
    else
        return 0;

}

#pragma mark - Opacity

+(void)setDefaultOpacity:(double)opacity forAnnotType:(PTAnnotType)type
{
	NSString* key = [self keyForAnnotType:type attribute:ATTRIBUTE_OPACITY];
	[[NSUserDefaults standardUserDefaults] setDouble:opacity forKey:key];
}

+(double)defaultOpacityForAnnotType:(PTAnnotType)type
{
	NSString* key = [self keyForAnnotType:type attribute:ATTRIBUTE_OPACITY];
	
	if( ![[NSUserDefaults standardUserDefaults] objectForKey:key])
	{
		return 1.0f;
	}
	
	return [[NSUserDefaults standardUserDefaults] doubleForKey:key];
}

#pragma mark - Border Thickness

+(void)setDefaultBorderThickness:(double)thickness forAnnotType:(PTAnnotType)type
{
	NSString* key = [self keyForAnnotType:type attribute:ATTRIBUTE_BORDER_THICKNESS];
	[[NSUserDefaults standardUserDefaults] setDouble:thickness forKey:key];
}

+(double)defaultBorderThicknessForAnnotType:(PTAnnotType)type
{
	NSString* key = [self keyForAnnotType:type attribute:ATTRIBUTE_BORDER_THICKNESS];
	
	if( ![[NSUserDefaults standardUserDefaults] objectForKey:key])
	{
		return 1.0f;
	}
	
	return [[NSUserDefaults standardUserDefaults] doubleForKey:key];
}

#pragma mark - FreeText size

+(void)setDefaultFreeTextSize:(double)size forAnnotType:(PTAnnotType)type
{
	NSString* key = [self keyForAnnotType:type attribute:ATTRIBUTE_FREETEXT_SIZE];
	[[NSUserDefaults standardUserDefaults] setDouble:size forKey:key];
}

+(double)defaultFreeTextSizeForAnnotType:(PTAnnotType)type
{
	NSString* key = [self keyForAnnotType:type attribute:ATTRIBUTE_FREETEXT_SIZE];
	
	if( ![[NSUserDefaults standardUserDefaults] doubleForKey:key])
	{
		return 16.0f;
	}
	
	return [[NSUserDefaults standardUserDefaults] doubleForKey:key];
}

@end
