//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//


#import "TextMarkupCreate.h"

#import "PanTool.h"
#import "ColorDefaults.h"
#import "AnnotEditTool.h"
#import "TextMarkupEditTool.h"
#import "FormFillTool.h"

@implementation TextMarkupCreate

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        selectionLayers = [[NSMutableArray alloc] init];
        shouldCancel = false;
		_thickness = -1;
    }
    
    return self;
}

+(BOOL)createsAnnotation
{
	return YES;
}

-(void)willMoveToSuperview:(UIView *)newSuperview
{
    if( newSuperview == Nil )
    {
        [self ClearSelectionOnly];

        [loupe removeFromSuperview];
    }
    else
    {

    }
    
    [super willMoveToSuperview:newSuperview];
}


-(void)pageNumberChangedFrom:(int)oldPageNumber To:(int)newPageNumber
{
	
	TrnPagePresentationMode mode = [m_pdfViewCtrl GetPagePresentationMode];
	
	if( mode == e_ptsingle_page || mode == e_ptfacing || mode == e_ptfacing_cover )
    {
	
		[self ClearSelectionOnly];
		
		[self hideMenu];
		
		selectionOnScreen = NO;
		
		// this stops old markup annot from being selected
		// by the onTouchesEnded: event of MarkupEditTool
		m_moving_annotation = Nil;
			
	}
	
	[super pageNumberChangedFrom:oldPageNumber To:newPageNumber];
}

-(BOOL)handleTap:(UITapGestureRecognizer *)sender
{
    // don't create a tiny annotation
    if( self.backToPanToolAfterUse)
    {
        nextToolType = [PanTool class];
        return NO;
    }
    else
    {
		// user has tapped on the screen, and we want to edit it
		// with the approriate tool. Fake a tap using the PanTool
		// to figure out which.
		
		Tool* editTool = [[PanTool alloc] initWithPDFViewCtrl:m_pdfViewCtrl];
		
		while(![editTool handleTap:sender])
		{
			editTool = [editTool getNewTool];
			
			if( [editTool isKindOfClass:[FormFillTool class]] )
				break;
		}
		
		if([editTool isKindOfClass:[AnnotEditTool class]] || [editTool isKindOfClass:[TextMarkupEditTool class]])
		{
			nextToolType = [editTool class];
			self.defaultClass = [self class];
			return NO;
		}
		
		[self ClearSelectionOnly];
		
		[self hideMenu];
		
		selectionOnScreen = NO;
		
		// this stops old markup annot from being selected
		// by the onTouchesEnded: event of MarkupEditTool
		m_moving_annotation = Nil;
		
        return YES;
    }
}

- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if( !self.allowScrolling )
	{
		if( event.allTouches.count  > 1 )
			shouldCancel = true;
		else
			shouldCancel = false;
		
		UITouch *touch = touches.allObjects[0];
		
		selectionStart = selectionEnd = [touch locationInView:m_pdfViewCtrl];
		
		pageNumber = [m_pdfViewCtrl GetPageNumberFromScreenPt:selectionStart.x y:selectionStart.y];
		
		[self ConvertScreenPtToPagePtX:&selectionStart.x Y:&selectionStart.y PageNumber:pageNumber];
		
		[self ConvertScreenPtToPagePtX:&selectionEnd.x Y:&selectionEnd.y PageNumber:pageNumber];
		
		self.backgroundColor = [UIColor clearColor];
		
		self.frame = m_pdfViewCtrl.bounds;
				
		CGPoint down = [touch locationInView:m_pdfViewCtrl];
		
		down.y += m_pdfViewCtrl.frame.origin.y;
		down.x += m_pdfViewCtrl.frame.origin.x;

	}
    
    return YES;
}


- (void)touchMove:(CGPoint)touch
{
	if( !self.allowScrolling )
	{
		selectionEnd = touch;
		
		[self ConvertScreenPtToPagePtX:&selectionEnd.x Y:&selectionEnd.y PageNumber:pageNumber];
		
		[m_pdfViewCtrl SelectX1:selectionStart.x Y1:selectionStart.y PageNumber1:pageNumber X2:selectionEnd.x Y2:selectionEnd.y PageNumber2:pageNumber];
		
		currentSelection = [self GetQuadsFromPage:pageNumber];
		
		[self DrawSelectionQuads:currentSelection];
		
		CGPoint down = touch;
		
		down.y += m_pdfViewCtrl.frame.origin.y;
		down.x += m_pdfViewCtrl.frame.origin.x;
		
		if(loupe == nil)
		{
			loupe = [[TrnMagnifierView alloc] init];
			loupe.viewToMagnify = m_pdfViewCtrl;
		}
		
		if(loupe.superview == nil)
			[m_pdfViewCtrl.superview addSubview:loupe];
		
		
		[loupe setMagnifyPoint:touch TouchPoint:down];
		[loupe setNeedsDisplay];
	}
	else
	{
		if( loupe.superview != Nil )
            [loupe removeFromSuperview];
		[self ClearSelectionOnly];
	}
}

-(BOOL)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    
    if( gestureRecognizer.state == UIGestureRecognizerStateChanged )
    {
        CGPoint touch = [gestureRecognizer locationInView:m_pdfViewCtrl];
        
        [self touchMove:touch];
    }
    else if( gestureRecognizer.state == UIGestureRecognizerStateEnded )
    {
        // same as onTouchesEnded
        [self ClearSelectionOnly];
        
        [self createTextMarkupAnnot];
        
        if( loupe.superview != Nil )
            [loupe removeFromSuperview];
        
        if( self.backToPanToolAfterUse )
        {
            nextToolType = [PanTool class];
            return NO;
        }
        else
            return YES;
    }
    
    return YES;
}

- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if( event.allTouches.count >1 )
        shouldCancel = true;
    else
        shouldCancel = false;
    
    UITouch *touch = touches.allObjects[0];
    
    [self touchMove:[touch locationInView:m_pdfViewCtrl]];
    
    return YES;
}

-(BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // same as handleLongPress ended
    [self ClearSelectionOnly];
    [self createTextMarkupAnnot];

    if( loupe.superview != Nil )
        [loupe removeFromSuperview];
	
	if( self.backToPanToolAfterUse )
		self.defaultClass = [PanTool class];
	else
		self.defaultClass = [self class];
    
	// edit this annotaiton immediately, providing instant access
	// to the note button, properties, etc.
	nextToolType = [TextMarkupEditTool class];
	return NO;

}

- (BOOL)onTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	return [self onTouchesEnded:touches withEvent:event];
}

-(void)ClearSelectionOnly
{
    
    [selectionLayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    [selectionLayers removeAllObjects];
    
}

-(void)DrawSelectionQuads:(NSMutableArray*)quads
{
	if( !self.allowScrolling )
	{
		int drawnQuads = 0;
		
		[self ClearSelectionOnly];
		
		PTColorPostProcessMode mode = [m_pdfViewCtrl GetColorPostProcessMode];
		
		for (NSValue* quad in quads) 
		{
			CALayer* selectionLayer = [[CALayer alloc] init];
			
			CGRect selectionRect = quad.CGRectValue;
			
			if( annotType == e_ptHighlight )
			{
				selectionLayer.frame = CGRectMake(selectionRect.origin.x+[m_pdfViewCtrl GetHScrollPos], selectionRect.origin.y+[m_pdfViewCtrl GetVScrollPos], selectionRect.size.width, selectionRect.size.height);
				
				if( !_colorPt )
					_colorPt = [ColorDefaults defaultColorPtForAnnotType:annotType attribute:ATTRIBUTE_STROKE_COLOR colorPostProcessMode:mode];//[m_default_text_annotation GetColorAsRGB];
				
				UIColor* defaultTextAnnotColor = [UIColor colorWithRed:[_colorPt Get:0] green:[_colorPt Get:1] blue:[_colorPt Get:2] alpha:0.20];
				
				CGColorRef cgDefaultTextAnnotColorr = defaultTextAnnotColor.CGColor;
				
				selectionLayer.backgroundColor = cgDefaultTextAnnotColorr;
			}
			else if( annotType == e_ptUnderline || annotType == e_ptSquiggly)
			{
				//BorderStyle* bs = [m_default_text_annotation GetBorderStyle];
				if( _thickness < 0 )
					_thickness = [ColorDefaults defaultBorderThicknessForAnnotType:annotType];//[bs GetWidth];
				
				selectionLayer.frame = CGRectMake(selectionRect.origin.x+[m_pdfViewCtrl GetHScrollPos], selectionRect.origin.y+selectionRect.size.height+[m_pdfViewCtrl GetVScrollPos] - _thickness, selectionRect.size.width, _thickness);
				
				if( !_colorPt )
					_colorPt = [ColorDefaults defaultColorPtForAnnotType:annotType attribute:ATTRIBUTE_STROKE_COLOR colorPostProcessMode:mode];//[m_default_text_annotation GetColorAsRGB];
				
				UIColor* defaultTextAnnotColor = [UIColor colorWithRed:[_colorPt Get:0] green:[_colorPt Get:1] blue:[_colorPt Get:2] alpha:1.0];
				
				CGColorRef cgDefaultTextAnnotColorr = defaultTextAnnotColor.CGColor;
				
				selectionLayer.backgroundColor = cgDefaultTextAnnotColorr;

			}
			else if( annotType == e_ptStrikeOut )
			{
				//BorderStyle* bs = [m_default_text_annotation GetBorderStyle];
				
				if ( _thickness < 0 )
					_thickness = [ColorDefaults defaultBorderThicknessForAnnotType:annotType];//[bs GetWidth];
				
				selectionLayer.frame = CGRectMake(selectionRect.origin.x+[m_pdfViewCtrl GetHScrollPos], selectionRect.origin.y+selectionRect.size.height/2+[m_pdfViewCtrl GetVScrollPos] - _thickness/2, selectionRect.size.width, _thickness);
				
				if( !_colorPt )
					_colorPt = [ColorDefaults defaultColorPtForAnnotType:annotType attribute:ATTRIBUTE_STROKE_COLOR colorPostProcessMode:mode];//[m_default_text_annotation GetColorAsRGB];
				
				UIColor* defaultTextAnnotColor = [UIColor colorWithRed:[_colorPt Get:0] green:[_colorPt Get:1] blue:[_colorPt Get:2] alpha:1.0];
				
				CGColorRef cgDefaultTextAnnotColorr = defaultTextAnnotColor.CGColor;
				
				selectionLayer.backgroundColor = cgDefaultTextAnnotColorr;
			}
			else
			{
				// http://stackoverflow.com/questions/5693297/how-to-draw-wavy-line-on-ios-device
				// squiggly not yet implemented - uses underline
				
				
			}
			
			[m_pdfViewCtrl->ContainerView.layer addSublayer:selectionLayer];
			
			[selectionLayers addObject:selectionLayer];
			
			
			drawnQuads++;
		}
	}
	else
	{
		[self ClearSelectionOnly];
	}
    
}

-(NSMutableArray*)GetQuadsFromPage:(int)page
{
    NSMutableArray* quadsToReturn = [[NSMutableArray alloc] init];
    
    PTSelection* selection = [m_pdfViewCtrl GetSelection:page];
    
    PTVectorQuadPoint* quads = [selection GetQuads];
    
    NSUInteger numberOfQuads = [quads size];
    
    if( numberOfQuads == 0 )
        return Nil;
    
    int selectionPageNumber = [selection GetPageNum];
    
    
    for(int ii = 0; ii < numberOfQuads; ii++)
    {
        PTQuadPoint* aQuad = [quads get:ii];
        
        TRN_point* t_point1 = [aQuad getP1];
        TRN_point* t_point2 = [aQuad getP2];
        TRN_point* t_point3 = [aQuad getP3];
        TRN_point* t_point4 = [aQuad getP4];
        
        CGPoint point1 = CGPointMake([t_point1 getX], [t_point1 getY]);
        CGPoint point2 = CGPointMake([t_point2 getX], [t_point2 getY]);
        CGPoint point3 = CGPointMake([t_point3 getX], [t_point3 getY]);
        CGPoint point4 = CGPointMake([t_point4 getX], [t_point4 getY]);
        
        @try
        { 
            [self ConvertPagePtToScreenPtX:&point1.x Y:&point1.y PageNumber:selectionPageNumber];
            [self ConvertPagePtToScreenPtX:&point2.x Y:&point2.y PageNumber:selectionPageNumber];
            [self ConvertPagePtToScreenPtX:&point3.x Y:&point3.y PageNumber:selectionPageNumber];
            [self ConvertPagePtToScreenPtX:&point4.x Y:&point4.y PageNumber:selectionPageNumber];
            
        }
        @catch(NSException *exception)
        {
            continue;
        }
        
        float left = MIN(point1.x, MIN(point2.x, MIN(point3.x, point4.x)));
        float right = MAX(point1.x, MAX(point2.x, MAX(point3.x, point4.x)));
        
        float top = MIN(point1.y, MIN(point2.y, MIN(point3.y, point4.y)));
        float bottom = MAX(point1.y, MAX(point2.y, MAX(point3.y, point4.y)));
        
        
        
        [quadsToReturn addObject:[NSValue valueWithCGRect:CGRectMake(left, top, (right-left), (bottom-top))]];
    }
    
    return quadsToReturn;
}

-(void)createTextMarkupAnnot
{
    NSUInteger num_quads;
	if( !self.allowScrolling && !CGPointEqualToPoint(selectionStart,selectionEnd) )
	{
		PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
        BOOL hitException = NO;
		@try
		{
			[m_pdfViewCtrl DocLock:YES];
		
			[m_pdfViewCtrl SelectX1:selectionStart.x Y1:selectionStart.y PageNumber1:pageNumber X2:selectionEnd.x Y2:selectionEnd.y PageNumber2:pageNumber];
			
			[self GetQuadsFromPage:pageNumber];
			
			PTPage* p = [doc GetPage:pageNumber];
				
			if( ![p IsValid] )
				return;
			
			PTSelection* sel = [m_pdfViewCtrl GetSelection:pageNumber];
			PTVectorQuadPoint* quads = [sel GetQuads];
			num_quads = [quads size];
			
			if( num_quads > 0 )
			{
				PTQuadPoint* qp = [quads get:0];
				
				TRN_point* point = [qp getP1];
				
				double x1 = [point getX];
				double y1 = [point getY];
				
				point = [qp getP3];
				
				double x2 = [point getX];
				double y2 = [point getY];
				
				PTPDFRect* r = [[PTPDFRect alloc] initWithX1:x1 y1:y1 x2:x2 y2:y2];
				
				
				PTTextMarkup* mktp;
				
				switch (annotType) {
					case e_ptHighlight:
						mktp = [PTHighlightAnnot Create:(PTSDFDoc*)doc pos:r];
						break;
					case e_ptUnderline:
						mktp = [PTUnderline Create:(PTSDFDoc*)doc pos:r];
						break;
					case e_ptStrikeOut:
						mktp = [PTStrikeOut Create:(PTSDFDoc*)doc pos:r];
						break;
					case e_ptSquiggly:
						mktp = [PTSquiggly Create:(PTSDFDoc*)doc pos:r];
						break;
					default:
						// not a supported text annotation type?
						assert(false);
						break;
				}

				for( int i=0; i < num_quads; ++i )
				{
					PTQuadPoint* quad = [quads get:i];

					if( textMarkupAdobeHack )
					{
						// Acrobat and Preview do not follow the PDF specification regarding
						// the ordering of quad points in a text markup annotation. Enable
						// this code for compatibility with those viewers.

						PTPDFPoint* point1 = [quad getP1];
						PTPDFPoint* point2 = [quad getP2];
						PTPDFPoint* point3 = [quad getP3];
						PTPDFPoint* point4 = [quad getP4];
					
						PTQuadPoint* newQuad = [[PTQuadPoint alloc] init];
					
						[newQuad setP1:point4];
						[newQuad setP2:point3];
						[newQuad setP3:point1];
						[newQuad setP4:point2];
					
						[mktp SetQuadPoint:i qp:newQuad];
					}
					else
					{
						[mktp SetQuadPoint:i qp:quad];
					}
				
				}
				
				if( self.annotationAuthor && self.annotationAuthor.length > 0 && [mktp isKindOfClass:[PTMarkup class]]	)
				{
					[(PTMarkup*)mktp SetTitle:self.annotationAuthor];
				}
				
				[p AnnotPushBack:mktp];
				
				PTColorPt* cp = [ColorDefaults defaultColorPtForAnnotType:annotType attribute:ATTRIBUTE_STROKE_COLOR colorPostProcessMode:e_ptpostprocess_none];//[m_default_text_annotation GetColorAsRGB];
				int compNum = [ColorDefaults numCompsInColorPtForAnnotType:annotType attribute:ATTRIBUTE_STROKE_COLOR];
				[mktp SetColor:cp numcomp:compNum];
				
				[mktp SetOpacity:[ColorDefaults defaultOpacityForAnnotType:annotType]];
				
				//BorderStyle* bs = [m_default_text_annotation GetBorderStyle];
				if( _thickness < 0 )
					_thickness = [ColorDefaults defaultBorderThicknessForAnnotType:annotType];
				PTBorderStyle* bs = [[PTBorderStyle alloc] initWithS:e_ptsolid b_width:_thickness b_hr:0 b_vr:0];
				
				[mktp SetBorderStyle:bs oldStyleOnly:NO];

				[mktp RefreshAppearance];
				
				m_moving_annotation = mktp;
				m_annot_page_number = pageNumber;
				
				[m_pdfViewCtrl UpdateWithAnnot:mktp page_num:pageNumber];

			}

		}
		@catch (NSException *exception) {
			NSLog(@"Exception: %@: %@",exception.name, exception.reason);
            hitException = YES;
		}
		@finally {
			[m_pdfViewCtrl DocUnlock];
		}
        
        if (!hitException && num_quads > 0)
            [self annotationAdded:m_moving_annotation onPageNumber:m_annot_page_number];
        
        
    }
	
    [self ClearSelectionOnly];
    
}

@end
