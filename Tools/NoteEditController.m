//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "NoteEditController.h"
#import "PDFViewCtrlToolsUtil.h"

@implementation NoteEditController

@class StickyNoteCreate;


- (instancetype)initWithDelegate:(id)del
{
	self = [super initWithNibName:Nil bundle:Nil];
    if (self) {
        tv = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        delegate = del;
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    //[super viewDidLoad];
	
	NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
	
    UIBarButtonItem *cancelButton;
    
    if( [delegate->m_moving_annotation GetType] == e_ptText )
    {
        cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Delete", @"PDFNet-Tools", toolsStringBundle, @"") style:UIBarButtonItemStylePlain target:self action:@selector(deleteButtonPressed)];
    }
    else {
        cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Cancel", @"PDFNet-Tools", toolsStringBundle, @"") style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    }
    
    self.navigationItem.title = NSLocalizedStringFromTableInBundle(@"Comment", @"PDFNet-Tools", toolsStringBundle, @"The note associated with the PDF annotation.");
    
    [self.navigationItem setLeftBarButtonItem:cancelButton animated:NO];
	
	// now save via popover dismissal on iPad
	if( [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone )
	{
		UIBarButtonItem *okButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Save", @"PDFNet-Tools", toolsStringBundle, @"") style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonPressed)];
		[self.navigationItem setRightBarButtonItem:okButton animated:NO];
	}
    

    tv.font = [UIFont fontWithName:@"Helvetica" size:16];
    
    if (@available(iOS 11, *)) {
        // The below behavior should handle safe area content insets,
        // but when there is no text the leading inset disappears.
        //tv.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentAlways;
        
        // Workaround for the issue described above.
        UIEdgeInsets safeInsets = self.view.safeAreaInsets;
        UIEdgeInsets oldInsets = tv.contentInset;
        [tv setContentInset:UIEdgeInsetsMake(oldInsets.top, safeInsets.left, oldInsets.bottom, safeInsets.right)];
    }
    
    self.view = tv;

    [super viewDidLoad];

}

-(void)cancelButtonPressed
{
    [self resignFirstResponder];
    // callback to have it closed
    
    [delegate noteEditCancelButtonPressed:YES];
    
    
}

-(void)deleteButtonPressed
{
    [self resignFirstResponder];
    // callback to have it closed
    
    [delegate deleteSelectedAnnotation];
    [delegate noteEditCancelButtonPressed:NO];

    
}

-(void)saveButtonPressed
{
    [delegate saveNewNoteForMovingAnnotationWithString:tv.text];
    
}

-(void)viewSafeAreaInsetsDidChange
{
    [super viewSafeAreaInsetsDidChange];
    
    // Update safe area content insets manually.
    // This is a workaround for the issue described in -viewDidLoad.
    UIEdgeInsets safeInsets = self.view.safeAreaInsets;
    UIEdgeInsets oldInsets = tv.contentInset;
    [tv setContentInset:UIEdgeInsetsMake(oldInsets.top, safeInsets.left, oldInsets.bottom, safeInsets.right)];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
