//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "DigitalSignatureTool.h"
#import "PanTool.h"
#import "DigSigViewController.h"
#import "FloatingSigViewController.h"
#import "DigSigView.h"
#import "TRNStampManager.h"
#import "PDFViewCtrlToolsUtil.h"


@interface DigitalSignatureTool ()
{
	TRNStampManager* m_stampManager;
	CGRect m_MenuFrame;
	ToolMode m_toolMode;
	BOOL m_exiting;
	BOOL m_firstTap;
	BOOL m_firstDrag;
}
@end

@implementation DigitalSignatureTool

@synthesize strokeThickness;
@synthesize strokeColor;

static NSString * const SignatureAnnotationIdentifyingString = @"pdftronSignatureStamp";

-(instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl *)in_pdfViewCtrl
{
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        // Initialization code
        self.strokeColor = UIColor.blackColor;
        self.strokeThickness = 2.0;
        
        m_toolMode = e_digital_mode;
        self.allowDigitalSigning = NO;
        m_exiting = NO;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		
        self.strokeColor = UIColor.blackColor;
        self.strokeThickness = 2.0;
		
		m_toolMode = e_digital_mode;
        self.allowDigitalSigning = NO;
		m_exiting = NO;
    }
    return self;
}

+(BOOL)createsAnnotation
{
	return YES;
}

- (BOOL)onCustomEvent:(id)userData
{
	if( m_exiting )
	{
		return NO; // finished creating signature
	}
	else
	{
		m_MenuFrame = CGRectMake(m_down.x, m_down.y, 1, 1);
		[self attachMenuItems];
	   return YES; // just opening this tool from pantool
	}
}

- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	nextToolType = self.defaultClass;
	return NO;
}


-(PTElement*)GetFirstElementUsingReader:(PTElementReader*)reader From:(PTObj*)obj ofType:(PTElementType)type
{
	@try
	{
		[m_pdfViewCtrl DocLockRead];
		
		if( [obj IsValid] )
		{
			[reader ReaderBeginWithSDFObj:obj resource_dict:Nil ocg_context:Nil];
			
			for( PTElement* element = [reader Next]; element != 0; element = [reader Next] )
			{
				if( [element GetType] == type )
				{										
					return element;
				}
			}
		}
	}
	@catch (NSException *exception) {
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally {
		[m_pdfViewCtrl DocUnlockRead];
	}

	return Nil;
}

-(SignatureState)signatureState
{
	PTWidget* wg4;
	PTField* f;

	@try
	{
		[m_pdfViewCtrl DocLockRead];
		wg4 = [[PTWidget alloc] initWithAnn:m_moving_annotation];
		f = [wg4 GetField];
		
		if( [[f GetValue] IsValid] )
		{
			return e_signature_digitally_signed;
		}
				
		PTObj* app = [m_moving_annotation GetAppearance:e_ptnormal app_state:0];
		
		PTElementReader* reader = [[PTElementReader alloc] init];
		
		// when the element reader is destroyed so is the element that it returned. It is therefore important
		// important to keep the reader alive until finished with the element
		PTElement* element = [self GetFirstElementUsingReader:(PTElementReader*)reader From:app ofType:e_ptform];

		if( !element )
		{
			return e_signature_empty;
		}
		
		PTObj* xobj = [element GetXObject];
		
		PTElementReader* objReader = [[PTElementReader alloc] init];
		
		[objReader ReaderBeginWithSDFObj:xobj resource_dict:0 ocg_context:0];
		
		for(PTElement* el = [objReader Next]; el != 0; el = [objReader Next])
		{
			if( [el GetType] == e_ptpath )
			{
				return e_signature_path_appearance_only;
			}
			if( [el GetType] == e_ptimage )
			{
				return e_signature_image_apearance_only;
			}
		}

	}
	@catch (NSException *exception) {
		
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally {
		[m_pdfViewCtrl DocUnlockRead];
	}
	
	return -1;
	
}

-(void)onToolAttachedToViewHierarchy
{
	//[self attachMenuItems];
}

-(void)attachMenuItems
{
	NSMutableArray* menuItems = [[NSMutableArray alloc] initWithCapacity:2];
	
	m_stampManager = [[TRNStampManager alloc] init];
	
    // add menu items common to all annotations EXCEPT movies
	
    UIMenuItem* menuItem;
	
	NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
	
	SignatureState state = -1;
	
	if( m_moving_annotation )
		state = [self signatureState];
	else
		state = e_signature_empty;
	
	if( ( m_moving_annotation == Nil || m_toolMode == e_floating_mode ) )
	{
		
		// not in a widget
		if( [m_stampManager HasDefaultSignature] )
		{
			menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"My Signature", @"PDFNet-Tools", toolsStringBundle, @"The user's saved e-signature") action:@selector(addDefaultStamp)];
			[menuItems addObject:menuItem];
		}
		
		menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"New Signature", @"PDFNet-Tools", toolsStringBundle, @"The user wants to create a new e-signature") action:@selector(showFloatingSignatureViewController)];
		[menuItems addObject:menuItem];
	
		UIMenuController *theMenu = [UIMenuController sharedMenuController];
		
		theMenu.menuItems = menuItems;
		
		if( m_moving_annotation == Nil )
			[self showSelectionMenu:m_MenuFrame animated:YES];
		else
		{
			[self reSelectAnnotation];
		}
		
		return;
	}

	if( state == e_signature_empty )
	{
		// pop up the signature dialog right away
		if( m_toolMode == e_floating_mode )
		{
			[self showFloatingSignatureViewController];
		}
		else if( m_toolMode == e_digital_mode )
		{
			[self showDigitalSignatureViewController];
		}
	}	
	else if( state == e_signature_path_appearance_only )
	{
		
		if( m_toolMode == e_floating_mode )
		{
			menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Delete", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(deleteAppearance)];
			[menuItems addObject:menuItem];
			menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Thickness", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(attachBorderThicknessMenuItems)];
			[menuItems addObject:menuItem];
		}
		else if( m_toolMode == e_digital_mode )
		{
			menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Thickness", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(attachBorderThicknessMenuItems)];
			[menuItems addObject:menuItem];
			//menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Color", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(showColorPicker)];
			//[menuItems addObject:menuItem];
            if( self.allowDigitalSigning )
            {
                menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Digitally Sign", @"PDFNet-Tools", toolsStringBundle, @"Sign the document with a cryptographic certificate.") action:@selector(signAndSave)];
                [menuItems addObject:menuItem];
            }
			menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Delete", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(deleteAppearance)];
			[menuItems addObject:menuItem];
		}
	}
	else if( state == e_signature_digitally_signed )
	{
		menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Digital Signature Information", @"PDFNet-Tools", toolsStringBundle, @"Displays the digital signature's certificate information.") action:@selector(showSignatureInfo)];
		[menuItems addObject:menuItem];
	}
	else if( state == e_signature_image_apearance_only )
	{
		if( m_toolMode == e_digital_mode  && self.allowDigitalSigning )
		{
			menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Digitally Sign", @"PDFNet-Tools", toolsStringBundle, @"Sign the document with a cryptographic certificate.") action:@selector(signAndSave)];
			[menuItems addObject:menuItem];
		}

		menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Delete", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(deleteAppearance)];
		[menuItems addObject:menuItem];
	}
	
    UIMenuController *theMenu = [UIMenuController sharedMenuController];
    
    theMenu.menuItems = menuItems;
    
}

-(void)showSelectionMenu:(CGRect)targetRect
{
	[super showSelectionMenu:targetRect];
	
	UIMenuController *theMenu = [UIMenuController sharedMenuController];
	
	m_MenuFrame = theMenu.menuFrame;
}

-(void)showSelectionMenu:(CGRect)targetRect animated:(BOOL)animated
{
	[super showSelectionMenu:targetRect animated:animated];
	
	UIMenuController *theMenu = [UIMenuController sharedMenuController];
	
	m_MenuFrame = theMenu.menuFrame;
}

// Does not offer to digitally sign, or use a built in image. Can save a new default signature presented to the user as "My Signature"
// Can save the signature anywhere on the document (as opposed to only a signature form field).
-(void)showFloatingSignatureViewController
{
	m_floatingSigViewController = [[FloatingSigViewController alloc] initWithNibName:Nil bundle:Nil];
    m_floatingSigViewController.modalPresentationStyle = UIModalPresentationFormSheet;
	m_floatingSigViewController.delegate = self;
    double screenHeight = [self viewController].view.bounds.size.height-50;
    double screenWidth = [self viewController].view.bounds.size.width-50;
    m_floatingSigViewController.preferredContentSize = CGSizeMake(screenWidth, screenHeight);
    m_floatingSigViewController.strokeColor = self.strokeColor;
    m_floatingSigViewController.strokeThickness = self.strokeThickness;
    
	[[self viewController] presentViewController:m_floatingSigViewController animated:YES completion:Nil];
}

// Save a signature to a signature form field. Can use a certificate to digitally sign.
-(void)showDigitalSignatureViewController
{
    m_digSigViewController = [[DigSigViewController alloc] initWithNibName:Nil bundle:Nil];
    m_digSigViewController.modalPresentationStyle = UIModalPresentationFormSheet;
	m_digSigViewController.delegate = self;
    double screenHeight = [self viewController].view.bounds.size.height-50;
    double screenWidth = [self viewController].view.bounds.size.width-50;
    m_digSigViewController.preferredContentSize = CGSizeMake(screenWidth, screenHeight);

    m_digSigViewController.strokeColor = self.strokeColor;
    m_digSigViewController.strokeThickness = self.strokeThickness;
    m_digSigViewController.allowDigitalSigning = self.allowDigitalSigning;
    
	[[self viewController] presentViewController:m_digSigViewController animated:YES completion:Nil];
}

-(void)addToWidget:(PTPDFDoc*)doc
{
	PTPDFDraw* draw = [[PTPDFDraw alloc] initWithDpi:72];
	
	PTPage* page = [doc GetPage:1];
	
	// First, we need to save the document to the apps sandbox.
	
	NSString* fullFileName = [NSTemporaryDirectory() stringByAppendingPathComponent:@"SignatureTempFile.png"];
	
	PTPDFRect* cropBox = [page GetCropBox];
	int width = [cropBox Width];
	int height = [cropBox Height];

	[draw SetImageSize:width height:height preserve_aspect_ratio:false];

	[draw Export:page filename:fullFileName format:@"png"];

	[self saveAppearanceWithImageFromFilename:fullFileName];
	
	CGPoint sigPoint = CGPointMake(m_MenuFrame.origin.x+m_MenuFrame.size.width/2, m_MenuFrame.origin.y+m_MenuFrame.size.height);
	
	int pageNumber = [m_pdfViewCtrl GetPageNumberFromScreenPt:sigPoint.x y:sigPoint.y];
	
	[m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:pageNumber];
}

-(void)addDefaultStamp
{
	PTPDFDoc* sigDoc = [m_stampManager GetDefaultSignature];
	if( !m_moving_annotation )
		[self addStamp:sigDoc];
	else
		[self addToWidget:sigDoc];
	m_exiting = YES;
	
	// makes no sense to stay in this tool mode
	nextToolType = [PanTool class];

	[m_pdfViewCtrl postCustomEvent:Nil];
}

-(void)addStamp:(PTPDFDoc*)stampDoc
{
	PTPage* stampPage = [stampDoc GetPage:1];
	assert(stampPage);
	
	CGPoint sigPoint;
	
	sigPoint = m_down;
	int pageNumber = [m_pdfViewCtrl GetPageNumberFromScreenPt:sigPoint.x y:sigPoint.y];
	
	if (pageNumber <= 0)
	{
		// tool was envoked from a toolbar
		pageNumber = [m_pdfViewCtrl GetCurrentPage];
		sigPoint = CGPointMake(m_pdfViewCtrl.frame.size.width/2, m_pdfViewCtrl.frame.size.height/2);
	}
	
	@try
	{
		[m_pdfViewCtrl DocLock:YES];
		PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
		
		
		PTPage* page = [doc GetPage:pageNumber];

		PTPDFRect* stampRect = [stampPage GetCropBox];
		double maxWidth = 200;
		double maxHeight = 200;

		PTPDFRect* pageCropBox = [page GetCropBox];
		
		if ([pageCropBox Width] < maxWidth)
		{
			maxWidth = [pageCropBox Width];
		}
		if ([pageCropBox Height] < maxHeight)
		{
			maxHeight = [pageCropBox Height];
		}
		
		double scaleFactor = MIN(maxWidth / [stampRect Width], maxHeight / [stampRect Height]);
		double stampWidth = [stampRect Width] * scaleFactor;
		double stampHeight = [stampRect Height] * scaleFactor;
		
		PTStamper* stamper = [[PTStamper alloc] initWithSize_type:e_ptabsolute_size a:stampWidth b:stampHeight];
		[stamper SetAlignment:e_pthorizontal_left vertical_alignment:e_ptvertical_bottom];
		[stamper SetAsAnnotation:YES];
		
		CGFloat x = sigPoint.x;
		CGFloat y = sigPoint.y;

		[self ConvertScreenPtToPagePtX:&x Y:&y PageNumber:pageNumber];

		double xPos = x - (stampWidth / 2);
		double yPos = y - (stampHeight / 2);

		double pageWidth = [[page GetCropBox] Width];
		if (xPos > pageWidth - stampWidth)
		{
			xPos = pageWidth - stampWidth;
		}
		if (xPos < 0)
		{
			xPos = 0;
		}
		double pageHeight = [[page GetCropBox] Height];
		if (yPos > pageHeight - stampHeight)
		{
			yPos = pageHeight - stampHeight;
		}
		if (yPos < 0)
		{
			yPos = 0;
		}
		
		[stamper SetPosition:xPos vertical_distance:yPos use_percentage:NO];
		
		PTPageSet* pageSet = [[PTPageSet alloc] initWithOne_page:pageNumber];
		
		[stamper StampPage:doc src_page:stampPage dest_pages:pageSet];
		
		int numAnnots = [page GetNumAnnots];

		assert(numAnnots > 0);
		
		PTAnnot* annot = [page GetAnnot:numAnnots - 1];
		PTObj* obj = [annot GetSDFObj];
		[obj PutString:SignatureAnnotationIdentifyingString value:@""];
		
		// Set up to transfer to AnnotEditTool
		m_moving_annotation = annot;
		[m_moving_annotation RefreshAppearance];
		
		m_annot_page_number = pageNumber;
		
		[m_pdfViewCtrl UpdateWithAnnot:annot page_num:pageNumber];
		
		nextToolType = [PanTool class];
		
	}
	@catch (NSException* e)
	{
		NSLog(@"Exception: %@:%@", e.name, e.reason);
	}
	@finally
	{
		[m_pdfViewCtrl DocUnlock];
	}
	
//	mToolManager.CreateTool(mNextToolMode, this);
//	if (mNextToolMode == ToolType.e_annot_edit)
//	{
//		AnnotEdit annotEdit = mToolManager.CurrentTool as AnnotEdit;
//		annotEdit.CreateAppearance();
//	}
	
	[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];

	// leave this tool
	[m_pdfViewCtrl postCustomEvent:Nil];

}

-(void)showSignatureInfo
{

	NSString* info;
	
	NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
	
	@try
	{
		[m_pdfViewCtrl DocLockRead];
		
		PTWidget* widget = [[PTWidget alloc] initWithAnn:m_moving_annotation];
		
		PTObj* sigDict = [[widget GetField] GetValue];
		
		if( [sigDict IsValid] )
		{
			NSString* locationWord = NSLocalizedStringFromTableInBundle(@"Location", @"PDFNet-Tools", toolsStringBundle, @"");
			NSString* reasonWord = NSLocalizedStringFromTableInBundle(@"Reason", @"PDFNet-Tools", toolsStringBundle, @"");
			NSString* nameWord = NSLocalizedStringFromTableInBundle(@"Name", @"PDFNet-Tools", toolsStringBundle, @"");
			
			NSString* location = [[sigDict FindObj:@"Location"] GetAsPDFText];
			NSString* reason = [[sigDict FindObj:@"Reason"] GetAsPDFText];
			NSString* name = [[sigDict FindObj:@"Name"] GetAsPDFText];
			info = [NSString stringWithFormat:@"%@: %@\n %@: %@\n %@: %@", locationWord, location, reasonWord, reason, nameWord, name];
		}
		
	}
	@catch (NSException *exception) {
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally {
		[m_pdfViewCtrl DocUnlockRead];
	}
	
	if( !info )
	{
		info = NSLocalizedStringFromTableInBundle(@"No Information", @"PDFNet-Tools", toolsStringBundle, @"No digital signature information.");
	}
	
	UIAlertController *alertController = [UIAlertController
										  alertControllerWithTitle:NSLocalizedStringFromTableInBundle(@"Digital Signature", @"PDFNet-Tools", toolsStringBundle, @"")
										  message:info
										  preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction *okAction = [UIAlertAction
								   actionWithTitle:NSLocalizedStringFromTableInBundle(@"OK", @"PDFNet-Tools", toolsStringBundle, @"")
								   style:UIAlertActionStyleDefault
							   handler:^(UIAlertAction *action){
								   m_firstTap = NO;
							   }];

	
	[alertController addAction:okAction];
	
	[self.viewController presentViewController:alertController animated:YES completion:nil];

}

-(void)deleteAppearance
{
	@try
	{
		[m_pdfViewCtrl DocLock:YES];
		
		PTWidget* widget = [[PTWidget alloc] initWithAnn:m_moving_annotation];
		
		[[widget GetSDFObj] EraseDictElementWithKey:@"AP"];
		
		[m_moving_annotation RefreshAppearance];
		
	}
	@catch (NSException *exception) {
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally {
		[m_pdfViewCtrl DocUnlock];
	}
	
	[m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
	
	[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
	
	m_exiting = YES;
	nextToolType = self.defaultClass;
	[m_pdfViewCtrl postCustomEvent:Nil];
}

- (void)colorPickerViewController:(UIViewController*)colorPicker didSelectColor:(UIColor *)color {
    
    @try
    {
        [m_pdfViewCtrl DocLock:YES];
		
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }
}

-(void)reSelectAnnotation
{
	if( ![m_moving_annotation IsValid] )
		return;
	
	PTPDFRect* rect = [m_moving_annotation GetRect];
	CGRect screenRect = [self PDFRectPage2CGRectScreen:rect PageNumber:m_annot_page_number];
	[self showSelectionMenu:screenRect];
}

- (void) attachBorderThicknessMenuItems
{
	[self hideMenu];
	
    NSMutableArray* menuItems = [[NSMutableArray alloc] initWithCapacity:2];
    
    UIMenuItem* menuItem;
	
	NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
	NSString* pt = NSLocalizedStringFromTableInBundle(@"pt", @"PDFNet-Tools", stringBundle, @"Abberviation for point, as in font point size.");
	
	menuItem = [[UIMenuItem alloc] initWithTitle:[NSString stringWithFormat:@"0.5 %@", pt] action:@selector(setThickness05)];
	[menuItems addObject:menuItem];
	menuItem = [[UIMenuItem alloc] initWithTitle:[NSString stringWithFormat:@"1 %@", pt] action:@selector(setThickness10)];
	[menuItems addObject:menuItem];
	menuItem = [[UIMenuItem alloc] initWithTitle:[NSString stringWithFormat:@"3 %@", pt] action:@selector(setThickness30)];
	[menuItems addObject:menuItem];
	menuItem = [[UIMenuItem alloc] initWithTitle:[NSString stringWithFormat:@"5 %@", pt] action:@selector(setThickness50)];
	
    UIMenuController *theMenu = [UIMenuController sharedMenuController];
    theMenu.menuItems = menuItems;
	
	PTPDFRect* rect = [m_moving_annotation GetRect];
	
	CGRect screenRect = [self PDFRectPage2CGRectScreen:rect PageNumber:m_annot_page_number];
	
	[self showSelectionMenu:screenRect];
}

-(void)setThickness05
{
	[self setThickness:0.5];
}

-(void)setThickness10
{
	[self setThickness:1.0];
}

-(void)setThickness30
{
	[self setThickness:3.0];
}

-(void)setThickness50
{
	[self setThickness:5.0];
}

-(void)setThickness:(double)thickness
{
	@try
	{
		[m_pdfViewCtrl DocLock:YES];
		
		PTObj* app = [m_moving_annotation GetAppearance:e_ptnormal app_state:Nil];
		PTElementReader* reader = [[PTElementReader alloc] init];
		PTElement* element = [self GetFirstElementUsingReader:reader From:app ofType:e_ptform];
		
		if( element != Nil )
		{
			PTObj* xobj = [element GetXObject];
			PTElementReader* reader2 = [[PTElementReader alloc] init];
			element = [self GetFirstElementUsingReader:reader2 From:xobj ofType:e_ptpath];
			
			if( element != Nil )
			{
				PTElementWriter* writer = [[PTElementWriter alloc] init];
				
				[writer WriterBeginWithSDFObj:xobj compress:YES];
				
				PTGState* gs = [element GetGState];
				[gs SetLineWidth:thickness];
				[writer WriteElement:element];
				[writer End];
				
				[m_moving_annotation RefreshAppearance];
				[m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
			}
			
		}
		
	}
	@catch (NSException *exception) {
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally {
		[m_pdfViewCtrl DocUnlock];
	}
	
	[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
}

-(NSData*)getNSDataFromUIImage:(UIImage*)image
{
	CGImageRef imageRef = image.CGImage;
	NSUInteger width = CGImageGetWidth(imageRef);
	NSUInteger height = CGImageGetHeight(imageRef);
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	unsigned char *rawData = (unsigned char*) malloc(height * width * 4 * sizeof(unsigned char));
	NSUInteger bytesPerPixel = 4;
	NSUInteger bytesPerRow = bytesPerPixel * width;
	NSUInteger bitsPerComponent = 8;

	CGContextRef context = CGBitmapContextCreate(rawData, width, height,
												 bitsPerComponent, bytesPerRow, colorSpace,
												 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
	CGColorSpaceRelease(colorSpace);

	CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
	CGContextRelease(context);
	
	unsigned char *noAlpha = (unsigned char*) malloc(height * width * 3* sizeof(unsigned char));
	
	for(int pix = 0; pix < height * width * 4; pix += bytesPerPixel)
	{
		memcpy((noAlpha+pix/bytesPerPixel*3), (rawData+pix), 3);
	}
	
	NSData* data = [[NSData alloc] initWithBytesNoCopy:noAlpha length:height*width*3*sizeof(unsigned char) freeWhenDone:YES];
	
	free(rawData);
	
	return data;
}

-(void)saveAppearanceWithPath:(NSMutableArray*)points withBoundingRect:(CGRect)boundingRect asDefault:(BOOL)asDefault
{
    
    if(points.count > 0 )
    {
        if( !m_stampManager )
        {
            // tool was probably instantiated directly from a button, not via a touch even prior
            m_stampManager = [[TRNStampManager alloc] init];
            
            // put signature in middle of frame
            m_down = CGPointMake(m_pdfViewCtrl.frame.size.width/2, m_pdfViewCtrl.frame.size.height/2);
        }
        
        PTPDFDoc* doc = [m_stampManager CreateSignature:points withStrokeColor:self.strokeColor withStrokeThickness:self.strokeThickness withinRect:boundingRect makeDefault:asDefault];
        assert(doc);
        
        if( !m_moving_annotation )
        {
            [self addStamp:doc];
            [self closeSignatureDialog];
        }
        else
            [self addToWidget:doc];
    }
    else
    {
        [self closeSignatureDialog];
    }
	
}

-(void)saveAppearanceWithImageFromFilename:(NSString*)fileName
{
	@try
	{
		[m_pdfViewCtrl DocLock:YES];
		
		PTSDFDoc* doc = [[m_pdfViewCtrl GetDoc] GetSDFDoc];
		
		PTImage* sigImg = [PTImage Create:doc filename:fileName];
		
		[self saveAppearanceWithTrnImage:sigImg];
		
	}
	@catch (NSException *exception)
	{
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally
	{
		[m_pdfViewCtrl DocUnlock];
	}
	
	[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
	
}

-(void)saveAppearanceWithUIImage:(UIImage*)uiImage
{
	@try
	{
		[m_pdfViewCtrl DocLock:YES];
		
		PTSDFDoc* doc = [[m_pdfViewCtrl GetDoc] GetSDFDoc];
		
		NSData* data = [self getNSDataFromUIImage:uiImage];
		
		PTObj* o = [[PTObj alloc] init];
        
		PTImage* trnImage = [PTImage CreateWithData:doc buf:data buf_size:data.length width:uiImage.size.width height:uiImage.size.height bpc:8 color_space:[PTColorSpace CreateDeviceRGB] encoder_hints:o];
			
		[self saveAppearanceWithTrnImage:trnImage];
		
	}
	@catch (NSException *exception)
	{
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally
	{
		[m_pdfViewCtrl DocUnlock];
	}
	
	
	[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
}

-(void)saveAppearanceWithTrnImage:(PTImage*)trnImage
{
	[self closeSignatureDialog];
	
	PTSDFDoc* doc = [[m_pdfViewCtrl GetDoc] GetSDFDoc];
	
	CGSize size = CGSizeMake([trnImage GetImageWidth], [trnImage GetImageHeight]);
	
	PTElementWriter* apWriter = [[PTElementWriter alloc] init];
	PTElementBuilder* apBuilder = [[PTElementBuilder alloc] init];

	[apWriter WriterBeginWithSDFDoc:doc compress:YES];

	PTPDFRect* widgetRect = [m_moving_annotation GetRect];
	[widgetRect InflateWithAmount:-1];

    
    
    PTRotate rotation = [[[m_pdfViewCtrl GetDoc] GetPage:m_annot_page_number] GetRotation];
    
	double formRatio;
	
    if (rotation == e_pt90 || rotation == e_pt270 ) {
        formRatio = [widgetRect Height]/[widgetRect Width];
    } else {
        formRatio  = [widgetRect Width]/[widgetRect Height];
    }

    
    double imageRatio = size.width/size.height;
    
    CGSize bbox = size;
    if (rotation == e_pt90 || rotation == e_pt270 ) {
        bbox.width = size.height;
        bbox.height = size.width;
    }

    
	double widthAdjust = 1.0;
	double heightAdjust = 1.0;
	double horzTranslate = 0.0;
	double vertTranslate = 0.0;
	
	if (imageRatio < formRatio )
	{
		widthAdjust = imageRatio/formRatio;
	}
	else if( imageRatio > formRatio )
	{
		heightAdjust = formRatio/imageRatio;
	}
	double widthRatio = [widgetRect Width]/size.width;
	double heightRatio = [widgetRect Height]/size.height;

	double minRatio = MIN(widthRatio, heightRatio);
	
	horzTranslate = ([widgetRect Width] - (size.width * minRatio))/2;
	vertTranslate = ([widgetRect Height] - (size.height * minRatio))/2;
	
    PTMatrix2D* mtx;
    
    if (rotation == e_pt0) {
        mtx = [[PTMatrix2D alloc] initWithA:size.width*widthAdjust b:0 c:0 d:size.height*heightAdjust h:horzTranslate*widthAdjust/minRatio v:vertTranslate*heightAdjust/minRatio];
    } else {
        mtx = [[PTMatrix2D alloc] initWithA:size.width*widthAdjust b:0 c:0 d:size.height*heightAdjust h:0 v:0];
    }

    PTMatrix2D* rotationMtx;

    if (rotation == e_pt90) {
        rotationMtx = [[PTMatrix2D alloc] initWithA:0 b:1 c:-1 d:0 h:size.height v:0];
    } else if (rotation == e_pt180) {
        rotationMtx = [[PTMatrix2D alloc] initWithA:-1 b:0 c:0 d:-1 h:size.width v:size.height];
    } else if (rotation == e_pt270) {
        rotationMtx = [[PTMatrix2D alloc] initWithA:0 b:-1 c:1 d:0 h:0 v:size.width];
    } else if (rotation == e_pt0) {
        rotationMtx = [[PTMatrix2D alloc] initWithA:1 b:0 c:0 d:1 h:0 v:0];
    }
    
    mtx = [rotationMtx Multiply:mtx];

	
	PTElement* apElement = [apBuilder CreateImageWithMatrix:trnImage mtx:mtx];

	[apWriter WritePlacedElement:apElement];
	PTObj* apObj = [apWriter End];
	
	[apObj PutRect:@"BBox" x1:0 y1:0 x2:bbox.width y2:bbox.height];
	[apObj PutName:@"Subtype" name:@"Form"];
	[apObj PutName:@"Type" name:@"XObject"];

	[m_moving_annotation SetAppearance:apObj annot_state:e_ptnormal app_state:0];
	[m_moving_annotation RefreshAppearance];

	[m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];

}

-(void)rotatePoints:(NSMutableArray*)points forPageRotation:(PTRotate)rotation
{

    double radians = 0;
    
    if( rotation == e_pt0 )
    {
        return;
    }
    else if( rotation == e_pt90 )
    {
        radians = 3*M_PI/2;
    }
    else if( rotation == e_pt180 )
    {
        radians = M_PI;
    }
    else if( rotation == e_pt270 )
    {
        radians = M_PI/2;
    }
    
    double x_min = DBL_MAX;
    double x_max = DBL_MIN;
    double y_min = DBL_MAX;
    double y_max = DBL_MIN;
    
    for (NSValue* val in points) {
        
        CGPoint point;
        [val getValue:&point];
        
        if( point.x > 0 )
        {
            x_min = MIN(x_min, point.x);
            x_max = MAX(x_max, point.x);
        }
        
        if( point.y > 0 )
        {
            y_min = MIN(y_min, point.y);
            y_max = MAX(y_max, point.y);
        }
    }
    
    CGPoint center;
    center.x = x_min + (x_max - x_min)/2;
    center.y = y_min + (y_max - y_min)/2;
    
    for (int i = 0; i < points.count; i++) {
        NSValue* val = points[i];
        CGPoint point;
        [val getValue:&point];
        
        if( !CGPointEqualToPoint(CGPointZero, point) )
        {
            CGAffineTransform translateTransform = CGAffineTransformMakeTranslation(center.x, center.y);
            CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(radians);
            
            CGAffineTransform pointsRotation = CGAffineTransformConcat(CGAffineTransformConcat( CGAffineTransformInvert(translateTransform), rotationTransform), translateTransform);
            
            point = CGPointApplyAffineTransform(point, pointsRotation);
            
            points[i] = [NSValue valueWithCGPoint:point];
            
        }
    }

}

-(void)normalizePoints:(NSMutableArray*)points onCanvasSize:(CGSize)canvasSize
{
    PTRotate rotation = [[[m_pdfViewCtrl GetDoc] GetPage:m_annot_page_number] GetRotation];
    [self rotatePoints:points forPageRotation:rotation];
    
	PTPDFRect* widgetRect = [m_moving_annotation GetRect];
	[widgetRect InflateWithAmount:-1];
    
    double min_x = DBL_MAX;
    double min_y = DBL_MAX;
    double max_x = DBL_MIN;
    double max_y = DBL_MIN;
    
    for (int i = 0; i < points.count; i++) {
        NSValue* pointContainer = points[i];
        
        CGPoint point;
        [pointContainer getValue:&point];
        
        if( CGPointEqualToPoint(point, CGPointZero) )
            continue;
        
        min_x = MIN(min_x, point.x);
        min_y = MIN(min_y, point.y);
        
        max_x = MAX(max_x, point.x);
        max_y = MAX(max_y, point.y);
    }
    double width = max_x - min_x;
    double height = max_y - min_y;
    
    
    // scale points to fit rectangle
    double horizScale = [widgetRect Width] / width;
    double vertScale = [widgetRect Height] / height;
    
    if(rotation == e_pt90 || rotation == e_pt270)
    {
        horizScale = [widgetRect Height] / height;
        vertScale = [widgetRect Width] / width;
    }
    
    double outerScaleFactor = MIN(horizScale, vertScale);

	
	for (int i = 0; i < points.count; i++) {
		NSValue* pointContainer = points[i];
		
		CGPoint point;
		[pointContainer getValue:&point];
		point.x *= outerScaleFactor;
		point.y *= outerScaleFactor;
		
		// flip y, center y
		if (point.y != 0) {
            point.y -= min_y*outerScaleFactor;
            point.y = height*outerScaleFactor - point.y;
            point.y += [widgetRect Height]/2 - outerScaleFactor*height/2;
		}
		
		// center x
        if (point.x != 0) {
            point.x -= min_x*outerScaleFactor;
            point.x += [widgetRect Width]/2 - outerScaleFactor*width/2;
        }
		
		points[i] = [NSValue valueWithCGPoint:point];
	}
}

-(void)saveAppearanceWithPath:(NSMutableArray*)points fromCanvasSize:(CGSize)canvasSize
{
    if( points.count > 0 )
    {
        @try
        {
            [self normalizePoints:points onCanvasSize:canvasSize];
            
            [m_pdfViewCtrl DocLock:YES];

            PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];

            // Add the signature appearance
            PTElementWriter* apWriter = [[PTElementWriter alloc] init];
            PTElementBuilder* apBuilder = [[PTElementBuilder alloc] init];
            
            [apWriter WriterBeginWithSDFDoc:[doc GetSDFDoc] compress:YES];
            
            [apBuilder PathBegin];

            CGPoint aPoint = CGPointZero, bPoint;
            
            for(NSValue* pointContainer in points)
            {
                bPoint = pointContainer.CGPointValue;
                if( !CGPointEqualToPoint(CGPointZero, aPoint) && !CGPointEqualToPoint(CGPointZero, bPoint) )
                {
                    [apBuilder LineTo:bPoint.x y:bPoint.y];
                }
                else
                {
                    [apBuilder MoveTo:bPoint.x y:bPoint.y];
                }
                aPoint = bPoint;
            }

            PTElement *element = [apBuilder PathEnd];
            [element SetPathStroke:YES];
            
            // Set default line color
            [[element GetGState] SetLineWidth:1];
            [[element GetGState] SetLineCap:e_ptround_cap];
            [[element GetGState] SetLineJoin:e_ptround_join];
            
            [apWriter WriteElement:element];

            PTObj* obj = [apWriter End];
            
            [obj PutRect:@"BBox" x1:0 y1:0 x2:[[m_moving_annotation GetRect] Width] y2:[[m_moving_annotation GetRect] Height]];
            [obj PutName:@"Subtype" name:@"Form"];
            [obj PutName:@"Type" name:@"XObject"];
            [obj PutName:@"Name" name:@"Signature"];
            
            [m_moving_annotation SetAppearance:obj annot_state:e_ptnormal app_state:0];
            
            [m_moving_annotation RefreshAppearance];
            
            [m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
        }
        @catch (NSException *exception)
        {
            NSLog(@"Exception: %@: %@",exception.name, exception.reason);
        }
        @finally
        {
            [m_pdfViewCtrl DocUnlock];
        }
        
        
        [m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
        
        [self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
    }
    
    [self closeSignatureDialog];

}

-(void)closeSignatureDialog
{
	if( m_floatingSigViewController || m_digSigViewController )
		[[self viewController] dismissViewControllerAnimated:YES completion:Nil];

	self->nextToolType = [PanTool class];
	m_exiting = YES;

	[m_pdfViewCtrl postCustomEvent:Nil];
	
}

- (BOOL)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if( gestureRecognizer.state != UIGestureRecognizerStateBegan )
        return YES;
    
	return [self selectDigSig:[gestureRecognizer locationInView:m_pdfViewCtrl]];
}


- (BOOL)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
	
	if( ![self selectDigSig:[gestureRecognizer locationInView:m_pdfViewCtrl]] && !m_firstTap)
	{
		m_down = [gestureRecognizer locationInView:m_pdfViewCtrl];
		m_moving_annotation = Nil;
		CGPoint down = [gestureRecognizer locationInView:m_pdfViewCtrl];
		m_MenuFrame = CGRectMake(down.x, down.y, 1, 1);
		[self attachMenuItems];
	}
	else if( m_firstTap )
	{
		// if a second tap is handled (i.e. the user did not choose my signture or new signature)
		// then dismiss tool
		self->nextToolType = [PanTool class];
		m_exiting = YES;
		
		[m_pdfViewCtrl postCustomEvent:Nil];
	}
    
    m_firstTap = YES;
    
	return YES;
}


- (BOOL)selectDigSig:(CGPoint)down
{
	BOOL foundSignature = NO;
    @try
    {
        [m_pdfViewCtrl DocLockRead];
		
		
        m_moving_annotation = [m_pdfViewCtrl GetAnnotationAt:down.x y:down.y distanceThreshold:GET_ANNOT_AT_DISTANCE_THRESHOLD minimumLineWeight:GET_ANNOT_AT_MINIMUM_LINE_WEIGHT];
		
		if([m_moving_annotation IsValid] && [m_moving_annotation GetType] == e_ptWidget)
		{
			foundSignature = YES;
			PTWidget* wg4;
			PTField* f;
			
			@try
			{
				[m_pdfViewCtrl DocLockRead];
				wg4 = [[PTWidget alloc] initWithAnn:m_moving_annotation];
				f = [wg4 GetField];
				
				if( [f GetType] != e_ptsignature )
				{
					foundSignature = NO;
					m_moving_annotation = Nil;
					return foundSignature;
				}
			}
			@catch (NSException *exception) {
				NSLog(@"Exception: %@: %@",exception.name, exception.reason);
			}
			@finally {
				[m_pdfViewCtrl DocUnlockRead];
			}
			
			m_annot_page_number = [m_pdfViewCtrl GetPageNumberFromScreenPt:down.x y:down.y];
			
			PTPDFRect* rect = [m_moving_annotation GetRect];
			
			CGRect screenRect = [self PDFRectPage2CGRectScreen:rect PageNumber:m_annot_page_number];
			
			[self attachMenuItems];
			
			[self showSelectionMenu:screenRect];
		}
		
		
	}
	@catch (NSException *exception) {
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally {
		[m_pdfViewCtrl DocUnlockRead];
	}
	
	nextToolType = [PanTool class];
	if( foundSignature )
		return YES;
	else
		return NO;
}
/// digital signature

-(void)signAndSave
{
	PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
	
	NSString* fullPath = [[NSBundle mainBundle] pathForResource:@"pdftron" ofType:@"pfx"];
	NSString* newFile;
	
	@try
	{
		[m_pdfViewCtrl DocLock:YES];
		
		SignatureHandlerId sigHandlerId =  [doc AddStdSignatureHandlerFromFile:fullPath pkcs12_keypass:@"password"];
		
		PTWidget* widget = [[PTWidget alloc] initWithAnn:m_moving_annotation];
		
		PTField* field = [widget GetField];
		
		PTObj* sigDict = [field UseSignatureHandler:sigHandlerId];
		
		[sigDict PutName:@"SubFilter" name:@"adbe.pkcs7.detached"];
		[sigDict PutString:@"Name" value:@"PDFTron"];
		[sigDict PutString:@"Location" value:@"Vancouver, BC"];
		[sigDict PutString:@"Reason" value:@"Document Verification"];
		
		NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = paths[0];
		
		NSString* baseName = [[m_pdfViewCtrl GetDoc] GetFileName].lastPathComponent.stringByDeletingPathExtension;
		NSString* signedPDFName = [baseName stringByAppendingString:@"-signed.pdf"];
		
		newFile = [documentsDirectory stringByAppendingPathComponent:signedPDFName];
		
		unsigned int tries = 1;
		
		BOOL isDir;
		
		// append numbers as to not overwrite previously signed files with the same name
		while([[NSFileManager defaultManager] fileExistsAtPath:newFile isDirectory:&isDir])
		{
			signedPDFName = [baseName stringByAppendingString:[NSString stringWithFormat:@"-signed %u.pdf",tries]];
			newFile = [documentsDirectory stringByAppendingPathComponent:signedPDFName];
			tries++;
		}
		
		[doc SaveToFile:newFile flags:e_ptincremental];
	}
	@catch (NSException *exception) {
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally {
		[m_pdfViewCtrl DocUnlock];
	}
	
	[m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
	
	[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
