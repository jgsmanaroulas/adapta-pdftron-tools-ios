//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "SelectionBar.h"

static const int circleDiameter = 12;

@implementation SelectionBar


- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {

        [self setUserInteractionEnabled:YES];
        
        self.contentMode = UIViewContentModeCenter;
		
		self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}

-(void)drawRect:(CGRect)rect
{
	CGRect myRect = CGRectMake(rect.size.width/2-circleDiameter/2, rect.size.height/2-circleDiameter/2, circleDiameter, circleDiameter);
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGContextClearRect(ctx, myRect);
    CGContextSetFillColorWithColor(ctx, [UIColor colorWithRed:0.4 green:0.4	blue:1.0 alpha:1.0].CGColor);
	CGContextFillEllipseInRect(ctx, myRect);
	CGContextStrokePath(ctx);
}

-(void)setIsLeft:(BOOL)left
{
    if( left != isLeft )
    {
        isLeft = left;
    }
}

-(BOOL)isLeft
{
    return isLeft;
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}


@end
