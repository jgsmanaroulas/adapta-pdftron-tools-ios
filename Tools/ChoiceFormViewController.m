//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "ChoiceFormViewController.h"


@implementation ChoiceFormViewController

@synthesize isMultiSelect;


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setIsMultiSelect:NO];
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)setDelegate:(id)delegate
{
    del = delegate;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [del numberOfChoices];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [del titleOfChoiceAtIndex:indexPath.row];
    
    NSMutableArray* items = [del getSelectedItemsInActiveListbox];
    

    cell.accessoryType = UITableViewCellAccessoryNone;
    
    for( int i = 0; i < items.count; i++)
    {
        NSNumber* nsnum = items[i];
        int num = nsnum.intValue;

        if( num == indexPath.row )
        {
            [tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:num inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryType=UITableViewCellAccessoryCheckmark;
        }
    }

    return cell;
    
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    //UIView *view = [[[UIView alloc] init] autorelease];
    UITableView *tableView = [[UITableView alloc] init];
    tableView.dataSource = self;
    tableView.delegate = del;
    tableView.frame = CGRectMake(0, 0, 250, 350);
    
    NSMutableArray* items = [del getSelectedItemsInActiveListbox];
    
    for( int i = 0; i < items.count; i++)
    {
        NSNumber* nsnum = items[i];
        int num = nsnum.intValue;

        if( num >= 0 )
        {
            [tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:num inSection:0] animated:NO scrollPosition:UITableViewScrollPositionMiddle];
            [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:num inSection:0]].selectionStyle = UITableViewCellSelectionStyleNone;
            [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:num inSection:0]].accessoryType=UITableViewCellAccessoryCheckmark;
        }
    }
    
    
    //[view addSubview:tableView];
    self.view = tableView;

    
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
