//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "Tool.h"
#import "PanTool.h"
#import "SelectionRectContainerView.h"
#import "NoteEditController.h"
#import "CreateToolBase.h"
#import "TextMarkupCreate.h"
#import "PDFViewCtrlToolsUtil.h"

@implementation ToolView

@end

@interface Tool ()
{
    UILabel* m_pageNumberLabel;
}
@end

@implementation Tool

@synthesize backToPanToolAfterUse;
@synthesize pageIndicatorIsVisible;
@synthesize annotationAuthor;
@synthesize pdfViewCtrl = m_pdfViewCtrl;
@synthesize defaultClass;
@synthesize allowScrolling;

@class StickyNoteCreate;
@class SelectionBar;

-(void)setPageIndicatorIsVisible:(BOOL)pageIndicatorIsVisibleValue
{
    m_pageNumberLabel.hidden = !pageIndicatorIsVisibleValue;
    pageIndicatorIsVisible = pageIndicatorIsVisibleValue;
}

-(void)executeAction:(PTActionParameter*)action_param
{
    PTAction* action = [action_param GetAction];
    if([action IsValid])
    {
        PTActionType actionType = [action GetType];
        if(actionType == e_ptURI)
        {
            PTObj* sdfObj = [action GetSDFObj];
            if([sdfObj IsValid])
            {
                PTObj* uriObj = [sdfObj FindObj:@"URI"];
                if(uriObj != Nil)
                {
                    NSString* uriDestination = [uriObj GetAsPDFText];
                    uriDestination = [PTLink GetNormalizedUrl:uriDestination];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: uriDestination]];
                    });
                }
            }
        }
        else if(actionType == e_ptSubmitForm)
        {
            PTObj* filespec = [[action GetSDFObj] FindObj:@"F"];
            
            if( [filespec IsValid] )
            {
                PTObj* fsEntry = [filespec FindObj:@"FS"];
                if( [fsEntry IsValid] && [[fsEntry GetName] isEqualToString:@"URL"] )
                {
                    PTObj* urlObj = [filespec FindObj:@"F"];
                    if( [urlObj IsValid])
                    {
                        NSString* urlStr = [urlObj GetAsPDFText];
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: urlStr]];
                        });
                    }
                }
            }

        }
        else if(actionType == e_ptGoTo)
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                @try {
                    [m_pdfViewCtrl ExecuteActionWithActionParameter:action_param];
                } @catch (NSException *exception) {
                    
                }
            });
        }
        else
        {
            [m_pdfViewCtrl ExecuteActionWithActionParameter:action_param];
        }
    }
}



- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        m_pdfViewCtrl = in_pdfViewCtrl;
        m_pagePt = [[PTPDFPoint alloc] init];
        m_screenPt = [[PTPDFPoint alloc] init];

        [self setUserInteractionEnabled:NO];
        
        [self setBackToPanToolAfterUse:YES];
		
		pageIndicatorIsVisible = YES;
		
		m_pageNumberLabel = [[UILabel alloc] init];
        m_pageNumberLabel.translatesAutoresizingMaskIntoConstraints = NO;
        m_pageNumberLabel.alpha = 0.0f;
        m_pageNumberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
        m_pageNumberLabel.backgroundColor = [UIColor blackColor];
        //m_pageNumberLabel.clipsToBounds = YES;
        //m_pageNumberLabel.layer.cornerRadius = 15.0f;
        m_pageNumberLabel.textAlignment = NSTextAlignmentCenter;
        m_pageNumberLabel.textColor = [UIColor whiteColor];
        m_pageNumberLabel.text = [NSString stringWithFormat:@"%d/%d", [m_pdfViewCtrl GetCurrentPage], [m_pdfViewCtrl GetPageCount]];
        
        // no longer used in sample app due to page slider
        // uncomment lines to add a page indicator in thex
        // upper left corner.
         [m_pdfViewCtrl addSubview:m_pageNumberLabel];
		
		[self positionPageNumberLabel];
		
		textMarkupAdobeHack = YES;
        
		defaultClass = [PanTool class];
		
    }
    
    return self;
}

- (void)positionPageNumberLabel
{

	CGSize size = [UIScreen mainScreen].bounds.size;
	if( UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) )
	{
		double width = size.width;
		size.width = size.height;
		size.height = width;
	}
    
    
    if( m_pageNumberLabel.superview )
    {
        CGSize textSize = [m_pageNumberLabel.text boundingRectWithSize:CGSizeMake(10000, 100) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Light" size:18] } context:nil].size;
    
        if( m_pageNumberWidthConstraint )
        {
            // get rid of old, possibly incorrect width constraint
            [m_pageNumberLabel removeConstraint:m_pageNumberWidthConstraint];
        }
        else
        {
            // first time we're adding constraints
            [m_pageNumberLabel.heightAnchor constraintEqualToConstant:textSize.height+m_pageNumberLabel.layer.cornerRadius+5].active = true;
            
            if (@available(iOS 11, *))
            {
                UILayoutGuide* guide = m_pageNumberLabel.superview.safeAreaLayoutGuide;
                [m_pageNumberLabel.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor constant:-50].active = true;
                [m_pageNumberLabel.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor constant:10].active = true;
            }
            else
            {
                
                [m_pageNumberLabel.bottomAnchor constraintEqualToAnchor:m_pageNumberLabel.superview.bottomAnchor constant:-50].active = true;
                [m_pageNumberLabel.leadingAnchor constraintEqualToAnchor:m_pageNumberLabel.superview.leadingAnchor constant:5].active = true;
            }
        }
    
        // add new width constraint
        m_pageNumberWidthConstraint = [m_pageNumberLabel.widthAnchor constraintEqualToConstant:textSize.width+m_pageNumberLabel.layer.cornerRadius+20];
        
        m_pageNumberWidthConstraint.active = YES;

    }
}


-(void)keepToolAppearanceOnScreen
{
    

	if(!CGSizeEqualToSize(CGSizeZero,self.frame.size))
	{
		// keep tool's appearance on screen even after the tool is removed.
		ToolView* contentView = [[ToolView alloc] initWithFrame:self.frame];
		
		float screenScale = [UIScreen mainScreen].scale;
		UIGraphicsBeginImageContextWithOptions(self.frame.size, false, screenScale);
		
		[self.layer renderInContext:UIGraphicsGetCurrentContext()];
		
		UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
		
		contentView.image = viewImage;
		
		UIGraphicsEndImageContext();
		
		[self.superview addSubview:contentView];
	}
}

-(void)willMoveToSuperview:(UIView *)newSuperview
{
	if( m_pdfViewCtrl )
	{
		if( newSuperview == Nil )
		{
			[m_pdfViewCtrl setScrollEnabled:YES];

			[m_pdfViewCtrl setZoomEnabled:YES];
		}
		else if( (self.createsAnnotation || ( self.backToPanToolAfterUse == NO )))
		{
			[m_pdfViewCtrl setScrollEnabled:YES];

			[m_pdfViewCtrl setZoomEnabled:NO];
		}
	}
	
    [super willMoveToSuperview:newSuperview];
}

- (void)dealloc
{
    [m_pageNumberLabel removeFromSuperview];
    
}


- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //NSLog(@"%@ touchesBegan, empty implementation.", [self class]);
    return YES;
}

- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    //NSLog(@"%@ touchesMoved, empty implementation.", [self class]);
    return YES;
}

- (BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //NSLog(@"%@ touchesEnded, empty implementation.", [self class]);
    return YES;
}

- (BOOL)onTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    //NSLog(@"%@ touchesCancelled, empty implementation.", [self class]);
    return YES;
}


- (BOOL)onCustomEvent:(id)userData
{
    //NSLog(@"%@ customEvent, empty implementation.", [self class]);
    return YES;
}

-(BOOL)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    //NSLog(@"%@ handleLongPress, empty implementation.", [self class]);
    return YES;
}

-(BOOL)handleDoubleTap:(UITapGestureRecognizer*)gestureRecognizer
{
    // if statement here to only allow double tap after view has stabilized
    if( fabs([m_pdfViewCtrl zoomScale] - 1.0) < 0.01)
    {
        CGPoint down = [gestureRecognizer locationOfTouch:0 inView:m_pdfViewCtrl];
        
        double zoomBy = ([m_pdfViewCtrl GetCanvasWidth])/m_pdfViewCtrl.bounds.size.width;

        if( zoomBy < 1.01 || [m_pdfViewCtrl GetPageViewMode] == e_trn_fit_width )
        {
            
            BOOL didSmartZoom = [m_pdfViewCtrl SmartZoomX:down.x y:down.y animated:YES];

            if( didSmartZoom == false )
            {
                CGRect inRect = CGRectMake([m_pdfViewCtrl GetHScrollPos]+down.x/2, [m_pdfViewCtrl GetVScrollPos]+down.y/2, m_pdfViewCtrl.bounds.size.width/2, m_pdfViewCtrl.bounds.size.height/2);
                
                
                [m_pdfViewCtrl zoomToRect:inRect animated:YES];
            }
        }
        else
        {
            if( zoomBy > 1.0 )
            {
                
                double outZoom = m_pdfViewCtrl.overlayView.frame.size.width/m_pdfViewCtrl.frame.size.width;
                
                CGRect outRect = CGRectMake([m_pdfViewCtrl GetHScrollPos]-down.x, [m_pdfViewCtrl GetVScrollPos]-down.y, m_pdfViewCtrl.bounds.size.width*outZoom, m_pdfViewCtrl.bounds.size.height*outZoom);
                
                [m_pdfViewCtrl zoomToRect:outRect animated:YES];
                
                
            }
            else
            {

                CGRect inRect = CGRectMake([m_pdfViewCtrl GetHScrollPos]+down.x/2, [m_pdfViewCtrl GetVScrollPos]+down.y/2, m_pdfViewCtrl.bounds.size.width/2, m_pdfViewCtrl.bounds.size.height/2);

                
                [m_pdfViewCtrl zoomToRect:inRect animated:YES];

            }
        }
    }
    return YES;
}


- (BOOL)handleTap:(UITapGestureRecognizer *)sender
{
	
    if (sender.state == UIGestureRecognizerStateEnded)     
    {   

        [m_pdfViewCtrl ClearSelection];
        [m_pdfViewCtrl becomeFirstResponder];
        
        nextToolType = [PanTool class];
        
        return NO;
    }
    
    return YES;
    
}

-(void)onRenderFinished
{
	[self removeAppearanceViews];
}

-(void)removeAppearanceViews
{
	for (__strong UIView* subView in self.superview.subviews) {
        if( [subView isKindOfClass:[ToolView class]] )
        {
            [subView removeFromSuperview];
            subView = 0;
        }
    }
}

-(void)onLayoutChanged
{
    // called in response to changes in the page layout to give the tool a chance to update itself it needs to.
    // see TextSelectTool for an example.
    
    [m_pdfViewCtrl bringSubviewToFront:m_pageNumberLabel];
	
	[self positionPageNumberLabel];
    
    [self removeAppearanceViews];
    
    [m_pdfViewCtrl bringSubviewToFront:self];
}

-(BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view
{
	if( self.backToPanToolAfterUse == YES )
	{
		self.allowScrolling = NO;
	}
	
	else if( event.allTouches.count > 1 && ( self.createsAnnotation || [self.defaultClass createsAnnotation] || (event.allTouches.count > 1 && self.defaultClass && ![self.defaultClass isSubclassOfClass:[PanTool class]])) )
	{
		// more than one touch, scroll the PDF
		m_pdfViewCtrl.zoomEnabled = NO;
		self.allowScrolling = YES;
	}
	else
	{
		// single touch, continue with tool touch handling
		m_pdfViewCtrl.zoomEnabled = YES;
		self.allowScrolling = NO;
	}
	
	return YES;
}

- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    // if this is not here scrollviewer could always will take over
    return self.allowScrolling;
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (BOOL) canPerformAction:(SEL)selector withSender:(id) sender {
    return [self respondsToSelector:selector];
}

-(Tool*)getNewTool
{

    if( nextToolType == 0 )
    {
        nextToolType = [PanTool class];
    }
    
    Tool* newTool = [[nextToolType alloc] initWithPDFViewCtrl:m_pdfViewCtrl];
	
	newTool.annotationAuthor = self.annotationAuthor;
	newTool->m_down = self->m_down;
	newTool.backToPanToolAfterUse = self.backToPanToolAfterUse;
	newTool.defaultClass = self.defaultClass;
	newTool->m_moving_annotation = m_moving_annotation;
	newTool->m_annot_page_number = m_annot_page_number;
	   
    return newTool;
}

-(BOOL)createsAnnotation
{
	// will return whatever the class method returns
	return [[self class] createsAnnotation];
}

+(BOOL)createsAnnotation
{
	return NO;
}

- (void) showSelectionMenu
{
    // do nothing
}

- (void) showSelectionMenu: (CGRect) targetRect animated:(BOOL)animated
{
    UIMenuController *theMenu = [UIMenuController sharedMenuController];

	[self becomeFirstResponder];
	
	if( !CGRectEqualToRect(targetRect, CGRectZero))
	{
		[theMenu setTargetRect:targetRect inView:m_pdfViewCtrl];
	}
	

	[theMenu setMenuVisible:YES animated:animated];
}

- (void) showSelectionMenu: (CGRect) targetRect
{
    [self showSelectionMenu:targetRect animated:YES];
}

-(void)hideMenu
{
    UIMenuController *theMenu = [UIMenuController sharedMenuController];
    [theMenu setMenuVisible:NO animated:NO];
}

// in base class rather than AnnotEditTool in order to allow StickyCreateTool to delete annotation.
-(void)deleteSelectedAnnotation
{
    
    PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
    
    @try
    {
        [m_pdfViewCtrl DocLock:YES];
    
        PTPage* pg = [doc GetPage:m_annot_page_number];
        
        if ([pg IsValid] && [m_moving_annotation IsValid]) {
            [pg AnnotRemoveWithAnnot:m_moving_annotation];
        }
    
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }
	
	@try
    {
		if( [m_moving_annotation IsValid] )
			[m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
		else
			[m_pdfViewCtrl Update:NO];
	}
    @catch (NSException *exception) {
		[m_pdfViewCtrl Update:NO];
	}
	
	[self annotationRemoved:m_moving_annotation onPageNumber:m_annot_page_number];
	
    m_moving_annotation = 0;
    
    [selectionRectContainerView setHidden:YES];
    
    nextToolType = self.defaultClass;

}

#pragma mark - Scroll View Responses

- (void)pdfScrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    [self showSelectionMenu];
}

-(void)pdfScrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self showSelectionMenu];
    [UIView animateWithDuration:0.5f delay:3.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        m_pageNumberLabel.alpha = 0;
    }
    completion:0];
}

-(void)outerScrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self showSelectionMenu];
    [UIView animateWithDuration:0.5f delay:3.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        m_pageNumberLabel.alpha = 0;
    }
                     completion:0];
}

-(void)pdfScrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if( decelerate == false )
    {
        [self showSelectionMenu];
    }
    
    if(!decelerate)
    {
        [UIView animateWithDuration:0.5f delay:3.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            m_pageNumberLabel.alpha = 0;
        }
        completion:0];
    }
}

-(void)pdfScrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self showSelectionMenu];
    [UIView animateWithDuration:0.5f delay:3.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        m_pageNumberLabel.alpha = 0;
    }
    completion:0];
}

- (void)pdfScrollViewDidScroll:(UIScrollView *)scrollView
{
    m_pageNumberLabel.alpha = 0.7;
    
    [UIView animateWithDuration:0.5f delay:3.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        m_pageNumberLabel.alpha = 0;
    }
completion:0];
    
}

- (void)pdfScrollViewDidZoom:(UIScrollView *)scrollView
{

}

- (void)pdfScrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{

}

-(void)outerScrollViewDidScroll:(UIScrollView *)scrollView
{
    if( [m_pdfViewCtrl GetDoc] == Nil )
        return;
    
    m_pageNumberLabel.alpha = 0.7;


    if( m_pageNumberLabel.text == Nil )
    {
        m_pageNumberLabel.text = [NSString stringWithFormat:@"%d/%d", [m_pdfViewCtrl GetCurrentPage], [m_pdfViewCtrl GetPageCount]];
        
		[self positionPageNumberLabel];
    }
    [UIView animateWithDuration:0.5f delay:3.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        m_pageNumberLabel.alpha = 0;
    }
                     completion:0];
}


-(void)pageNumberChangedFrom:(int)oldPageNumber To:(int)newPageNumber
{
    if( [m_pdfViewCtrl GetDoc] == Nil )
        return;
    
    m_pageNumberLabel.text = [NSString stringWithFormat:@"%d/%d", newPageNumber, [m_pdfViewCtrl GetPageCount]];
    
   [self positionPageNumberLabel];
    
    m_pageNumberLabel.alpha = 0.7;
    
    [UIView animateWithDuration:0.5f delay:3.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
            m_pageNumberLabel.alpha = 0;
    }
    completion:0];

}

#pragma mark - Annotation note editing
// used by AnnotEditTool, StickyNoteCreate and DigitalSignatureTool

- (UIViewController *)viewController {
	
	UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
	
	while (topController.presentedViewController) {
		topController = topController.presentedViewController;
	}
	
	return topController;
}

-(void)editSelectedAnnotationNote
{
    
    if( ![m_moving_annotation IsValid] )
        return;
    
    NoteEditController* noteEditController = [[NoteEditController alloc] initWithDelegate:self];
    UINavigationController* noteNavController = [[UINavigationController alloc] initWithRootViewController:noteEditController];
    

    
    PTPDFRect* rect = [m_moving_annotation GetRect];
    

    PTMarkup* annot = [[PTMarkup alloc] initWithAnn:m_moving_annotation];
    
    PTPopup* popup = [annot GetPopup];
    
    if( ![popup IsValid] )
    {
        PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
        
        @try
        {
            [m_pdfViewCtrl DocLock:YES];
        
            [rect Normalize];
            
            PTPDFRect* offsetRect = [[PTPDFRect alloc] initWithX1:[rect GetX2]+30 y1:[rect GetY2]+30 x2:[rect GetX2]+150 y2:[rect GetY2]+150];
            
            popup = [PTPopup Create:(PTSDFDoc*)doc pos:offsetRect];
            
            [popup SetParent:m_moving_annotation];
            
            [annot SetPopup:popup];
            
            PTPage* page = [doc GetPage:m_annot_page_number];
            
            [page AnnotPushBack:popup];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@: %@",exception.name, exception.reason);
        }
        @finally {
            [m_pdfViewCtrl DocUnlock];
        }
    }

	NSString* contents = [m_moving_annotation GetContents];
	
	if( contents == Nil || [contents isEqualToString:@""] )
	{
		contents = [popup GetContents];
	}

	if( contents != Nil )
		noteEditController->tv.text = contents;
	
	noteNavController.modalPresentationStyle = UIModalPresentationPopover;
	[[self viewController] presentViewController:noteNavController animated:YES completion:Nil];
	
	UIPopoverPresentationController *popController = noteNavController.popoverPresentationController;
	popController.permittedArrowDirections = UIPopoverArrowDirectionAny;

    
    PTPDFRect* screen_rect = [m_pdfViewCtrl GetScreenRectForAnnot:m_moving_annotation page_num: m_annot_page_number];
    double x1 = [screen_rect GetX1];
    double x2 = [screen_rect GetX2];
    double y1 = [screen_rect GetY1];
    double y2 = [screen_rect GetY2];
    
    CGRect annotRect = CGRectMake(MIN(x1,x2), MIN(y1, y2), MAX(x1,x2) - MIN(x1,x2), MAX(y1, y2) - MIN(y1, y2));
    
	CGRect annotRectOnScreen = CGRectIntersection(annotRect, m_pdfViewCtrl.bounds);
	
	// use centre of rectangle to guarantee the control will appear on screen
	annotRectOnScreen = CGRectMake(annotRectOnScreen.origin.x+annotRectOnScreen.size.width/2,
								   annotRectOnScreen.origin.y+annotRectOnScreen.size.height/2,
								   1,
								   1);

	popController.sourceRect = annotRectOnScreen;
	popController.sourceView = m_pdfViewCtrl;
	popController.delegate = self;

    [noteEditController->tv becomeFirstResponder];

}

-(void)noteEditCancelButtonPressed:(BOOL)showSelectionMenu
{

	[[self viewController] dismissViewControllerAnimated:YES completion:Nil];

    
    if( showSelectionMenu )
	{
		PTPDFRect* rect = [m_moving_annotation GetRect];
		CGRect annnot_rect = [self PDFRectPage2CGRectScreen:rect PageNumber:m_annot_page_number];
		
		// if not dispatched, will flash without reason
		dispatch_async(dispatch_get_main_queue(), ^() {
			[self showSelectionMenu:annnot_rect];
		});
	}

    // used by stickyNoteCreate so that it goes back to the pan tool when dismissed (rather than staying in sticky create)
    // annotEditTool does nothing in response to this event (implemented in base tool class)
    [m_pdfViewCtrl postCustomEvent:Nil];

}


-(void)saveNewNoteForMovingAnnotationWithString:(NSString*)str
{
    
    PTMarkup* annot;
    
    @try
    {
        [m_pdfViewCtrl DocLock:YES];
    
        annot = [[PTMarkup alloc] initWithAnn:m_moving_annotation];
        
        PTPopup* popup = [annot GetPopup];
        
        [popup SetContents:str];
		
		[m_moving_annotation SetContents:str];
    
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }
    

	[[self viewController] dismissViewControllerAnimated:YES completion:Nil];


	[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];

    
    // used by stickyNoteCreate so that it goes back to the pan tool when dismissed (rather than staying in sticky create)
    // annotEditTool does nothing in response to this event (implemented in base tool class)
    [m_pdfViewCtrl postCustomEvent:Nil];
    
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController;
{

	if( [popoverPresentationController.presentedViewController isKindOfClass:[UINavigationController class]] )
	{
		UINavigationController* vc = (UINavigationController*)popoverPresentationController.presentedViewController;
		NoteEditController* nec = (NoteEditController*)(vc.topViewController);
		UITextView* tv = nec->tv;
		NSString* str = tv.text;
		
		NSString* newContentsOfNote = str;
		
		[self saveNewNoteForMovingAnnotationWithString:newContentsOfNote];
		
	}
	// else this is a colour swatch dismissal
	return YES;
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
	// if not dispatched, will disappear without reason
	dispatch_async(dispatch_get_main_queue(), ^() {
		if( m_moving_annotation )
		{
			PTPDFRect* rect = [m_moving_annotation GetRect];
			CGRect rectOnScreen = [self PDFRectPage2CGRectScreen:rect PageNumber:m_annot_page_number];
			[self showSelectionMenu:rectOnScreen];
		}
	});
}


#pragma mark - Helpers

//--------- text search/selection and annotations -------------//

-(void)ConvertScreenPtToPagePtX:(CGFloat*)x Y:(CGFloat*)y PageNumber:(int)pageNumber
{
    [m_screenPt setX:*x];
    [m_screenPt setY:*y];
    
    
    m_pagePt = [m_pdfViewCtrl ConvScreenPtToPagePt:m_screenPt page_num:pageNumber];
    
    *x = (float)[m_pagePt getX];
    *y = (float)[m_pagePt getY];
}

-(void)ConvertPagePtToScreenPtX:(CGFloat*)x Y:(CGFloat*)y PageNumber:(int)pageNumber
{

    [m_pagePt setX:*x];
    [m_pagePt setY:*y];
    
    
    m_screenPt = [m_pdfViewCtrl ConvPagePtToScreenPt:m_pagePt page_num:pageNumber];
    
    *x = (float)[m_screenPt getX];
    *y = (float)[m_screenPt getY];
}

-(CGRect)PDFRectScreen2CGRectScreen:(PTPDFRect*)screenRect PageNumber:(int)pageNumber
{
	double x1 = [screenRect GetX1];
	double x2 = [screenRect GetX2];
	double y1 = [screenRect GetY1];
	double y2 = [screenRect GetY2];
	
	CGRect cgScreenRect = CGRectMake(MIN(x1,x2), MIN(y1, y2), MAX(x1,x2) - MIN(x1,x2), MAX(y1, y2) - MIN(y1, y2));
	
	return cgScreenRect;
	
}

-(CGRect)PDFRectPage2CGRectScreen:(PTPDFRect*)r PageNumber:(int)pageNumber
{
    PTPDFPoint* pagePtA = [[PTPDFPoint alloc] init];
    PTPDFPoint* pagePtB = [[PTPDFPoint alloc] init];
    
    [pagePtA setX:[r GetX1]];
    [pagePtA setY:[r GetY2]];
    
    [pagePtB setX:[r GetX2]];
    [pagePtB setY:[r GetY1]];
    
    CGFloat paX = [pagePtA getX];
    CGFloat paY = [pagePtA getY];
    
    CGFloat pbX = [pagePtB getX];
    CGFloat pbY = [pagePtB getY];
    
    [self ConvertPagePtToScreenPtX:&paX Y:&paY PageNumber:pageNumber];
    [self ConvertPagePtToScreenPtX:&pbX Y:&pbY PageNumber:pageNumber];
    
    
    float x, y, width, height;
    x = MIN(paX, pbX);
    y = MIN(paY, pbY);
    width = MAX(paX, pbX)-x;
    height = MAX(paY, pbY)-y;
    
    return CGRectMake(x, y, width, height);
}

-(PTPDFRect*)CGRectScreen2PDFRectPage:(CGRect)cgRect PageNumber:(int)pageNumber
{
	CGPoint topLeft = cgRect.origin;
	CGPoint bottomRight = CGPointMake(CGRectGetMaxX(cgRect), CGRectGetMaxY(cgRect));
	
	[self ConvertScreenPtToPagePtX:&topLeft.x Y:&topLeft.y PageNumber:pageNumber];
	[self ConvertScreenPtToPagePtX:&bottomRight.x Y:&bottomRight.y PageNumber:pageNumber];
	
	PTPDFRect* ptRect = [[PTPDFRect alloc] initWithX1:topLeft.x y1:topLeft.y x2:bottomRight.x y2:bottomRight.y];
	
	[ptRect Normalize];
	
	return ptRect;
}

-(PTPDFRect*)GetRectUnion:(PTPDFRect*)rect1 Rect2:(PTPDFRect*)rect2
{
    PTPDFRect* rectUnion = [[PTPDFRect alloc] init];
    
    [rectUnion setX1:MIN([rect1 GetX1], [rect2 GetX1])];
    [rectUnion setY1:MIN([rect1 GetY1], [rect2 GetY1])];
    
    [rectUnion setX2:MAX([rect1 GetX2], [rect2 GetX2])];
    [rectUnion setY2:MAX([rect1 GetY2], [rect2 GetY2])];
    
    return rectUnion;
}

- (void)annotationAdded:(PTAnnot*)annotation onPageNumber:(unsigned long)pageNumber
{
	if( annotation && pageNumber && [m_pdfViewCtrl.toolDelegate respondsToSelector:@selector(annotationAdded:onPageNumber:) ] )
	{
		[m_pdfViewCtrl.toolDelegate annotationAdded:annotation onPageNumber:pageNumber];
	}
}

- (void)annotationModified:(PTAnnot*)annotation onPageNumber:(unsigned long)pageNumber
{
	if( annotation && pageNumber && [m_pdfViewCtrl.toolDelegate respondsToSelector:@selector(annotationModified:onPageNumber:) ] )
	{
		[m_pdfViewCtrl.toolDelegate annotationModified:annotation onPageNumber:pageNumber];
	}
}

- (void)annotationRemoved:(PTAnnot*)annotation onPageNumber:(unsigned long)pageNumber
{
	if( annotation && pageNumber && [m_pdfViewCtrl.toolDelegate respondsToSelector:@selector(annotationRemoved:onPageNumber:) ] )
	{
		[m_pdfViewCtrl.toolDelegate annotationRemoved:annotation onPageNumber:pageNumber];
	}
}

-(void)swapA:(CGFloat*)a B:(CGFloat*)b
{
	CGFloat tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}
-(void)javascriptCallback:(const char*)event_type json:(const char*)json
{
    NSString* type = @(event_type);
    NSString* message = @(json);
    if([type isEqualToString:@"alert"])
    {
		NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
		
        NSData* jsonData = [message dataUsingEncoding:NSUTF8StringEncoding];
        NSError* e;
        NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&e];
        NSString* alert_message = dict[@"cMsg"];
		NSString* title = dict[@"cTitle"] ? dict[@"cTitle"] : NSLocalizedStringFromTableInBundle(@"JavaScript Alert", @"PDFNet-Tools", stringBundle, @"JavaScript Alert.");
		
		UIAlertController *alertController = [UIAlertController
											  alertControllerWithTitle:title
											  message:alert_message
											  preferredStyle:UIAlertControllerStyleAlert];
		
		UIAlertAction *okAction = [UIAlertAction
								   actionWithTitle:NSLocalizedStringFromTableInBundle(@"OK", @"PDFNet-Tools", stringBundle, @"")
								   style:UIAlertActionStyleDefault
								   handler:Nil];
		
		
		[alertController addAction:okAction];
		
		[self.viewController presentViewController:alertController animated:YES completion:nil];
    }
}

@end
