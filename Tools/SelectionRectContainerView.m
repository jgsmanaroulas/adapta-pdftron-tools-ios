//
//  Copyright 2011-14 PDFTron Systems Inc All rights reserved.
//

// NOTE: This file should be compiled without optimizations or the selection rectangle will not
//       be visible on devices with an A5. In Build Phases, add -O0 in this file's compiler flags.

#import "SelectionRectContainerView.h"
#import "ResizeWidgetView.h"


@implementation SelectionRectContainerView

@synthesize isLineEdit;


- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)pdfViewCtrl forAnnot:(PTAnnot*)annot
{
	CGRect frame = CGRectZero;
    const int length = [ResizeWidgetView getLength];
    frame.origin.x -= length/2;
    frame.origin.y -= length/2;
    frame.size.width += length;
    frame.size.height += length;
	
	if( length == 0 )
	{
		assert(false);
		return Nil;
	}
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        isLineEdit = false;
        
        frame.origin.x = length/2;
        frame.origin.y = length/2;
        frame.size.width -= length;
        frame.size.height -= length;

        selectionRectView = [[SelectionRectView alloc] initWithFrame:frame forAnnot:(PTAnnot*)annot];
        
        UIColor* color = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
       
        self.backgroundColor = color;
        
        [self addSubview:selectionRectView];
        
        self.m_pdfViewCtrl = pdfViewCtrl;

        selectionRectView.m_pdfViewCtrl = self.m_pdfViewCtrl;

        rwvNW = [[ResizeWidgetView alloc] initAtPoint:CGPointMake(0, 0) WithLocation:e_topleft];
        rwvN = [[ResizeWidgetView alloc] initAtPoint:CGPointMake(self.frame.size.width/2-length/2, 0) WithLocation:e_top];
        rwvNE = [[ResizeWidgetView alloc] initAtPoint:CGPointMake(self.frame.size.width-length, 0) WithLocation:e_topright];
        rwvE = [[ResizeWidgetView alloc] initAtPoint:CGPointMake(self.frame.size.width-length, self.frame.size.height/2-length/2) WithLocation:e_right];
        rwvSE = [[ResizeWidgetView alloc] initAtPoint:CGPointMake(self.frame.size.width-length, self.frame.size.height-length) WithLocation:e_bottomright];
        rwvS = [[ResizeWidgetView alloc] initAtPoint:CGPointMake(self.frame.size.width/2-length/2, self.frame.size.height-length) WithLocation:e_bottom];
        rwvSW = [[ResizeWidgetView alloc] initAtPoint:CGPointMake(0, self.frame.size.height-length) WithLocation:e_bottomleft];
        rwvW = [[ResizeWidgetView alloc] initAtPoint:CGPointMake(0, self.frame.size.height/2-length/2) WithLocation:e_left];
        
        [self addSubview:rwvNW];
        [self addSubview:rwvN];
        [self addSubview:rwvNE];
        [self addSubview:rwvE];
        [self addSubview:rwvSE];
        [self addSubview:rwvS];
        [self addSubview:rwvSW];
        [self addSubview:rwvW];
        
        
    }
    return self;
}

-(void)hideResizeWidgetViews
{
    [rwvNW setHidden:YES];
    [rwvN setHidden:YES];
    [rwvNE setHidden:YES];
    [rwvE setHidden:YES];
    [rwvSE setHidden:YES];
    [rwvS setHidden:YES];
    [rwvSW setHidden:YES];
    [rwvW setHidden:YES];
}

-(void)setAnnot:(PTAnnot*)annot
{
    [selectionRectView setAnnot:annot];
}

-(void)showLine
{
    if( selectionRectView.hidden == YES)
    {
        [selectionRectView setHidden:NO];
        selectionRectView.alpha = 1;
        [selectionRectView setNeedsDisplay];
    }
}

-(void)hideLine
{
    if( selectionRectView.hidden == NO)
    {
		// ensure we don't get a stale view
		// flash on screen next time
		{
			PTAnnot* theAnnot = selectionRectView->m_annot;
			CGRect theFrame = selectionRectView.frame;
			BOOL startAtNE = selectionRectView->m_startAtNE;
			[selectionRectView removeFromSuperview];
			selectionRectView = [[SelectionRectView alloc] initWithFrame:theFrame forAnnot:theAnnot];
			selectionRectView->m_isLineAnnot = YES;
			selectionRectView.backgroundColor = [UIColor clearColor];
			selectionRectView->m_startAtNE = startAtNE;
			selectionRectView.m_pdfViewCtrl = self.m_pdfViewCtrl;
			[self insertSubview:selectionRectView atIndex:0];
		}
		
        [selectionRectView setHidden:YES];
        selectionRectView.alpha = 0;
        [selectionRectView setNeedsDisplay];
    }
}

// for lines and arrows, show only the NE and SW
// resize dots
-(void)showNESWWidgetViews
{
    
    if( rwvNW == Nil )
    {
        return;
    }
    
    [selectionRectView setEditLine:NO];
    [self hideResizeWidgetViews];
    [rwvNE setHidden:NO];
    [rwvSW setHidden:NO];
    //[selectionRectView setHidden:YES];
}

// for lines and arrows, show only the NW and SE
// resize dots
-(void)showNWSEWidgetViews
{

    if( rwvNW == Nil )
    {
        return;
    }
    [selectionRectView setEditLine:YES];
    [self hideResizeWidgetViews];
    [rwvNW setHidden:NO];
    [rwvSE setHidden:NO];
}


-(void)showResizeWidgetViews
{
    if( rwvNW == Nil )
        return;
    
    [rwvNW setHidden:NO];
    [rwvW setHidden:NO];
    [rwvNE setHidden:NO];
    [rwvE setHidden:NO];
    [rwvSE setHidden:NO];
    [rwvS setHidden:NO];
    [rwvSW setHidden:NO];
    [rwvW setHidden:NO];
}

-(void)setAnnotationContents:(PTAnnot*)annot
{
    if( tv != Nil)
    {
        [annot SetContents:tv.text];
        [annot RefreshAppearance];
        [tv resignFirstResponder];
    }
}

-(void)setEditTextSizeForZoom:(double)zoom forFontSize:(int)size
{
    tv.font = [UIFont fontWithName:@"Helvetica" size:size*zoom];
}

-(void)useTextViewWithText:(NSString*)text withAlignment:(int)alignment atZoom:(double)zoom forFontSize:(int)size withDelegate:(id<UITextViewDelegate>)delegateView
{

    tv = [[UITextView alloc] initWithFrame:selectionRectView.frame];
    
    tv.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    
    tv.backgroundColor = [UIColor whiteColor];
    
    tv.textColor = [UIColor redColor];
    tv.font = [UIFont fontWithName:@"Helvetica" size:size*zoom];
    
    tv.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    tv.text = text;
    
    if(alignment == 1 )
        tv.textAlignment = NSTextAlignmentCenter;
    else if(alignment == 2)
        tv.textAlignment = NSTextAlignmentRight;
    else
        tv.textAlignment = NSTextAlignmentLeft;
    
    [self insertSubview:tv atIndex:1];
    
    [tv becomeFirstResponder];
    
    tv.delegate = delegateView;
    
}

-(void)setLineStartPoint:(CGPoint)sPoint EndPoint:(CGPoint)ePoint
{
    startPoint = sPoint;
    endPoint = ePoint;;
}

-(void)setFrame:(CGRect)frame
{
    const int length = [ResizeWidgetView getLength];
    
    // could avoid calculations by setting up struts and springs?
    frame.origin.x -= length/2;
    frame.origin.y -= length/2;
    frame.size.width += length;
    frame.size.height += length;
    
    super.frame = frame;
    
    frame.origin.x = length/2;
    frame.origin.y = length/2;
    frame.size.width -= length;
    frame.size.height -= length;

    
    rwvNW.frame = CGRectMake(0, 0, length, length);
    rwvN.frame = CGRectMake(self.frame.size.width/2-length/2, 0, length, length);
    rwvNE.frame = CGRectMake(self.frame.size.width-length, 0, length, length);
    rwvE.frame = CGRectMake(self.frame.size.width-length, self.frame.size.height/2-length/2, length, length);
    rwvSE.frame = CGRectMake(self.frame.size.width-length, self.frame.size.height-length, length, length);
    rwvS.frame = CGRectMake(self.frame.size.width/2-length/2, self.frame.size.height-length, length, length);
    rwvSW.frame = CGRectMake(0, self.frame.size.height-length, length, length);
    rwvW.frame = CGRectMake(0, self.frame.size.height/2-length/2, length, length);

    selectionRectView.frame = frame;
    

}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect
//{
//    // Drawing code
//}



@end
