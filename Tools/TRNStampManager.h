//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@class PTPage;
@class PTPDFDoc;

@interface TRNStampManager : NSObject

-(BOOL)HasDefaultSignature;
-(PTPDFDoc*)GetDefaultSignature;
-(void)DeleteDefaultSignatureFile;
-(PTPDFDoc*)CreateSignature:(NSMutableArray*)points withStrokeColor:(UIColor*)strokeColor withStrokeThickness:(CGFloat)thickness withinRect:(CGRect)rect makeDefault:(BOOL)asDefault;


@end
