//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "Tool.h"
#import "UIKit/UIKit.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@interface RichMediaTool : Tool

@property (nonatomic, strong) AVPlayerViewController* moviePlayer;
@property (nonatomic, copy) NSString* moviePath;

@end
