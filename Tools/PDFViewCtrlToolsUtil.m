//
//  Util.m
//  Tools
//
//  Created by PDFTron on 2015-03-17.
//
//

#import "PDFViewCtrlToolsUtil.h"


@implementation PDFViewCtrlToolsUtil

static NSBundle *_stringBundle;
static NSBundle *_toolsBundle;

+ (UIImage*)toolImageNamed:(NSString*)name
{
	NSArray<NSString*>* imageExtensions = @[@"png", @"jpg", @"jpeg", @"tif", @"tiff", @"gif", @"bmp", @"BMPf", @"ico", @"cur", @"xbm"];
	
	NSString* nameNoExt = [name stringByDeletingPathExtension];
	
	for (NSString* ext in imageExtensions) {
		
		
		NSString* path = [[PDFViewCtrlToolsUtil toolsBundle] pathForResource:[@"Images" stringByAppendingPathComponent:nameNoExt] ofType:ext];
		
		if( path )
		{
			UIImage* img = [UIImage imageWithContentsOfFile:path];
			
			if( img )
				return img;
		}

	}

	return Nil;
}

+ (NSBundle*)toolsBundle
{
	if( !_toolsBundle )
	{
		_toolsBundle = [NSBundle bundleForClass:[self class]];
	}
	
	return _toolsBundle;
}


+ (NSBundle*)toolsStringBundle
{
	if( !_stringBundle )
	{
		NSString* stringBundlePath = [[NSBundle bundleForClass:[self class]] pathForResource:@"tools-strings" ofType:@"bundle"];

		_stringBundle = [NSBundle bundleWithPath:stringBundlePath];
	}

	return _stringBundle;
}

@end
