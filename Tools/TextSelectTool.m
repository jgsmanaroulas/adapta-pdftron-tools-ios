//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "TextSelectTool.h"
#import "SelectionBar.h"
#import "PanTool.h"
#import "AnalyticsHandlerAdapter.h"
#import "ColorDefaults.h"
#import "TextMarkupEditTool.h"

#import "PDFViewCtrlToolsUtil.h"

@implementation TextSelectTool


- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        selectionLayers = [[NSMutableArray alloc] init];
        m_pdfViewCtrl = in_pdfViewCtrl;
    }
    
    return self;
}

-(void)removeAppearance
{
	[self ClearSelectionBars];
    [self ClearSelectionOnly];
    if( loupe.superview != Nil )
        [loupe removeFromSuperview];
}

- (void)dealloc
{
	[self removeAppearance];
}

-(void)willMoveToSuperview:(UIView *)newSuperview
{
	if( newSuperview == Nil)
		[self removeAppearance];
	
	[super willMoveToSuperview:newSuperview];
}

- (void) attachInitialMenuItems
{
    NSMutableArray* menuItems = [[NSMutableArray alloc] initWithCapacity:2];
    
    UIMenuItem* menuItem;
	
	NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
//    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Copy", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(textCopy)];
//    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Highlight", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(textHighlight)];
    [menuItems addObject:menuItem];
	menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Strikeout", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(textStrikeout)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Underline", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(textUnderline)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Squiggly", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(textSquiggly)];
    [menuItems addObject:menuItem];
//    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Define", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(defineTerm)];
//    [menuItems addObject:menuItem];
    
    
    UIMenuController *theMenu = [UIMenuController sharedMenuController];
    theMenu.menuItems = menuItems;
    
}

-(void)defineTerm
{
	NSString* term = [self GetSelectedTextFromPage:selectionStartPage ToPage:selectionEndPage];
	term = [term stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];
	UIReferenceLibraryViewController* reference = [[UIReferenceLibraryViewController alloc] initWithTerm:term];
	reference.modalPresentationStyle = UIModalPresentationPopover;
	
	[[self viewController] presentViewController:reference animated:YES completion:Nil];
	
	UIPopoverPresentationController *popController = reference.popoverPresentationController;
	popController.permittedArrowDirections = (UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown);
	CGRect presentationRect = CGRectMake(selectionStartCorner.x, selectionStartCorner.y-15, selectionEndCorner.x-selectionStartCorner.x, selectionEndCorner.y-selectionStartCorner.y+30);
	
	popController.sourceRect = presentationRect;
	popController.sourceView = self;

}

-(void)textCopy
{
    // copy string to the pasteboard
    [self CopySelectedTextToPasteboard];
    
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] Copy selected"];
}

- (BOOL)onCustomEvent:(id)userData
{
    return NO;
}

-(void)textHighlight
{
    [self createTextMarkupAnnot:e_ptHighlight];
	
	nextToolType = [TextMarkupEditTool class];
	[m_pdfViewCtrl postCustomEvent:Nil];
    
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] Highlight selected"];
}

-(void)textSquiggly
{
    [self createTextMarkupAnnot:e_ptSquiggly];
	
	nextToolType = [TextMarkupEditTool class];
	[m_pdfViewCtrl postCustomEvent:Nil];
    
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] Squiggly selected"];
}

-(void)textUnderline
{
    [self createTextMarkupAnnot:e_ptUnderline];
	
	nextToolType = [TextMarkupEditTool class];
	[m_pdfViewCtrl postCustomEvent:Nil];
    
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] Underline selected"];
}

-(void)textStrikeout
{
    [self createTextMarkupAnnot:e_ptStrikeOut];
	
	nextToolType = [TextMarkupEditTool class];
	[m_pdfViewCtrl postCustomEvent:Nil];
    
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] Strikeout selected"];
}

-(BOOL)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{

    CGPoint down = [gestureRecognizer locationInView:m_pdfViewCtrl];

    [self longPressTextSelectAt: down WithRecognizer: gestureRecognizer];
    
    return YES;
}

- (BOOL)handleTap:(UITapGestureRecognizer *)sender
{
    
    if (sender.state == UIGestureRecognizerStateEnded && tapOK)     
    {   
        [self ClearSelectionBars];
        [self ClearSelectionOnly];
        [m_pdfViewCtrl becomeFirstResponder]; // (hides keyboard)
        
        nextToolType = [PanTool class];
        
        return NO;
    }
    
    return YES;
    
}


- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    tapOK = YES;

    UITouch *touch = touches.allObjects[0];
    
    if( [touch.view isKindOfClass:[SelectionBar class]] )
    {

        m_moving_selection_bar = (SelectionBar*)touch.view;
        
        if( selectionOnScreen ) // there is currently selected text
        {
            NSMutableArray* selection = [self MakeSelection];
            
            CGRect firstQuad = [selection[0] CGRectValue];
			
			int ltrOffsetFirst = 0;
			if( [m_pdfViewCtrl GetRightToLeftLanguage] == YES)
			{
				ltrOffsetFirst = firstQuad.size.width;
			}
			
            selectionStartCorner = CGPointMake(firstQuad.origin.x + ltrOffsetFirst +[m_pdfViewCtrl GetHScrollPos], firstQuad.origin.y + [m_pdfViewCtrl GetVScrollPos]+firstQuad.size.height/2);
            
            CGRect lastQuad = [selection[selection.count-1] CGRectValue];
			
			int ltrOffsetLast = 0;
			
			if( [m_pdfViewCtrl GetRightToLeftLanguage] == YES)
			{
				ltrOffsetLast = lastQuad.size.width;
			}
			
            selectionEndCorner = CGPointMake(lastQuad.origin.x + lastQuad.size.width - ltrOffsetLast + [m_pdfViewCtrl GetHScrollPos], lastQuad.origin.y + lastQuad.size.height/2 + [m_pdfViewCtrl GetVScrollPos]);
        }
        
    }
    else
    {

        m_moving_selection_bar = 0;
    }
    
    
    if( nextToolType )
        return NO;

    return YES;
}

- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    tapOK = NO;
	
	UITouch *touch = touches.allObjects[0];
    CGPoint down = [touch locationInView:m_pdfViewCtrl];
	
	// make sure we don't try to select off the page.
	if( [m_pdfViewCtrl GetPageNumberFromScreenPt:down.x y:down.y] <= 0 )
	{
		[loupe removeFromSuperview];
		return YES;
	}
	
    if( m_moving_selection_bar )
        [self selectionBarMoved:m_moving_selection_bar withTouches:touches withEvent:event];
    return YES;
}

- (BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    tapOK = YES;
    
	
	UITouch *touch = touches.allObjects[0];
    CGPoint down = [touch locationInView:m_pdfViewCtrl];
	
    if( m_moving_selection_bar && [m_pdfViewCtrl GetPageNumberFromScreenPt:down.x y:down.y] > 0 )
        [self selectionBarUp:m_moving_selection_bar withTouches:touches withEvent:event];
    
    [m_pdfViewCtrl RequestRendering];
    
    return YES;
}

- (BOOL)onTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    return [self onTouchesEnded:touches withEvent:event];
}

-(void)showSelectionMenu
{
    [self ShowMenuController];
}

- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    if ( [view isKindOfClass:[SelectionBar class]])
    {
        // if this is not done, the scroll view takes over if it looks like a scroll action
        return NO;
    }
    else
        return YES;
}

-(CGPoint)GetFirstDotPoint:(NSMutableArray*)selection
{
    CGRect firstSelectionRect = [selection[0] CGRectValue];
	
	PTRotate currentRotation = [m_pdfViewCtrl GetRotation];
	
	int rtlOffset = ([m_pdfViewCtrl GetRightToLeftLanguage] == YES) ? firstSelectionRect.size.width : 0;

	if( currentRotation == e_pt0 || currentRotation == e_pt180 )
		return CGPointMake(firstSelectionRect.origin.x+[m_pdfViewCtrl GetHScrollPos]-2+rtlOffset, firstSelectionRect.origin.y+[m_pdfViewCtrl GetVScrollPos]);
	else
		return CGPointMake(firstSelectionRect.origin.x+rtlOffset+[m_pdfViewCtrl GetHScrollPos], firstSelectionRect.origin.y+[m_pdfViewCtrl GetVScrollPos]+7);

}

-(CGPoint)GetSecondDotPoint:(NSMutableArray*)selection
{
    CGRect lastSelectionRect = [selection[selection.count-1] CGRectValue];

	PTRotate currentRotation = [m_pdfViewCtrl GetRotation];
	

	if( [m_pdfViewCtrl GetRightToLeftLanguage] == YES )
	{
		if( currentRotation == e_pt0 || currentRotation == e_pt180 )
			return CGPointMake(lastSelectionRect.origin.x+[m_pdfViewCtrl GetHScrollPos], lastSelectionRect.origin.y+[m_pdfViewCtrl GetVScrollPos]+lastSelectionRect.size.height);
		else
			return CGPointMake(lastSelectionRect.origin.x+[m_pdfViewCtrl GetHScrollPos]+lastSelectionRect.size.width, lastSelectionRect.origin.y+[m_pdfViewCtrl GetVScrollPos]+lastSelectionRect.size.height-5);

	}
	else
	{
		if( currentRotation == e_pt0 || currentRotation == e_pt180 )
			return CGPointMake(lastSelectionRect.origin.x+[m_pdfViewCtrl GetHScrollPos]+lastSelectionRect.size.width, lastSelectionRect.origin.y+[m_pdfViewCtrl GetVScrollPos]+lastSelectionRect.size.height);
		else
			return CGPointMake(lastSelectionRect.origin.x+[m_pdfViewCtrl GetHScrollPos], lastSelectionRect.origin.y+[m_pdfViewCtrl GetVScrollPos]+lastSelectionRect.size.height-5);

		

	}
}

- (void) DrawSelectionBars:(NSMutableArray*)selection {
	// draws dots
	
    const int padding = 14;
    const int vOffset = 14;
    const int hOffset = -6;
    
    if( selection.count == 0 )
        return;
    
    CGPoint dotPoint = [self GetFirstDotPoint:selection];
    
	if( leftBar.superview )
		[leftBar removeFromSuperview];
	
    leftBar = [[SelectionBar alloc] initWithFrame:CGRectMake(dotPoint.x-padding+hOffset, dotPoint.y-padding-vOffset, 13+padding*2, 14+padding*2)];
    [leftBar setIsLeft:YES];
    
    [m_pdfViewCtrl->ContainerView addSubview:leftBar];
    
    dotPoint = [self GetSecondDotPoint:selection];
	
	if( rightBar.superview )
		[rightBar removeFromSuperview];
	
    rightBar = [[SelectionBar alloc] initWithFrame:CGRectMake(dotPoint.x-padding+hOffset, dotPoint.y-padding, 13+padding*2, 14+padding*2)];
    [rightBar setIsLeft:NO];
    [m_pdfViewCtrl->ContainerView addSubview:rightBar];
    
}

- (void) longPressTextSelectAt: (CGPoint) down WithRecognizer: (UILongPressGestureRecognizer *) gestureRecognizer
{
    [self ClearSelectionBars];

    double screenScale = 1;
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] == YES ) 
        screenScale = [UIScreen mainScreen].scale ;
    
    [m_pdfViewCtrl SetTextSelectionMode:e_ptrectangular];
    [m_pdfViewCtrl SelectX1:down.x Y1:down.y X2:1/screenScale/2+down.x Y2:1/screenScale/2+down.y];  
    [m_pdfViewCtrl SetTextSelectionMode:e_ptstructural];
    
    NSMutableArray* selections = [self GetQuadsFromPage:-1 ToPage:-1];
    
    if (selections == 0 || selections.count == 0)
    {
        [self ClearSelectionOnly];
        [self ClearSelectionBars];
        
        // just show loupe
        if ( gestureRecognizer.state != UIGestureRecognizerStateEnded && gestureRecognizer.state != UIGestureRecognizerStateCancelled)
        {
            if(loupe == nil)
            {
                loupe = [[TrnMagnifierView alloc] init];
                loupe.viewToMagnify = m_pdfViewCtrl;
            }
            
            [m_pdfViewCtrl.superview addSubview:loupe];
            
            if( [m_pdfViewCtrl GetPageNumberFromScreenPt:down.x y:down.y] > 0 )
            {
                down.y += m_pdfViewCtrl.frame.origin.y;
                down.x += m_pdfViewCtrl.frame.origin.x;
                
                [loupe setMagnifyPoint:[gestureRecognizer locationInView:m_pdfViewCtrl] TouchPoint:down];
                [loupe setNeedsDisplay];
            }
            else
            {
                [loupe removeFromSuperview];
            }
        }
        else
        {
            [loupe removeFromSuperview];
        }
        
        nextToolType = [PanTool class];
        
        return;
    }

    if (gestureRecognizer.state == UIGestureRecognizerStateEnded || gestureRecognizer.state == UIGestureRecognizerStateCancelled)
    {
        // executed on touch up

		CGRect selection = [selections[0] CGRectValue];
		
		//in case two bounding boxes overlap.
		CGRect lastSelection = [selections[selections.count-1] CGRectValue];
		
		selectionEnd.x = selection.origin.x + selection.size.width-1;
		selectionEnd.y = selection.origin.y + selection.size.height-1;
		
		selectionStart = selection.origin;
		
		//ensures it is on the page
		selectionStart.x++;
		selectionStart.y++;
		
		selectionStartPage = [m_pdfViewCtrl GetPageNumberFromScreenPt:selectionStart.x y:selectionStart.y];
		selectionEndPage = [m_pdfViewCtrl GetPageNumberFromScreenPt:selectionEnd.x y:selectionEnd.y];
		
		[self ConvertScreenPtToPagePtX:&selectionStart.x Y:&selectionStart.y PageNumber:selectionStartPage];
		
		[self ConvertScreenPtToPagePtX:&selectionEnd.x Y:&selectionEnd.y PageNumber:selectionEndPage];
        
        [self DrawSelectionQuads:selections WithLines:YES];
        
        NSMutableArray* selectionArray = [[NSMutableArray alloc] init];
        
        [selectionArray addObject:[NSValue valueWithCGRect:selection]];
        
        [self DrawSelectionBars:selectionArray];
        
        
        selectionStartCorner.x = selection.origin.x + [m_pdfViewCtrl GetHScrollPos];
        selectionStartCorner.y = selection.origin.y + [m_pdfViewCtrl GetVScrollPos];
        
        selectionEndCorner = CGPointMake(lastSelection.origin.x+lastSelection.size.width + [m_pdfViewCtrl GetHScrollPos], lastSelection.origin.y+lastSelection.size.height/2+ [m_pdfViewCtrl GetVScrollPos]);
        
        [loupe removeFromSuperview];

        
        [self ShowMenuController];
    }
    else
    {
        // executed on drag, when word selected
        if(loupe == nil){
            loupe = [[TrnMagnifierView alloc] init];
            loupe.viewToMagnify = m_pdfViewCtrl;
        }
        
        [self DrawSelectionQuads:selections WithLines:NO];
        
        [m_pdfViewCtrl.superview addSubview:loupe];
        
        CGPoint magnifyPoint = [gestureRecognizer locationInView:m_pdfViewCtrl];
        
        down.y += m_pdfViewCtrl.frame.origin.y;
        down.x += m_pdfViewCtrl.frame.origin.x;
        
        [loupe setMagnifyPoint:magnifyPoint TouchPoint:down];
        [loupe setNeedsDisplay];

        
    }
    
}

-(void)ClearSelectionBars
{
    
    if( leftBar != 0 )
    {
        [leftBar removeFromSuperview];
        leftBar = 0;
    }
    
    if( rightBar != 0 )
    {
        [rightBar removeFromSuperview];
        rightBar = 0;
    }
    
    [m_pdfViewCtrl ClearSelection];
}

-(void)ClearSelectionOnly
{
	assert(selectionLayers);
    [selectionLayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    [selectionLayers removeAllObjects];
}

-(void)DrawSelectionQuads:(NSMutableArray*)quads WithLines:(BOOL)lines
{
    int drawnQuads = 0;
    
    [self ClearSelectionOnly];
	
	PTRotate currentRotation = [self->m_pdfViewCtrl GetRotation];
    
    for (NSValue* quad in quads) 
    {
        CALayer* selectionLayer = [[CALayer alloc] init];
        
        CGRect selectionRect = quad.CGRectValue;
        
        selectionLayer.frame = CGRectMake(selectionRect.origin.x+[m_pdfViewCtrl GetHScrollPos], selectionRect.origin.y+[m_pdfViewCtrl GetVScrollPos], selectionRect.size.width, selectionRect.size.height);
		
        UIColor* blueish = [UIColor colorWithRed:0.1 green:0.2 blue:0.9 alpha:0.20];
        
        CGColorRef cgBlueish = blueish.CGColor;
        
        selectionLayer.backgroundColor = cgBlueish;
        
        [m_pdfViewCtrl->ContainerView.layer addSublayer:selectionLayer];
        
        [selectionLayers addObject:selectionLayer];
        
        
        // lines that form selection bars
        if( lines == YES )
        {
			int rtlOffset = 0;
			if( [m_pdfViewCtrl GetRightToLeftLanguage] == YES )
				rtlOffset = selectionRect.size.width;
			
            selectionOnScreen = true;
            // start line
            if( drawnQuads == 0 )
            {
                CALayer* lineLayer = [[CALayer alloc] init];
				


				if( currentRotation == e_pt0 || currentRotation == e_pt180 )
					lineLayer.frame = CGRectMake(selectionRect.origin.x+[m_pdfViewCtrl GetHScrollPos]-2+rtlOffset, selectionRect.origin.y+[m_pdfViewCtrl GetVScrollPos]-5, 2, selectionRect.size.height+5);
                else
					lineLayer.frame = CGRectMake(selectionRect.origin.x+[m_pdfViewCtrl GetHScrollPos]-2+rtlOffset, selectionRect.origin.y+[m_pdfViewCtrl GetVScrollPos], selectionRect.size.width+5, 2);
                
                CGColorRef cgBlue = [UIColor colorWithRed:0.4 green:0.4 blue:1.0 alpha:1.0].CGColor;
                lineLayer.backgroundColor = cgBlue;
                
                [m_pdfViewCtrl->ContainerView.layer addSublayer:lineLayer];
                
                [selectionLayers addObject:lineLayer];
                
            }
            
            // end line
            if(drawnQuads == quads.count-1)
            {
                CALayer* lineLayer = [[CALayer alloc] init];
				

				if( currentRotation == e_pt0 || currentRotation == e_pt180 )
					lineLayer.frame = CGRectMake(selectionRect.origin.x+[m_pdfViewCtrl GetHScrollPos]+selectionRect.size.width-rtlOffset, selectionRect.origin.y+[m_pdfViewCtrl GetVScrollPos], 2, selectionRect.size.height+5);
                else
					lineLayer.frame = CGRectMake(selectionRect.origin.x+[m_pdfViewCtrl GetHScrollPos]-2-rtlOffset, selectionRect.origin.y+[m_pdfViewCtrl GetVScrollPos]+selectionRect.size.height, selectionRect.size.width+5, 2);
                
                CGColorRef cgBlue = [UIColor colorWithRed:0.4 green:0.4 blue:1.0 alpha:1.0].CGColor;
                lineLayer.backgroundColor = cgBlue;
                
                [m_pdfViewCtrl->ContainerView.layer addSublayer:lineLayer];
                
                [selectionLayers addObject:lineLayer];
                
            }
        }
        else
            selectionOnScreen = false;
        drawnQuads++;
    }
    
}

- (NSMutableArray *)MakeSelection
{   
    NSMutableArray* selection;
    if( selectionStartPage <= selectionEndPage )
    {
        [m_pdfViewCtrl SelectX1:selectionStart.x Y1:selectionStart.y PageNumber1:selectionStartPage X2:selectionEnd.x Y2:selectionEnd.y PageNumber2:selectionEndPage];
        
        selection = [self GetQuadsFromPage:selectionStartPage ToPage:selectionEndPage];
    }
    else
    {
        [m_pdfViewCtrl SelectX1:selectionEnd.x Y1:selectionEnd.y PageNumber1:selectionEndPage X2:selectionStart.x Y2:selectionStart.y PageNumber2:selectionStartPage];
        
        selection = [self GetQuadsFromPage:selectionEndPage ToPage:selectionStartPage];
    }
    
    return selection;
}

-(void) selectionBarMoved:(SelectionBar*) bar withTouches:(NSSet *) touches withEvent: (UIEvent *) event
{

    UITouch *touch = touches.allObjects[0];
    
    CGPoint down = [touch locationInView:m_pdfViewCtrl];
    
    CGPoint offsetSelectionStart, offsetSelectionEnd;

    if( [bar isLeft] == true)
    {
        offsetSelectionStart = down;

        selectionStartPage = [m_pdfViewCtrl GetPageNumberFromScreenPt:offsetSelectionStart.x y:offsetSelectionStart.y];
        selectionStart = CGPointMake(offsetSelectionStart.x, offsetSelectionStart.y);
        [self ConvertScreenPtToPagePtX:&selectionStart.x Y:&selectionStart.y PageNumber:selectionStartPage];
        
    }
    else
    {
        offsetSelectionEnd = down;

        selectionEndPage = [m_pdfViewCtrl GetPageNumberFromScreenPt:offsetSelectionEnd.x y:offsetSelectionEnd.y];
        selectionEnd = CGPointMake(offsetSelectionEnd.x, offsetSelectionEnd.y);
        [self ConvertScreenPtToPagePtX:&selectionEnd.x Y:&selectionEnd.y PageNumber:selectionEndPage];
    }

    const int padding = 14;
    const int vOffset = 14;
    const int hOffset = -6;
    
    NSMutableArray *selection;
    
    selection = [self MakeSelection];
    
    if( selection.count == 0 )
    {
        return;
    }


    CGRect lastQuad = [selection[selection.count-1] CGRectValue];
    CGRect firstQuad = [selection[0] CGRectValue];
	
	if( [m_pdfViewCtrl GetRightToLeftLanguage] ==  NO )
	{
		selectionStartCorner = CGPointMake(firstQuad.origin.x + [m_pdfViewCtrl GetHScrollPos], firstQuad.origin.y + [m_pdfViewCtrl GetVScrollPos]);
		selectionEndCorner = CGPointMake(lastQuad.origin.x + lastQuad.size.width + [m_pdfViewCtrl GetHScrollPos], lastQuad.origin.y + lastQuad.size.height/2 + [m_pdfViewCtrl GetVScrollPos]);
	}
	else
	{
		selectionStartCorner = CGPointMake(firstQuad.origin.x + [m_pdfViewCtrl GetHScrollPos] + firstQuad.size.width, firstQuad.origin.y-firstQuad.size.height/2 + [m_pdfViewCtrl GetVScrollPos]);
		selectionEndCorner = CGPointMake(lastQuad.origin.x + [m_pdfViewCtrl GetHScrollPos], lastQuad.origin.y + lastQuad.size.height/2 + [m_pdfViewCtrl GetVScrollPos]);
	}
	
    [self DrawSelectionQuads:selection WithLines:YES];
    
    CGRect oldLeft = leftBar.frame;
    CGRect oldRight = rightBar.frame;


    CGPoint dotPoint = [self GetFirstDotPoint:selection];
    leftBar.frame = CGRectMake(dotPoint.x-padding+hOffset, dotPoint.y-padding-vOffset, bar.frame.size.width, bar.frame.size.height);

    dotPoint = [self GetSecondDotPoint:selection];
    rightBar.frame = CGRectMake(dotPoint.x-padding+hOffset, dotPoint.y-padding, bar.frame.size.width, bar.frame.size.height);


    if( !(CGRectEqualToRect(oldLeft, leftBar.frame) && CGRectEqualToRect(oldRight, rightBar.frame)))
    {
       if(CGRectEqualToRect(oldLeft, leftBar.frame))
       {
           if( [bar isLeft] )
           {
               swapBars = YES;
               
           }
           else
               swapBars = NO;
       }
        else
        {
            if( [bar isLeft] )
            {
                swapBars = NO;
            }
            else
                swapBars = YES;
        }
    }
    
    
    
    
    // just create one loupe and re-use it.
	if(loupe == nil){
		loupe = [[TrnMagnifierView alloc] init];
		loupe.viewToMagnify = m_pdfViewCtrl;
	}
    
	[m_pdfViewCtrl.superview addSubview:loupe];
    
    down.y += m_pdfViewCtrl.frame.origin.y;
    down.x += m_pdfViewCtrl.frame.origin.x;
    
	[loupe setMagnifyPoint:[touch locationInView:m_pdfViewCtrl] TouchPoint:down];
	[loupe setNeedsDisplay];
    
    // keep dot on top
    [leftBar.superview bringSubviewToFront:leftBar];
    [rightBar.superview bringSubviewToFront:rightBar];
    
}

-(void) selectionBarUp:(SelectionBar*) bar withTouches:(NSSet *) touches withEvent: (UIEvent *) event
{

    
    // keep dot on top
    [leftBar.superview bringSubviewToFront:leftBar];
    [rightBar.superview bringSubviewToFront:rightBar];
    
    UITouch *touch = touches.allObjects[0];
    
    CGPoint down = [touch locationInView:m_pdfViewCtrl];
    
    down.x += [m_pdfViewCtrl GetHScrollPos];
    down.y += [m_pdfViewCtrl GetVScrollPos];
    
    NSMutableArray *selection;
    
    selection = [self MakeSelection];
    
    CGRect firstQuad = [selection[0] CGRectValue];
    CGRect lastQuad = [selection[selection.count-1] CGRectValue];
	
	int rtlOffsetFirst = 0;
	int rtlOffsetLast = 0;
	if( [m_pdfViewCtrl GetRightToLeftLanguage] == YES )
	{
		rtlOffsetFirst = firstQuad.size.width;
		rtlOffsetLast = lastQuad.size.width;
	}
	
    selectionStart = CGPointMake(firstQuad.origin.x+rtlOffsetFirst, firstQuad.origin.y+firstQuad.size.height/2);
    [self ConvertScreenPtToPagePtX:&selectionStart.x Y:&selectionStart.y PageNumber:selectionStartPage];
    selectionEnd = CGPointMake(lastQuad.origin.x + lastQuad.size.width - rtlOffsetLast, lastQuad.origin.y + lastQuad.size.height/2);
    [self ConvertScreenPtToPagePtX:&selectionEnd.x Y:&selectionEnd.y PageNumber:selectionEndPage];
    
    
    if( swapBars )
    {
        leftBar.isLeft = NO;
        rightBar.isLeft = YES;
        
        selectionStart = CGPointMake(lastQuad.origin.x + lastQuad.size.width - rtlOffsetLast, lastQuad.origin.y + lastQuad.size.height/2);
        selectionEnd = CGPointMake(firstQuad.origin.x + rtlOffsetFirst, firstQuad.origin.y+firstQuad.size.height/2);
        [self ConvertScreenPtToPagePtX:&selectionStart.x Y:&selectionStart.y PageNumber:selectionStartPage];
        [self ConvertScreenPtToPagePtX:&selectionEnd.x Y:&selectionEnd.y PageNumber:selectionEndPage];
    }
    else
    {
        leftBar.isLeft = YES;
        rightBar.isLeft = NO;
        
        selectionStart = CGPointMake(firstQuad.origin.x + rtlOffsetFirst, firstQuad.origin.y+firstQuad.size.height/2);
        [self ConvertScreenPtToPagePtX:&selectionStart.x Y:&selectionStart.y PageNumber:selectionStartPage];
        selectionEnd = CGPointMake(lastQuad.origin.x + lastQuad.size.width - rtlOffsetLast, lastQuad.origin.y + lastQuad.size.height/2);
        [self ConvertScreenPtToPagePtX:&selectionEnd.x Y:&selectionEnd.y PageNumber:selectionEndPage];
    }
    
    [self ShowMenuController];
    
    [loupe removeFromSuperview];
}

-(void)createTextMarkupAnnot:(PTAnnotType)annotType
{
	PTColorPostProcessMode mode = [m_pdfViewCtrl GetColorPostProcessMode];
	
	[self createTextMarkupAnnot:annotType
					  withColor:[ColorDefaults defaultColorPtForAnnotType:annotType attribute:ATTRIBUTE_STROKE_COLOR colorPostProcessMode:mode]
				 withComponents:[ColorDefaults numCompsInColorPtForAnnotType:annotType attribute:ATTRIBUTE_STROKE_COLOR]
					withOpacity:[ColorDefaults defaultOpacityForAnnotType:annotType]];
}


-(void)createTextMarkupAnnot:(PTAnnotType)annotType withColor:(PTColorPt*)color withComponents:(int)components withOpacity:(double)opacity
{
    PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
    
    @try
    {
        [m_pdfViewCtrl DocLock:YES];
    
        for ( int ipage = selectionStartPage; ipage <= selectionEndPage; ++ipage )
        {
            PTPage* p = [doc GetPage:ipage];
            
            
            if( ![p IsValid] )
            {
                return;
            }
			
			assert([p IsValid]);

            PTSelection* sel = [m_pdfViewCtrl GetSelection:ipage];
            PTVectorQuadPoint* quads = [sel GetQuads];
            NSUInteger num_quads = [quads size];
            
            if( num_quads > 0 )
            {
                PTQuadPoint* qp = [quads get:0];
                
                TRN_point* point = [qp getP1];
                
                double x1 = [point getX];
                double y1 = [point getY];
                
                point = [qp getP3];
                
                double x2 = [point getX];
                double y2 = [point getY];
                
                PTPDFRect* r = [[PTPDFRect alloc] initWithX1:x1 y1:y1 x2:x2 y2:y2];
                
                
                PTTextMarkup* mktp;
                
                switch (annotType) {
                    case e_ptHighlight:
                        mktp = [PTHighlightAnnot Create:(PTSDFDoc*)doc pos:r];
                        break;
                    case e_ptUnderline:
                        mktp = [PTUnderline Create:(PTSDFDoc*)doc pos:r];
                        break;
                    case e_ptStrikeOut:
                        mktp = [PTStrikeOut Create:(PTSDFDoc*)doc pos:r];
                        break;
                    case e_ptSquiggly:
                        mktp = [PTSquiggly Create:(PTSDFDoc*)doc pos:r];
                        break;
                    default:
                        // not a supported text annotation type?
                        assert(false);
                        break;
                }
                
                for( int i=0; i < num_quads; ++i )
                {
                    PTQuadPoint* quad = [quads get:i];

                    if( textMarkupAdobeHack )
                    {
                        // Acrobat and Preview do not follow the PDF specification regarding
                        // the ordering of quad points in a text markup annotation. Enable
                        // this code for compatibility with those viewers.

						PTPDFPoint* point1 = [quad getP1];
						PTPDFPoint* point2 = [quad getP2];
						PTPDFPoint* point3 = [quad getP3];
						PTPDFPoint* point4 = [quad getP4];

						PTQuadPoint* newQuad = [[PTQuadPoint alloc] init];

						[newQuad setP1:point4];
						[newQuad setP2:point3];
						[newQuad setP3:point1];
						[newQuad setP4:point2];
						
						[mktp SetQuadPoint:i qp:newQuad];
						
                    }
					else
					{
						[mktp SetQuadPoint:i qp:quad];
					}
                }
				
				if( self.annotationAuthor && self.annotationAuthor.length > 0 && [mktp isKindOfClass:[PTMarkup class]]	)
				{
					[(PTMarkup*)mktp SetTitle:self.annotationAuthor];
				}
                
                [p AnnotPushBack:mktp];

                [mktp SetColor:color numcomp:components];

                [mktp SetOpacity:opacity];
				
				double width = [ColorDefaults defaultBorderThicknessForAnnotType:annotType];
                
				PTBorderStyle* bs = [[PTBorderStyle alloc] initWithS:e_ptsolid b_width:width b_hr:0 b_vr:0];
				
				//[m_default_text_annotation GetBorderStyle];
                
                [mktp SetBorderStyle:bs oldStyleOnly:NO];
                
                [mktp RefreshAppearance];
                                
                [m_pdfViewCtrl UpdateWithAnnot:mktp page_num:ipage];
				
				m_moving_annotation = mktp;
				m_annot_page_number = ipage;
				
            }
        }
    
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }
	
	[self annotationAdded:m_moving_annotation onPageNumber:m_annot_page_number];
    
    [self ClearSelectionBars];
    
    [self ClearSelectionOnly];
}

-(void)CopySelectedTextToPasteboard
{
    [UIPasteboard generalPasteboard].string = [self GetSelectedTextFromPage:selectionStartPage ToPage:selectionEndPage];
}

#pragma mark - Scroll View Responses

-(void)pdfScrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self showSelectionMenu];
}

-(void)pdfScrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if( decelerate == false )
    {
        [self showSelectionMenu];
    }
}

-(void)pdfScrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self showSelectionMenu];
}

- (void)pdfScrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
    [loupe removeFromSuperview];
    
    if (leftBar)
        selectionOnScreen = true;
    else
        selectionOnScreen = false;
    
    [super pdfScrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view];
}

-(void)onLayoutChanged
{
    [self ClearSelectionBars];
    
    [self ClearSelectionOnly];
    
    TrnPagePresentationMode mode = [m_pdfViewCtrl GetPagePresentationMode];
    
    int page = [m_pdfViewCtrl GetCurrentPage];
    
    if( mode == e_ptsingle_page || mode == e_ptfacing || mode == e_ptfacing_cover )
    {
        if( selectionStartPage != page || selectionEndPage != page )
            return;
    }
    
    if( selectionOnScreen ) // there is currently selected text
    {
		// required to prevent possible deadlock
		dispatch_async( dispatch_get_main_queue(), ^{
			@try {
				[m_pdfViewCtrl DocLockRead];
				NSMutableArray* selection = [self MakeSelection];
				
				CGRect firstQuad = [selection[0] CGRectValue];
				
				int ltrOffsetFirst = 0;
				if( [m_pdfViewCtrl GetRightToLeftLanguage] == YES)
				{
					ltrOffsetFirst = firstQuad.size.width;
				}
				
				selectionStartCorner = CGPointMake(firstQuad.origin.x + ltrOffsetFirst + [m_pdfViewCtrl GetHScrollPos], firstQuad.origin.y + [m_pdfViewCtrl GetVScrollPos]);
				
				CGRect lastQuad = [selection[selection.count-1] CGRectValue];
				
				int ltrOffsetLast = 0;
				if( [m_pdfViewCtrl GetRightToLeftLanguage] == YES)
				{
					ltrOffsetLast = lastQuad.size.width;
				}
				
				selectionEndCorner = CGPointMake(lastQuad.origin.x + lastQuad.size.width - ltrOffsetLast + [m_pdfViewCtrl GetHScrollPos], lastQuad.origin.y + lastQuad.size.height/2 + [m_pdfViewCtrl GetVScrollPos]);
				
				[self DrawSelectionQuads:selection WithLines:YES];
				[self DrawSelectionBars:selection];
				
				[self showSelectionMenu];
			}
			@catch (NSException *exception) {
				NSLog(@"Exception: %@: %@",exception.name, exception.reason);
			}
			@finally {
				[m_pdfViewCtrl DocUnlockRead];
			}

		});
    }
    else
		[self showSelectionMenu];
    
    [super onLayoutChanged];
}

- (void)pdfScrollViewDidScroll:(UIScrollView *)scrollView
{
    
    [loupe removeFromSuperview];
    
    [super pdfScrollViewDidScroll:(UIScrollView *)scrollView];
    
}

- (void)ShowMenuController
{
    if( selectionLayers.count != 0 && selectionOnScreen == true )
    {   
        [self becomeFirstResponder];
        [self attachInitialMenuItems];
			
        UIMenuController *theMenu = [UIMenuController sharedMenuController];
        [theMenu setTargetRect:CGRectMake(selectionStartCorner.x, selectionStartCorner.y-15, selectionEndCorner.x-selectionStartCorner.x, selectionEndCorner.y-selectionStartCorner.y+30) inView:self];
        [theMenu setMenuVisible:YES animated:YES];
    }
    
}

#pragma mark - helper

-(NSMutableArray*)GetQuadsFromPage:(int)page1 ToPage:(int)page2
{
    NSMutableArray* quadsToReturn = [[NSMutableArray alloc] init];
    
    for(int page = page1; page <= page2; page++)
    {
        PTSelection* selection = [m_pdfViewCtrl GetSelection:page];
        
        PTVectorQuadPoint* quads = [selection GetQuads];
        
        NSUInteger numberOfQuads = [quads size];
      
        if( numberOfQuads == 0 )
            return Nil;
        
        int pageNumber = [selection GetPageNum];


        for(int ii = 0; ii < numberOfQuads; ii++)
        {
            PTQuadPoint* aQuad = [quads get:ii];
            
            TRN_point* t_point1 = [aQuad getP1];
            TRN_point* t_point2 = [aQuad getP2];
            TRN_point* t_point3 = [aQuad getP3];
            TRN_point* t_point4 = [aQuad getP4];
            
            CGPoint point1 = CGPointMake([t_point1 getX], [t_point1 getY]);
            CGPoint point2 = CGPointMake([t_point2 getX], [t_point2 getY]);
            CGPoint point3 = CGPointMake([t_point3 getX], [t_point3 getY]);
            CGPoint point4 = CGPointMake([t_point4 getX], [t_point4 getY]);
            
            @try
            { 
                [self ConvertPagePtToScreenPtX:&point1.x Y:&point1.y PageNumber:pageNumber];
                [self ConvertPagePtToScreenPtX:&point2.x Y:&point2.y PageNumber:pageNumber];
                [self ConvertPagePtToScreenPtX:&point3.x Y:&point3.y PageNumber:pageNumber];
                [self ConvertPagePtToScreenPtX:&point4.x Y:&point4.y PageNumber:pageNumber];
                
            }
            @catch(NSException *exception)
            {
                continue;
            }
            
            float left = MIN(point1.x, MIN(point2.x, MIN(point3.x, point4.x)));
            float right = MAX(point1.x, MAX(point2.x, MAX(point3.x, point4.x)));
            
            float top = MIN(point1.y, MIN(point2.y, MIN(point3.y, point4.y)));
            float bottom = MAX(point1.y, MAX(point2.y, MAX(point3.y, point4.y)));
            
            
            
            [quadsToReturn addObject:[NSValue valueWithCGRect:CGRectMake(left, top, (right-left), (bottom-top))]];
        }
        
    }

    return quadsToReturn;
}

-(NSString*)GetSelectedTextFromPage:(int)page1 ToPage:(int)page2
{
    NSMutableString* totalSelection = [[NSMutableString alloc] init];
	 
    for(int page = page1; page <= page2; page++)
    {
        PTSelection* selection = [m_pdfViewCtrl GetSelection:page];
        
        NSString* pageString = [selection GetAsUnicode];
                
        [totalSelection appendString:pageString];
    }
    
    return totalSelection;
}

-(void)pageNumberChangedFrom:(int)oldPageNumber To:(int)newPageNumber
{
	TrnPagePresentationMode mode = [m_pdfViewCtrl GetPagePresentationMode];
	
	if( mode == e_ptsingle_page || mode == e_ptfacing || mode == e_ptfacing_cover )
    {		
		[self ClearSelectionBars];
		
		[self ClearSelectionOnly];
		
		selectionOnScreen = NO;
		
		m_moving_annotation = Nil;
		
		[self hideMenu];
	}
	[super pageNumberChangedFrom:oldPageNumber To:newPageNumber];
}


@end
