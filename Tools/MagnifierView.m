//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "MagnifierView.h"
#import <QuartzCore/QuartzCore.h>
#import "PDFViewCtrlToolsUtil.h"

@implementation TrnMagnifierView	
@synthesize viewToMagnify, magnifyPoint;

- (instancetype)initWithFrame:(CGRect)frame {
    
	if (self = [super initWithFrame:CGRectMake(0, 0, 120, 120)]) {

		self.layer.borderColor = [UIColor clearColor].CGColor;
		self.layer.borderWidth = 3;
		self.layer.cornerRadius = 60;
		self.layer.masksToBounds = YES;
        
        UIImageView *loupeImageView = [[UIImageView alloc] initWithFrame:CGRectOffset(CGRectInset(self.bounds, -5.0, -5.0), 0, 2)];
		loupeImageView.image = [PDFViewCtrlToolsUtil toolImageNamed:@"loupe"];
		loupeImageView.backgroundColor = [UIColor clearColor];
        
		[self addSubview:loupeImageView];
	}
    
	return self;
}

- (void)setMagnifyPoint:(CGPoint)mt TouchPoint:(CGPoint)tp{
	magnifyPoint = mt;
    mt.y -= 10;
	self.center = CGPointMake(tp.x, tp.y-60);
}

- (void)drawRect:(CGRect)rect {
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextTranslateCTM(context,1*(self.frame.size.width*0.5),1*(self.frame.size.height*0.5));
	CGContextScaleCTM(context, 1.8, 1.8);
	CGContextTranslateCTM(context,-1*(magnifyPoint.x),-1*(magnifyPoint.y));
	[self.viewToMagnify.layer renderInContext:context];
}



@end
