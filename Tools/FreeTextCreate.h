//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "Tool.h"


@interface FreeTextCreate: Tool<UITextViewDelegate, UIGestureRecognizerDelegate> 
{
    CGPoint startPoint;
    CGPoint endPoint;
    UITextView* textView;
    UIScrollView* sv;
    BOOL wroteAnnot;
    BOOL created;
    BOOL keyboardOnScreen;
}

@property (assign, nonatomic) BOOL isDrag;



@property (strong, nonatomic) NSDate* touchesEndedTime;

@end
