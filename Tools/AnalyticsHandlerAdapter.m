//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------


#import "AnalyticsHandlerAdapter.h"

@implementation AnalyticsHandlerAdapter

static id _INSTANCE;

+ (id) getInstance
{
    if (!_INSTANCE) {
        _INSTANCE = [[self alloc] init];
		
    }
    return _INSTANCE;
}

+ (void) setInstance:(AnalyticsHandlerAdapter *)value
{
    _INSTANCE = value;
}

- (BOOL) sendCustomEventWithTag:(NSString *)tag
{
    //NSLog(@"AnalyticsHandlerAdapter: %@", tag);
    return false;
}

- (BOOL) logException:(NSException *)exception withExtraData:(NSDictionary *)extraData
{
	assert(false);
	@throw [[NSException alloc] initWithName:@"Invalid" reason:@"Use a concrete class" userInfo:Nil];
}

- (void)setUserIdentifier:(NSString *)identifier
{
	assert(false);
	@throw [[NSException alloc] initWithName:@"Invalid" reason:@"Use a concrete class" userInfo:Nil];
}

-(void)initializeHandler
{
	assert(false);
	@throw [[NSException alloc] initWithName:@"Invalid" reason:@"Use a concrete class" userInfo:Nil];
}

@end
