//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "LineCreate.h"
#import "ColorDefaults.h"

@class PTPDFViewCtrl;

@implementation LineCreate


- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        pdfNetAnnotType = [PTLineAnnot class];
		_width = -1;
    }
    
    return self;
}


- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = touches.allObjects[0];
    
    CGPoint touchPoint = [touch locationInView:m_pdfViewCtrl];
	
	endPoint = [super boundToPageScreenPoint:touchPoint withThicknessCorrection:0];
    
    double thickness;
    
    @try {
        [m_pdfViewCtrl DocLock:YES];
		if( _width < 0 )
			_width = [ColorDefaults defaultBorderThicknessForAnnotType:e_ptLine];
		
        thickness = _width*[m_pdfViewCtrl GetZoom];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }

    // max size of control in both directions
    double width = m_pdfViewCtrl.frame.size.width;
    double height = m_pdfViewCtrl.frame.size.height;
    
    self.frame = CGRectMake([m_pdfViewCtrl GetHScrollPos], [m_pdfViewCtrl GetVScrollPos], width, height);
    
    return YES;
}

-(PTAnnotType)annotType
{
	return e_ptLine;
}

- (void)drawRect:(CGRect)rect
{
    if( pageNumber >= 1 && !self.allowScrolling)
    {
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        
        [super setupContextFromDefaultAnnot:currentContext];
        
        CGContextBeginPath (currentContext);
        
        CGContextMoveToPoint(currentContext, startPoint.x, startPoint.y);
        CGContextAddLineToPoint(currentContext, endPoint.x, endPoint.y);

        CGContextStrokePath(currentContext);
    }

}

@end
