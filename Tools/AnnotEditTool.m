//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "AnnotEditTool.h"
#import "PanTool.h"
#import "ResizeWidgetView.h"
#import "NoteEditController.h"
#import <PDFNet//PDFNetOBJC.h>
#import "ChoiceFormViewController.h"
#import "FormFillTool.h"
#import "SelectionRectContainerView.h"
#import "ColorSwatchViewController.h"
#import "ColorDefaults.h"
#import "PDFViewCtrlToolsUtil.h"
@implementation AnnotEditTool

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        selectionRectContainerView = [[SelectionRectContainerView alloc] initWithPDFViewCtrl:in_pdfViewCtrl forAnnot:m_moving_annotation];
        baseMenuOptions = [[NSMutableArray alloc] initWithCapacity:10];
		
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector (keyboardWillShow:)
                                                     name: UIKeyboardWillShowNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector (keyboardWillHide:)
                                                     name: UIKeyboardWillHideNotification object:nil];
		
}
	
    return self;
}

-(void)willMoveToSuperview:(UIView*)newSuperview
{
	if( newSuperview == Nil )
	{
		[self deselectAnnotation];
		if( m_select_rect_on_subview )
			[selectionRectContainerView removeFromSuperview];
	}
	
	[super willMoveToSuperview:newSuperview];
	
}

- (void)dealloc
{
	if( m_select_rect_on_subview )
		[selectionRectContainerView removeFromSuperview];
	
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    
    m_moving_annotation = 0;
    
	

}

-(void)keyboardWillShow:(NSNotification *)notification
{
 
    if( [m_pdfViewCtrl GetPagePresentationMode] == e_trn_single_continuous || 
       [m_pdfViewCtrl GetPagePresentationMode] == e_trn_facing_continuous || 
       [m_pdfViewCtrl GetPagePresentationMode] == e_trn_facing_continuous_cover )
    {
        [m_pdfViewCtrl keyboardWillShow:notification rectToNotOverlapWith:selectionRectContainerView.frame];
    }
	
    keyboardOnScreen = true;
}

-(void)keyboardWillHide:(NSNotification *)notification 
{
    
    [m_pdfViewCtrl keyboardWillHide:notification];
    
    keyboardOnScreen = false;
}

- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{

    if( [view isKindOfClass:[SelectionRectView class]] || [view isKindOfClass:[ResizeWidgetView class]])
	{
        return NO;
	}
    else
	{
        return YES;
	}
}

- (BOOL) canPerformAction:(SEL)selector withSender:(id) sender {
    
    return [self respondsToSelector:selector];
}

- (void) moveAnnotation: (CGPoint) down 
{

    if ([m_moving_annotation IsValid])
    {
		
        m_annnot_rect.origin.x = selectionRectContainerView.frame.origin.x+selectionRectContainerView->selectionRectView.frame.origin.x - [m_pdfViewCtrl GetHScrollPos];
        m_annnot_rect.origin.y = selectionRectContainerView.frame.origin.y+selectionRectContainerView->selectionRectView.frame.origin.y - [m_pdfViewCtrl GetVScrollPos];
        
        m_annnot_rect.size.width = selectionRectContainerView->selectionRectView.frame.size.width;
        m_annnot_rect.size.height = selectionRectContainerView->selectionRectView.frame.size.height;
        
        [self SetAnnotationRect:m_moving_annotation Rect:m_annnot_rect OnPage:m_annot_page_number];

        [self showSelectionMenu: m_annnot_rect];
        
        [m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
        
    }
}

- (void) setAnnotationRectDelta: (CGRect) deltaRect 
{
    
    if ([m_moving_annotation IsValid])
    {
        CGRect annotRect;
        
        @try
        {
            [m_pdfViewCtrl DocLock:YES];

            annotRect.origin.x = selectionRectContainerView.frame.origin.x+selectionRectContainerView->selectionRectView.frame.origin.x - [m_pdfViewCtrl GetHScrollPos];
            annotRect.origin.y = selectionRectContainerView.frame.origin.y+selectionRectContainerView->selectionRectView.frame.origin.y - [m_pdfViewCtrl GetVScrollPos];
            
            annotRect.size.width = selectionRectContainerView->selectionRectView.frame.size.width;
            annotRect.size.height = selectionRectContainerView->selectionRectView.frame.size.height;
            
            
            
            [self SetAnnotationRect:m_moving_annotation Rect:annotRect OnPage:m_annot_page_number];

            PTAnnotType annotType = [m_moving_annotation GetType];
            
            switch( annotType )
            {
                case e_ptStamp:
                case e_ptText:
                case e_ptSound:
                case e_ptFileAttachment:
                    break;
                default:
                    [m_moving_annotation RefreshAppearance];
                    break;
            }
        
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@: %@",exception.name, exception.reason);
        }
        @finally {
            [m_pdfViewCtrl DocUnlock];
        }
        
        [self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
        
        [self showSelectionMenu: annotRect];
    }
}

- (void) setSelectionRectDelta: (CGRect) deltaRect
{
    
    if ([m_moving_annotation IsValid])
    {
        
        CGRect selRect = firstSelectionRect;
		
        if( selRect.size.width + deltaRect.size.width >= 0 )
        {
            selRect.origin.x += deltaRect.origin.x;
            selRect.size.width += deltaRect.size.width;
        }
        else if( deltaRect.origin.x != 0 )
        {
            selRect.origin.x += selRect.size.width;
            selRect.size.width = 0;
        }
        else
        {
            selRect.size.width = 0;
        }
        
        if(selRect.size.height + deltaRect.size.height >= 0 )
        {
            selRect.origin.y += deltaRect.origin.y;
            selRect.size.height += deltaRect.size.height;
        }
        else if( deltaRect.origin.y != 0 )
        {
            selRect.origin.y += selRect.size.height;
            selRect.size.height = 0;
        }
        else
        {
            selRect.size.height = 0;
        }
        
		[self boundRectToPage:&selRect isResizing:YES];
		
        selectionRectContainerView.frame = selRect;
        
        [self hideMenu];
        
    }
}


-(void) computePageBoxInScreenPts:(PTPDFRect *)rect pagenumber:(int)pageNumber
{
	@try{
		[m_pdfViewCtrl DocLockRead];
		PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
		PTPage* page = [doc GetPage:pageNumber];
		
		PTPDFRect* cropbox = [page GetCropBox];
		CGFloat x1 = [cropbox GetX1];
		CGFloat y1 = [cropbox GetY1];
		CGFloat x2 = [cropbox GetX2];
		CGFloat y2 = [cropbox GetY2];
		
		CGFloat x1t,y1t,x2t,y2t,x3t,y3t,x4t,y4t;
		
		CGFloat retx,rety;
		
		// Need to compute the transformed coordinates for the four corners
		// of the page bounding box, since a page can be rotated.
		retx = x1;rety = y1;
		[self ConvertPagePtToScreenPtX:&retx Y:&rety PageNumber:pageNumber];
		x1t = retx;y1t = rety;
		
		retx = x2;rety = y1;
		[self ConvertPagePtToScreenPtX:&retx Y:&rety PageNumber:pageNumber];
		x2t = retx;y2t = rety;
		
		retx = x2;rety = y2;
		[self ConvertPagePtToScreenPtX:&retx Y:&rety PageNumber:pageNumber];
		x3t = retx;y3t = rety;
		
		retx = x1;rety = y2;
		[self ConvertPagePtToScreenPtX:&retx Y:&rety PageNumber:pageNumber];
		x4t = retx;y4t = rety;
		
		CGFloat min_x = MIN(MIN(MIN(x1t,x2t),x3t),x4t);
		CGFloat max_x = MAX(MAX(MAX(x1t,x2t),x3t),x4t);
		CGFloat min_y = MIN(MIN(MIN(y1t,y2t),y3t),y4t);
		CGFloat max_y = MAX(MAX(MAX(y1t,y2t),y3t),y4t);
		
		[rect SetX1:min_x];
		[rect SetY1:min_y];
		[rect SetX2:max_x];
		[rect SetY2:max_y];
	}
	@catch(NSException *exception){
		NSLog(@"Exception: %@: %@",[exception name], [exception reason]);
	}
	@finally{
		[m_pdfViewCtrl DocUnlockRead];
	}
}

- (void) boundRectToPage:(CGRect *) annotRect_p isResizing:(BOOL)resizing
{
	if( ![m_moving_annotation IsValid] )
		return;
	
	
	PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
	
	CGFloat x1, y1;
	PTRotate rotation;
	PTPage* page;
	
	@try
	{
		[m_pdfViewCtrl DocLockRead];
		
		x1 = annotRect_p->origin.x - [m_pdfViewCtrl GetHScrollPos];
		y1 = annotRect_p->origin.y - [m_pdfViewCtrl GetVScrollPos];
		
		page = [doc GetPage:m_annot_page_number];
		
		rotation = [page GetRotation];
		
	}
	@catch (NSException *exception) {
		NSLog(@"Exception: %@: %@",[exception name], [exception reason]);
	}
	@finally {
		[m_pdfViewCtrl DocUnlockRead];
	}
	
	// build page box start
	
	PTPDFRect* page_rect = [page GetCropBox];
	[self computePageBoxInScreenPts:page_rect pagenumber:m_annot_page_number];
	
	CGFloat minX = [page_rect GetX1];
	CGFloat maxX = [page_rect GetX2];
	CGFloat minY = [page_rect GetY1];
	CGFloat maxY = [page_rect GetY2];
	
	// build page box end
	
	if( x1 < minX )
	{
		CGFloat diff = annotRect_p->origin.x;
		annotRect_p->origin.x = minX + [m_pdfViewCtrl GetHScrollPos];
		diff -= annotRect_p->origin.x;
		if( resizing )
		{
			annotRect_p->size.width -= ABS(diff);
		}
	}
	
	if( y1 < minY )
	{
		CGFloat diff = annotRect_p->origin.y;
		annotRect_p->origin.y = minY + [m_pdfViewCtrl GetVScrollPos];
		diff -= annotRect_p->origin.y;
		if( resizing )
		{
			annotRect_p->size.height -= ABS(diff);
		}
	}
	
	
	CGFloat x2 = annotRect_p->origin.x - [m_pdfViewCtrl GetHScrollPos] + annotRect_p->size.width;
	CGFloat y2 = annotRect_p->origin.y - [m_pdfViewCtrl GetVScrollPos] + annotRect_p->size.height;
	
	if( y2 > maxY )
	{
		CGFloat diff = annotRect_p->origin.y;
		annotRect_p->origin.y = maxY + [m_pdfViewCtrl GetVScrollPos] - annotRect_p->size.height;
		if( resizing )
		{
			diff -= annotRect_p->origin.y;
			annotRect_p->size.height -= ABS(diff);
			annotRect_p->origin.y += ABS(diff);
		}
	}
	
	if( x2 > maxX )
	{
		CGFloat diff = annotRect_p->origin.x;
		annotRect_p->origin.x = maxX + [m_pdfViewCtrl GetHScrollPos] -annotRect_p->size.width;
		if( resizing )
		{
			diff -= annotRect_p->origin.x;
			annotRect_p->size.width -= ABS(diff);
			annotRect_p->origin.x += ABS(diff);
		}
	}
}

- (bool) moveSelectionRect: (CGPoint) down
{
	
	PTPDFRect* screen_rect = [m_pdfViewCtrl GetScreenRectForAnnot: m_moving_annotation page_num: m_annot_page_number];
	
	CGRect annotRect = [self PDFRectScreen2CGRectScreen:screen_rect PageNumber:m_annot_page_number];
	
    if( fabs(annotRect.origin.x - (down.x + offset.x)) > 3 ||
        fabs(annotRect.origin.y - (down.y + offset.y)) > 3)
    {
        annotRect.origin.x = down.x + offset.x;
        annotRect.origin.y = down.y + offset.y;

        [self boundRectToPage:&annotRect isResizing:NO];


        selectionRectContainerView.frame = annotRect;
        
        [self hideMenu];
        
        return true;
    }
    else
        return false;
}


- (void) attachHighlightAnnotMenuItems: (NSMutableArray *) menuItems
{
    UIMenuItem* menuItem;
	
	NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
	
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Comment", @"PDFNet-Tools", stringBundle, @"Note tool name") action:@selector(editSelectedAnnotationNote)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Color", @"PDFNet-Tools", stringBundle, @"Color") action:@selector(editSelectedAnnotationStrokeColor)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Opacity", @"PDFNet-Tools", stringBundle, @"Opacity") action:@selector(editSelectedAnnotationOpacity)];
    [menuItems addObject:menuItem];
}

- (void) attachTextAnnotMenuItems: (NSMutableArray *) menuItems
{
    UIMenuItem* menuItem;
	
	NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Comment", @"PDFNet-Tools", stringBundle, @"Note tool name") action:@selector(editSelectedAnnotationNote)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Color", @"PDFNet-Tools", stringBundle, @"Color") action:@selector(editSelectedAnnotationStrokeColor)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Tbickness", @"PDFNet-Tools", stringBundle, @"Line Thickness") action:@selector(editSelectedAnnotationBorder)];
    [menuItems addObject:menuItem];
     menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Opacity", @"PDFNet-Tools", stringBundle, @"Opacity") action:@selector(editSelectedAnnotationOpacity)];
    [menuItems addObject:menuItem];
}

- (void) attachFreeTextMenuItems: (NSMutableArray *) menuItems
{
    UIMenuItem* menuItem;
	
	NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
    // items commented out below are compatible with free text annotations, uncomment if desired.
    
//    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Note", @"PDFNet-Tools", stringBundle, @"Note tool name") action:@selector(editSelectedAnnotationNote)];
//    [menuItems addObject:menuItem];
//    [menuItem release];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Edit Text", @"PDFNet-Tools", stringBundle, @"") action:@selector(editSelectedAnnotationFreeText)];
    [menuItems addObject:menuItem];
    
    // not available!
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Text Color", @"PDFNet-Tools", stringBundle, @"") action:@selector(editFreeTextColor)];
    [menuItems addObject:menuItem];
    
    // not available!
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Text Size", @"PDFNet-Tools", stringBundle, @"") action:@selector(editFreeTextSize)];
    [menuItems addObject:menuItem];
    
    // not available!
    //menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Text Size", @"PDFNet-Tools", stringBundle, @"") action:@selector(editFreeTextFont)];
    //[menuItems addObject:menuItem];
    
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Fill Color", @"PDFNet-Tools", stringBundle, @"Colour with which to fill a shape.") action:@selector(editSelectedAnnotationStrokeColor)];
    [menuItems addObject:menuItem];

//    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Thickness", @"PDFNet-Tools", stringBundle, @"") action:@selector(editSelectedAnnotationBorder)];
//    [menuItems addObject:menuItem];
//    [menuItem release];
//    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Opacity", @"PDFNet-Tools", stringBundle, @"") action:@selector(editSelectedAnnotationOpacity)];
//    [menuItems addObject:menuItem];
//    [menuItem release];
}

- (void) attachFreeHandMenuItems: (NSMutableArray *) menuItems
{
    UIMenuItem* menuItem;
	
	NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
    
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Comment", @"PDFNet-Tools", stringBundle, @"") action:@selector(editSelectedAnnotationNote)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Color", @"PDFNet-Tools", stringBundle, @"") action:@selector(editSelectedAnnotationStrokeColor)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Thickness", @"PDFNet-Tools", stringBundle, @"Line thickness") action:@selector(editSelectedAnnotationBorder)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Opacity", @"PDFNet-Tools", stringBundle, @"") action:@selector(editSelectedAnnotationOpacity)];
    [menuItems addObject:menuItem];
}


- (void) attachMarkupMenuItems: (NSMutableArray *) menuItems
{
    UIMenuItem* menuItem;

	NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Comment", @"PDFNet-Tools", stringBundle, @"") action:@selector(editSelectedAnnotationNote)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Stroke Color", @"PDFNet-Tools", stringBundle, @"Line Color") action:@selector(editSelectedAnnotationStrokeColor)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Fill Color", @"PDFNet-Tools", stringBundle, @"Colour with which to fill a shape.") action:@selector(editSelectedAnnotationFillColor)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Thickness", @"PDFNet-Tools", stringBundle, @"Line thickness") action:@selector(editSelectedAnnotationBorder)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Opacity", @"PDFNet-Tools", stringBundle, @"") action:@selector(editSelectedAnnotationOpacity)];
    [menuItems addObject:menuItem];
}

-(void) attachFreeTextFontSizeMenuItems
{
	NSMutableArray* menuItems = [[NSMutableArray alloc] initWithCapacity:2];
    
    UIMenuItem* menuItem;
	
	menuItem = [[UIMenuItem alloc] initWithTitle:@"8" action:@selector(setFreeTextSize8)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:@"11" action:@selector(setFreeTextSize11)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:@"16" action:@selector(setFreeTextSize16)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:@"24" action:@selector(setFreeTextSize24)];
    [menuItems addObject:menuItem];
	menuItem = [[UIMenuItem alloc] initWithTitle:@"36" action:@selector(setFreeTextSize36)];
    [menuItems addObject:menuItem];

	UIMenuController *theMenu = [UIMenuController sharedMenuController];
    theMenu.menuItems = menuItems;

}

- (void) attachBorderThicknessMenuItems
{
	NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
	
    NSMutableArray* menuItems = [[NSMutableArray alloc] initWithCapacity:2];
    
    UIMenuItem* menuItem;
	
	NSString* pt = NSLocalizedStringFromTableInBundle(@"pt", @"PDFNet-Tools", stringBundle, @"Abberviation for point, as in font point size.");
    
    menuItem = [[UIMenuItem alloc] initWithTitle:[NSString stringWithFormat:@"0.5 %@", pt] action:@selector(setAnnotBorder05)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:[NSString stringWithFormat:@"1 %@", pt] action:@selector(setAnnotBorder10)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:[NSString stringWithFormat:@"1.5 %@", pt] action:@selector(setAnnotBorder15)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:[NSString stringWithFormat:@"3 %@", pt] action:@selector(setAnnotBorder30)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:[NSString stringWithFormat:@"5 %@", pt] action:@selector(setAnnotBorder50)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:[NSString stringWithFormat:@"9 %@", pt] action:@selector(setAnnotBorder90)];
    [menuItems addObject:menuItem];
    
    UIMenuController *theMenu = [UIMenuController sharedMenuController];
    theMenu.menuItems = menuItems;
    
}

- (void) attachOpacityMenuItems
{
	
    NSMutableArray* menuItems = [[NSMutableArray alloc] initWithCapacity:2];
    
    UIMenuItem* menuItem;
    
    menuItem = [[UIMenuItem alloc] initWithTitle:@"25%" action:@selector(setAnnotOpacity25)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:@"50%" action:@selector(setAnnotOpacity50)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:@"75%" action:@selector(setAnnotOpacity75)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:@"100%" action:@selector(setAnnotOpacity10)];
    [menuItems addObject:menuItem];

    UIMenuController *theMenu = [UIMenuController sharedMenuController];
    theMenu.menuItems = menuItems;
    
}

- (void) attachInitialMenuItemsForAnnotType: (PTAnnotType) annotType
{
	NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
	
    NSMutableArray* menuItems = [[NSMutableArray alloc] init];
    
    // add menu items common to all annotations EXCEPT movies
    
    UIMenuItem* menuItem;
	
	if( annotType != e_ptRichMedia )
	{
		menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Done", @"PDFNet-Tools", stringBundle, @"") action:@selector(deselectAnnotation)];
		[menuItems addObject:menuItem];
		menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Delete", @"PDFNet-Tools", stringBundle, @"") action:@selector(deleteSelectedAnnotation)];
		[menuItems addObject:menuItem];
	}

    
    switch (annotType) {
        case e_ptText:
             menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Comment", @"PDFNet-Tools", stringBundle, @"") action:@selector(editSelectedAnnotationNote)];
            [menuItems addObject:menuItem];
            break;
        case e_ptLink:
            break;
        case e_ptFreeText:
            [self attachFreeTextMenuItems: menuItems];
            break;
        case e_ptLine:
            [self attachFreeHandMenuItems: menuItems];
            break;
        case e_ptSquare:
            [self attachMarkupMenuItems: menuItems];
            break;
        case e_ptCircle:
            [self attachMarkupMenuItems: menuItems];
            break;
        case e_ptPolygon:
            [self attachMarkupMenuItems: menuItems];
            break;
        case e_ptPolyline:
            [self attachMarkupMenuItems: menuItems];
            break;
        case e_ptHighlight:
            [self attachHighlightAnnotMenuItems: menuItems];
            break;
        case e_ptUnderline:
            [self attachTextAnnotMenuItems: menuItems];
            break;
        case e_ptSquiggly:
            [self attachTextAnnotMenuItems: menuItems];
            break;
        case e_ptStrikeOut:
            [self attachTextAnnotMenuItems: menuItems];
            break;
        case e_ptStamp:
            menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Note", @"PDFNet-Tools", stringBundle, @"") action:@selector(editSelectedAnnotationNote)];
            [menuItems addObject:menuItem];
            break;
        case e_ptCaret:
            break;
        case e_ptInk:
            [self attachFreeHandMenuItems: menuItems];
            break;
        case e_ptPopup:
            break;
        case e_ptFileAttachment:
            break;
        case e_ptSound:
            break;
        case e_ptMovie:
            break;
        case e_ptWidget:
            break;
        case e_ptScreen:
            break;
        case e_ptPrinterMark:
            break;
        case e_ptTrapNet:
            break;
        case e_ptWatermark:
            break;
        case e_pt3D:
            break;
        case e_ptRedact:
            break;
        case e_ptProjection:
            break;
        case e_ptRichMedia:
            break;
        case e_ptUnknown:
            break;
        default:
            break;
    }
        
    UIMenuController *theMenu = [UIMenuController sharedMenuController];
    
    theMenu.menuItems = menuItems;
    
}

-(void)onSetDoc
{
    m_moving_annotation = 0;
}

-(void) onLayoutChanged
{
    TrnPagePresentationMode mode = [m_pdfViewCtrl GetPagePresentationMode];
    
    int page = [m_pdfViewCtrl GetCurrentPage];
    
    
    if( m_annot_page_number != page && (mode == e_ptsingle_page || mode == e_ptfacing || mode == e_ptfacing_cover) )
    {
        [selectionRectContainerView setHidden:YES];
        return;
    }
    else if( m_moving_annotation )
    {
        [self reSelectAnnotation];
    }
    
    [super onLayoutChanged];
}

-(BOOL)annotIsMoveable:(PTAnnot*)annot
{
    if ([annot IsValid])
    {
        PTAnnotType annotType = [m_moving_annotation GetType];
        
        if(annotType == e_ptLink || annotType == e_ptHighlight || annotType == e_ptUnderline || annotType == e_ptStrikeOut || annotType ==  e_ptSquiggly || annotType == e_ptRichMedia)
            return NO;
        
        if( [annot GetFlag:e_ptlocked] )
            return NO;
    }
    
    return YES;
}

-(BOOL)annotIsResizeable:(PTAnnot*)annot
{
    
    if ([annot IsValid])
    {
        PTAnnotType annotType = [m_moving_annotation GetType];
        
        if(annotType == e_ptLink || annotType == e_ptText || annotType == e_ptHighlight || annotType == e_ptUnderline || annotType == e_ptStrikeOut || annotType ==  e_ptSquiggly || annotType == e_ptRichMedia)
            return NO;
        
        if( [annot GetFlag:e_ptlocked] )
            return NO;
    }
    
    return YES;
}

- (void) reSelectAnnotation
{
    PTAnnotType annotType = [m_moving_annotation GetType];

    if( (![m_moving_annotation IsValid] || selectionRectContainerView.hidden) && annotType != e_ptWidget )
        return;
    
    if( annotType == e_ptLink )
        return;
    
    @try
    {
        [m_pdfViewCtrl DocLockRead];
		
		PTPDFRect* screen_rect = [m_pdfViewCtrl GetScreenRectForAnnot: m_moving_annotation page_num: m_annot_page_number];
		double x1 = [screen_rect GetX1];
		double x2 = [screen_rect GetX2];
		double y1 = [screen_rect GetY1];
		double y2 = [screen_rect GetY2];
		
		m_annnot_rect = CGRectMake(MIN(x1,x2), MIN(y1, y2), MAX(x1,x2) - MIN(x1,x2), MAX(y1, y2) - MIN(y1, y2));
	}
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }
    
    m_annnot_rect.origin.x = m_annnot_rect.origin.x + [m_pdfViewCtrl GetHScrollPos];
    m_annnot_rect.origin.y = m_annnot_rect.origin.y + [m_pdfViewCtrl GetVScrollPos];
            
    [selectionRectContainerView setHidden:NO];
    
    selectionRectContainerView.frame = m_annnot_rect;
    
    
    if( ![self annotIsResizeable:m_moving_annotation] )
        [selectionRectContainerView hideResizeWidgetViews];
    else
        [selectionRectContainerView showResizeWidgetViews];
    
	if( [m_moving_annotation GetType] == e_ptFreeText )
	{
		PTFreeText* ft = [[PTFreeText alloc] initWithAnn:m_moving_annotation];
		
		[selectionRectContainerView setEditTextSizeForZoom:[m_pdfViewCtrl GetZoom] forFontSize:[ft GetFontSize]];
	}
    
    if( !m_select_rect_on_subview )
    {
        [m_pdfViewCtrl->ContainerView addSubview:selectionRectContainerView];
        m_select_rect_on_subview = YES;
    }
    
    m_annnot_rect.origin.x = m_annnot_rect.origin.x - [m_pdfViewCtrl GetHScrollPos];
    m_annnot_rect.origin.y = m_annnot_rect.origin.y - [m_pdfViewCtrl GetVScrollPos];
	
	m_annot_page_number = [m_pdfViewCtrl GetPageNumberFromScreenPt:m_annnot_rect.origin.x+m_annnot_rect.size.width/2 y:m_annnot_rect.origin.y+m_annnot_rect.size.height/2];

	// Demonstrates how to extract under an annotation
//	TextExtractor *textExtractor = [[TextExtractor alloc] init];
//	[textExtractor Begin:[m_moving_annotation GetPage]
//				clip_ptr:0
//				   flags:e_no_ligature_exp];
//	
//	NSString* annotationText = [textExtractor GetTextUnderAnnot:m_moving_annotation];
//	
//	NSLog(@"Text under annot is %@", annotationText);
	
    [self showSelectionMenu:m_annnot_rect];
    
}



- (BOOL) makeNewAnnotationSelection: (UIGestureRecognizer *) gestureRecognizer
{
    CGPoint down = [gestureRecognizer locationInView:m_pdfViewCtrl];

    [self deselectAnnotation];
    [selectionRectContainerView setNeedsDisplay];
    if( m_moving_annotation )
    {
        [self deselectAnnotation];
        [m_pdfViewCtrl setNeedsDisplay];
    }
    BOOL hasReadLock = NO;
    BOOL hasWriteLock = NO;
    @try
    {
        [m_pdfViewCtrl DocLockRead];
        hasReadLock = YES;
		m_moving_annotation = [m_pdfViewCtrl GetAnnotationAt:down.x y:down.y distanceThreshold:GET_ANNOT_AT_DISTANCE_THRESHOLD minimumLineWeight:GET_ANNOT_AT_MINIMUM_LINE_WEIGHT];
		
        PTLinkInfo* linkInfo = 0;
        
        // checks if there is text that looks like a link but is missing a link annot
        // note that [pdfViewCtrl SetUrlExtraction:YES]; must be called
        if( ![m_moving_annotation IsValid] )
            linkInfo = [m_pdfViewCtrl GetLinkAt:down.x y:down.y];
        
        if( [m_moving_annotation IsValid] || (linkInfo.getUrl).length > 0)
        {
            
            PTAnnotType annotType= -1;
            
            if( [m_moving_annotation IsValid] )
                annotType = [m_moving_annotation GetType];

            // IF IT IS HYPERLINK, FOLLOW HYPERLINK INSTEAD
            if((annotType == e_ptLink || (linkInfo.getUrl).length > 0) )
            {
                // external links
                
                CGRect screenRect;
                PTActionType actionType = -1;
                PTAction* action;
                PTLink* myLink;
                
                if( annotType == e_ptLink )
                {
                    myLink = [[PTLink alloc] initWithAnn:m_moving_annotation];
                
                    action = [myLink GetAction];

                    actionType = [action GetType];
                    
                    m_annot_page_number = [m_pdfViewCtrl GetPageNumberFromScreenPt:down.x y:down.y];
					
					PTPDFRect* screen_rect = [m_pdfViewCtrl GetScreenRectForAnnot: m_moving_annotation page_num: m_annot_page_number];
					
					screenRect = [self PDFRectScreen2CGRectScreen:screen_rect PageNumber:m_annot_page_number];
					
					
                }
                else // extracted link
                {
                    PTPDFRect* rect = [linkInfo getRect];
                    [rect setSwigCMemOwn:NO];
                    int pageNumber = [m_pdfViewCtrl GetPageNumberFromScreenPt:down.x y:down.y];
                    screenRect = [self PDFRectPage2CGRectScreen:rect PageNumber:pageNumber];
                }
                
                screenRect.origin.x += [m_pdfViewCtrl GetHScrollPos];
                screenRect.origin.y += [m_pdfViewCtrl GetVScrollPos];
                
                UIView* flashLink = [[UIView alloc] initWithFrame:screenRect];
                
                flashLink.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
                
                [m_pdfViewCtrl->ContainerView addSubview:flashLink];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [flashLink removeFromSuperview];
                });
                
                if([action IsValid])
                {
                    if([action NeedsWriteLock])
                    {
                        hasReadLock = NO;
                        [m_pdfViewCtrl DocUnlockRead];
                        [m_pdfViewCtrl DocLock:true];
                        hasWriteLock = YES;
                    }
                    PTActionParameter* action_parameter = [[PTActionParameter alloc]initWithAction:action annot:m_moving_annotation];
                    [self executeAction:action_parameter];
                    return YES;
                }
            }
                
			@try
			{
				[m_pdfViewCtrl DocLockRead];


				m_annot_page_number = [m_pdfViewCtrl GetPageNumberFromScreenPt:down.x y:down.y];

				PTPDFRect* screen_rect = [m_pdfViewCtrl GetScreenRectForAnnot: m_moving_annotation page_num: m_annot_page_number];
				
				m_annnot_rect = [self PDFRectScreen2CGRectScreen:screen_rect PageNumber:m_annot_page_number];

				
			}
			@catch (NSException *exception) {
				NSLog(@"Exception: %@: %@",exception.name, exception.reason);
			}
			@finally {
				[m_pdfViewCtrl DocUnlockRead];
			}
			
			m_annnot_rect.origin.x = m_annnot_rect.origin.x + [m_pdfViewCtrl GetHScrollPos];
			m_annnot_rect.origin.y = m_annnot_rect.origin.y + [m_pdfViewCtrl GetVScrollPos];
			
			selectionRectContainerView.frame = m_annnot_rect;
			[selectionRectContainerView setHidden:NO];

			
			if( ![self annotIsResizeable:m_moving_annotation])
				[selectionRectContainerView hideResizeWidgetViews];
			else
				[selectionRectContainerView showResizeWidgetViews];
			
			if( !m_select_rect_on_subview )
			{
				[m_pdfViewCtrl->ContainerView addSubview:selectionRectContainerView];
				m_select_rect_on_subview = YES;
			}


			[self attachInitialMenuItemsForAnnotType: annotType];

			m_annnot_rect.origin.x = m_annnot_rect.origin.x - [m_pdfViewCtrl GetHScrollPos];
			m_annnot_rect.origin.y = m_annnot_rect.origin.y - [m_pdfViewCtrl GetVScrollPos];
			
			[self showSelectionMenu: m_annnot_rect];
			
            
            return YES;
        }
        else
        {
            m_moving_annotation = 0;
        }

        nextToolType = self.defaultClass;
        return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        if(hasReadLock)
        {
            [m_pdfViewCtrl DocUnlockRead];
        }
        if(hasWriteLock)
        {
            [m_pdfViewCtrl DocUnlock];
        }
    }

}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if( [scrollView isKindOfClass:[UITextView class]] )
    {
        // scrolling PDF form widget
    }
    else
    {
        // sent from outside
    }
}


- (BOOL)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    moveOccuredPreTap = YES;
    [self deselectAnnotation];
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        if (selectionRectContainerView.superview == Nil)
        {
            if( [self makeNewAnnotationSelection: gestureRecognizer] )
            {
                //remove appearance views
                [self removeAppearanceViews];
                
                if( [m_moving_annotation IsValid] )
                {
                    PTAnnotType annotType = [m_moving_annotation GetType];
                    
					// check for self.superview to prevent two popovers from being created
					// in while loops in CreateToolBase handleTap: and TextMarkupCreate
					// handleTap:
                    if( annotType == e_ptText && (self.superview || [self isKindOfClass:[PanTool class]]) )
                    {
                        [self hideMenu];
                        [self editSelectedAnnotationNote];
                    }
                }

                return  YES;
            }
        }

        CGPoint touchPoint = [gestureRecognizer locationInView:selectionRectContainerView];

        if( !CGRectContainsPoint(selectionRectContainerView.bounds, touchPoint) || m_moving_annotation == Nil)
        {
            nextToolType = self.defaultClass;

            return  NO;
        }
        
    }

    [selectionRectContainerView setHidden:NO];

    [self reSelectAnnotation];

    [self showSelectionMenu:m_annnot_rect];

    return YES;
}


- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if( [m_pdfViewCtrl GetDoc] == Nil )
    {
        return YES;
    }

    moveOccuredPreTap = YES;
	
	if (selectionRectContainerView.hidden)
    {
        [self deselectAnnotation];
        nextToolType = self.defaultClass;
        return NO;
    }
    
    if (![m_moving_annotation IsValid] ) {
        return YES;
    }
    
    UITouch *touch = touches.allObjects[0];
    
    CGPoint down = [touch locationInView:touch.view];
    
    offset.x = -down.x;
    offset.y = -down.y;

    
    if ([touch.view isKindOfClass:[SelectionRectView class]])
    {
        down = [touch locationInView:m_pdfViewCtrl];
        
        down.x += [m_pdfViewCtrl GetHScrollPos];
        down.y += [m_pdfViewCtrl GetVScrollPos];
        return YES;
    }
    else if([touch.view isKindOfClass:[ResizeWidgetView class]])
    {

        UITouch *touch = touches.allObjects[0];
        
        CGPoint down = [touch locationInView:m_pdfViewCtrl];
        
        down.x += [m_pdfViewCtrl GetHScrollPos];
        down.y += [m_pdfViewCtrl GetVScrollPos];
        
        firstSelectionRect = selectionRectContainerView->selectionRectView.frame;
        
        firstSelectionRect.origin.x += selectionRectContainerView.frame.origin.x;
        firstSelectionRect.origin.y += selectionRectContainerView.frame.origin.y;
        
        firstTouchPoint = down;
        mostRecentTouchPoint = down;
        
        self.touchedSelectWidget = touch.view;
        
        return YES;
    }

    nextToolType = self.defaultClass;

    return NO;
}

- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    moveOccuredPreTap = NO;
    
    if( ![m_moving_annotation IsValid] )
    {
        return YES;
    }
    
    PTAnnotType annotType = [m_moving_annotation GetType];
    
    if (![self annotIsMoveable:m_moving_annotation]) {
        return YES;
    }
    
    UITouch *touch = touches.allObjects[0];
    
    if ([touch.view isKindOfClass:[SelectionRectView class]] )
    {   
        CGPoint down = [touch locationInView:m_pdfViewCtrl];
        
        down.x += [m_pdfViewCtrl GetHScrollPos];
        down.y += [m_pdfViewCtrl GetVScrollPos];
        

        [self moveSelectionRect:down];
    }
    else if([touch.view isKindOfClass:[ResizeWidgetView class]])
    {
        
        if (annotType == e_ptText ) {
            return YES;
        }
        
        CGPoint down = [touch locationInView:m_pdfViewCtrl];
        
        down.x += [m_pdfViewCtrl GetHScrollPos];
        down.y += [m_pdfViewCtrl GetVScrollPos];
        
        CGPoint delta;
        delta.x = down.x - firstTouchPoint.x;
        delta.y = down.y - firstTouchPoint.y;
        
        mostRecentTouchPoint = down;
        
        ResizeWidgetView* resizeWidget = (ResizeWidgetView*)touch.view;
        
        Location location = resizeWidget->m_location;
        
        if( location == e_topleft )
            [self setSelectionRectDelta:CGRectMake(delta.x, delta.y, -delta.x, -delta.y)];
        else if( location == e_top )
            [self setSelectionRectDelta:CGRectMake(0, delta.y, 0, -delta.y)];
        else if( location == e_topright )
            [self setSelectionRectDelta:CGRectMake(0, delta.y, delta.x, -delta.y)];
        else if( location == e_right )
            [self setSelectionRectDelta:CGRectMake(0, 0, delta.x, 0)];
        else if( location == e_bottomright )
            [self setSelectionRectDelta:CGRectMake(0, 0, delta.x, delta.y)];
        else if( location == e_bottom )
            [self setSelectionRectDelta:CGRectMake(0, 0, 0, delta.y)];
        else if( location == e_bottomleft )
            [self setSelectionRectDelta:CGRectMake(delta.x, 0, -delta.x, delta.y)];
        else if( location == e_left )
            [self setSelectionRectDelta:CGRectMake(delta.x, 0, -delta.x, 0)];
        
    }
    
    return YES;
}

- (BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    moveOccuredPreTap = YES;

    
    if( ![m_moving_annotation IsValid] )
    {
        return YES;
    }
    
    PTAnnotType annotType = [m_moving_annotation GetType];
    
    if (![self annotIsMoveable:m_moving_annotation]) {
		
		UITouch *touch = touches.allObjects[0];
		
		CGPoint down = [touch locationInView:m_pdfViewCtrl];
        
        down.x += [m_pdfViewCtrl GetHScrollPos];
        down.y += [m_pdfViewCtrl GetVScrollPos];
		
		PTAnnot* touchedAnnot = [m_pdfViewCtrl GetAnnotationAt:down.x y:down.y distanceThreshold:GET_ANNOT_AT_DISTANCE_THRESHOLD minimumLineWeight:GET_ANNOT_AT_MINIMUM_LINE_WEIGHT];
		
		if( ![touchedAnnot IsValid] )
		{
			[self showSelectionMenu:m_annnot_rect];
		}
		
        return YES;
    }

    UITouch *touch = touches.allObjects[0];
    
    if ([touch.view isKindOfClass:[SelectionRectView class]])
    {
        UITouch *touch = touches.allObjects[0];
        
        CGPoint down = [touch locationInView:m_pdfViewCtrl];
        
        down.x += [m_pdfViewCtrl GetHScrollPos];
        down.y += [m_pdfViewCtrl GetVScrollPos];

        if( [self moveSelectionRect:down] )
            [self moveAnnotation:down];
		[self showSelectionMenu:m_annnot_rect];
    }
    else if([touch.view isKindOfClass:[ResizeWidgetView class]])
    {
        if (annotType == e_ptText ) {
            return YES;
        }
        
        CGPoint down = [touch locationInView:m_pdfViewCtrl];
        
        down.x += [m_pdfViewCtrl GetHScrollPos];
        down.y += [m_pdfViewCtrl GetVScrollPos];
        
        CGPoint delta;
        delta.x = down.x - firstTouchPoint.x;
        delta.y = down.y - firstTouchPoint.y;
        
        mostRecentTouchPoint = down;
        
        ResizeWidgetView* resizeWidget = (ResizeWidgetView*)touch.view;
        
        Location location = resizeWidget->m_location;
        
        if( location == e_topleft )
            [self setAnnotationRectDelta:CGRectMake(delta.x, delta.y, -delta.x, -delta.y)];
        else if( location == e_top )
            [self setAnnotationRectDelta:CGRectMake(0, delta.y, 0, -delta.y)];
        else if( location == e_topright )
            [self setAnnotationRectDelta:CGRectMake(0, delta.y, delta.x, -delta.y)];
        else if( location == e_right )
            [self setAnnotationRectDelta:CGRectMake(0, 0, delta.x, 0)];
        else if( location == e_bottomright )
            [self setAnnotationRectDelta:CGRectMake(0, 0, delta.x, delta.y)];
        else if( location == e_bottom )
            [self setAnnotationRectDelta:CGRectMake(0, 0, 0, delta.y)];
        else if( location == e_bottomleft )
            [self setAnnotationRectDelta:CGRectMake(delta.x, 0, -delta.x, delta.y)];
        else if( location == e_left )
            [self setAnnotationRectDelta:CGRectMake(delta.x, 0, -delta.x, 0)];

    }
	else
	{
		[self attachInitialMenuItemsForAnnotType:[m_moving_annotation GetType]];
		[self reSelectAnnotation];
	}
    
    return YES;
}

- (BOOL)onTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    return YES;
}

-(BOOL)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint down = [gestureRecognizer locationInView:m_pdfViewCtrl];
    
    if( gestureRecognizer.state == UIGestureRecognizerStateBegan )
    {
        
        if( [self makeNewAnnotationSelection: gestureRecognizer])
        {
            CGPoint off = [gestureRecognizer locationInView:selectionRectContainerView->selectionRectView];
            
            offset.x = -off.x;
            offset.y = -off.y;

            return  YES;
        }
        else
        {
            m_moving_annotation = 0;
            nextToolType = self.defaultClass;
            return NO;
        }
    }
    else if( gestureRecognizer.state == UIGestureRecognizerStateEnded )
    {
        
        if (![m_moving_annotation IsValid]) {
            return YES;
        }
        
        if (![self annotIsMoveable:m_moving_annotation]) {
            return YES;
        }
        
        down.x += [m_pdfViewCtrl GetHScrollPos];
        down.y += [m_pdfViewCtrl GetVScrollPos];
        
        if([self moveSelectionRect:down])
            [self moveAnnotation: down];
        
        return  YES;
    }
    else // UIGestureRecognizerStateChanged
    {
        
        if (![m_moving_annotation IsValid]) {
            return YES;
        }
        
        
        if (![self annotIsMoveable:m_moving_annotation]) {
            return YES;
        }
        
        down.x += [m_pdfViewCtrl GetHScrollPos];
        down.y += [m_pdfViewCtrl GetVScrollPos];

        [self moveSelectionRect: down];
        
        return  YES;
    }
}

// ANNOTATION MENU ACTION RESPONSES

-(void)cancelMenu
{
    [self hideMenu];
    [self attachInitialMenuItemsForAnnotType:[m_moving_annotation GetType]];
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    NSString* theText = textView.text;
    [m_moving_annotation SetContents:theText];
}

-(void)editSelectedAnnotationFreeText
{
    NSString* text = [m_moving_annotation GetContents];
    PTFreeText* ft = [[PTFreeText alloc] initWithAnn:m_moving_annotation];
	int fontSize = [ft GetFontSize];
    int alignment = [ft GetQuaddingFormat];
    
    [selectionRectContainerView useTextViewWithText:text withAlignment:alignment atZoom:[m_pdfViewCtrl GetZoom] forFontSize:fontSize withDelegate:self];
}


-(void)editSelectedAnnotationBorder
{
    [self hideMenu];
    [self attachBorderThicknessMenuItems];
    [self showSelectionMenu:m_annnot_rect];
}

-(void)setFreeTextSize:(double)size
{
	PTAnnotType annotType = [m_moving_annotation GetType];
    [self attachInitialMenuItemsForAnnotType:annotType];
	
	@try
    {
        [m_pdfViewCtrl DocLock:YES];
		PTFreeText* ft = [[PTFreeText alloc] initWithAnn:m_moving_annotation];
		[ft SetFontSize:size];
		[ft RefreshAppearance];
		
		[ColorDefaults setDefaultFreeTextSize:size forAnnotType:annotType];
		
	}
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }
	[m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
	[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
}

-(void)setAnnotationBorder:(float)thickness
{
    PTAnnotType annotType = [m_moving_annotation GetType];
    [self attachInitialMenuItemsForAnnotType:annotType];
    
    @try
    {
        [m_pdfViewCtrl DocLock:YES];
    
        PTBorderStyle* bs = [m_moving_annotation GetBorderStyle];
        [bs SetWidth:thickness];
        [m_moving_annotation SetBorderStyle:bs oldStyleOnly:NO];
        [m_moving_annotation RefreshAppearance];

        [m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
        
        [ColorDefaults setDefaultBorderThickness:thickness forAnnotType:annotType];
    
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }
	
	[self showSelectionMenu:m_annnot_rect];
	
	[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
}

// gets us a popover on iPhone (iOS8+)
- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

- (void) showColorPicker
{
    if( ![m_moving_annotation IsValid] )
        return;

	ColorSwatchViewController *colorSwatchController = [[ColorSwatchViewController alloc] initWithNibName:Nil bundle:Nil];
	
	colorSwatchController.annotationType = [m_moving_annotation GetType];
	colorSwatchController.forFillColor = m_fill_color;
	
	// opposite for freetext
	if( [m_moving_annotation GetType] == e_ptFreeText )
		colorSwatchController.forFillColor = !m_fill_color;
	
	colorSwatchController.delegate = self;
	
	@try
	{
		[m_pdfViewCtrl DocLockRead];

		if( !selectionRectContainerView->selectionRectView.frame.size.width && !selectionRectContainerView->selectionRectView.frame.size.height)
		{
			CGRect annotRect;
			// coming from a text annot
			@try
			{
				[m_pdfViewCtrl DocLockRead];

				PTPDFRect* screen_rect = [m_pdfViewCtrl GetScreenRectForAnnot: m_moving_annotation page_num: m_annot_page_number];
				
				m_annnot_rect = [self PDFRectScreen2CGRectScreen:screen_rect PageNumber:m_annot_page_number];

			}
			@catch (NSException *exception) {
				NSLog(@"Exception: %@: %@",exception.name, exception.reason);
			}
			@finally {
				[m_pdfViewCtrl DocUnlockRead];
			}
			
			annotRect.origin.x = annotRect.origin.x + [m_pdfViewCtrl GetHScrollPos];
			annotRect.origin.y = annotRect.origin.y + [m_pdfViewCtrl GetVScrollPos];
			
			[selectionRectContainerView setHidden:YES];
			
			[m_pdfViewCtrl->ContainerView addSubview:selectionRectContainerView];
			m_select_rect_on_subview = YES;
			
			selectionRectContainerView.frame = annotRect;
		}
		

		colorSwatchController.preferredContentSize = colorSwatchController.view.bounds.size;
		colorSwatchController.modalPresentationStyle = UIModalPresentationPopover;
		colorSwatchController.popoverPresentationController.delegate = self;
		colorSwatchController.popoverPresentationController.sourceRect = [self.viewController.view convertRect:m_annnot_rect fromView:m_pdfViewCtrl];
		colorSwatchController.popoverPresentationController.sourceView = [self viewController].view;
		[[self viewController] presentViewController:colorSwatchController animated: YES completion: nil];

	}
	@catch (NSException *exception) {
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally {
		[m_pdfViewCtrl DocUnlockRead];
	}
}

-(void)editFreeTextSize
{
    [self hideMenu];
    [self attachFreeTextFontSizeMenuItems];
    
    [self showSelectionMenu:m_annnot_rect];
}

-(void)editFreeTextColor
{
    m_fill_color = true;
    [self showColorPicker];
}

-(void)editSelectedAnnotationStrokeColor
{
    m_fill_color = false;
    [self showColorPicker];
}

-(void)editSelectedAnnotationFillColor
{
    m_fill_color = true;
    [self showColorPicker];
}

-(void)saveNewNoteForMovingAnnotationWithString:(NSString*)str
{
	[super saveNewNoteForMovingAnnotationWithString:str];
	//[self showSelectionMenu:m_annnot_rect];
}

-(void)noteEditCancelButtonPressed
{
	[[self viewController] dismissViewControllerAnimated:YES completion:Nil];

	[self showSelectionMenu:m_annnot_rect];
}

- (void)colorPickerViewController:(UIViewController*)colorPicker didSelectColor:(UIColor *)color {
    
    @try
    {
        [m_pdfViewCtrl DocLock:YES];


		if( !m_fill_color ) // stroke colour
        {
            
            PTColorPt* cp = [ColorDefaults colorPtFromUIColor:color];
            
            if( CGColorGetNumberOfComponents(color.CGColor) > 3 && ![color isEqual:[UIColor clearColor]] )
                [m_moving_annotation SetColor:cp numcomp:3];
            else
                [m_moving_annotation SetColor:cp numcomp:0];
			
            [m_moving_annotation RefreshAppearance];
            
            [m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
            
			
        }
		else if( [m_moving_annotation GetType] == e_ptFreeText)
		{
            
            PTFreeText* ft = [[PTFreeText alloc] initWithAnn:m_moving_annotation];
            
            PTColorPt* cp = [ColorDefaults colorPtFromUIColor:color];
            
            if( CGColorGetNumberOfComponents(color.CGColor) > 3 && ![color isEqual:[UIColor clearColor]] )
                [ft SetTextColor:cp col_comp:3];
            else
                [ft SetTextColor:cp col_comp:0];
            
            [ft RefreshAppearance];
            
            [m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
		}
        if (m_fill_color)
        {
            PTColorPt* cp = [ColorDefaults colorPtFromUIColor:color];
            
            PTMarkup* markup = [[PTMarkup alloc] initWithAnn:m_moving_annotation];
            
			
            if( CGColorGetNumberOfComponents(color.CGColor) > 3 && ![color isEqual:[UIColor clearColor]] )
			{
                [markup SetInteriorColor:cp CompNum:3];
			}
            else
			{
                [markup SetInteriorColor:cp CompNum:0];
			}
            
            [markup RefreshAppearance];
            
            [m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];

            
        }
		
        
        [[self viewController] dismissViewControllerAnimated:YES completion:nil];
        
    
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }
	
	[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
	
	[self showSelectionMenu:m_annnot_rect];
}

-(void)deselectAnnotation
{
	BOOL modified = NO;
    if( [m_moving_annotation IsValid] && [m_moving_annotation GetType] == e_ptFreeText )
    {
        @try
        {
            [m_pdfViewCtrl DocLock:YES];
        
            //requires a write lock for refresh appearance
            [selectionRectContainerView setAnnotationContents:m_moving_annotation];
			
			modified = YES;
            
            [m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
        
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@: %@",exception.name, exception.reason);
        }
        @finally {
            [m_pdfViewCtrl DocUnlock];
        }
    }
    
    [selectionRectContainerView setHidden:YES];
	
	if( modified )
		[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
	
	if (m_moving_annotation) {
        m_moving_annotation = 0;
    }
    

    
}

-(void)editSelectedAnnotationOpacity
{
    [self hideMenu];
    [self attachOpacityMenuItems];
    [self showSelectionMenu:m_annnot_rect];
}

-(void)setAnnotationOpacity:(double)alpha
{
    
    [self attachInitialMenuItemsForAnnotType:[m_moving_annotation GetType]];
    
    @try
    {
        [m_pdfViewCtrl DocLock:YES];
    
        PTMarkup* annot = [[PTMarkup alloc] initWithAnn:m_moving_annotation];
        
        [annot SetOpacity:alpha];
        
        
        [m_moving_annotation RefreshAppearance];

        [m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
        
        PTAnnotType annotType = [m_moving_annotation GetType];
        
		[ColorDefaults setDefaultOpacity:(double)alpha forAnnotType:annotType];

    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }
	
	[self showSelectionMenu:m_annnot_rect];
	
	[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
}


-(void)SetAnnotationRect:(PTAnnot*)annot Rect:(CGRect)rect OnPage:(int)pageNumber
{
	// CGRect's origin is at the top-left corner
	PTPDFPoint* sceenUpperLeft = [[PTPDFPoint alloc] init];
	PTPDFPoint* sceenLowerRight = [[PTPDFPoint alloc] init];
	
	[sceenUpperLeft setX:rect.origin.x];
	[sceenUpperLeft setY:rect.origin.y];
	
	[sceenLowerRight setX:rect.origin.x+rect.size.width];
	[sceenLowerRight setY:rect.origin.y+rect.size.height];
	
	PTPDFPoint* pageUpperLeft = [m_pdfViewCtrl ConvScreenPtToPagePt:sceenUpperLeft page_num:pageNumber];
	PTPDFPoint* pageLowerRight = [m_pdfViewCtrl ConvScreenPtToPagePt:sceenLowerRight page_num:pageNumber];
	
	PTPDFRect* r = [[PTPDFRect alloc] init];
	PTPDFRect* startRect = [annot GetRect];
	
    double startRectWidth = MAX([startRect getX1],[startRect getX2]) - MIN([startRect getX1], [startRect getX2]);
    double startRectHeight = MAX([startRect getY1], [startRect getY2]) - MIN([startRect getY1], [startRect getY2]);
    if ([annot GetFlag: e_ptno_zoom])
	{
		[r SetX1: [pageUpperLeft getX]];
		[r SetY1: [pageUpperLeft getY] - startRectHeight];
		[r SetX2: [pageUpperLeft getX] + startRectWidth];
		[r SetY2: [pageUpperLeft getY]];
	}
	else
	{
		[r SetX1:[pageUpperLeft getX]];
		[r SetY1:[pageUpperLeft getY]];
		[r SetX2:[pageLowerRight getX]];
		[r SetY2:[pageLowerRight getY]];
	}
	[r Normalize];
	
	@try
	{
		[m_pdfViewCtrl DocLock:YES];
		
		PTPDFRect* old_rect = [m_pdfViewCtrl GetScreenRectForAnnot:annot page_num:pageNumber];
		[old_rect Normalize];
		
		[annot Resize:r];
		
		[m_pdfViewCtrl UpdateWithRect:old_rect];
		[m_pdfViewCtrl UpdateWithAnnot:annot page_num:pageNumber];
		
	}
	@catch (NSException *exception) {
		NSLog(@"Exception: %@: %@",[exception name], [exception reason]);
	}
	@finally {
		[m_pdfViewCtrl DocUnlock];
	}
	
	[self annotationModified:annot onPageNumber:pageNumber];
	
}




-(void)setAnnotOpacity00
{
    [self setAnnotationOpacity:0.0];
}

-(void)setAnnotOpacity25
{
    [self setAnnotationOpacity:0.25];
}

-(void)setAnnotOpacity50
{
    [self setAnnotationOpacity:0.50];
}

-(void)setAnnotOpacity75
{
    [self setAnnotationOpacity:0.75];
}

-(void)setAnnotOpacity10
{
    [self setAnnotationOpacity:1.0];
}

-(void)setAnnotBorder05
{
    [self setAnnotationBorder:0.5];
}

-(void)setAnnotBorder10
{
    [self setAnnotationBorder:1.0];
}

-(void)setAnnotBorder15
{
    [self setAnnotationBorder:1.5];
}

-(void)setAnnotBorder30
{
    [self setAnnotationBorder:3.0];
}

-(void)setAnnotBorder50
{
    [self setAnnotationBorder:5.0];
}

-(void)setAnnotBorder90
{
    [self setAnnotationBorder:9.0];
}

-(void)setFreeTextSize8
{
	[self setFreeTextSize:8.0];
}

-(void)setFreeTextSize11
{
	[self setFreeTextSize:11.0];
}

-(void)setFreeTextSize16
{
	[self setFreeTextSize:16.0];
}

-(void)setFreeTextSize24
{
	[self setFreeTextSize:24.0];
}

-(void)setFreeTextSize36
{
	[self setFreeTextSize:36.0];
}

@end
