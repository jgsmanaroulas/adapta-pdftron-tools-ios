//
//  Copyright 2011-12 PDFTron Systems Inc. All rights reserved.
//

#import "NoFadeTiledLayer.h"


@implementation NoFadeTiledLayer

+ (CFTimeInterval)fadeDuration
{
    return 0.0;
}

@end
