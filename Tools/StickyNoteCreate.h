//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "CreateToolBase.h"

#define STICKY_NOTE_SIZE 20

@interface StickyNoteCreate : CreateToolBase {
    BOOL creating;
	BOOL m_initialSticky;
}


@end
