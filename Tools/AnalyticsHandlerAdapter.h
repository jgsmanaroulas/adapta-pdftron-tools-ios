//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>

#define ANALYTICS_LOG(__EXCEPTION, __EXTRADATA) [[AnalyticsHandlerAdapter getInstance] logException:__EXCEPTION withExtraData:__EXTRADATA];

/**
 This class is used to easily add an analytics framework to tools.
 Events of interest are be passed to (a subclass of) this generic class,
 and can then passed on to the analytics framework(s) of your choice.
 */
@interface AnalyticsHandlerAdapter : NSObject

// Current instance of AnalyticsHandlerAdapter
+ (id) getInstance;
+ (void) setInstance:(AnalyticsHandlerAdapter*)value;

/**
 Sets a string for identifying the user.
 
 @param identifier the string identifying the user.
 */
- (void)setUserIdentifier:(NSString *)identifier;

/** 
 * Performs sdk specific initializations
 *
 */
- (void)initializeHandler;

- (BOOL) logException:(NSException *)exception withExtraData:(NSDictionary *)extraData;

- (BOOL) sendCustomEventWithTag:(NSString *)tag;

@end
