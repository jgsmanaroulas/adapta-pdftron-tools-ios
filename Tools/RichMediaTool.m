//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "RichMediaTool.h"

@implementation RichMediaTool

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(BOOL)supportedMediaType:(NSString*)extention
{
	NSArray* supportedFormats = @[@"mov", @"mp4", @"m4v", @"3gp"];
	for (NSString* str in supportedFormats) {
		if( [extention isEqualToString:str] )
			return true;
	}
	return false;
}

- (BOOL)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
	@try {
		[m_pdfViewCtrl DocLockRead];

	
		if (m_moving_annotation) {
			m_moving_annotation = 0;
		}
		
		CGPoint down = [gestureRecognizer locationInView:m_pdfViewCtrl];
		
		m_moving_annotation = [m_pdfViewCtrl GetAnnotationAt:down.x y:down.y distanceThreshold:GET_ANNOT_AT_DISTANCE_THRESHOLD minimumLineWeight:GET_ANNOT_AT_MINIMUM_LINE_WEIGHT];
		m_annot_page_number = [m_pdfViewCtrl GetPageNumberFromScreenPt:down.x y:down.y];

		PTObj* annotObj = [m_moving_annotation GetSDFObj];
		PTObj* content = [annotObj FindObj:@"RichMediaContent"];
		
		if( [content IsValid] )
		{

			PTObj* assetsObj = [content FindObj:@"Assets"];

			if( [assetsObj IsValid] )
			{

				PTNameTree* assets = [[PTNameTree alloc] initWithName_tree:assetsObj];

				for(PTDictIterator* itr = [assets GetIterator]; [itr HasNext]; [itr Next] )
				{
					NSString* asset_name = [[itr Key] GetAsPDFText];
					
					// prefix file name so that files with no name such as ".mp4" will still play
					asset_name = [@"a" stringByAppendingString:asset_name];
					
					BOOL supportedType = [self supportedMediaType:asset_name.pathExtension];
										
					if( !supportedType )
					{
						continue;
					}
					else
					{
						if( self.moviePlayer )
						{
							
							NSFileManager* fileManager = [NSFileManager defaultManager];
							[fileManager removeItemAtPath:self.moviePath error:NULL];
						}
						
						self.moviePath = [NSTemporaryDirectory() stringByAppendingPathComponent:asset_name];

						BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:self.moviePath];

						if( !fileExists )
						{
							PTFileSpec* fileSpec = [[PTFileSpec alloc] initWithF:[itr Value]];
							PTFilter* data = [fileSpec GetFileData];
							[data WriteToFile:self.moviePath append:false];
						}

						PTPDFRect* rect = [m_moving_annotation GetRect];
						
						CGRect screenRect = [self PDFRectPage2CGRectScreen:rect PageNumber:m_annot_page_number];
						
						screenRect.origin.x += [m_pdfViewCtrl GetHScrollPos];
						screenRect.origin.y += [m_pdfViewCtrl GetVScrollPos];
						
						self.moviePlayer = [[AVPlayerViewController alloc] init];
						self.moviePlayer.player = [AVPlayer playerWithURL:[NSURL fileURLWithPath:self.moviePath]];
						(self.moviePlayer.view).frame = CGRectMake(screenRect.origin.x, screenRect.origin.y, screenRect.size.width, screenRect.size.height);
						[self.moviePlayer.player play];
						[m_pdfViewCtrl->ContainerView addSubview:self.moviePlayer.view];

						break;
					}
					
				}
			}

			return YES;
		}
		
		return NO;
		}
	@catch (NSException *exception) {
		//log
	}
	@finally {
		[m_pdfViewCtrl DocUnlockRead];
	}
	
}

-(void) onLayoutChanged
{
	PTPDFRect* rect;
	CGRect screenRect;
	@try
    {
        [m_pdfViewCtrl DocLockRead];
        rect = [m_moving_annotation GetRect];
		
        screenRect = [self PDFRectPage2CGRectScreen:rect PageNumber:m_annot_page_number];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }
    
    screenRect.origin.x += [m_pdfViewCtrl GetHScrollPos];
    screenRect.origin.y += [m_pdfViewCtrl GetVScrollPos];

	(self.moviePlayer.view).frame = CGRectMake(screenRect.origin.x, screenRect.origin.y, screenRect.size.width, screenRect.size.height);

	[super onLayoutChanged];
}

-(void)dealloc
{
	if( self.moviePlayer )
	{
		[self.moviePlayer.view removeFromSuperview];
		
		NSFileManager* fileManager = [NSFileManager defaultManager];
		[fileManager removeItemAtPath:self.moviePath error:NULL];
	}
	
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
