//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>


typedef enum {
    e_topleft,
    e_top,
    e_topright,
    e_right,
    e_bottomright,
    e_bottom,
    e_bottomleft,
    e_left
} Location;


@interface ResizeWidgetView : UIView {
    
    @public
    Location m_location;
}

- (instancetype)initAtPoint:(CGPoint)point WithLocation:(Location)loc NS_DESIGNATED_INITIALIZER;

- (instancetype)initWithFrame:(CGRect)frame __attribute__((unavailable("Not the designated initializer")));
- (instancetype)initWithCoder:(NSCoder *)aDecoder __attribute__((unavailable("Not the designated initializer")));

+(int)getLength;

@end
