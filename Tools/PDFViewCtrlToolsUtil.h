//
//  Util.h
//  Tools
//
//  Created by PDFTron on 2015-03-17.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PDFViewCtrlToolsUtil : NSObject

+ (UIImage*)toolImageNamed:(NSString*)name;

+ (NSBundle*)toolsBundle;

+ (NSBundle*)toolsStringBundle;



@end
