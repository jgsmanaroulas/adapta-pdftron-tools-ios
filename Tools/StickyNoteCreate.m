//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "StickyNoteCreate.h"
#import "PanTool.h"

@implementation StickyNoteCreate


- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        pdfNetAnnotType = [PTText class];
        
		m_initialSticky = YES;
    }
    
    return self;
}

-(PTAnnotType)annotType
{
	return e_ptText;
}

+(BOOL)createsAnnotation
{
	return YES;
}

-(void)setFrame:(CGRect)frame
{
    self.backgroundColor = [UIColor clearColor];
}

- (BOOL)handleTap:(UITapGestureRecognizer *)sender
{
//    handled by touchesended

    return YES;
}

- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    creating = true;

    return [super onTouchesBegan:touches withEvent:event];
}

- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    return [super onTouchesMoved:touches withEvent:event];
}

- (BOOL)onCustomEvent:(id)userData
{
    if( !m_initialSticky && self.backToPanToolAfterUse )
    {
        nextToolType = [PanTool class];
        return NO;
    }
	else if( m_initialSticky )
	{
		endPoint = startPoint = m_down;
		pageNumber = [m_pdfViewCtrl GetPageNumberFromScreenPt:startPoint.x y:startPoint.y];
		[super onTouchesEnded:Nil withEvent:Nil];
		m_initialSticky = NO;
		[m_pdfViewCtrl postCustomEvent:Nil];
		return YES;
	}
    else
	{
        return YES;
	}
}

- (BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    creating = false;
    UITouch *touch = touches.allObjects[0];

    
    endPoint = [touch locationInView:m_pdfViewCtrl];

    pageNumber = [m_pdfViewCtrl GetPageNumberFromScreenPt:endPoint.x y:endPoint.y];

    if( pageNumber < 1 )
    {
        self.hidden = YES;
        
        m_initialSticky = NO;
        return YES;
        
    }
    else
    {
        BOOL returning = [super onTouchesEnded:touches withEvent:event];
        
        self.hidden = YES;
        
        m_initialSticky = NO;
        
    return returning;
    }
}

- (BOOL)onTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{

    creating = false;
	UITouch *touch = touches.allObjects[0];
    
    endPoint = [touch locationInView:m_pdfViewCtrl];
    
    endPoint = [super boundToPageScreenPoint:endPoint withPaddingLeft:0 right:STICKY_NOTE_SIZE bottom:STICKY_NOTE_SIZE top:0];

    
    BOOL returning = [super onTouchesEnded:touches withEvent:event];
    
    self.hidden = YES;
	
	m_initialSticky = NO;
    
    return returning;
}


@end
