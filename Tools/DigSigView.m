//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "DigSigView.h"
#import <math.h>
#import "PDFViewCtrlToolsUtil.h"

@interface DigSigView()
{
	CALayer* _topBorder;
	UILabel* _signHereLabel;
}

@end

@implementation DigSigView

@synthesize m_dig_sig_points;
@synthesize m_boundingRect;
@synthesize strokeColor;
@synthesize strokeThickness;

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [self initWithFrame:CGRectZero withColour:[UIColor blackColor] withStrokeThickness:2.0];
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame withColour:(UIColor*)color withStrokeThickness:(CGFloat)thickness
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        m_boundingRect = CGRectZero;
        m_rightMost = m_bottomMost = 0;
        m_leftMost = frame.size.width;
        m_topMost = frame.size.height;
        self.strokeColor = color;
        self.strokeThickness = thickness;
        
        // sign here label
        _signHereLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height*2/3, frame.size.width, 30)];
        
        NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
        
        _signHereLabel.text = NSLocalizedStringFromTableInBundle(@"Sign Here", @"PDFNet-Tools", toolsStringBundle, @"Where to drag their finger to apply a signature to the document.");
        _signHereLabel.textAlignment = NSTextAlignmentCenter;
        
        // line to sign on
        _topBorder = [CALayer layer];
        _topBorder.frame = CGRectMake(0.0f, 0.0f, frame.size.width, 1.0f);
        _topBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
        [_signHereLabel.layer addSublayer:_topBorder];
        [self addSubview:_signHereLabel];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [self initWithFrame:(CGRect)frame withColour:[UIColor blackColor] withStrokeThickness:2.0];
    return self;
}

- (void)layoutSubviews {
	// resize your layers based on the view's new bounds
	_signHereLabel.frame = CGRectMake(0, self.frame.size.height*2/3, self.frame.size.width, 30);
	_topBorder.frame = CGRectMake(0.0f, 0.0f, self.frame.size.width, 1.0f);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = touches.allObjects[0];
    
    m_startPoint = [touch locationInView:self];
	
    CGPoint pagePoint = CGPointMake(m_startPoint.x, m_startPoint.y);
    
	if( !m_dig_sig_points )
	{
		m_dig_sig_points = [[NSMutableArray alloc] initWithCapacity:50];
		m_numberOfPoints = 0;
	}
	
	m_boundingRect = CGRectMake(m_startPoint.x, m_startPoint.y, 1, 1);
    
    [m_dig_sig_points addObject:[NSValue valueWithCGPoint:pagePoint]];
    
    m_numberOfPoints++;

}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = touches.allObjects[0];
    
    m_currentPoint = [touch locationInView:self];
    CGPoint pagePoint = CGPointMake(m_currentPoint.x, m_currentPoint.y);
    
    [m_dig_sig_points addObject:[NSValue valueWithCGPoint:pagePoint]];
    
    m_numberOfPoints++;
	

	m_leftMost = MIN(m_leftMost, m_currentPoint.x);
	m_rightMost = MAX(m_rightMost, m_currentPoint.x);
	m_topMost = MIN(m_topMost, m_currentPoint.y);
	m_bottomMost = MAX(m_bottomMost, m_currentPoint.y);
	
    [self setNeedsDisplay];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	[m_dig_sig_points addObject:[NSValue valueWithCGPoint:CGPointZero]];
    
    m_numberOfPoints++;
	
	[self setNeedsDisplay];
	
	m_leftMost = MIN(m_leftMost, m_currentPoint.x);
	m_rightMost = MAX(m_rightMost, m_currentPoint.x);
	m_topMost = MIN(m_topMost, m_currentPoint.y);
	m_bottomMost = MAX(m_bottomMost, m_currentPoint.y);
	
	m_boundingRect = CGRectMake(m_leftMost, m_topMost, m_rightMost-m_leftMost, m_bottomMost-m_topMost);
    
}

-(void)setupContext
{
	if( m_context )
	{
		CGContextSetLineWidth(m_context, self.strokeThickness);
        CGContextSetStrokeColorWithColor(m_context, self.strokeColor.CGColor);
		CGContextSetLineCap(m_context, kCGLineCapRound);
		CGContextSetLineJoin(m_context, kCGLineJoinRound);
		CGContextSetAlpha(m_context, 1.0);
	}
}

-(CGPoint) midPoint:(CGPoint)p1  p2:(CGPoint)p2
{
    return CGPointMake((p1.x + p2.x) * 0.5, (p1.y + p2.y) * 0.5);
}

- (void)drawRect:(CGRect)rect
{
    if( m_dig_sig_points )
    {
        NSValue* val;
        CGPoint previousPoint1 = CGPointZero, previousPoint2 = CGPointZero, currentPoint;
        
        m_context = UIGraphicsGetCurrentContext();
        
		if( ! m_context )
			return;
        
        [self setupContext];
        
		CGPoint firstPoint = val.CGPointValue;
		CGContextMoveToPoint(m_context, firstPoint.x, firstPoint.y);
		previousPoint1 = CGPointZero, previousPoint2 = CGPointZero;
		
		for (NSValue* val in m_dig_sig_points)
		{
			currentPoint = val.CGPointValue;
			if( !CGPointEqualToPoint(CGPointZero, currentPoint) )
			{
				if( CGPointEqualToPoint(previousPoint1, CGPointZero))
					previousPoint1 = currentPoint;
				
				if( CGPointEqualToPoint(previousPoint2, CGPointZero))
					previousPoint2 = currentPoint;
				
				CGPoint mid1 = [self midPoint:previousPoint1 p2:previousPoint2];
				CGPoint	mid2 = [self midPoint:currentPoint p2:previousPoint1];
				
				CGContextMoveToPoint(m_context, mid1.x, mid1.y);
				
				CGContextAddQuadCurveToPoint(m_context, previousPoint1.x, previousPoint1.y, mid2.x, mid2.y);
			}
			else
			{
				CGContextMoveToPoint(m_context, currentPoint.x, currentPoint.y);
				previousPoint1 = CGPointZero, previousPoint2 = CGPointZero;
			}
			
			previousPoint2 = previousPoint1;
			previousPoint1 = currentPoint;
		}
        
        CGContextStrokePath(m_context);
    }
}

@end
