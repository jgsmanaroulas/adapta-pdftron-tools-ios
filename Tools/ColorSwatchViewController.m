//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "ColorSwatchViewController.h"
#import "ColorSwatchesView.h"
#import "ColorDefaults.h"

@interface ColorSwatchViewController ()

@end

@implementation ColorSwatchViewController

@synthesize annotationType;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	//[self.view addSubview:[[ColorSwatchesView alloc] init]];
	NSMutableArray* colors;
	
	if( annotationType == e_ptUnderline ||
	    annotationType == e_ptStrikeOut ||
	    annotationType == e_ptSquiggly ) {
		
		colors = [@[[UIColor colorWithRed:1 green:0 blue:0 alpha:1],      //red
					[UIColor colorWithRed:1 green:0.6667 blue:0 alpha:1], //orange
					[UIColor colorWithRed:1 green:1 blue:0 alpha:1],      //yellow
					[UIColor colorWithRed:0 green:1 blue:0 alpha:1],      //green
					[UIColor colorWithRed:0 green:0 blue:1 alpha:1],      //blue
					[UIColor colorWithRed:1 green:0 blue:1 alpha:1],      //violet
					[UIColor colorWithRed:0 green:0 blue:0 alpha:1]]     //black
				  mutableCopy];
		
		
	} else if ( annotationType == e_ptHighlight ) {
		
		colors = [@[[UIColor colorWithRed:1 green:0 blue:0 alpha:1],      //red
					[UIColor colorWithRed:1 green:0.6667 blue:0 alpha:1], //orange
					[UIColor colorWithRed:1 green:1 blue:0 alpha:1],      //yellow
					[UIColor colorWithRed:0 green:1 blue:0 alpha:1],      //green
					[UIColor colorWithRed:0 green:0 blue:1 alpha:1],      //blue
					[UIColor colorWithRed:1 green:0 blue:1 alpha:1],      //violet
					[UIColor colorWithRed:0 green:1 blue:1 alpha:1],      //teal
					[UIColor colorWithRed:0.733 green:0.733 blue:0.733 alpha:1]]	//grey
					mutableCopy];
		
	} else {
		
		colors = [@[[UIColor colorWithRed:1 green:0 blue:0 alpha:1],      //red
					[UIColor colorWithRed:1 green:0.6667 blue:0 alpha:1], //orange
					[UIColor colorWithRed:1 green:1 blue:0 alpha:1],      //yellow
					[UIColor colorWithRed:0 green:1 blue:0 alpha:1],      //green
					[UIColor colorWithRed:0 green:0 blue:1 alpha:1],      //blue
					[UIColor colorWithRed:1 green:0 blue:1 alpha:1]]       //violet
				  mutableCopy];
		
		if( self.forFillColor )
		{
			// creates the transparency grid
			UIView* holder = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
			UIView* lightGray1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
			UIView* lightGray2 = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 10, 10)];
			lightGray1.backgroundColor = [UIColor colorWithWhite:0.4 alpha:1];
			lightGray2.backgroundColor = [UIColor colorWithWhite:0.4 alpha:1];
			UIView* darkGray1 = [[	UIView alloc] initWithFrame:CGRectMake(10, 0, 10, 10)];
			UIView* darkGray2 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 10, 10)];
			lightGray1.backgroundColor = [UIColor colorWithWhite:0.6 alpha:1];
			lightGray2.backgroundColor = [UIColor colorWithWhite:0.6 alpha:1];
			[holder addSubview:lightGray1];
			[holder addSubview:lightGray2];
			[holder addSubview:darkGray1];
			[holder addSubview:darkGray2];
			UIImage* squares = [self imageWithView:holder];
			
			[colors addObjectsFromArray:@[[UIColor colorWithRed:0 green:0 blue:0 alpha:1],//black
										  [UIColor colorWithPatternImage:squares]]]; //transparent
			
		}
		else
		{
			[colors addObjectsFromArray:@[[UIColor brownColor],  //brown
										  [UIColor colorWithRed:0 green:0 blue:0 alpha:1]]]; //black
		}
		
		
	}
	
	float spacing = 10.0;
	CGPoint origin = CGPointMake(spacing, spacing);
	const CGSize size = CGSizeMake(50.0, 50.0);
	int ncols = MIN(4, (int)[colors count]);
	int nrows = (int)colors.count/ncols + MIN(1,(int)[colors count]%ncols);
	
	self.view.frame = CGRectMake(0, 0, ncols*(50.0+spacing)+spacing, nrows*(50.0+spacing)+spacing);
	
	
	for (UIColor* color in colors) {
		
		UIButton* colorSwatchButton = [UIButton buttonWithType:UIButtonTypeCustom];
		colorSwatchButton.frame = CGRectMake(origin.x, origin.y, size.width, size.height);
		colorSwatchButton.backgroundColor = color;
		
		
		(colorSwatchButton.layer).borderColor = [UIColor blackColor].CGColor;
		(colorSwatchButton.layer).borderWidth = 0.5;
		
		[colorSwatchButton addTarget:self action:@selector(pickedColor:) forControlEvents:UIControlEventTouchUpInside];
		[self.view addSubview:colorSwatchButton];
		
		origin.x += size.width + spacing;
		
		if( origin.x > (size.width + spacing)*ncols )
		{
			origin.x = spacing;
			origin.y += size.height + spacing;
		}
	}
}

-(UIImage*)imageWithView:(UIView*)view
{
	UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0);
	[view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage* img = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return img;
}

-(void)pickedColor:(UIButton*)button
{
	// save as new default

	NSString *attribute;
	
	if( self.forFillColor )
		attribute = ATTRIBUTE_FILL_COLOR;
	else
		attribute = ATTRIBUTE_STROKE_COLOR;
	
	
	CGRect endFrame = button.frame;
	CGRect bigFrame = CGRectInset(endFrame, -4, -4);
	
	[UIView animateWithDuration:0.1 animations:^(){
		button.frame = bigFrame;
	} completion:^(BOOL finished) {
		 [UIView animateWithDuration:0.1 animations:^(){
			  button.frame = endFrame;
		  }];
	 }];
	
	UIColor* color = button.backgroundColor;
	
	// all values will remain if the transaprent colour was chosen because
	// it's made via a pattern and does not have the "correct" colour space.
	CGFloat r, g, b, a = -1;
	[color getRed:&r green:&g blue:&b alpha:&a];
	if( a <= 0 )
	{
		color = [UIColor clearColor];
	}
	
	if( annotationType )
		[ColorDefaults setDefaultColor:color forAnnotType:annotationType attribute:attribute colorPostProcessMode:e_ptpostprocess_none];

	[self.delegate colorPickerViewController:self didSelectColor:color];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
