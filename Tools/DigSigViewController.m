//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "DigSigViewController.h"
#import "DigSigView.h"
#import "PDFViewCtrlToolsUtil.h"

@implementation DigSigViewController

@synthesize delegate;
@synthesize m_image;
@synthesize strokeThickness;
@synthesize strokeColor;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.strokeThickness = 2;
        self.strokeColor = UIColor.blackColor;
        self.allowDigitalSigning = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	self.view.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];

	CGRect frame = self.view.frame;
	frame.size.width += 100;
	self.view.frame = frame;
	
	CGRect baseFrame;
	
	if( [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone )
	{
		if( ([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeLeft) ||
		   ([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeRight))
		{
			baseFrame = CGRectMake(10, 120, [UIScreen mainScreen].bounds.size.height-20, [UIScreen mainScreen].bounds.size.width-200);
		}
		else
		{
			baseFrame = CGRectMake(10, 120, [UIScreen mainScreen].bounds.size.width-20, [UIScreen mainScreen].bounds.size.height-240);
		}
	}
	else
	{
		baseFrame = CGRectMake(10, 100, 520, 400);
	}
    
    UIView *container = [[UIView alloc] initWithFrame:CGRectZero];
    container.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:container];
	
    if (@available(iOS 11, *)) {
        // iPhone X - respect safe area.
        UILayoutGuide *safeArea = self.view.safeAreaLayoutGuide;
        [NSLayoutConstraint activateConstraints:
         @[
           [container.leadingAnchor constraintEqualToAnchor:safeArea.leadingAnchor],
           [container.widthAnchor constraintEqualToAnchor:safeArea.widthAnchor],
           [container.topAnchor constraintEqualToAnchor:safeArea.topAnchor],
           [container.heightAnchor constraintEqualToAnchor:safeArea.heightAnchor],
           ]];
    } else {
        [NSLayoutConstraint activateConstraints:
         @[
           [container.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
           [container.widthAnchor constraintEqualToAnchor:self.view.widthAnchor],
           [container.topAnchor constraintEqualToAnchor:self.view.topAnchor],
           [container.heightAnchor constraintEqualToAnchor:self.view.heightAnchor],
           ]];
    }
    
	m_digSigView = [[DigSigView alloc] initWithFrame:CGRectZero];
	
	m_digSigView.backgroundColor = [UIColor whiteColor];
	[container addSubview:m_digSigView];
    
    m_digSigView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:|-10-[m_digSigView]-10-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(m_digSigView)]];
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:|-120-[m_digSigView]-60-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(m_digSigView)]];
    
	const int fontSize = 18;
	UITextView* aboutBlurb = [[UITextView alloc] initWithFrame:CGRectZero];
	aboutBlurb.backgroundColor = [UIColor clearColor];
	
	NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
	
	NSString* signOrPick = NSLocalizedStringFromTableInBundle(@"Sign in the white area below, or pick a photo.", @"PDFNet-Tools", toolsStringBundle, @"For digital signatures.");
	
	aboutBlurb.text = signOrPick;
	aboutBlurb.editable = NO;
	aboutBlurb.font = [UIFont fontWithName:@"Helvetica" size:fontSize];
	[container addSubview:aboutBlurb];
    
    aboutBlurb.translatesAutoresizingMaskIntoConstraints = NO;
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:|-10-[aboutBlurb]-10-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(aboutBlurb)]];
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:|-15-[aboutBlurb]-10-[m_digSigView]"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(aboutBlurb,m_digSigView)]];
    
	
	// add button to choose photo
	UIButton *button3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[button3 addTarget:self action:@selector(showPhotoPicker)
	  forControlEvents:UIControlEventTouchUpInside];
	
	NSString* pickAPhoto = NSLocalizedStringFromTableInBundle(@"Pick a Photo", @"PDFNet-Tools", toolsStringBundle, @"For digital signatures.");
	
	
	[button3 setTitle:pickAPhoto forState:UIControlStateNormal];
    button3.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;

	[container addSubview:button3];
    
    button3.translatesAutoresizingMaskIntoConstraints = NO;
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:[button3(150)]-10-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button3)]];
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:[button3(25)]-10-[m_digSigView]"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button3,m_digSigView)]];
    
    
	
	// add button to clear appearance
	UIButton *button4 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[button4 addTarget:self action:@selector(resetDigSigView)
	  forControlEvents:UIControlEventTouchUpInside];
	
    
	NSString* clearSig = NSLocalizedStringFromTableInBundle(@"Clear", @"PDFNet-Tools", toolsStringBundle, @"Clear a signature");

	[button4 setTitle:clearSig forState:UIControlStateNormal];
    button4.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
	[container addSubview:button4];
    
    button4.translatesAutoresizingMaskIntoConstraints = NO;
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:|-10-[button4(70)]"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button4)]];
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:[button4(25)]-10-[m_digSigView]"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button4,m_digSigView)]];
	
	// add button for saving appearance
	UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[button addTarget:self action:@selector(saveAppearanceOnly)
	forControlEvents:UIControlEventTouchUpInside];
	
	NSString* saveAppearance = NSLocalizedStringFromTableInBundle(@"Save Appearance", @"PDFNet-Tools", toolsStringBundle, @"Save signature appearance.");
	
	[button setTitle:saveAppearance forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;

	[container addSubview:button];
    
    button.translatesAutoresizingMaskIntoConstraints = NO;
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:[button(150)]-10-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button)]];
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:[m_digSigView]-10-[button(25)]"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button,m_digSigView)]];
	
	// add button for digitally signing
    if( self.allowDigitalSigning )
    {
        UIButton *button5 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button5 addTarget:self action:@selector(digitallySign)
        forControlEvents:UIControlEventTouchUpInside];
        
        NSString* digitallySign = NSLocalizedStringFromTableInBundle(@"Digitally Sign", @"PDFNet-Tools", toolsStringBundle, @"");

        [button5 setTitle:digitallySign forState:UIControlStateNormal];
        if( [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone )
        {
            button5.frame = CGRectMake(baseFrame.size.width-140, baseFrame.size.height+177, 150.0, 40.0);
        }
        else
        {
            button5.frame = CGRectMake(220.0, 550.0, 160.0, 40.0);
        }
        [container addSubview:button5];
    }
	
	// add button to cancel
	UIButton *button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[button2 addTarget:delegate action:@selector(closeSignatureDialog)
	forControlEvents:UIControlEventTouchUpInside];
	[button2 setTitle:NSLocalizedStringFromTableInBundle(@"Cancel", @"PDFNet-Tools", toolsStringBundle, @"") forState:UIControlStateNormal];
    button2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
	[container addSubview:button2];
    button2.translatesAutoresizingMaskIntoConstraints = NO;
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:|-10-[button2(150)]"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button2)]];
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:[m_digSigView]-10-[button2(25)]"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button2,m_digSigView)]];
}

-(void)resetDigSigView
{
	// clears paths
	[m_digSigView.m_dig_sig_points removeAllObjects];
	
	// clears image
    for(UIView* subView in m_digSigView.subviews)
        if( [subView isMemberOfClass:[UIImageView class]])
        {
            [subView removeFromSuperview];
            break;
        }

	m_image = Nil;
	
	[m_digSigView setNeedsDisplay];
	
	m_digSigView.userInteractionEnabled = YES;
}

-(UIImage*)correctForRotation:(UIImage*)src
{
    UIGraphicsBeginImageContext(src.size);
	
    [src drawAtPoint:CGPointMake(0, 0)];
	
    UIImage* img =  UIGraphicsGetImageFromCurrentImageContext();
	
	UIGraphicsEndImageContext();
	
	return img;
}



-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	[self resetDigSigView];
	
	m_image = [self correctForRotation:info[UIImagePickerControllerOriginalImage]];
	
	UIImageView* imageView = [[UIImageView alloc] initWithImage:m_image];
	imageView.frame = CGRectMake(0, 0, m_digSigView.frame.size.width, m_digSigView.frame.size.height);
	imageView.contentMode = UIViewContentModeScaleAspectFit;
	
	m_digSigView.userInteractionEnabled = NO;
	[m_digSigView addSubview:imageView];
	

	[self dismissViewControllerAnimated:YES completion:Nil];
	
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	[self dismissViewControllerAnimated:YES completion:Nil];
}

-(void)showPhotoPicker
{
	
	UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
	imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
	imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	imagePickerController.delegate = self;
	imagePickerController.modalPresentationStyle = UIModalPresentationPopover;
	
	[self presentViewController:imagePickerController animated:YES completion:Nil];
	
	UIPopoverPresentationController *popController = imagePickerController.popoverPresentationController;
	
	popController.sourceRect = CGRectInset(m_digSigView.frame,m_digSigView.frame.size.width/2-1, m_digSigView.frame.size.height/2-1);
	popController.sourceView = self.view;
}

-(void)digitallySign
{
	[self saveAppearanceOnly];
	[delegate signAndSave];
}

-(void)saveAppearanceOnly
{
	@try
	{
		if( m_image )
			[delegate saveAppearanceWithUIImage:m_image];
		else
			[delegate saveAppearanceWithPath:m_digSigView.m_dig_sig_points fromCanvasSize:CGSizeMake(m_digSigView.frame.size.width, m_digSigView.frame.size.height)];
		
	}
	@catch (NSException *exception)
	{
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
