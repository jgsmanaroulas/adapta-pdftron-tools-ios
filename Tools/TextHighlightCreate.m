//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//


#import "TextHighlightCreate.h"

@implementation TextHighlightCreate

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        annotType = e_ptHighlight;
    }
    
    return self;
}

@end
