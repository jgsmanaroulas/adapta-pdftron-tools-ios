//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "Tool.h"
#import "MagnifierView.h"

@interface TextMarkupCreate : Tool
{
    CGPoint selectionStart;
    CGPoint selectionEnd;
    
    BOOL shouldCancel;
    
    int pageNumber;
    
    NSMutableArray* currentSelection;
    
    PTAnnotType annotType;
    
    TrnMagnifierView* loupe;

    CGPoint selectionStartCorner, selectionEndCorner;
    
    NSMutableArray* selectionLayers;
    
    BOOL selectionOnScreen;
    
    BOOL tapOK;
	
	PTColorPt* _colorPt;
	double _thickness;
	int _compNums;
}

-(NSMutableArray*)GetQuadsFromPage:(int)page;
-(void)DrawSelectionQuads:(NSMutableArray*)quads;

@end
