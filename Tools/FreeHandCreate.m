//
//  Copyright 2012 PDFTron Systems Inc All rights reserved.
//

#import "FreeHandCreate.h"
#import "PanTool.h"
@class PTPDFViewCtrl;


@interface FreeHandCreate ()
{
	int m_startPageNum;
}
@end

@implementation FreeHandCreate

@synthesize multistrokeMode;

-(void)ignorePinch:(UIGestureRecognizer *)gestureRecognizer
{
    return;
}

-(BOOL)handleDoubleTap:(UITapGestureRecognizer*)gestureRecognizer
{
	// ignore
	return YES;
}

CGPoint midPoint(CGPoint p1, CGPoint p2)
{
	
    return CGPointMake((p1.x + p2.x) * 0.5, (p1.y + p2.y) * 0.5);
	
}

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        pdfNetAnnotType = [PTInk class];
        self.opaque = NO;
        
		m_startPageNum = 0; // non-existant page in PDF
		
		UIPinchGestureRecognizer* pgr = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(ignorePinch:)];
  
        [self addGestureRecognizer:pgr];

        self.frame = CGRectMake([m_pdfViewCtrl GetHScrollPos], [m_pdfViewCtrl GetVScrollPos], m_pdfViewCtrl.frame.size.width, m_pdfViewCtrl.frame.size.height);
        
    }

    return self;
}

-(PTAnnotType)annotType
{
	return e_ptInk;
}

- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    if( self.multistrokeMode == YES )
		return NO;
	else
		return [super touchesShouldCancelInContentView:view];
}

+(BOOL)createsAnnotation
{
	return YES;
}

- (void)drawRect:(CGRect)rect
{
    if( m_free_hand_points && (self.multistrokeMode == YES || !self.allowScrolling))
    {
        NSValue* val;
        CGPoint previousPoint1 = CGPointZero, previousPoint2 = CGPointZero, currentPoint;
        
        context = UIGraphicsGetCurrentContext();
        
		if( ! context )
			return;
		
        [super setupContextFromDefaultAnnot:context];
        
        CGContextSetLineJoin(context, kCGLineJoinRound);
        
		for (NSMutableArray* point_array in m_free_hand_strokes)
		{
			CGPoint firstPoint = val.CGPointValue;
			CGContextMoveToPoint(context, firstPoint.x, firstPoint.y);
			previousPoint1 = CGPointZero, previousPoint2 = CGPointZero;
			
			for (NSValue* val in point_array)
			{
				currentPoint = val.CGPointValue;
				
				if( CGPointEqualToPoint(previousPoint1, CGPointZero))
					previousPoint1 = currentPoint;
				
				if( CGPointEqualToPoint(previousPoint2, CGPointZero))
					previousPoint2 = currentPoint;
				
				CGPoint mid1 = midPoint(previousPoint1, previousPoint2);
				CGPoint	mid2 = midPoint(currentPoint, previousPoint1);
				
				CGContextMoveToPoint(context, mid1.x, mid1.y);
				
				CGContextAddQuadCurveToPoint(context, previousPoint1.x, previousPoint1.y, mid2.x, mid2.y);
			
				previousPoint2 = previousPoint1;
				previousPoint1 = currentPoint;
			}
			
		}
        
        CGContextStrokePath(context);
    }
}

-(void)addFreehandPoint:(NSSet*)touches
{
    UITouch *touch = touches.allObjects[0];
    
    CGPoint touchPoint = [touch locationInView:m_pdfViewCtrl];
    
    endPoint = [super boundToPageScreenPoint:touchPoint withThicknessCorrection:_thickness/2];
    
    [m_free_hand_points addObject:[NSValue valueWithCGPoint:endPoint]];
}

- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{

    UITouch *touch = touches.allObjects[0];
    
    startPoint = [touch locationInView:m_pdfViewCtrl];
    
    pageNumber = [m_pdfViewCtrl GetPageNumberFromScreenPt:startPoint.x y:startPoint.y];
    
    if (pageNumber < 1 || (m_startPageNum > 0 && m_startPageNum != pageNumber) ) {
        return YES;
    }
    
	if( context )
		CGContextSetLineJoin(context, kCGLineJoinRound);

    CGPoint pagePoint = CGPointMake(startPoint.x, startPoint.y);
    
	
	m_free_hand_points = [[NSMutableArray alloc] initWithCapacity:50];
	
	[m_free_hand_points addObject:[NSValue valueWithCGPoint:pagePoint]];

	if( !m_free_hand_strokes )
	{
		m_startPageNum = pageNumber;
		
		m_free_hand_strokes = [[NSMutableArray alloc] initWithCapacity:5];
		
		m_redo_strokes = [[NSMutableArray alloc] initWithCapacity:5];
		
		self.frame = CGRectMake([m_pdfViewCtrl GetHScrollPos], [m_pdfViewCtrl GetVScrollPos], m_pdfViewCtrl.bounds.size.width, m_pdfViewCtrl.bounds.size.height);
	}
	
	[m_free_hand_strokes addObject:m_free_hand_points];
	   
    return YES;
}

- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	if (pageNumber < 1 || m_startPageNum != pageNumber) {
        return YES;
    }
	
    [self addFreehandPoint:touches];
    
    [self setNeedsDisplay];
    
    return YES;
}

- (BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{

	if (pageNumber < 1 || m_startPageNum != pageNumber) {
        return YES;
    }
    
    if( m_free_hand_points.count == 1)
    {
        [self addFreehandPoint:touches];
    }
    
	if( !multistrokeMode )
	{
		[self commitAnnotation];
		
		if( self.backToPanToolAfterUse )
		{
			nextToolType = [PanTool class];
			return NO;
		}
		else
			return YES;
	}
	else
	{
		[m_redo_strokes removeAllObjects];
		
		if( [self.delegate respondsToSelector:@selector(strokeAdded:)] )
		{
			[self.delegate strokeAdded:self];
		}
		
		return YES;
	}
}

-(void)commitAnnotation
{
	if( pageNumber > 0 )
	{
		[self keepToolAppearanceOnScreen];
		
		PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
		
		PTInk* inkAnnot;
		
		@try
		{
			[m_pdfViewCtrl DocLock:YES];
			
			PTPage* pg = [doc GetPage:m_startPageNum];
			
			if( [pg IsValid] )
			{
				
				PTPDFRect* myRect = [[PTPDFRect alloc] init];;
				
				
				inkAnnot = [PTInk Create:(PTSDFDoc*)doc pos:myRect];
				
				[super setPropertiesFromDefaultAnnotation: inkAnnot];
				
				int i = 0;
				
				double minX = RAND_MAX, minY = RAND_MAX, maxX = -RAND_MAX, maxY = -RAND_MAX;
				
				int numStrokes = 0;
				
				for (NSMutableArray* point_array in m_free_hand_strokes)
				{

					for (NSValue* point in point_array)
					{
						CGPoint aPoint = point.CGPointValue;
						
						PTPDFPoint* pdfPoint = [[PTPDFPoint alloc] init];
						
						[self ConvertScreenPtToPagePtX:&aPoint.x Y:&aPoint.y PageNumber:m_startPageNum];
						
						[pdfPoint setX:aPoint.x];
						[pdfPoint setY:aPoint.y];
						
						[inkAnnot SetPoint:numStrokes pointindex:i++ pt:pdfPoint];
						
						minX = MIN(minX, aPoint.x);
						minY = MIN(minY, aPoint.y);

						maxX = MAX(maxX, aPoint.x);
						maxY = MAX(maxY, aPoint.y);
					}
					numStrokes++;
					i = 0;
				}

				[myRect SetX1:minX];
				[myRect SetY1:minY];
				
				[myRect setX2:maxX];
				[myRect setY2:maxY];
				
				[inkAnnot SetRect:myRect];
				
				[inkAnnot RefreshAppearance];
				
				if( self.annotationAuthor && self.annotationAuthor.length > 0 && [inkAnnot isKindOfClass:[PTMarkup class]]	)
				{
					[(PTMarkup*)inkAnnot SetTitle:self.annotationAuthor];
				}
			
				[pg AnnotPushBack:inkAnnot];
			}
			
		}
		@catch (NSException *exception) {
			NSLog(@"Exception: %@: %@",exception.name, exception.reason);
		}
		@finally {
			[m_pdfViewCtrl DocUnlock];
		}
		
		m_free_hand_points = 0;
		
		[m_pdfViewCtrl UpdateWithAnnot:inkAnnot page_num:m_startPageNum];
		[m_pdfViewCtrl RequestRendering];
		
		[self annotationAdded:inkAnnot onPageNumber:pageNumber];
		
	}
	
	[m_free_hand_strokes removeAllObjects];
	[m_redo_strokes removeAllObjects];
	[m_free_hand_points removeAllObjects];
	[self setNeedsDisplay];
}

- (BOOL)handleTap:(UITapGestureRecognizer *)sender
{
	if( !multistrokeMode )
	{
		return [super handleTap:sender];
	}
	
	[self setNeedsDisplay];
	return YES;
}

#pragma mark - multistroke undo/redo methods

-(void)undoStroke
{
	if( m_free_hand_strokes.count )
	{
		[m_redo_strokes addObject:m_free_hand_strokes.lastObject];
		[m_free_hand_strokes removeLastObject];
		[self setNeedsDisplay];
	}
}

-(void)redoStroke
{
	if( m_redo_strokes.count )
	{
		[m_free_hand_strokes addObject:m_redo_strokes.lastObject];
		[m_redo_strokes removeLastObject];
		[self setNeedsDisplay];
	}
}

-(BOOL)canUndoStroke
{
	return m_free_hand_strokes.count > 0 ? YES : NO;
}

-(BOOL)canRedoStroke
{
	return m_redo_strokes.count > 0 ? YES : NO;
}

@end
