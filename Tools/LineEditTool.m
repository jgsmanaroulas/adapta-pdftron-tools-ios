//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------


#import "LineEditTool.h"
#import "SelectionRectContainerView.h"

@implementation LineEditTool

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void) setSelectionRectDelta: (CGRect) deltaRect
{
	self.frame = CGRectMake([m_pdfViewCtrl GetHScrollPos], [m_pdfViewCtrl GetVScrollPos], m_pdfViewCtrl.frame.size.width, m_pdfViewCtrl.frame.size.height );
    
    // make sure you can't drag the end of the line off the page
    [self boundPointToPage:&(m_touchPoint)];
    
    // position the resize widget correctly
    CGPoint newLocation = [m_pdfViewCtrl convertPoint:m_touchPoint toView:[self.touchedSelectWidget superview]];
    
    CGRect resizeWidgetFrame;
    const int length = [ResizeWidgetView getLength];
    resizeWidgetFrame.origin.x = newLocation.x-length/2;
    resizeWidgetFrame.origin.y = newLocation.y-length/2;
    resizeWidgetFrame.size.width = self.touchedSelectWidget.frame.size.width;
    resizeWidgetFrame.size.height = self.touchedSelectWidget.frame.size.height;
    
    self.touchedSelectWidget.frame = resizeWidgetFrame;
    
	m_resizing = YES;

}


- (void) boundPointToPage:(CGPoint *)annotPoint
{
    if( ![m_moving_annotation IsValid] )
        return;
    
    PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
    
    PTRotate rotation;
    PTPage* page;
    
    @try
    {
        [m_pdfViewCtrl DocLockRead];
        
        page = [doc GetPage:m_annot_page_number];
        
        rotation = [page GetRotation];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",[exception name], [exception reason]);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }
    
    // build page box start
    
    PTPDFRect* page_rect = [page GetCropBox];
    [super computePageBoxInScreenPts:page_rect pagenumber:m_annot_page_number];
    
    CGFloat minX = [page_rect GetX1];
    CGFloat maxX = [page_rect GetX2];
    CGFloat minY = [page_rect GetY1];
    CGFloat maxY = [page_rect GetY2];
    
    
    if( annotPoint->x < minX )
    {
        annotPoint->x = minX;
    }
    else if( annotPoint->x > maxX )
    {
        annotPoint->x = maxX;
    }
    
    if( annotPoint->y < minY )
    {
        annotPoint->y = minY;
    }
    else if( annotPoint->y > maxY )
    {
        annotPoint->y = maxY;
    }
}

-(void)selectCurrentAnnotation
{
	PTPDFPoint *basePoint, *endPoint;
	PTPDFRect *tightRect;
	@try
	{
		[m_pdfViewCtrl DocLockRead];
		m_lineAnnot = [[PTLineAnnot alloc] initWithAnn:m_moving_annotation];
		
		basePoint = [m_lineAnnot GetStartPoint];
		endPoint = [m_lineAnnot GetEndPoint];
		tightRect = [[PTPDFRect alloc] initWithX1:[basePoint getX] y1:[basePoint getY] x2:[endPoint getX] y2:[endPoint getY]];
	}
	@catch (NSException *exception) {
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally {
		[m_pdfViewCtrl DocUnlockRead];
	}
	
	PTPDFPoint* sp = [m_pdfViewCtrl ConvPagePtToScreenPt:basePoint page_num:m_annot_page_number];

	CGPoint head = CGPointMake([sp getX], [sp getY]);
	
	m_annnot_rect = [self PDFRectPage2CGRectScreen:tightRect PageNumber:m_annot_page_number];
	
	if( [self PointsAreEqualA:head	B:m_annnot_rect.origin] )
	{
		// arrow points up, left
		m_pointing = e_upleft;
	}
	else if( [self PointsAreEqualA:head	B:CGPointMake(m_annnot_rect.origin.x, m_annnot_rect.origin.y+m_annnot_rect.size.height)] )
	{
		m_pointing = e_downleft;
	}
	else if( [self PointsAreEqualA:head	B:CGPointMake(m_annnot_rect.origin.x+m_annnot_rect.size.width, m_annnot_rect.origin.y)] )
	{
		m_pointing = e_upright;
	}
	else
	{
		m_pointing = e_downright;
	}
	
	m_annot_page_number = [m_pdfViewCtrl GetPageNumberFromScreenPt:m_annnot_rect.origin.x+m_annnot_rect.size.width/2 y:m_annnot_rect.origin.y+m_annnot_rect.size.height/2];
	
	m_annnot_rect.origin.x = m_annnot_rect.origin.x + [m_pdfViewCtrl GetHScrollPos];
	m_annnot_rect.origin.y = m_annnot_rect.origin.y + [m_pdfViewCtrl GetVScrollPos];
	
	selectionRectContainerView.frame = m_annnot_rect;
	[selectionRectContainerView setHidden:NO];
	
	PTPDFPoint* pagePoint1 = [m_pdfViewCtrl ConvPagePtToScreenPt:basePoint page_num:m_annot_page_number];
	PTPDFPoint* pagePoint2 = [m_pdfViewCtrl ConvPagePtToScreenPt:endPoint page_num:m_annot_page_number];
	
	double slope = ([pagePoint2 getY] - [pagePoint1 getY])/([pagePoint2 getX] - [pagePoint1 getX]);
	
	if( slope > 0 )
	{
		[selectionRectContainerView showNWSEWidgetViews];
	}
	else
	{
		[selectionRectContainerView showNESWWidgetViews];
	}
	
	[selectionRectContainerView setAnnot:m_moving_annotation];
	
	if( !m_select_rect_on_subview )
	{
		[m_pdfViewCtrl->ContainerView addSubview:selectionRectContainerView];
		m_select_rect_on_subview = YES;
	}
	
	m_annnot_rect.origin.x = m_annnot_rect.origin.x - [m_pdfViewCtrl GetHScrollPos];
	m_annnot_rect.origin.y = m_annnot_rect.origin.y - [m_pdfViewCtrl GetVScrollPos];
	
	[self hideMenu];
	[self showSelectionMenu: m_annnot_rect];

}

- (BOOL) makeNewAnnotationSelection: (UIGestureRecognizer *) gestureRecognizer
{
    BOOL annotEditResult = [super makeNewAnnotationSelection:gestureRecognizer];
    
    if( annotEditResult )
    {
        [self selectCurrentAnnotation];
		
		return YES;
    }
    else
    {
        return annotEditResult;
    }
}

- (void) reSelectAnnotation
{
	
	[self selectCurrentAnnotation];
}

-(void)deleteSelectedAnnotation
{
    [super deleteSelectedAnnotation];
    m_lineAnnot = Nil;
}


- (void) moveAnnotation: (CGPoint) down
{
	[selectionRectContainerView hideLine];
    if ([m_moving_annotation IsValid])
	{
		@try
		{
			[m_pdfViewCtrl DocLock:YES];
			m_lineAnnot = [[PTLineAnnot alloc] initWithAnn:m_moving_annotation];

			CGRect frame = selectionRectContainerView.frame;
			frame = CGRectInset(frame, 5*2, 5*2);
			
			frame.origin.x -= [m_pdfViewCtrl GetHScrollPos];
			frame.origin.y -= [m_pdfViewCtrl GetVScrollPos];
			
			PTPDFPoint* newStartPoint = [[PTPDFPoint alloc] init];
			PTPDFPoint* newEndPoint = [[PTPDFPoint alloc] init];
			
			switch( m_pointing )
			{
				case e_upleft:
					[newStartPoint setX:frame.origin.x];
					[newStartPoint setY:frame.origin.y];
					[newEndPoint setX:frame.origin.x +frame.size.width];
					[newEndPoint setY:frame.origin.y + frame.size.height];
					break;
				case e_upright:
					[newStartPoint setX:frame.origin.x +frame.size.width];
					[newStartPoint setY:frame.origin.y];
					[newEndPoint setX:frame.origin.x];
					[newEndPoint setY:frame.origin.y + frame.size.height];
					break;
					
				case e_downleft:
					[newStartPoint setX:frame.origin.x];
					[newStartPoint setY:frame.origin.y+ frame.size.height];
					[newEndPoint setX:frame.origin.x +frame.size.width];
					[newEndPoint setY:frame.origin.y ];
					break;
					
				case e_downright:
					[newStartPoint setX:frame.origin.x+frame.size.width];
					[newStartPoint setY:frame.origin.y + frame.size.height];
					[newEndPoint setX:frame.origin.x ];
					[newEndPoint setY:frame.origin.y];
					break;
			}
			
			
			PTPDFPoint* pagePointStart = [m_pdfViewCtrl ConvScreenPtToPagePt:newStartPoint page_num:m_annot_page_number];
			PTPDFPoint* pagePointEnd = [m_pdfViewCtrl ConvScreenPtToPagePt:newEndPoint page_num:m_annot_page_number];
			
			[m_lineAnnot SetStartPoint:pagePointStart];
			[m_lineAnnot SetEndPoint:pagePointEnd];
		
			[m_lineAnnot RefreshAppearance];
			
			m_annnot_rect.origin.x = selectionRectContainerView.frame.origin.x+selectionRectContainerView->selectionRectView.frame.origin.x - [m_pdfViewCtrl GetHScrollPos];
			m_annnot_rect.origin.y = selectionRectContainerView.frame.origin.y+selectionRectContainerView->selectionRectView.frame.origin.y - [m_pdfViewCtrl GetVScrollPos];
			
			m_annnot_rect.size.width = selectionRectContainerView->selectionRectView.frame.size.width;
			m_annnot_rect.size.height = selectionRectContainerView->selectionRectView.frame.size.height;
			
			
			[self showSelectionMenu: m_annnot_rect];
			
			[m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
		}
		@catch (NSException *exception) {
			NSLog(@"Exception: %@: %@",exception.name, exception.reason);
		}
		@finally {
			[m_pdfViewCtrl DocUnlock];
		}
		
		[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
    }
	
	
}

- (bool) moveSelectionRect: (CGPoint) down
{
    PTPDFPoint *basePoint, *endPoint;
    PTPDFRect *tightRect;
    
    @try
    {
        [m_pdfViewCtrl DocLockRead];
        
        m_lineAnnot = [[PTLineAnnot alloc] initWithAnn:m_moving_annotation];
        
        basePoint = [m_lineAnnot GetStartPoint];
        endPoint = [m_lineAnnot GetEndPoint];
        tightRect = [[PTPDFRect alloc] initWithX1:[basePoint getX] y1:[basePoint getY] x2:[endPoint getX] y2:[endPoint getY]];
    
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }
    
    CGRect annotRect = [self PDFRectPage2CGRectScreen:tightRect PageNumber:m_annot_page_number];
    
    if( fabs(annotRect.origin.x - (down.x + offset.x)) > 3 ||
       fabs(annotRect.origin.y - (down.y + offset.y)) > 3)
    {
        annotRect.origin.x = down.x + offset.x;
        annotRect.origin.y = down.y + offset.y;
        
        [self boundRectToPage:&annotRect isResizing:NO];
        
        selectionRectContainerView.frame = annotRect;
		
		[selectionRectContainerView showLine];
        
		
        [self hideMenu];
        
        return true;
    }
    else
        return false;
}



-(void)SetAnnotationRect:(PTAnnot*)annot Rect:(CGRect)rect OnPage:(int)pageNumber
{
	return;
}

-(BOOL)PointsAreEqualA:(CGPoint)a B:(CGPoint)b
{
    return sqrt((b.x-a.x)*(b.x-a.x) + (b.y-a.y)*(b.y-a.y)) < 0.01;
}

-(BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	
	self.backgroundColor = [UIColor clearColor];
	m_resizing = NO;
	UITouch *touch = touches.allObjects[0];
    
    m_touchPoint = [touch locationInView:m_pdfViewCtrl];
	
	PTPDFPoint* sp = [m_pdfViewCtrl ConvPagePtToScreenPt:[m_lineAnnot GetStartPoint] page_num:m_annot_page_number];
	
	PTPDFPoint* ep = [m_pdfViewCtrl ConvPagePtToScreenPt:[m_lineAnnot GetEndPoint] page_num:m_annot_page_number];

	if( pow((m_touchPoint.x-[sp getX]),2)+pow((m_touchPoint.y-[sp getY]),2) > pow((m_touchPoint.x-[ep getX]),2)+pow((m_touchPoint.y-[ep getY]),2)  )
	{
		movedEndPt = NO;
		m_ptToDrawTo = sp;
	}
	else
	{
		m_ptToDrawTo = ep;
		movedEndPt = YES;
	}
	
	
	return [super onTouchesBegan:touches withEvent:event];
}

- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = touches.allObjects[0];
    
    m_touchPoint = [touch locationInView:m_pdfViewCtrl];
    

    
	[self setNeedsDisplay];
	
	return [super onTouchesMoved:touches withEvent:event];
}

-(BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	if( m_resizing )
	{
		
		@try {
			
			[m_pdfViewCtrl DocLock:YES];
			PTPDFPoint* newPt;
			if( !movedEndPt )
			{
				newPt = [[PTPDFPoint alloc] initWithPx:m_touchPoint.x py:m_touchPoint.y];
				PTPDFPoint* newPtpage = [m_pdfViewCtrl ConvScreenPtToPagePt:newPt page_num:m_annot_page_number];
				[m_lineAnnot SetEndPoint:newPtpage];
			}
			else
			{
				newPt = [[PTPDFPoint alloc] initWithPx:m_touchPoint.x py:m_touchPoint.y];
				PTPDFPoint* newPtpage = [m_pdfViewCtrl ConvScreenPtToPagePt:newPt page_num:m_annot_page_number];
				[m_lineAnnot SetStartPoint:newPtpage];
			}
			
			[m_lineAnnot RefreshAppearance];
		
		}
		@catch (NSException *exception) {
			NSLog(@"Exception: %@: %@",exception.name, exception.reason);
		}
		@finally {
			[m_pdfViewCtrl DocUnlock];
		}
	
		[m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
		[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
	
		self.backgroundColor = [UIColor clearColor];
		m_moving_annotation = m_lineAnnot;
		selectionRectContainerView.hidden = NO;
	}
	
	[self reSelectAnnotation];
	
	m_resizing = NO;
	
	[self setNeedsDisplay];
	
	[self showSelectionMenu: m_annnot_rect];
	
	return [super onTouchesEnded:touches withEvent:event];
}

-(void)noteEditCancelButtonPressed:(BOOL)showSelectionMenu
{
    [super noteEditCancelButtonPressed:showSelectionMenu];
	[self showSelectionMenu:m_annnot_rect];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
	if( [m_lineAnnot IsValid] && m_resizing && !self.allowScrolling)
	{
		
		CGContextRef currentContext = UIGraphicsGetCurrentContext();
		
		PTColorPt* strokePoint = [m_lineAnnot GetColorAsRGB];
		
		double r = [strokePoint Get:0];
		double g = [strokePoint Get:1];
		double b = [strokePoint Get:2];
		
		if( [m_pdfViewCtrl GetColorPostProcessMode] == e_ptpostprocess_invert )
		{
			r = 1.-r;
			g = 1.-g;
			b = 1.-b;
		}
		
		int strokeColorComps = [m_lineAnnot GetColorCompNum];
		
		UIColor* strokeColor = [UIColor colorWithRed:r green:g blue:b alpha:(strokeColorComps > 0 ? 1 : 0)];
		
		double thickness = [[m_lineAnnot GetBorderStyle] GetWidth];
		
		thickness *= [m_pdfViewCtrl GetZoom];
		
		CGContextSetLineWidth(currentContext, thickness);
		CGContextSetLineCap(currentContext, kCGLineCapButt);
		CGContextSetLineJoin(currentContext, kCGLineJoinMiter);
		CGContextSetStrokeColorWithColor(currentContext, strokeColor.CGColor);

	
		CGContextBeginPath (currentContext);
		
		CGContextMoveToPoint(currentContext, m_touchPoint.x, m_touchPoint.y);
		CGContextAddLineToPoint(currentContext, [m_ptToDrawTo getX], [m_ptToDrawTo getY]);
		
		CGContextStrokePath(currentContext);
		
	}
}


@end
