//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "NoFadeTiledLayer.h"
#import <PDFNet/PDFViewCtrl.h>



@interface SelectionRectView : UIView {

    @public
    BOOL m_isLineAnnot;
    BOOL m_startAtNE;
    PTAnnot* m_annot;
    int m_rectOffset;
}

@property (nonatomic, weak) PTPDFViewCtrl* m_pdfViewCtrl;

-(instancetype)initWithFrame:(CGRect)frame forAnnot:(PTAnnot*)annot NS_DESIGNATED_INITIALIZER;
-(void)setEditLine:(BOOL)isLine;
-(void)setAnnot:(PTAnnot*)annot;


-(instancetype)initWithCoder:(NSCoder *)aDecoder __attribute__((unavailable("Not the designated initializer")));

-(instancetype)initWithFrame:(CGRect)frame __attribute__((unavailable("Not the designated initializer")));

@end
