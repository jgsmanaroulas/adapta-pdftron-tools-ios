//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//


#import "TextStrikeoutCreate.h"

@implementation TextStrikeoutCreate

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        annotType = e_ptStrikeOut;
    }
    
    return self;
}

@end
