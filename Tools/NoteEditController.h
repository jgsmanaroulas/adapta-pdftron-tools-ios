//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>
#import "AnnotEditTool.h"

@interface NoteEditController : UIViewController {
    Tool* delegate;

    @public
    UITextView* tv;
}

- (instancetype)initWithDelegate:(Tool*)del NS_DESIGNATED_INITIALIZER;
-(void)cancelButtonPressed;
-(void)deleteButtonPressed;
-(void)saveButtonPressed;


-(instancetype)initWithCoder:(NSCoder *)aDecoder __attribute__((unavailable("Not the designated initializer")));

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil __attribute__((unavailable("Not the designated initializer")));

@end
