//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "Tool.h"
#import "ColorSwatchViewController.h"
#import "UIKit/UIKit.h"

@class ColorPickerViewController;


@interface AnnotEditTool : Tool<UIPopoverPresentationControllerDelegate, UITextViewDelegate, ColorPickerViewControllerDelegate>
{
    BOOL m_fill_color;
    BOOL m_select_rect_on_subview;
	BOOL m_external_colorpicker;
    CGPoint offset;

    
    CGPoint firstTouchPoint;
    CGPoint mostRecentTouchPoint;
    CGRect firstSelectionRect;
    
    NSMutableArray* baseMenuOptions;
    
    BOOL keyboardOnScreen;
    
    CGRect m_annnot_rect;
    
    BOOL moveOccuredPreTap;

    NSMutableArray* choices;

}

@property (nonatomic, weak) UIView* touchedSelectWidget;

// override
- (BOOL)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer;
- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
- (BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;
- (BOOL)onTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event;

// internal methods
-(void)setAnnotationBorder:(float)thickness;
-(void)deselectAnnotation;
-(void)reSelectAnnotation;
-(void)SetAnnotationRect:(PTAnnot*)annot Rect:(CGRect)rect OnPage:(int)pageNumber;
-(BOOL)makeNewAnnotationSelection:(UIGestureRecognizer*)gestureRecognizer;
-(void)moveAnnotation:(CGPoint)point;
-(void)boundRectToPage:(CGRect *)annotRect_p isResizing:(BOOL)resizing;
-(void)setSelectionRectDelta:(CGRect)deltaRect;
-(void)computePageBoxInScreenPts:(PTPDFRect *)rect pagenumber:(int)pageNumber;

// used by TextMarkupEditTool
-(void)editSelectedAnnotationStrokeColor;
-(void)editSelectedAnnotationBorder;
-(void)editSelectedAnnotationOpacity;

-(void)setAnnotationOpacity:(double)opacity;

@end
