//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "RectangleCreate.h"
#import "PanTool.h"

@implementation RectangleCreate


- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        pdfNetAnnotType = [PTSquare class];
    }
    
    return self;
}

+(BOOL)createsAnnotation
{
	return YES;
}

-(PTAnnotType)annotType
{
	return e_ptSquare;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    
    if( pageNumber >= 1 && !self.allowScrolling)
    {
        
        double thickness = [super setupContextFromDefaultAnnot:currentContext];
        
        CGRect myRect = drawArea;
        
        myRect.origin.x += thickness/2;
        myRect.origin.y += thickness/2;
        myRect.size.width -= thickness;
        myRect.size.height -= thickness;

        CGContextFillRect(currentContext, myRect);
        CGContextStrokeRectWithWidth(currentContext, myRect, thickness);
        
        CGContextStrokePath(currentContext);
    }
}

@end
