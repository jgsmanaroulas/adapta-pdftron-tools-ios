//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

@interface SelectionBar : UIView {
    BOOL isLeft;
}

-(void)setIsLeft:(BOOL)left;
-(BOOL)isLeft;
- (BOOL)canBecomeFirstResponder;

@end
