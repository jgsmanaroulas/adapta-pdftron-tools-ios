//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------


#import "FloatingSigViewController.h"
#import "DigSigView.h"
#import "PDFViewCtrlToolsUtil.h"

@interface FloatingSigViewController ()

@end

@implementation FloatingSigViewController

@synthesize delegate;
@synthesize strokeThickness;
@synthesize strokeColor;


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.strokeThickness = 2;
        self.strokeColor = UIColor.blackColor;
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
	m_digSigView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	self.view.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];

    UIView *container = [[UIView alloc] initWithFrame:CGRectZero];
    container.translatesAutoresizingMaskIntoConstraints = NO;
	
    [self.view addSubview:container];
    
    if (@available(iOS 11, *)) {
        UILayoutGuide *safeArea = self.view.safeAreaLayoutGuide;
        [NSLayoutConstraint activateConstraints:
         @[
           [container.leadingAnchor constraintEqualToAnchor:safeArea.leadingAnchor],
           [container.widthAnchor constraintEqualToAnchor:safeArea.widthAnchor],
           [container.topAnchor constraintEqualToAnchor:safeArea.topAnchor],
           [container.heightAnchor constraintEqualToAnchor:safeArea.heightAnchor],
           ]];
    } else {
        [NSLayoutConstraint activateConstraints:
         @[
           [container.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor],
           [container.widthAnchor constraintEqualToAnchor:self.view.widthAnchor],
           [container.topAnchor constraintEqualToAnchor:self.view.topAnchor],
           [container.heightAnchor constraintEqualToAnchor:self.view.heightAnchor],
           ]];
    }
    
    m_digSigView = [[DigSigView alloc] initWithFrame:CGRectZero];
    
    m_digSigView.backgroundColor = [UIColor whiteColor];
    
    [container addSubview:m_digSigView];
    
    m_digSigView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:|-10-[m_digSigView]-10-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(m_digSigView)]];
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:|-60-[m_digSigView]-100-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(m_digSigView)]];

	
	NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
	
	// add button to clear appearance
	UIButton *button4 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[button4 addTarget:self action:@selector(resetDigSigView)
	  forControlEvents:UIControlEventTouchUpInside];
	[button4 setTitle:NSLocalizedStringFromTableInBundle(@"Clear", @"PDFNet-Tools", toolsStringBundle, @"") forState:UIControlStateNormal];

    button4.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [container addSubview:button4];
    
    button4.translatesAutoresizingMaskIntoConstraints = NO;
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:|-10-[button4(70)]"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button4)]];
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:[button4(25)]-10-[m_digSigView]"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button4,m_digSigView)]];
	
	// add button for saving appearance
	UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[button addTarget:self action:@selector(saveAppearance)
	 forControlEvents:UIControlEventTouchUpInside];
	[button setTitle:NSLocalizedStringFromTableInBundle(@"Save Appearance", @"PDFNet-Tools", toolsStringBundle, @"For signatures") forState:UIControlStateNormal];

    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    [container addSubview:button];
    
    button.translatesAutoresizingMaskIntoConstraints = NO;
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:[button(150)]-10-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button)]];
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:[button(25)]-10-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button,m_digSigView)]];
	

	
	// add button to cancel
	UIButton *button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[button2 addTarget:delegate action:@selector(closeSignatureDialog)
	  forControlEvents:UIControlEventTouchUpInside];
	[button2 setTitle:NSLocalizedStringFromTableInBundle(@"Cancel", @"PDFNet-Tools", toolsStringBundle, @"") forState:UIControlStateNormal];
    button2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [container addSubview:button2];
    button2.translatesAutoresizingMaskIntoConstraints = NO;
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:|-10-[button2(150)]"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button2)]];
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:[button2(25)]-10-|"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(button2,m_digSigView)]];
	

	
	UILabel* switchLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	[switchLabel setText:NSLocalizedStringFromTableInBundle(@"Save as My Signature", @"PDFNet-Tools", toolsStringBundle, @"For digital signatures")];
	[container addSubview:switchLabel];
    switchLabel.textAlignment = NSTextAlignmentRight;
    switchLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:[switchLabel(180)]-10-|"
                               options:NSLayoutFormatAlignAllCenterY
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(switchLabel)]];
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:[m_digSigView]-20-[switchLabel(25)]"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(switchLabel,m_digSigView)]];
    
    
    UISwitch *defaultSwitch;
    defaultSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
    [defaultSwitch addTarget:self action:@selector(defaultSwitchChanged:) forControlEvents:UIControlEventValueChanged];
    
    [container addSubview:defaultSwitch];
    defaultSwitch.translatesAutoresizingMaskIntoConstraints = NO;
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"H:[defaultSwitch(40)]-10-[switchLabel]"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(defaultSwitch,switchLabel)]];
    
    [container addConstraints:[NSLayoutConstraint
                               constraintsWithVisualFormat:@"V:[m_digSigView]-17-[defaultSwitch(25)]"
                               options:NSLayoutFormatDirectionLeadingToTrailing
                               metrics:nil
                               views:NSDictionaryOfVariableBindings(defaultSwitch,m_digSigView)]];
}

-(void)defaultSwitchChanged:(UISwitch*)mySwitch
{
	m_asDefault = mySwitch.on;
}


-(void)saveAppearance
{
	@try
	{
		// no m_moving_annotation, delegate method needs to be changed
		[delegate saveAppearanceWithPath:m_digSigView.m_dig_sig_points withBoundingRect:m_digSigView.m_boundingRect asDefault:m_asDefault];
	}
	@catch (NSException *exception)
	{
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
}

-(void)resetDigSigView
{
	// clears paths
	[m_digSigView.m_dig_sig_points removeAllObjects];
	
	[m_digSigView setNeedsDisplay];
	
	m_digSigView.userInteractionEnabled = YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
