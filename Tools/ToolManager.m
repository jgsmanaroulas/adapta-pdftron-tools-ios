//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "ToolManager.h"
#import "PanTool.h"

@interface ToolManager()

@end

// pragma sliences warning regarding incomplete implementation because most
// relevant selectors are automatically forwarded to tool via forwardingTargetForSelector:
#pragma clang diagnostic ignored "-Wincomplete-implementation"
@implementation ToolManager

@synthesize tool;
@synthesize pdfViewCtrl;

// these two are irrelevant for the ToolManager and are only here for
// backward compatibility with the tool protocol.
@synthesize annotationAuthor;
@synthesize createsAnnotation;

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    self = [super initWithFrame:CGRectZero];
    
    if (self) {
        self.pdfViewCtrl = in_pdfViewCtrl;
		self.tool = [[PanTool alloc] initWithPDFViewCtrl:in_pdfViewCtrl];
    }
    
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
	NSAssert(false, @"You must init this class using the designated initializer initWithPDFViewCtrl");
	self = [self initWithFrame:frame];
	return Nil;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
	NSAssert(false, @"You must init this class using the designated initializer initWithPDFViewCtrl");
	self = [self initWithCoder:aDecoder];
	return Nil;
}

- (UIView<PTTool>*)getNewTool
{
	// Here for backward compatibility with the tool delegate. Should never be called.
	assert(false);
	return Nil;
}

// this selector exists so that classes that use it do not need to hold an instance
// of pdfviewctrl.
-(UIView<PTTool>*)changeTool:(Class)toolType
{
    UIView<PTTool> * newTool = [[toolType alloc] initWithPDFViewCtrl:self.pdfViewCtrl];
	self.tool = newTool;
	return newTool;
}

- (void)setTool:(UIView<PTTool> *)aTool
{
    if( tool.superview != 0 )
        [tool removeFromSuperview];
	
	if( !aTool )
		return;
    
	if( !aTool.pdfViewCtrl )
	{
		NSException* noPDFViewCtrl = [[NSException alloc] initWithName:@"pdfViewCtrl Is Nil" reason:@"A tool must have a valid pdfViewCtrl" userInfo:Nil];
		@throw noPDFViewCtrl;
		return;
	}
	
    if( aTool != Nil )
        [self.pdfViewCtrl->ContainerView addSubview:aTool];
	
	tool = aTool;
    
    if( [self.delegate conformsToProtocol:@protocol(ToolManagerDelegate)] )
    {
        if( [self.delegate respondsToSelector:@selector(toolChanged:)] )
        {
            [self.delegate toolChanged:self];
        }
    }
	
	
	
	if( [tool respondsToSelector:@selector(onToolAttachedToViewHierarchy)] )
		[tool onToolAttachedToViewHierarchy];
}

#pragma mark - Tool Loop

-(BOOL)runToolLoop:(BOOL(^)(id<PTTool>))toolEventBlock
{
	if( !tool )
	{
		// cannot run if we have no tool to forward the event to.
        return YES;
	}
    
    const int maximumToolSwitches = 10;
    int numToolSwitches = 0;
    BOOL handled = YES;
    do {
        
		@try {
			handled = toolEventBlock(tool);
		}
		@catch (NSException *exception) {
			return YES;
		}
		@finally {
			
		}
        
		
		// did tool finish processing this event?
        if( !handled )
        {
			// no, so create a new instance of a tool that it should be forwarded to
            UIView<PTTool>* tempTool = [tool getNewTool];
            
            if( [tempTool isMemberOfClass:[tool class]] )
                break;
			
			// set the new tool as tool
			self.tool = tempTool;
            
			// keep track of how many switches to prevent an accidental infinite loop
            numToolSwitches++;
        }
        
    } while (!handled && numToolSwitches < maximumToolSwitches);
    
    if( numToolSwitches >= maximumToolSwitches)
    {
        // there was an error condition in one of the tools, and we are escaping a likely infinite loop
        return YES;
    }
    
    return handled;
}

#pragma mark - Events that need to be looped

- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self runToolLoop:^BOOL(id<PTTool> aTool){
		return [aTool onTouchesBegan:touches withEvent:event];
	}];
	
	return YES;
}

- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self runToolLoop:^BOOL(id<PTTool> aTool){
		return [aTool onTouchesMoved:touches withEvent:event];
	}];
	
	return YES;
}

- (BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self runToolLoop:^BOOL(id<PTTool> aTool){
		return [aTool onTouchesEnded:touches withEvent:event];
	}];
	
	return YES;
}

- (BOOL)onTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self runToolLoop:^BOOL(id<PTTool> aTool){
		return [aTool onTouchesCancelled:touches withEvent:event];
	}];
	
	return YES;
}

- (BOOL)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
	[self runToolLoop:^BOOL(id<PTTool> aTool){
		return [aTool handleLongPress:gestureRecognizer];
	}];
	
	return YES;
}

- (BOOL)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
	[self runToolLoop:^BOOL(id<PTTool> aTool){
		return [aTool handleTap:gestureRecognizer];
	}];
	
	return YES;
}

- (BOOL)handleDoubleTap:(UITapGestureRecognizer *)gestureRecognizer
{
	[self runToolLoop:^BOOL(id<PTTool> aTool){
		return [aTool handleDoubleTap:gestureRecognizer];
	}];
	
	return YES;
}

- (BOOL)onCustomEvent:(id)userData
{
	[self runToolLoop:^BOOL(id<PTTool> aTool){
		return [aTool onCustomEvent:userData];
	}];
	
	return YES;
}

#pragma mark - Events that do NOT need to be looped

// return YES if tool manager or current tool responds to the selector
-(BOOL)respondsToSelector:(SEL)aSelector
{
	// if the tool manager itself implements it (i.e. the method needs to be run in runToolLoop)
	if ([[self class] instancesRespondToSelector:aSelector] ) return YES;
	
	// if the active tool implements it
	if ([[tool class] instancesRespondToSelector:aSelector] ) return YES;

	return NO;
}

// this will directly forward all other tool delegate methods to the active tool
-(id)forwardingTargetForSelector:(SEL)aSelector
{
	// allows methods not called explicitly by the tool manager on the tool (as is done in runToolLoop)
	// to be automatically forwarded
	return tool;
}

#pragma mark - For Events

- (void)annotationAdded:(PTAnnot*)annotation onPageNumber:(unsigned long)pageNumber
{
	if ( [self.delegate respondsToSelector:@selector(annotationAdded:onPageNumber:)])
	{
		[self.delegate annotationAdded:annotation onPageNumber:pageNumber];
	}
}

- (void)annotationModified:(PTAnnot*)annotation onPageNumber:(unsigned long)pageNumber
{
	if ( [self.delegate respondsToSelector:@selector(annotationModified:onPageNumber:)])
	{
		[self.delegate annotationModified:annotation onPageNumber:pageNumber];
	}
}

- (void)annotationRemoved:(PTAnnot*)annotation onPageNumber:(unsigned long)pageNumber
{
	if ( [self.delegate respondsToSelector:@selector(annotationRemoved:onPageNumber:)])
	{
		[self.delegate annotationRemoved:annotation onPageNumber:pageNumber];
	}
}

#pragma mark - Other

-(void)dealloc
{
    if( [tool superview] )
        [tool removeFromSuperview];
}

@end
