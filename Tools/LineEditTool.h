//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "AnnotEditTool.h"

@interface LineEditTool : AnnotEditTool
{
	CGPoint m_touchPoint;
    PTLineAnnot* m_lineAnnot;
	PTPDFPoint* m_ptToDrawTo;
	BOOL movedEndPt;
	BOOL m_resizing;
	
	enum direction
    {
        e_upleft,
        e_upright,
		e_downleft,
		e_downright
    };
    enum direction m_pointing;
}
@end
