//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "FreeTextCreate.h"
#import "PanTool.h"
#import "ColorDefaults.h"

@interface FreeTextCreate ()

@property (nonatomic, strong) PTPDFRect* writeRect;
@property (nonatomic, strong) PTPage* writePage;
/**
 * Defines if the text box should orient itself for right-to-left language input.
 * Switches based on language that is first used when creating the annotation
 */
@property (assign, nonatomic) BOOL isRTL;

@end

@implementation FreeTextCreate


- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
		self.backgroundColor = [UIColor clearColor];
        
        self.isRTL = false;
        
        // use a temporary textView to see if the user is about to write in a RTL or LTR language
        UITextView* textView = [[UITextView alloc] initWithFrame:CGRectZero];
        [in_pdfViewCtrl addSubview:textView];
        [textView becomeFirstResponder];


        if( [NSLocale characterDirectionForLanguage:[textView textInputMode].primaryLanguage] == NSLocaleLanguageDirectionRightToLeft)
        {
            self.isRTL = true;
        }
        
        [textView resignFirstResponder];
        
        [textView removeFromSuperview];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector (UITextInputCurrentInputModeDidChangeNotification:)
                                                     name: UITextInputCurrentInputModeDidChangeNotification object:nil];
        
    }
    
    return self;
}

+(BOOL)createsAnnotation
{
	return YES;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)UITextInputCurrentInputModeDidChangeNotification:(NSNotification*)notification
{
    if( textView.text.length == 0 )
    {
        
        bool prior = self.isRTL;
        
        if( [NSLocale characterDirectionForLanguage:[textView textInputMode].primaryLanguage] == NSLocaleLanguageDirectionRightToLeft)
        {
            self.isRTL = true;
        }
        else
        {
            self.isRTL = false;
        }
        
        if( prior != self.isRTL )
        {
            [self setTextAreaFrame];
        }
    }
}

-(void)setUpTextEntry
{
	PTColorPostProcessMode mode = [m_pdfViewCtrl GetColorPostProcessMode];
    textView = [[UITextView alloc] init];
    sv = [[UIScrollView alloc] init];
    
    textView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    sv.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [sv addSubview:textView];
    [self addSubview:sv];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector (keyboardWillShow:)
                                                 name: UIKeyboardWillShowNotification object:nil];
    
    sv.backgroundColor = [UIColor clearColor];
	
    self.userInteractionEnabled = YES;
    sv.userInteractionEnabled = YES;
    textView.userInteractionEnabled = YES;
    textView.delegate = self;
    textView.textColor = [ColorDefaults defaultColorForAnnotType:e_ptFreeText attribute:ATTRIBUTE_STROKE_COLOR colorPostProcessMode:mode];
    textView.font = [UIFont fontWithName:@"Helvetica" size:[ColorDefaults defaultFreeTextSizeForAnnotType:e_ptFreeText]*[m_pdfViewCtrl GetZoom]];
    textView.backgroundColor = [UIColor clearColor];

	// one day add background colour to live editing too?
	//http://stackoverflow.com/questions/15438869/uitextview-text-background-colour
	
    created = NO;
    
    wroteAnnot = NO;

    return;
}



-(BOOL)setTextAreaFrame
{
    startPoint = m_down;
    
	int pageNumber = [m_pdfViewCtrl GetPageNumberFromScreenPt:startPoint.x y:startPoint.y];
	
	if (pageNumber < 1 )
		return NO;

	assert(startPoint.x > 0);
	assert(startPoint.y > 0);
	
	PTPDFRect* testRect = [[[m_pdfViewCtrl GetDoc] GetPage:pageNumber] GetCropBox];
	
	CGFloat cbx1 = [testRect GetX1];
	CGFloat cbx2 = [testRect GetX2];
	CGFloat cby1 = [testRect GetY1];
	CGFloat cby2 = [testRect GetY2];
	
	CGFloat pageLeft;
	CGFloat pageTop;
	
	[self ConvertPagePtToScreenPtX:&cbx1 Y:&cby1 PageNumber:pageNumber];
	[self ConvertPagePtToScreenPtX:&cbx2 Y:&cby2 PageNumber:pageNumber];
	
    if( self.isRTL == false )
    {
        pageLeft = MIN(cbx1, cbx2);
        pageTop =  MIN(cby1, cby2);
        endPoint.x = MAX(cbx1, cbx2);
        endPoint.y = MAX(cby1, cby2);
        
        startPoint.x = MAX(pageLeft, startPoint.x-20);
        startPoint.y = MAX(pageTop, startPoint.y-20);
    }
    else
    {
        pageLeft = MIN(cbx1, cbx2);
        pageTop =  MIN(cby1, cby2);
        
        endPoint.x = startPoint.x;
        endPoint.y = MAX(cby1, cby2);
        
        startPoint.x = pageLeft;
        startPoint.y = MAX(pageTop, startPoint.y-20);
        
    }
	
	
	self.frame = CGRectMake([m_pdfViewCtrl GetHScrollPos]+startPoint.x, [m_pdfViewCtrl GetVScrollPos]+startPoint.y, fabs(endPoint.x-startPoint.x), fabs(endPoint.y-startPoint.y));

	return YES;
}

-(void)activateTextEntry
{
    assert([NSThread isMainThread]);
    
    sv.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    textView.frame = CGRectMake(0, 0, sv.frame.size.width, sv.frame.size.height);
    
    if ([textView canBecomeFirstResponder]) {

        
        [textView becomeFirstResponder];

    }
    
    created = YES;
    
    UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textTap:)];
    tgr.delegate = self;
    
    NSUInteger numTaps = 1;
    
    tgr.numberOfTapsRequired = numTaps;
    
    [tgr setCancelsTouchesInView:YES];
    [tgr setDelaysTouchesEnded:NO];
    
    [textView addGestureRecognizer:tgr];
    
    PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
    
    [m_pdfViewCtrl DocLock:YES];
    
    @try {
        int pageNumber = [m_pdfViewCtrl GetPageNumberFromScreenPt:startPoint.x y:startPoint.y];
        
        if (pageNumber < 1)
            pageNumber = [m_pdfViewCtrl GetPageNumberFromScreenPt:endPoint.x y:endPoint.y];
        
        if (pageNumber < 1)
        {
            nextToolType = [PanTool class];
            
            return;
        }
        
        m_annot_page_number = pageNumber;
        
        self.writePage = [doc GetPage:pageNumber];

        
    }
    @catch (NSException *exception) {
        NSLog(@"activateTextEntry Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }
}

- (void)writeAnnotation
{
    if(wroteAnnot || !textView || [textView.text isEqualToString:@""])
        return;
    PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
    
    PTFreeText* annotation;
	
    @try
    {
        [m_pdfViewCtrl DocLock:YES];
        
        CGRect screenRect = [m_pdfViewCtrl convertRect:textView.frame fromView:textView.superview];
        
        CGFloat x1 = screenRect.origin.x;
        CGFloat y1 = screenRect.origin.y;
        CGFloat x2 = screenRect.origin.x+screenRect.size.width;
        CGFloat y2 = screenRect.origin.y+screenRect.size.height;
        
        [self ConvertScreenPtToPagePtX:&x1 Y:&y1 PageNumber:m_annot_page_number];
        [self ConvertScreenPtToPagePtX:&x2 Y:&y2 PageNumber:m_annot_page_number];
        
        self.writeRect = [[PTPDFRect alloc] initWithX1:x1 y1:y1 x2:x2 y2:y2];

        annotation = [PTFreeText Create:(PTSDFDoc*)doc pos:self.writeRect];
		
		[annotation SetFontSize:[ColorDefaults defaultFreeTextSizeForAnnotType:[annotation GetType]]];

        [annotation SetContents:textView.text];
        
        PTBorderStyle* bs = [annotation GetBorderStyle];
        [bs SetWidth:0];
        [annotation SetBorderStyle:bs oldStyleOnly:NO];
		
		if( self.annotationAuthor && self.annotationAuthor.length > 0 && [annotation isKindOfClass:[PTMarkup class]]	)
		{
			[(PTMarkup*)annotation SetTitle:self.annotationAuthor];
		}
		
		// write text colour
		int numComps = [ColorDefaults numCompsInColorPtForAnnotType:e_ptFreeText attribute:ATTRIBUTE_STROKE_COLOR];
		
		PTColorPt* cp = [ColorDefaults defaultColorPtForAnnotType:e_ptFreeText attribute:ATTRIBUTE_STROKE_COLOR  colorPostProcessMode:e_ptpostprocess_none];
		
		if( numComps > 0 )
			[annotation SetTextColor:cp col_comp:3];
		else
			[annotation SetTextColor:cp col_comp:0];
		
        
        // push back annotation now in case of rotated page
        [self.writePage AnnotPushBack:annotation];
        [annotation RefreshAppearance];
        
        // Get the annotation's content stream
        PTObj* contentStream = [[[annotation GetSDFObj] FindObj:@"AP"] FindObj:@"N"];
        
        // use element reader to iterate through elements and union their bounding boxes
        PTElementReader* er = [[PTElementReader alloc] init];
        
        PTObj* dict = [[PTObj alloc] init];
        
        PTContext* context = [[PTContext alloc] init];
        
        [er ReaderBeginWithSDFObj:contentStream resource_dict:dict ocg_context:context];
        
        PTElement *element;
        
        PTPDFRect* unionRect = 0;
        
        for (element=[er Next]; element != NULL; element = [er Next])
        {
            PTPDFRect* rect = [element GetBBox];
            
            if([element GetType] == e_pttext_obj)
            {
                if( [rect Width] && [rect Height] )
                {
                    if( unionRect == 0 )
                        unionRect = rect;
                    
                    unionRect = [self GetRectUnion:unionRect Rect2:rect];
                }
            }

        }

        double width = fabs([unionRect GetX2] - [unionRect GetX1])+10;
        double height = fabs([unionRect GetY2] - [unionRect GetY1])+10;
        
        PTRotate ctrlRotation = [m_pdfViewCtrl GetRotation];
        PTRotate pageRotation = [[[m_pdfViewCtrl GetDoc] GetPage:m_annot_page_number] GetRotation];
        int annotRotation = ((pageRotation + ctrlRotation) % 4) * 90;
        
        if( self.isRTL == false )
        {
            [unionRect SetX1:[self.writeRect GetX1]];
            [unionRect SetY1:[self.writeRect GetY1]];
            
            if( annotRotation == 0 )
            {
                [unionRect SetX2:[self.writeRect GetX1]+width];
                [unionRect SetY2:[self.writeRect GetY1]-height];
            }
            else if( annotRotation == 90 )
            {
                [unionRect SetX2:[self.writeRect GetX1]+height];
                [unionRect SetY2:[self.writeRect GetY1]+width];
            }
            else if(annotRotation == 180)
            {
                [unionRect SetX2:[self.writeRect GetX1]-width];
                [unionRect SetY2:[self.writeRect GetY1]+height];
            }
            else if(annotRotation == 270)
            {
                [unionRect SetX2:[self.writeRect GetX1]-height];
                [unionRect SetY2:[self.writeRect GetY1]-width];
            }
            

            
        }
        else
        {
            const int rightAligned = 2;
            
            [annotation SetQuaddingFormat:rightAligned];
            
            if( annotRotation == 0 )
            {
                [unionRect SetX1:[self.writeRect GetX2]-width];
                [unionRect SetY1:[self.writeRect GetY1]-height];
                
                [unionRect SetX2:[self.writeRect GetX2]];
                [unionRect SetY2:[self.writeRect GetY1]];
            }
            else if( annotRotation == 90 )
            {

                [unionRect SetX1:[self.writeRect GetX1]];
                [unionRect SetY1:[self.writeRect GetY2]-width];

                [unionRect SetX2:[self.writeRect GetX1]+height];
                [unionRect SetY2:[self.writeRect GetY2]];

            }
            else if( annotRotation == 180 )
            {
                [unionRect SetX1:[self.writeRect GetX2]+width];
                [unionRect SetY1:[self.writeRect GetY1]+height];
                
                [unionRect SetX2:[self.writeRect GetX2]];
                [unionRect SetY2:[self.writeRect GetY1]];
            }
            else if( annotRotation == 270 )
            {
                [unionRect SetX1:[self.writeRect GetX1]-height];
                [unionRect SetY1:[self.writeRect GetY2]];
                
                [unionRect SetX2:[self.writeRect GetX1]];
                [unionRect SetY2:[self.writeRect GetY2]+width];
            }
            
            
        }

        
        [unionRect Normalize];
        
        [annotation Resize:unionRect];
        
        [annotation RefreshAppearance];
        
        // write fill colour
        numComps = [ColorDefaults numCompsInColorPtForAnnotType:e_ptFreeText attribute:ATTRIBUTE_FILL_COLOR];
        cp = [ColorDefaults defaultColorPtForAnnotType:e_ptFreeText attribute:ATTRIBUTE_FILL_COLOR colorPostProcessMode:e_ptpostprocess_none];
        
        if( numComps > 0 )
            [annotation SetColor:cp numcomp:3];
        else
            [annotation SetColor:cp numcomp:0];
        
        [annotation SetRotation:annotRotation];
        [annotation RefreshAppearance];
        
        wroteAnnot = YES;
        
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }
    
    
    if( m_annot_page_number > 0 )
        [m_pdfViewCtrl UpdateWithAnnot:annotation page_num:m_annot_page_number];
	
	[self annotationAdded:annotation onPageNumber:m_annot_page_number];
    
    [textView removeFromSuperview];
    textView = 0;
    [sv removeFromSuperview];
    sv = 0;


}

- (void)textViewDidEndEditing:(UITextView *)textView
{
	self.isDrag = NO;
    [self writeAnnotation];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

-(void)keyboardWillShow:(NSNotification *)notification
{
	[m_pdfViewCtrl keyboardWillShow:notification rectToNotOverlapWith:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 100)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector (keyboardWillHide:)
                                                 name: UIKeyboardWillHideNotification object:nil];

    keyboardOnScreen = true;
}

- (BOOL)onCustomEvent:(id)userData
{
	NSString* start = (NSString*)userData;
    if( userData && ((![start isEqualToString:@"Start"] && self.backToPanToolAfterUse) || (self.backToPanToolAfterUse && [start isEqualToString:@"BackToPan"])))
    {
        nextToolType = [PanTool class];
        return NO;
    }
	else if( [start isEqualToString:@"BackToPan"] && !self.backToPanToolAfterUse )
	{
		nextToolType = [PanTool class];

		return YES;
	}
    else
    {
        created = NO;

		startPoint = m_down;

		[self setTextAreaFrame];
        [self setUpTextEntry];
		[self activateTextEntry];

        return YES;
    }
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    
    created = NO;
    
	[m_pdfViewCtrl keyboardWillHide:notification];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    keyboardOnScreen = false;
	[m_pdfViewCtrl postCustomEvent:@"BackToPan"];
}

-(BOOL)createEntryAtPoint:(CGPoint)point
{
    assert([NSThread isMainThread]);
	[self.superview bringSubviewToFront:self];
	startPoint = point;
    m_down = point;
	BOOL couldSetFrame = [self setTextAreaFrame];
	
	if( !couldSetFrame )
	{
		nextToolType = [PanTool class];
        return NO;
	}
	else
	{
		if( textView == 0 && sv == 0 )
			[self setUpTextEntry];
		
		[self activateTextEntry];
        
        return YES;
	}
}

- (BOOL)handleTap:(UITapGestureRecognizer *)sender
{
    if( created == NO )
    {
        self.isDrag = NO;
        [self createEntryAtPoint:[sender locationOfTouch:0 inView:m_pdfViewCtrl]];
	
        return YES;
    }
    else
    {
        if( textView != Nil )
		{
            // UI gesture recognizer may also come in after the touches ended. This is to prevent
            // one from firing after the other, causing the keyboard to popup and then immediately
            // dismiss.
            if( self.isDrag && [[NSDate date] timeIntervalSinceDate:self.touchesEndedTime] < 0.85 )
                return YES;

            [textView resignFirstResponder];
            self.frame = CGRectZero;
            
		}
		
		if( self.backToPanToolAfterUse )
		{
			nextToolType = [PanTool class];
			return NO;
		}
		else
		{
			created = NO;
			return YES;
		}
    }
}


- (void)textTap:(UITapGestureRecognizer *)gestureRecognizer { 

	CGSize textSize = [textView.text boundingRectWithSize:textView.bounds.size options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"Helvetica" size:[ColorDefaults defaultFreeTextSizeForAnnotType:e_ptFreeText]*[m_pdfViewCtrl GetZoom]] } context:nil].size;
	
    CGPoint down = [gestureRecognizer locationInView:textView];
    
    // 6 for textview inset
    if (down.x-6 < textSize.width && down.y-6 < textSize.height) {
        // tap was on text so ignore
    }
    else
    {
        // tap was outside of text, dismiss textview and commit annotation
        [self handleTap:gestureRecognizer];
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    // required for the textview gesture recognizer to fire
    return YES;
}

- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    return [super touchesShouldCancelInContentView: view];
}


- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	self.isDrag = YES;
    self.touchesEndedTime = [NSDate date];
    return YES;
}


- (BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	if( !self.isDrag )
		return YES;
	
    UITouch *touch = touches.allObjects[0];

    
	if( created == NO )
        [self createEntryAtPoint:[touch locationInView:m_pdfViewCtrl]];

    created = YES;

    if( self.backToPanToolAfterUse )
    {
        nextToolType = [PanTool class];
        return NO;
    }
    else
        return YES;
}

- (BOOL)onTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	return YES;
}



@end
