//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "TextMarkupEditTool.h"
#import "TextSelectTool.h"
#import "SelectionBar.h"
#import "PanTool.h"
#import "AnalyticsHandlerAdapter.h"
#import "ColorDefaults.h"

#import "PDFViewCtrlToolsUtil.h"

@implementation TextMarkupEditTool

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)prepUtilityTool
{
	if( !self.annotEditUtilityTool )
		self.annotEditUtilityTool = [[AnnotEditTool alloc] initWithPDFViewCtrl:self.pdfViewCtrl];
	
	self.annotEditUtilityTool->m_moving_annotation = m_moving_annotation;
	self.annotEditUtilityTool->m_annot_page_number = m_annot_page_number;
}

#pragma mark - UIMenuController actions

-(void)editSelectedAnnotationStrokeColor
{
	[self prepUtilityTool];
	
	[self.annotEditUtilityTool editSelectedAnnotationStrokeColor];
}

-(void)editSelectedAnnotationBorder
{
	[self prepUtilityTool];
	
	[self.annotEditUtilityTool editSelectedAnnotationBorder];
	[self ShowUtilityMenuController];
}

-(void)editSelectedAnnotationOpacity
{
	[self prepUtilityTool];
	
	[self.annotEditUtilityTool editSelectedAnnotationOpacity];
	[self ShowUtilityMenuController];
}

-(void)editSelectedAnnotationType
{
	[self attachAnnotTypeChangeMenuItems];
	[self ShowUtilityMenuController];
}

-(void)setAnnotOpacity00
{
    [self.annotEditUtilityTool setAnnotationOpacity:0.0];
	[self hideMenu];
	[self ShowMenuController];
}

-(void)setAnnotOpacity25
{
    [self.annotEditUtilityTool setAnnotationOpacity:0.25];
	[self hideMenu];
	[self ShowMenuController];
}

-(void)setAnnotOpacity50
{
    [self.annotEditUtilityTool setAnnotationOpacity:0.50];
	[self hideMenu];
	[self ShowMenuController];
}

-(void)setAnnotOpacity75
{
    [self.annotEditUtilityTool setAnnotationOpacity:0.75];
	[self hideMenu];
	[self ShowMenuController];
}

-(void)setAnnotOpacity10
{
    [self.annotEditUtilityTool setAnnotationOpacity:1.0];
	[self hideMenu];
	[self ShowMenuController];
}

-(void)setAnnotBorder05
{
    [self.annotEditUtilityTool setAnnotationBorder:0.5];
	[self hideMenu];
	[self ShowMenuController];
}

-(void)setAnnotBorder10
{
    [self.annotEditUtilityTool setAnnotationBorder:1.0];
	[self hideMenu];
	[self ShowMenuController];
}

-(void)setAnnotBorder15
{
    [self.annotEditUtilityTool setAnnotationBorder:1.5];
	[self hideMenu];
	[self ShowMenuController];
}

-(void)setAnnotBorder30
{
    [self.annotEditUtilityTool setAnnotationBorder:3.0];
	[self hideMenu];
	[self ShowMenuController];
}

-(void)setAnnotBorder50
{
    [self.annotEditUtilityTool setAnnotationBorder:5.0];
	[self hideMenu];
	[self ShowMenuController];
}

-(void)setAnnotBorder90
{
    [self.annotEditUtilityTool setAnnotationBorder:9.0];
	[self hideMenu];
	[self ShowMenuController];
}


-(void)changeSelectedAnnotationToHighlight
{
	[self changeSelectedAnnotationTo:e_ptHighlight];
}

-(void)changeSelectedAnnotationToUnderline
{
	[self changeSelectedAnnotationTo:e_ptUnderline];
}

-(void)changeSelectedAnnotationToSquiggly
{
	[self changeSelectedAnnotationTo:e_ptSquiggly];
}

-(void)changeSelectedAnnotationToStrikeout
{
	[self changeSelectedAnnotationTo:e_ptStrikeOut];
}

-(void)deleteSelectedAnnotation
{
	[self ClearSelectionBars];
	[self ClearSelectionOnly];
	self->selectionStart = self->selectionEnd = CGPointZero;
	[super deleteSelectedAnnotation];
}

#pragma mark - MenuController menu adding

-(void)attachAnnotTypeChangeMenuItems
{
	NSMutableArray* menuItems = [[NSMutableArray alloc] init];
    
    UIMenuItem* menuItem;
	
	NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
	
	if( [m_moving_annotation GetType] != e_ptHighlight )
	{
		menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Highlight", @"PDFNet-Tools", toolsStringBundle, @"Highlight tool name") action:@selector(changeSelectedAnnotationToHighlight)];
		[menuItems addObject:menuItem];
	}
	if( [m_moving_annotation GetType] != e_ptUnderline )
	{
		menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Underline", @"PDFNet-Tools", toolsStringBundle, @"Underline tool name") action:@selector(changeSelectedAnnotationToUnderline)];
		[menuItems addObject:menuItem];
	}
	if( [m_moving_annotation GetType] != e_ptSquiggly )
	{
		menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Squiggly", @"PDFNet-Tools", toolsStringBundle, @"Squiggly tool name") action:@selector(changeSelectedAnnotationToSquiggly)];
		[menuItems addObject:menuItem];
	}
	if( [m_moving_annotation GetType] != e_ptStrikeOut )
	{
		menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Strikeout", @"PDFNet-Tools", toolsStringBundle, @"Strikeout tool name") action:@selector(changeSelectedAnnotationToStrikeout)];
		[menuItems addObject:menuItem];
	}

    
    UIMenuController *theMenu = [UIMenuController sharedMenuController];
    theMenu.menuItems = menuItems;
}

- (void) attachInitialMenuItems
{
    NSMutableArray* menuItems = [[NSMutableArray alloc] initWithCapacity:2];
    
    UIMenuItem* menuItem;
	
	NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
	menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Delete", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(deleteSelectedAnnotation)];
    [menuItems addObject:menuItem];
	menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Comment", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(editSelectedAnnotationNote)];
    [menuItems addObject:menuItem];
    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Color", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(editSelectedAnnotationStrokeColor)];
    [menuItems addObject:menuItem];
	
	if( [m_moving_annotation GetType] != e_ptHighlight )
	{
		menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Thickness", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(editSelectedAnnotationBorder)];
		[menuItems addObject:menuItem];
	}
	
	menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Opacity", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(editSelectedAnnotationOpacity)];
    [menuItems addObject:menuItem];
	menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Type", @"PDFNet-Tools", toolsStringBundle, @"") action:@selector(editSelectedAnnotationType)];
    [menuItems addObject:menuItem];
    
    UIMenuController *theMenu = [UIMenuController sharedMenuController];
    theMenu.menuItems = menuItems;
    
}

- (void)ShowUtilityMenuController
{
    if( selectionLayers.count != 0 && selectionOnScreen == true )
    {
        UIMenuController *theMenu = [UIMenuController sharedMenuController];
        [theMenu setTargetRect:CGRectMake(selectionStartCorner.x, selectionStartCorner.y-15, selectionEndCorner.x-selectionStartCorner.x, selectionEndCorner.y-selectionStartCorner.y+30) inView:self];
        [theMenu setMenuVisible:YES animated:YES];
    }
}

#pragma mark - Annotation Modification

-(void)changeSelectedAnnotationTo:(PTAnnotType)annotType
{
	@try
	{
		[m_pdfViewCtrl DocLock:YES];
		PTTextMarkup* mkup = [[PTTextMarkup alloc] initWithAnn:m_moving_annotation];
		PTObj* sdfObj = [mkup GetSDFObj];
		
		if( annotType == e_ptHighlight )
			[sdfObj PutName:@"Subtype" name:@"Highlight"];
		else if( annotType == e_ptStrikeOut )
			[sdfObj PutName:@"Subtype" name:@"StrikeOut"];
		else if ( annotType == e_ptUnderline )
			[sdfObj PutName:@"Subtype" name:@"Underline"];
		else if ( annotType == e_ptSquiggly )
			[sdfObj PutName:@"Subtype" name:@"Squiggly"];
		
		[mkup RefreshAppearance];
		
	}
	@catch (NSException *exception) {
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally {
		[m_pdfViewCtrl DocUnlock];
	}
	
	[m_pdfViewCtrl UpdateWithAnnot:m_moving_annotation page_num:m_annot_page_number];
	
	[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
}

-(void)changeSelectedTextMarkupAnnotQuads
{
    PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
    
    @try
    {
        [m_pdfViewCtrl DocLock:YES];
		
		
		PTPage* p = [doc GetPage:m_annot_page_number];
		
		
		if( ![p IsValid] )
		{
			return;
		}
		
		assert([p IsValid]);
		
		PTSelection* sel = [m_pdfViewCtrl GetSelection:m_annot_page_number];
		PTVectorQuadPoint* quads = [sel GetQuads];
		NSUInteger numQuads = [quads size];
		
		PTPDFRect* oldRect = [m_moving_annotation GetRect];
		
		PTPDFPoint* startRectPointA = [[PTPDFPoint alloc] initWithPx:[oldRect GetX1] py:[oldRect GetY1]];
		PTPDFPoint* startRectPointB = [[PTPDFPoint alloc] initWithPx:[oldRect GetX2] py:[oldRect GetY2]];
		
		PTPDFPoint* newPtA = [m_pdfViewCtrl ConvPagePtToScreenPt:startRectPointA page_num:m_annot_page_number];
		PTPDFPoint* newPtB = [m_pdfViewCtrl ConvPagePtToScreenPt:startRectPointB page_num:m_annot_page_number];
		
		PTPDFRect* pageSpaceOldRect = [[PTPDFRect alloc] initWithX1:[newPtA getX] y1:[newPtA getY] x2:[newPtB getX] y2:[newPtB getY]];
		
		PTPDFRect* boundingRect;
		
		// overwrite existing quads with nothing
		[[m_moving_annotation GetSDFObj] EraseDictElementWithKey:@"QuadPoints"];
		[[m_moving_annotation GetSDFObj] EraseDictElementWithKey:@"Rect"];
		
		if( numQuads > 0 )
		{
			PTQuadPoint* qp = [quads get:0];
			
			TRN_point* point = [qp getP1];
			
			double x1 = [point getX];
			double y1 = [point getY];
			
			point = [qp getP3];
			
			double x2 = [point getX];
			double y2 = [point getY];
			
			PTPDFRect* r = [[PTPDFRect alloc] initWithX1:x1 y1:y1 x2:x2 y2:y2];
			
			if( ! boundingRect )
			{
				boundingRect = [[PTPDFRect alloc] initWithX1:x1 y1:y1 x2:x2 y2:y2];
				[m_moving_annotation SetRect:boundingRect];
			}
			else
				boundingRect = [self GetRectUnion:boundingRect Rect2:r];
			
			PTTextMarkup* mktp = [[PTTextMarkup alloc] initWithAnn:m_moving_annotation];
			
			for( int i=0; i < numQuads; ++i )
			{
				PTQuadPoint* quad = [quads get:i];

				if( textMarkupAdobeHack )
				{
					// Acrobat and Preview do not follow the PDF specification regarding
					// the ordering of quad points in a text markup annotation. Enable
					// this code for compatibility with those viewers.

					PTPDFPoint* point1 = [quad getP1];
					PTPDFPoint* point2 = [quad getP2];
					PTPDFPoint* point3 = [quad getP3];
					PTPDFPoint* point4 = [quad getP4];
					
					PTQuadPoint* newQuad = [[PTQuadPoint alloc] init];
					
					[newQuad setP1:point4];
					[newQuad setP2:point3];
					[newQuad setP3:point1];
					[newQuad setP4:point2];
					
					[mktp SetQuadPoint:i qp:newQuad];
					
				}
				else
				{
					[mktp SetQuadPoint:i qp:quad];
				}
				
			}
			
			[mktp RefreshAppearance];
			[m_pdfViewCtrl UpdateWithRect:pageSpaceOldRect];
			[m_pdfViewCtrl UpdateWithAnnot:mktp page_num:m_annot_page_number];
			
		}
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }
	
	[self annotationModified:m_moving_annotation onPageNumber:m_annot_page_number];
	
    [self ClearSelectionBars];
    
    [self ClearSelectionOnly];
}

#pragma mark - Annotation Selection

-(void)selectCurrentTextMarkupAnnotation
{
	if( !m_moving_annotation )
		return;
	
	// get the quads that represent the text that is annotated (not the entire anntation rect)
	PTTextMarkup* mkup = [[PTTextMarkup alloc] initWithAnn:m_moving_annotation];
	
	int numQuads = [mkup GetQuadPointCount];
	
	NSMutableArray* textMarkupQuads = [[NSMutableArray alloc] init];
	
	for(int m = 0; m < numQuads; ++m)
	{
		PTQuadPoint* qp = [mkup GetQuadPoint:m];
		
		double minX = fmin([[qp getP1] getX], fmin([[qp getP2] getX], fmin([[qp getP3] getX], [[qp getP4] getX])));
		double minY = fmin([[qp getP1] getY], fmin([[qp getP2] getY], fmin([[qp getP3] getY], [[qp getP4] getY])));
		double maxX = fmax([[qp getP1] getX], fmax([[qp getP2] getX], fmax([[qp getP3] getX], [[qp getP4] getX])));
		double maxY = fmax([[qp getP1] getY], fmax([[qp getP2] getY], fmax([[qp getP3] getY], [[qp getP4] getY])));
		
		PTPDFRect* bbox = [[PTPDFRect alloc] initWithX1:minX y1:minY x2:maxX y2:maxY];
		
		[bbox Normalize];
		[textMarkupQuads addObject:bbox];
	}
	
	selectionStartPage = selectionEndPage = m_annot_page_number;
	
	PTPDFRect* startRect = textMarkupQuads.firstObject;
	PTPDFRect* endRect = textMarkupQuads.lastObject;
	
	selectionStart.x = [m_pdfViewCtrl GetRightToLeftLanguage] ? [startRect GetX2] :[startRect GetX1];
	selectionStart.y = [startRect GetY1];
	
	selectionEnd.x = [m_pdfViewCtrl GetRightToLeftLanguage] ? [endRect GetX1] : [endRect GetX2];
	selectionEnd.y = [endRect GetY2];
	
	NSMutableArray* selection = [self MakeSelection];
	
	
	CGRect firstQuad = [selection.firstObject CGRectValue];

	int rtlOffset = 0;
	if( [m_pdfViewCtrl GetRightToLeftLanguage] == YES)
		rtlOffset = firstQuad.size.width;

	selectionStartCorner = CGPointMake(firstQuad.origin.x + [m_pdfViewCtrl GetHScrollPos]+rtlOffset, firstQuad.origin.y + [m_pdfViewCtrl GetVScrollPos]);
	
	CGRect lastQuad = [selection.lastObject CGRectValue];
	
	rtlOffset = 0;
	if( [m_pdfViewCtrl GetRightToLeftLanguage] == YES)
		rtlOffset = lastQuad.size.width;
	
	selectionEndCorner = CGPointMake(lastQuad.origin.x + lastQuad.size.width + [m_pdfViewCtrl GetHScrollPos] - rtlOffset, lastQuad.origin.y + lastQuad.size.height/2 + [m_pdfViewCtrl GetVScrollPos]);
	
	
	[self DrawSelectionQuads:selection WithLines:YES];
	[self DrawSelectionBars:selection];
}

-(BOOL)handleSelectionEvent:(UIGestureRecognizer *)sender
{

	CGPoint down = [sender locationInView:m_pdfViewCtrl];
	
	m_annot_page_number = [m_pdfViewCtrl GetPageNumberFromScreenPt:down.x y:down.y];
	
	double screenScale = 1;
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] == YES )
        screenScale = [UIScreen mainScreen].scale ;
    
    @try
	{
		[m_pdfViewCtrl DocLockRead];
		
		m_moving_annotation = [m_pdfViewCtrl GetAnnotationAt:down.x y:down.y distanceThreshold:GET_ANNOT_AT_DISTANCE_THRESHOLD minimumLineWeight:GET_ANNOT_AT_MINIMUM_LINE_WEIGHT];
		
		if( [m_moving_annotation IsValid] )
		{
			PTAnnotType annotType = [m_moving_annotation GetType];
			
			if( annotType == e_ptHighlight || annotType == e_ptUnderline || annotType == e_ptStrikeOut || annotType == e_ptSquiggly	)
			{
				[self ClearSelectionBars];
				[self selectCurrentTextMarkupAnnotation];
				[self attachInitialMenuItems];
				[self ShowMenuController];
			}
			else
			{
				nextToolType = self.defaultClass;
				return NO;
			}
		}
		
	}
	@catch (NSException *exception) {
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally {
		[m_pdfViewCtrl DocUnlockRead];
	}
	
	if( ![m_moving_annotation IsValid] )
    {
		nextToolType = self.defaultClass;
		return NO;
	}
    else
	{
		
	}
	
    return YES;
}

- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    if( ![super touchesShouldCancelInContentView:view] )
	{
		return NO;
	}
	else
	{
		nextToolType = self.defaultClass;
		return YES;
	}
}

- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	UITouch *touch = touches.allObjects[0];
	
    if( [touch.view isKindOfClass:[SelectionBar class]])
	{
		return [super onTouchesBegan:touches withEvent:event];
	}
    else
	{
		[self ClearSelectionBars];
		[self ClearSelectionOnly];
		nextToolType = self.defaultClass;
		return NO;
	}
}

-(BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	
	PTAnnotType annotType = [m_moving_annotation GetType];
	
	if( !(annotType == e_ptHighlight || annotType == e_ptUnderline || annotType == e_ptStrikeOut || annotType == e_ptSquiggly)	)
	{
		return YES;
	}

	[super onTouchesEnded:touches withEvent:event];
	
	// 2nd condition: only select if not already selected
	if( m_moving_annotation )
	{
		// remove little dots after adjusting an annotation in creation mode, and clicking to dismiss
		
		if( leftBar != 0 )
		{
			[leftBar removeFromSuperview];
			leftBar = 0;
		}
		
		if( rightBar != 0 )
		{
			[rightBar removeFromSuperview];
			rightBar = 0;
		}
		
		// necessary to select an annot immediately after it is created
		[self selectCurrentTextMarkupAnnotation];
		
		[self attachInitialMenuItems];
		[self ShowMenuController];
	}
	
	return YES;
	
}

-(BOOL)onCustomEvent:(id)userData
{
	[self selectCurrentTextMarkupAnnotation];
	[self attachInitialMenuItems];
	[self ShowMenuController];
	return YES;
}

- (BOOL)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    return [self handleSelectionEvent:(UITapGestureRecognizer *)gestureRecognizer];
}

- (BOOL)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    return [self handleSelectionEvent:(UILongPressGestureRecognizer *)gestureRecognizer];
}

-(void) selectionBarUp:(SelectionBar*) bar withTouches:(NSSet *) touches withEvent: (UIEvent *) event
{
	[super selectionBarUp:bar withTouches:touches withEvent:event];
	
	@try
	{
		[m_pdfViewCtrl DocLock:YES];

		[self changeSelectedTextMarkupAnnotQuads];

		[self selectCurrentTextMarkupAnnotation];
		
		nextToolType = [TextMarkupEditTool class];
	}
	@catch (NSException *exception) {
		NSLog(@"Exception: %@: %@",exception.name, exception.reason);
	}
	@finally {
		[m_pdfViewCtrl DocUnlock];
	}
	
}

-(void)saveNewNoteForMovingAnnotationWithString:(NSString*)str
{
	[super saveNewNoteForMovingAnnotationWithString:str];
	[self ShowMenuController];
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
