//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "SelectionRectView.h"
#import "LineEditTool.h"

@implementation SelectionRectView

- (instancetype)initWithFrame:(CGRect)frame forAnnot:(PTAnnot*)annot
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        m_annot = annot;
        m_rectOffset = 5;
        self.alpha = 0.1;
    }
    
    return self;
}

+(Class)layerClass
{
    // permits layer to become very big, for
    // when we have zoomed in close and selected
    // a large annotation.
    // otherwise consumes too much memory and sometimes crashes.
    return [NoFadeTiledLayer class];
}

-(void)setEditLine:(BOOL)startAtNE
{
    if( m_startAtNE != startAtNE || !m_isLineAnnot)
    {
        self.backgroundColor = [UIColor clearColor];
        m_isLineAnnot = true;
        m_startAtNE = startAtNE;
        self.alpha = 0;
        [self setHidden:YES];
        self.contentMode = UIViewContentModeRedraw;
    }
    else
    {
        // default, no changes needed
    }
}

-(void)setFrame:(CGRect)frame
{
    if( m_isLineAnnot )
    {
        super.frame = CGRectInset(frame, -m_rectOffset, -m_rectOffset);
    }
    else
    {
        super.frame = frame;
    }
}

-(CGRect)frame
{
    if( m_isLineAnnot )
    {
        CGRect rectForResizeWidgets = CGRectInset(super.frame, m_rectOffset, m_rectOffset);
        rectForResizeWidgets.size.width = MAX(rectForResizeWidgets.size.width, 0);
        rectForResizeWidgets.size.height = MAX(rectForResizeWidgets.size.height, 0);
        return rectForResizeWidgets;
    }
    else
    {
        return super.frame;
    }
}

-(void)setAnnot:(PTAnnot*)annot
{
    m_annot = annot;

}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    if( m_isLineAnnot )
    {
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        
        if( [m_annot IsValid] )
        {
            PTColorPt* strokePoint = [m_annot GetColorAsRGB];
            
            double r = [strokePoint Get:0];
            double g = [strokePoint Get:1];
            double b = [strokePoint Get:2];
			
			if( [self.m_pdfViewCtrl GetColorPostProcessMode] == e_ptpostprocess_invert )
			{
				r = 1.-r;
				g = 1.-g;
				b = 1.-b;
			}
            
            int strokeColorComps = [m_annot GetColorCompNum];
            
            UIColor* strokeColor = [UIColor colorWithRed:r green:g blue:b alpha:(strokeColorComps > 0 ? 1 : 0)];
            
            double thickness = [[m_annot GetBorderStyle] GetWidth];
            
            thickness *= [self.m_pdfViewCtrl GetZoom];
            
            CGContextSetLineWidth(currentContext, thickness);
            CGContextSetLineCap(currentContext, kCGLineCapButt);
            CGContextSetLineJoin(currentContext, kCGLineJoinMiter);
            CGContextSetStrokeColorWithColor(currentContext, strokeColor.CGColor);
        }
        
        CGContextBeginPath (currentContext);

        if( !m_startAtNE )
        {
            CGContextMoveToPoint(currentContext, m_rectOffset, MAX(self.bounds.size.height-m_rectOffset,0));
            CGContextAddLineToPoint(currentContext, MAX(self.bounds.size.width-m_rectOffset,0), m_rectOffset);
        }
        else
        {
            CGContextMoveToPoint(currentContext, m_rectOffset, m_rectOffset);
            CGContextAddLineToPoint(currentContext, MAX(self.bounds.size.width-m_rectOffset,0), MAX(self.bounds.size.height-m_rectOffset,0));
        }
        
        CGContextStrokePath(currentContext);
    }
}

-(UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if( [self.m_pdfViewCtrl.toolDelegate.tool isKindOfClass:[LineEditTool class]])
    {
        // we don't use a selection rectangle for arrows and lines,
        // so only allow movement if the actual annotation is hit
        point = [self convertPoint:point toView:self.m_pdfViewCtrl];
        PTAnnot* annot = [self.m_pdfViewCtrl GetAnnotationAt:point.x y:point.y distanceThreshold:GET_ANNOT_AT_DISTANCE_THRESHOLD minimumLineWeight:GET_ANNOT_AT_MINIMUM_LINE_WEIGHT];
        
        if( [annot IsValid] )
            return self;
        else
            return Nil;
    }
    
    return self;
}


@end
