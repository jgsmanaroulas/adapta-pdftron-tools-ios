//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "EllipseCreate.h"


@implementation EllipseCreate

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        pdfNetAnnotType = [PTCircle class];
    }
    
    return self;
}

-(PTAnnotType)annotType
{
	return e_ptCircle;
}

- (void)drawRect:(CGRect)rect
{
    if( pageNumber >= 1 && !self.allowScrolling )
    {
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        
        double thickness = [super setupContextFromDefaultAnnot:currentContext];

        CGRect myRect = drawArea;

        myRect.origin.x += thickness/2;
        myRect.origin.y += thickness/2;
        myRect.size.width -= thickness;
        myRect.size.height -= thickness;

        CGContextFillEllipseInRect(currentContext, myRect);
        CGContextAddEllipseInRect(currentContext, myRect);

        
        CGContextStrokePath(currentContext);
    }

    
    [super drawRect:rect];
    
}


@end
