//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "Tool.h"

@interface FormFillTool : Tool<UIPopoverPresentationControllerDelegate, UITextViewDelegate, UITextFieldDelegate, UITableViewDelegate>
{
    UIView* responder;
    BOOL keyboardOnScreen;
    NSMutableArray* choices;
    int characterLimit;
    NSInteger selectionStart;
    NSInteger selectionEnd;
    NSString* originalText;
    UITextRange *originalRange;
}

@property (nonatomic) CGRect fontRect;

-(NSInteger)numberOfChoices;
-(NSString*)titleOfChoiceAtIndex:(NSUInteger)num;
-(NSMutableArray*)getSelectedItemsInActiveListbox;
-(void)javascriptCallback:(const char*)event_type json:(const char*)json;

@end
