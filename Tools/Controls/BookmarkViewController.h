//
//  BookmarkViewController.h
//  Tools
//
//  Created by PDFTron on 2014-09-23.
//
//

#import <UIKit/UIKit.h>

#import <PDFNet/PDFViewCtrl.h>



@class BookmarkViewController;

/**
 * The methods declared by the OutlineViewControllerDelegate protocol allow the adopting delegate to respond to messages from
 * the AnnotationViewController class.
 *
 */
@protocol BookmarkViewControllerDelegate <NSObject>
@optional
- (void)bookmarkViewController: (BookmarkViewController*)bookmarkViewController selectedBookmark: (NSDictionary*)aBookmark;
- (void)bookmarkViewControllerDidCancel: (BookmarkViewController*)bookmarkViewController;
- (void)bookmarkViewControllerSwitchToAnnotations:(BookmarkViewController*)bookmarkViewController;
- (void)bookmarkViewControllerSwitchToOutline:(BookmarkViewController*)bookmarkViewController;
@end



@interface BookmarkViewController : UITableViewController<UITextViewDelegate>

/**
 * An object that conforms to the BookmarkViewControllerDelegate protocol.
 *
 */
@property (nonatomic, assign) id<BookmarkViewControllerDelegate> delegate;

/**
 * Returns a new instance of a BookmarkViewController.
 *
 */
- (instancetype)initWithPDFViewCtrl: (PTPDFViewCtrl*)pdfViewCtrl userId:(NSString*) userId NS_DESIGNATED_INITIALIZER;

/**
 *
 */
@property (nonatomic, strong) NSURL* docPath;

/**
 * If true, will display a toggle button that can be used to replace the control with an BookmarkViewController.
 */
@property (nonatomic, assign) BOOL alsoAnnotations;

/**
 * If true, will display a toggle button that can be used to replace the control with an OutlineViewController.
 */
@property (nonatomic, assign) BOOL alsoOutline;

/**
 * User bookmarks
 */
@property (nonatomic, strong) NSMutableArray* bookmarks;


@property (nonatomic, strong) NSString* userId;


- (void)dismiss;
- (void)refresh;
- (void)refreshToolbarItens;

-(instancetype)initWithStyle:(UITableViewStyle)style __attribute__((unavailable("Not the designated initializer")));

-(instancetype)initWithCoder:(NSCoder *)aDecoder __attribute__((unavailable("Not the designated initializer")));

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil __attribute__((unavailable("Not the designated initializer")));

@end
