//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

#import <PDFNet/PDFViewCtrl.h>

#import "Tool.h"
#import "FreeHandCreate.h"
#import "FreehandCreateToolbar.h"
#import "ToolManager.h"

@class AnnotationToolbar;

/**
 * The methods declared by the AnnotationToolbarDelegate protocol allow the adopting delegate to respond to messages from
 * the AnnotationToolbar class.
 *
 */
@protocol AnnotationToolbarDelegate <NSObject>

- (void)annotationToolbarDidCancel: (AnnotationToolbar*)annotationToolbar;

/**
 * Asks the delegate if the tool should stay in creation mode
 *
 * @return if the tool should stay in creation mode.
 *
 */
- (BOOL)toolShouldGoBackToPan;

@end

/**
 * The AnnotationToolbar will display a toolbar with buttons that allows the user to switch between tools. See the
 * Complete Reader sample project for example usage.
 *
 */
@interface AnnotationToolbar : UIToolbar<FreeHandCreateDelegate, FreehandCreateToolbarDelegate>

//@property (nonatomic, strong) PTPDFViewCtrl *pdfView;
@property (nonatomic, strong) ToolManager* toolManager;
@property (nonatomic, assign) id<AnnotationToolbarDelegate, UIToolbarDelegate> delegate;



-(void)setButtonForTool:(Tool*)tool;

/**
 *
 * An asynchronous selector that prepares a document for printing and calls the PrintDelegate selector
 * PreparedToPrint:UserData: when perperation is complete. It may take a moment to prepare the document,
 * so it is recommended that the user be presented with information to this effect.
 *
 * @param orientation - The new orrientation.
 *
 */
-(void)rotateToOrientation:(UIInterfaceOrientation)orientation;

@end
