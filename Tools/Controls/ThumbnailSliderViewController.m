//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "ThumbnailSliderViewController.h"


@interface ThumbnailSliderViewController () {
    UILabel *_label;
    UISlider *_slider;
    CGPoint _startPoint;
	PTPDFViewCtrl* _pdfViewCtrl;
	BOOL _touchDown;
	UIView* _thumbView;
	int _pageNumber;
	UILabel* _pageLabel;
	UIImageView* _thumbnailView;
}

@end

@interface ThumbnailSliderViewController ()

@end

@implementation ThumbnailSliderViewController

-(UIImage*)GetThumbnailImageWithProperRotation:(UIImage*)thumbnail
{
	
	
	PTRotate rotation = [_pdfViewCtrl GetRotation];
	int orientation;
	int originalOrientation = thumbnail.imageOrientation;
	
	
	switch (rotation) {
		case e_pt0:
			orientation = UIImageOrientationUp;
			break;
		case e_pt90:
			orientation = UIImageOrientationRight;
			break;
		case e_pt180:
			orientation = UIImageOrientationDown;
			break;
		case e_pt270:
			orientation = UIImageOrientationLeft;
			break;
			
		default:
			break;
	}
	
	if( orientation != originalOrientation )
	{
		UIImage* rotatedImage;
		rotatedImage = [[UIImage alloc] initWithCGImage:thumbnail.CGImage scale:1.0 orientation:orientation];
		return rotatedImage;
	}
	else
	{
		return thumbnail;
	}
}

-(UIImage*)imageWithView:(UIView*)view
{
	UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0);
	[view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage* img = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return img;
}

-(void)setSliderValue:(int)pageNumber
{
	if ([[_pdfViewCtrl GetDoc] GetPageCount] > 1 && _touchDown == false) {
        _slider.value = ((float)(pageNumber - 1)) / ((float)([[_pdfViewCtrl GetDoc] GetPageCount] - 1));
    }
}

- (void)setPage: (int)pageNumber {
    
	[self setSliderValue:pageNumber];
	
	if ([_pdfViewCtrl GetDoc] && _touchDown == true) {
        [_pdfViewCtrl SetCurrentPage:pageNumber];
        [_pdfViewCtrl hideSelectedTextHighlights];
    }
	
}

-(void)setThumbnail:(UIImage*)image forPage:(int)pageNum
{
	if( _pageNumber == pageNum && (_thumbnailView.tag != _pageNumber))
	{
		if( !image )
		{
			UIView* whiteView = [[UIView alloc] initWithFrame:CGRectMake(0,
																		 0,
																		 _thumbView.frame.size.width-10,
																		 _thumbView.frame.size.height-30)];
			whiteView.backgroundColor = [UIColor whiteColor];
			image = [self imageWithView:whiteView];
		}
		
		if( _thumbnailView )
			[_thumbnailView removeFromSuperview];

		UIImage* rotatedImage = [self GetThumbnailImageWithProperRotation:image];
		
		_thumbnailView = [[UIImageView alloc] initWithImage:rotatedImage];
		_thumbnailView.frame = CGRectMake(5, 25, _thumbView.frame.size.width-10, _thumbView.frame.size.height-30);
		_thumbnailView.opaque = YES;
		_thumbnailView.tag = pageNum;
		_thumbnailView.contentMode = UIViewContentModeScaleAspectFit;
		[_thumbView addSubview:_thumbnailView];
		
		if( _pageNumber != pageNum )
		{
			UIImageView* deathView = _thumbnailView;
			
			// since this is the wrong thumbnail for this slider value
			// we replace it with a white rectangle after 0.3 seconds
			dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
			
				UIView* whiteView = [[UIView alloc] initWithFrame:CGRectMake(0,
																			 0,
																			 _thumbView.frame.size.width-10,
																			 _thumbView.frame.size.height-30)];
				whiteView.backgroundColor = [UIColor whiteColor];
				if( deathView )
					[deathView addSubview:whiteView];
				
			});
		}
	}
}

-(void)valueChanged:(UISlider*)slider
{	
	int pageNumber = 1 + roundf(([[_pdfViewCtrl GetDoc] GetPageCount] - 1) * _slider.value);
	if( pageNumber != _pageNumber )
	{
		[_pdfViewCtrl GetThumbAsync:pageNumber];
		_pageNumber = pageNumber;
		
		_pageLabel.text = [NSString stringWithFormat:@"%d", _pageNumber];
	}
	
}

-(void)setControlPageNumberToCurrentSliderPage
{
	assert([NSThread isMainThread]);
	[_pdfViewCtrl SetCurrentPage:_pageNumber];
}

-(void)touchDown:(UISlider*)slider
{
	if([self.delegate respondsToSelector:@selector(thumbnailSliderViewInUse:)])
		[self.delegate thumbnailSliderViewInUse:self];
	
	_touchDown = YES;
	
	CGSize screenSize = [UIScreen mainScreen].bounds.size;
	CGSize pdfViewCtrlSize = _pdfViewCtrl.frame.size;
	
	CGSize thumbSize = CGSizeMake(screenSize.width/8.0f*[UIScreen mainScreen].scale,screenSize.height/8.0f*[UIScreen mainScreen].scale);
	
	CGRect thumbFrame = CGRectMake(pdfViewCtrlSize.width/2-thumbSize.width/2-5,
								   pdfViewCtrlSize.height-thumbSize.height-self.view.frame.size.height,
								   thumbSize.width+10,
								   thumbSize.height+30);
	
	if( _thumbnailView )
	{
		[_thumbnailView removeFromSuperview];
	}
	
	if( _thumbView )
	{
		// should not occur
		[_thumbView removeFromSuperview];
		
		_thumbView = Nil;
		_thumbnailView = Nil;
	}
	
	_thumbView = [[UIView alloc] initWithFrame:thumbFrame];
	
	_thumbView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
	_thumbView.layer.cornerRadius = 5;
	_thumbView.layer.masksToBounds = YES;
	
	[_pdfViewCtrl GetThumbAsync:_pageNumber];
	
	UIImage* thumbnail;
	_thumbnailView = [[UIImageView alloc] initWithImage:thumbnail];
	_thumbnailView.frame = CGRectMake(5, 25, thumbFrame.size.width-10, thumbFrame.size.height-30);
	_thumbnailView.opaque = YES;
	_thumbnailView.alpha = 1.0;
	_thumbnailView.contentMode = UIViewContentModeScaleAspectFit;
	[_thumbView addSubview:_thumbnailView];
	
	_pageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, _thumbView.frame.size.width, 20)];
	_pageLabel.textAlignment = NSTextAlignmentCenter;
	_pageLabel.textColor = [UIColor whiteColor];
	UIFont* font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
	_pageLabel.font = font;
	_pageLabel.text = [NSString stringWithFormat:@"%d", _pageNumber];
	
	[_thumbView	addSubview:_pageLabel];
	[_pdfViewCtrl addSubview:_thumbView];
}

-(void)touchUp:(UISlider*)slider
{
	[_pdfViewCtrl  CancelAllThumbRequests];
	
	int pageNumber = 1 + roundf(([[_pdfViewCtrl GetDoc] GetPageCount] - 1) * _slider.value);
	[self setPage:pageNumber];
	
	_touchDown = NO;
	
	[UIView animateWithDuration:0.2f animations:^(void) {
		_thumbView.alpha = 0;
		
    } completion:^(BOOL finished) {
		[_thumbnailView removeFromSuperview];
		[_thumbView removeFromSuperview];
		
		_thumbView = Nil;
		_thumbnailView = Nil;
    }];
	
	
	if([self.delegate respondsToSelector:@selector(thumbnailSliderViewInUse:)])
		[self.delegate thumbnailSliderViewNotInUse:self];
}

// designated initializer
-(instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)pdfViewCtrl
{
	_pdfViewCtrl = pdfViewCtrl;
	
	return [self initWithNibName:Nil bundle:Nil];
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
	
		CGRect bounds = _pdfViewCtrl.bounds;
		
		if(([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeLeft) ||
		 ([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeRight))
		{
			double swap = bounds.size.height;
			bounds.size.height = bounds.size.width;
			bounds.size.width = swap;
		}

		_slider = [[UISlider alloc] initWithFrame:CGRectMake(0, -35.0f, bounds.size.width, 35.0f)];
		
        _slider.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        [_slider addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchDown];
		[_slider addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchUpOutside];
		[_slider addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchUpInside];
		[_slider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
		self.view = [[UIView alloc] initWithFrame:CGRectMake(0, bounds.size.height-35.0f, bounds.size.width, 35.0f)];
		[self.view addSubview:_slider];
		
		// OVAL
		UIView* thumb = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
		thumb.backgroundColor = [UIColor blackColor];
		thumb.layer.cornerRadius = 3;
		thumb.layer.masksToBounds = YES;
		thumb.alpha = 0.6;
		
		// SQUARE
		//		UIView* thumb = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
		//		[thumb setBackgroundColor:[UIColor blackColor]];
		//		thumb.alpha = 0.6;
		
		[_slider setThumbImage:[self imageWithView:thumb] forState:UIControlStateNormal];
        
		_slider.minimumTrackTintColor = [UIColor clearColor];
		_slider.maximumTrackTintColor = [UIColor clearColor];
		
		UIView* blackLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1.0f)];
		blackLine.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		blackLine.backgroundColor = [UIColor blackColor];
		[self.view addSubview:blackLine];
		(self.view).backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.95];
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
