//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "ThumbnailsViewController.h"
#import "DraggableCollectionViewFlowLayout.h"
#include "BookmarkUtils.h"

#import "PDFViewCtrlToolsUtil.h"

#define NAVIBAR_HEIGHT 64.0
#define EDITBAR_HEIGHT 44.0
#define THUMB_RATIO 3.5
#define SPACING_RATIO 12.0

@implementation ThumbnailsViewController

// designated initializer
-(instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)pdfViewCtrl
{
    if (!pdfViewCtrl) {
        [NSException raise:@"PDFViewCtrl is nil" format:@"PDFViewCtrl cannot be nil!"];
    } else {
        self = [super init];
        
        if( self )
        {
        _pdfViewCtrl = pdfViewCtrl;
        _thumbsList = [[NSMutableDictionary alloc]init];
        _selectedList = [[NSMutableArray alloc]init];
        }
    }
    
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
	
	NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
	
    // navigation bar
    _naviBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, NAVIBAR_HEIGHT)];
    _naviBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//    NSString* title = [[[_pdfViewCtrl GetDoc] GetDocInfo] GetTitle];
    NSString* title = self.title;
    UINavigationItem *titleItem = [[UINavigationItem alloc] initWithTitle:title];
    [_naviBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Voltar", @"PDFNet-Tools", toolsStringBundle, @"Close dialog")
																   style:UIBarButtonItemStylePlain
																  target:self
																  action:@selector(done)];
//    leftButton.
    
//    UIButton * customButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [customButton setTitle:@"Voltar" forState:UIControlStateNormal];
//    customButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:11.0f];
//    [customButton.layer setMasksToBounds:YES];
//    [customButton setImage:[UIImage imageNamed:@"ic_nav_back"] forState:UIControlStateNormal];
//    customButton.frame=CGRectMake(0.0, 100.0, 100.0, 25.0);
//    [customButton addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem * customItem = [[UIBarButtonItem alloc] initWithCustomView:customButton];
//    customItem.tintColor=[UIColor whiteColor];
//    self.navigationItem.leftBarButtonItem = customItem;
    
    titleItem.leftBarButtonItem = leftButton;
//    titleItem.rightBarButtonItem = self.editButtonItem;
    [_naviBar pushNavigationItem:titleItem animated:NO];
    _naviBar.tintColor = [UIColor whiteColor];
//    _naviBar.backgroundColor =  _toolbarBgColor;
    _naviBar.barTintColor = _toolbarBgColor;
    [self.view addSubview:_naviBar];
    
    // edit bar
    _editToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, NAVIBAR_HEIGHT, self.view.frame.size.width, EDITBAR_HEIGHT)];
    _editToolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _editToolBar.tintColor = [UIColor darkTextColor];
    _editToolBar.backgroundColor = [UIColor whiteColor];
    _deleteButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Delete", @"PDFNet-Tools", toolsStringBundle, @"") style:UIBarButtonItemStylePlain target:self action:@selector(deleteItem:)];
    _deleteButton.enabled = NO;
    _selectAllButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Select All", @"PDFNet-Tools", toolsStringBundle, @"For text selection") style:UIBarButtonItemStylePlain target:self action:@selector(selectAll:)];
    _selectAllButton.enabled = YES;
	
	NSString* selected = NSLocalizedStringFromTableInBundle(@"Selected", @"PDFNet-Tools", toolsStringBundle, @"");
	
    _selectedCountButton = [[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%@: 0", selected] style:UIBarButtonItemStylePlain target:self action:nil];
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    _editToolBar.items = @[_selectedCountButton, flexibleSpaceLeft, _deleteButton];
    [self.view addSubview:_editToolBar];
    _editToolBar.hidden = YES;
    
    // collection view layout
    CGFloat ratio = THUMB_RATIO; // Thumb size ratio
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
	CGSize pdfViewCtrlSize = _pdfViewCtrl.frame.size;
	
	CGSize thumbSize = CGSizeMake(screenSize.width/ratio,screenSize.height/ratio);
	
    _thumbFrame = CGRectMake(pdfViewCtrlSize.width/2-thumbSize.width/2,
                             pdfViewCtrlSize.height-thumbSize.height-self.view.frame.size.height,
                             thumbSize.width,
                             thumbSize.height);
    
    _spacingSize = _thumbFrame.size.height / SPACING_RATIO;
    
    DraggableCollectionViewFlowLayout *layout = [[DraggableCollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(_thumbFrame.size.width, _thumbFrame.size.height);
    layout.minimumInteritemSpacing = _spacingSize/2;
    layout.minimumLineSpacing = _spacingSize/2;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.sectionInset = UIEdgeInsetsMake(_spacingSize, _spacingSize, _spacingSize, _spacingSize);
    
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, NAVIBAR_HEIGHT, self.view.frame.size.width, self.view.frame.size.height-NAVIBAR_HEIGHT) collectionViewLayout:layout];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [_collectionView registerClass:[ThumbnailViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    _collectionView.backgroundColor = [UIColor darkGrayColor];
    if ([NSProcessInfo.processInfo isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){10,0,0}]) {
        _collectionView.prefetchingEnabled = NO;
    }
    
    
    [self.view addSubview:_collectionView];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _currentPageIndex = [_pdfViewCtrl GetCurrentPage];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if (self.editing) {
        [self setEditing:NO animated:NO];
    }
    
    [self scrollToCurrentPage];
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self performSelector:@selector(flashTableScrollIndicators) withObject:nil afterDelay:0.0];
}

- (void)flashTableScrollIndicators
{
    [_collectionView flashScrollIndicators];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [_thumbsList removeAllObjects];
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[_pdfViewCtrl GetDoc] GetPageCount];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ThumbnailViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
	cell.nightMode = !([_pdfViewCtrl GetColorPostProcessMode] == e_ptpostprocess_none);
	
    BOOL isCurrentPage = _currentPageIndex == (indexPath.item+1);
    BOOL isChecked = NO;
    if ([_selectedList containsObject:indexPath]) {
        isChecked = YES;
    }
    [cell setPageNumber:indexPath.item+1 isCurrentPage:isCurrentPage isEditing:self.editing isChecked:isChecked];
    
    if ([_thumbsList.allKeys containsObject:indexPath]) {
        [cell setThumbnail:_thumbsList[indexPath] forPage:indexPath.item+1];
    } else {
        [self requestThumbnail:(int)indexPath.item+1];
    }
    if (self.editing){
        [cell.checkbox addTarget:self action:@selector(checkboxClickEvent:event:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate methods
- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];

	
    if (self.editing) {
        ThumbnailViewCell *cell=(ThumbnailViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
        cell.checkbox.selected = !cell.checkbox.selected;
        if ([_selectedList containsObject:indexPath]) {
            [_selectedList removeObject:indexPath];
        } else {
            [_selectedList addObject:indexPath];
        }
        
        if (_selectedList.count > 0 && _selectedList.count < [[_pdfViewCtrl GetDoc] GetPageCount])
        {
            _deleteButton.enabled = YES;
        } else {
            _deleteButton.enabled = NO;
        }
		
		NSString* selected = NSLocalizedStringFromTableInBundle(@"Selected", @"PDFNet-Tools", toolsStringBundle, @"");
		
        _selectedCountButton.title = [NSString stringWithFormat:@"%@: %lu", selected, (unsigned long)_selectedList.count];
        
    } else {
        _currentPageIndex = (int)indexPath.item+1;
        [self done];
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    
    if (editing) {
        _collectionView.draggable = YES;
        _editToolBar.hidden = NO;
        CGRect frame = _collectionView.frame;
        frame.origin.x = 0;
        frame.origin.y = NAVIBAR_HEIGHT+EDITBAR_HEIGHT;
        frame.size.height = frame.size.height - EDITBAR_HEIGHT;
        _collectionView.frame = frame;
		
		NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
		
		NSString* selected = NSLocalizedStringFromTableInBundle(@"Selected", @"PDFNet-Tools", toolsStringBundle, @"");
        
        _selectedCountButton.title = [NSString stringWithFormat:@"%@: %lu", selected, (unsigned long)_selectedList.count];
        
    } else {
        _collectionView.draggable = NO;
        _editToolBar.hidden = YES;
        CGRect frame = _collectionView.frame;
        frame.origin.x = 0;
        frame.origin.y = NAVIBAR_HEIGHT;
        frame.size.height = frame.size.height + EDITBAR_HEIGHT;
        _collectionView.frame = frame;
    }
    [self clearSelection:YES];
    [_collectionView reloadData];
}

#pragma mark - Selector
-(void)done
{
    [_pdfViewCtrl CancelAllThumbRequests];
    [_pdfViewCtrl UpdatePageLayout];
    [_pdfViewCtrl SetCurrentPage:_currentPageIndex];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)deleteItem:(id)sender
{
    // show alert message
	
	NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
	
	NSString* title = NSLocalizedStringFromTableInBundle(@"Delete Page(s)?", @"PDFNet-Tools", toolsStringBundle, @"");
	NSString* message = NSLocalizedStringFromTableInBundle(@"Do you want to delete the selected page(s)? This action cannot be undone.", @"PDFNet-Tools", toolsStringBundle, @"");
	NSString* cancel = NSLocalizedStringFromTableInBundle(@"Cancel", @"PDFNet-Tools", toolsStringBundle, @"");
	NSString* delete = NSLocalizedStringFromTableInBundle(@"Delete", @"PDFNet-Tools", toolsStringBundle, @"");
	
	UIAlertController *alertController = [UIAlertController
										  alertControllerWithTitle:title
										  message:message
										  preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancel
														   style:UIAlertActionStyleCancel
														 handler:Nil];
	
	UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:delete
														   style:UIAlertActionStyleDestructive
														 handler:^(UIAlertAction *action){
															 [self deleteItems:_selectedList];
														 }];
	
	[alertController addAction:cancelAction];
	[alertController addAction:deleteAction];

	
	[self presentViewController:alertController animated:YES completion:nil];
}

-(void)deleteItems:(NSArray*)items
{
    // sort selected items by index
    NSArray *sortedArray;
    sortedArray = [items sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSIndexPath *first = (NSIndexPath*)a;
        NSIndexPath *second = (NSIndexPath*)b;
        return (first.item < second.item);
    }];
    
    // delete from back
    if (sortedArray.count > 0) {
        for (NSIndexPath *indexPath in sortedArray) {
            
            [_collectionView performBatchUpdates:^{
                
                // change current page
                if (_currentPageIndex == indexPath.item+1) {
                    if (indexPath.item+1 < [[_pdfViewCtrl GetDoc] GetPageCount]) {
                        //_currentPageIndex = _currentPageIndex+1;
                    } else {
                        _currentPageIndex = _currentPageIndex-1;
                    }
                } else if (_currentPageIndex > indexPath.item+1) {
                    _currentPageIndex--;
                }
                
                // delete page from PDFDoc
                PTPageIterator *itr = [[_pdfViewCtrl GetDoc] GetPageIterator: (int)indexPath.item+1];
                [[_pdfViewCtrl GetDoc] PageRemove: itr];
                
                // delete cell from control
                [_collectionView deleteItemsAtIndexPaths:@[indexPath]];
                
                [_thumbsList removeObjectForKey:indexPath];
                
            }completion:^(BOOL finished) {
                
            }];
        }
        [self updateThumbsList];
        [self updatePageNumber:items];
        [self clearSelection:NO];
    }
}

-(void)updatePageNumber:(NSArray*)items
{
    NSArray *sortedArray;
    sortedArray = [_collectionView.indexPathsForVisibleItems sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSIndexPath *first = (NSIndexPath*)a;
        NSIndexPath *second = (NSIndexPath*)b;
        return (first.item > second.item);
    }];
    
    NSIndexPath* first = sortedArray[0];
    int pageNum = (int)first.item+1;
    
    for (int i=0; i<[[_pdfViewCtrl GetDoc] GetPageCount]; i++) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        if (indexPath == nil){
            //NSLog(@"couldn't find index path");
        } else {
            ThumbnailViewCell *cell = (ThumbnailViewCell *)[_collectionView cellForItemAtIndexPath:indexPath];
            if (cell && [_collectionView.visibleCells containsObject:cell]) {
                BOOL isCurrentPage = _currentPageIndex == pageNum;
                BOOL isChecked = NO;
                [cell setPageNumber:pageNum isCurrentPage:isCurrentPage isEditing:self.editing isChecked:isChecked];
                if ([_thumbsList.allKeys containsObject:indexPath]) {
                    [cell setThumbnail:_thumbsList[indexPath] forPage:indexPath.item+1];
                } else {
                    [self requestThumbnail:(int)indexPath.item+1];
                }
                
                pageNum++;
            }
        }
    }
}

-(void)updateThumbsList
{
    if (_thumbsList) {
        for (int i=0; i<[[_pdfViewCtrl GetDoc] GetPageCount]; i++) {
            NSIndexPath * indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            if (indexPath == nil){
                //NSLog(@"couldn't find index path");
            } else {
                if ([_thumbsList.allKeys containsObject:indexPath] ) {
                    // do nothing
                } else {
                    NSIndexPath* next = [self findNextThumb:indexPath];
                    if (next) {
                        UIImage* thumb = _thumbsList[next];
                        _thumbsList[indexPath] = thumb;
                        [_thumbsList removeObjectForKey:next];
                    }
                }
            }
        }
    }
}

-(NSIndexPath*)findNextThumb:(NSIndexPath*)start
{
    if (start && _thumbsList) {
        if ([_thumbsList.allKeys containsObject:start] ) {
            return start;
        } else {
            if (start.item+1 <[[_pdfViewCtrl GetDoc] GetPageCount]) {
                NSIndexPath* next = [NSIndexPath indexPathForItem:start.item+1 inSection:0];
                return [self findNextThumb:next];
            } else {
                return nil;
            }
        }
    } else
        return nil;
}

-(void)selectAll:(id)sender
{
	NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
	
	NSString* selectAll = NSLocalizedStringFromTableInBundle(@"Select All", @"PDFNet-Tools", toolsStringBundle, @"");
	
    if ([_selectAllButton.title  isEqual:selectAll]) {
        
        for (int i=0; i<[[_pdfViewCtrl GetDoc]GetPageCount]; i++) {
            NSIndexPath * indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            if (indexPath == nil){
                //NSLog(@"couldn't find index path");
            } else {
                ThumbnailViewCell *cell = (ThumbnailViewCell *)[_collectionView cellForItemAtIndexPath:indexPath];
                cell.checkbox.selected = YES;
                [_selectedList addObject:indexPath];
                
            }
        }
        
        _deleteButton.enabled = NO;
		
		NSString* deselectAll = NSLocalizedStringFromTableInBundle(@"Deselect All", @"PDFNet-Tools", toolsStringBundle, @"");
        
        _selectAllButton.title = deselectAll;
    } else {
        
        [self clearSelection:YES];
        
        _selectAllButton.title = selectAll;
    }
    
}

- (IBAction)checkboxClickEvent:(id)sender event:(id)event {
    
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:_collectionView];
    NSIndexPath *indexPath = [_collectionView indexPathForItemAtPoint: currentTouchPosition];
    
    if (indexPath == nil){
        //NSLog(@"couldn't find index path");
    } else {
        ThumbnailViewCell *cell=(ThumbnailViewCell*)[_collectionView cellForItemAtIndexPath:indexPath];
        cell.checkbox.selected = !cell.checkbox.selected;
        
        //NSLog(@"%ld indexPath", (long)indexPath.item);
        
        if ([_selectedList containsObject:indexPath]) {
            [_selectedList removeObject:indexPath];
        } else {
            [_selectedList addObject:indexPath];
        }
        
        if (_selectedList.count > 0)
        {
            _deleteButton.enabled = YES;
        } else {
            _deleteButton.enabled = NO;
        }
        
        _selectedCountButton.title = [NSString stringWithFormat:@"Selected: %lu", (unsigned long)_selectedList.count];
    }
}

#pragma mark - Thumbnail

-(void)requestThumbnail:(int)pageNumber
{
    [_pdfViewCtrl GetThumbAsync:pageNumber];
}

-(void)setThumbnail:(UIImage*)image forPage:(NSInteger)pageNum
{
    
    NSIndexPath * indexPath = [NSIndexPath indexPathForItem:pageNum-1 inSection:0];
    if (indexPath == nil){
        //NSLog(@"couldn't find index path");
    } else {
        _thumbsList[indexPath] = image;
        ThumbnailViewCell *cell = (ThumbnailViewCell *)[_collectionView cellForItemAtIndexPath:indexPath];
        if (cell) {
            [cell setThumbnail:image forPage:pageNum];
        }
    }
}

#pragma mark - Actions
-(void)scrollToCurrentPage
{
    int num = _currentPageIndex-1;
    NSIndexPath * indexPath = [NSIndexPath indexPathForItem:num inSection:0];
    if (indexPath == nil){
        //NSLog(@"couldn't find index path");
    } else {
        [_collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:NO];
    }
}


#pragma mark - UICollectionViewDataSource_Draggable

- (BOOL)collectionView:(LSCollectionViewHelper *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    // only allows moving pages when all page thumbnails are ready in the data source
    return [self isAllThumbsExistFromIndexPath:indexPath toIndexPath:toIndexPath];
}

- (void)collectionView:(LSCollectionViewHelper *)collectionView moveItemAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
//    [self clearSelection:YES];
//	
//	
//	
//    UIImage *thumbFrom = _thumbsList[fromIndexPath];
//    BOOL didSetCurrentPage = NO;
//    
//    // update all cells in between
//    if (fromIndexPath.item < toIndexPath.item) {
//        for (int index = (int)fromIndexPath.item; index < toIndexPath.item; index++) {
//            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
//            NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:index+1 inSection:0];
//            UIImage *thumb = _thumbsList[nextIndexPath];
//            
//            // update data source
//            _thumbsList[indexPath] = thumb;
//            
//            if (indexPath.item+1 == _currentPageIndex) {
//                if (indexPath.item == fromIndexPath.item) {
//                    _currentPageIndex = (int)toIndexPath.item+1;
//                } else {
//                    _currentPageIndex = _currentPageIndex-1;
//                }
//                didSetCurrentPage = YES;
//            }
//            
//        }
//        if (toIndexPath.item+1 == _currentPageIndex && !didSetCurrentPage) {
//            if (toIndexPath.item == fromIndexPath.item) {
//                // current page index did not change
//            } else {
//                _currentPageIndex = _currentPageIndex-1;
//            }
//            didSetCurrentPage = YES;
//        }
//    } else {
//        for (int index = (int)fromIndexPath.item; index > toIndexPath.item; index--) {
//            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
//            NSIndexPath *prevIndexPath = [NSIndexPath indexPathForItem:index-1 inSection:0];
//            UIImage *thumb = _thumbsList[prevIndexPath];
//            
//            // update data source
//            _thumbsList[indexPath] = thumb;
//            
//            if (indexPath.item+1 == _currentPageIndex) {
//                if (indexPath.item == fromIndexPath.item) {
//                    _currentPageIndex = (int)toIndexPath.item+1;
//                } else {
//                    _currentPageIndex = _currentPageIndex+1;
//                }
//                didSetCurrentPage = YES;
//            }
//            
//        }
//        if (toIndexPath.item+1 == _currentPageIndex && !didSetCurrentPage) {
//            if (toIndexPath.item == fromIndexPath.item) {
//                // current page index did not change
//            } else {
//                _currentPageIndex = _currentPageIndex+1;
//            }
//            didSetCurrentPage = YES;
//        }
//    }
//    _thumbsList[toIndexPath] = thumbFrom;
//    
//    // update UI
//    [self updateCellsFromIndexPath:fromIndexPath toIndexPath:toIndexPath];
//    
//    // do nothing if the page was not moved
//    if (fromIndexPath.item != toIndexPath.item) {
//        // update PDFDoc
//        //[[_pdfViewCtrl GetDoc] Lock]; //ask james
//        
//        // get the page to move
//        PTPage *pageToMove = [[_pdfViewCtrl GetDoc] GetPage: (int)fromIndexPath.item+1];
//		
//		NSString* docPath = [[_pdfViewCtrl GetDoc] GetFileName];
//		
//		NSURL* url = [NSURL fileURLWithPath:docPath];
//		
//		NSArray* bookmarkArray = [NSArray arrayWithArray:[BookmarkUtils bookmarkDataForDocument:url]];
//		
//		NSArray* sortedBookmarks = [bookmarkArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
//			int first = [a[@"page-number"] intValue];
//			int second = [b[@"page-number"] intValue];
//			return (first > second);
//		}];
//		
//		NSMutableArray* mutableBookmarkArray = [sortedBookmarks mutableCopy];
//		
//        if (fromIndexPath.item < toIndexPath.item) {
//            
//            // get destination page iterator
//            PTPageIterator *dest = [[_pdfViewCtrl GetDoc] GetPageIterator: (int)toIndexPath.item+2];
//            // copy page to destination
//            [[_pdfViewCtrl GetDoc] PageInsert: dest page: pageToMove];
//            // delete original page
//            PTPageIterator *itr = [[_pdfViewCtrl GetDoc] GetPageIterator: (int)fromIndexPath.item+1];
//            [[_pdfViewCtrl GetDoc] PageRemove: itr];
//        } else {
//            
//            // get destination page iterator
//            PTPageIterator *dest = [[_pdfViewCtrl GetDoc] GetPageIterator: (int)toIndexPath.item+1];
//            // copy page to destination
//            [[_pdfViewCtrl GetDoc] PageInsert: dest page: pageToMove];
//            // delete original page
//            PTPageIterator *itr = [[_pdfViewCtrl GetDoc] GetPageIterator: (int)fromIndexPath.item+2];
//            [[_pdfViewCtrl GetDoc] PageRemove: itr];
//        }
//		
//		PTPage* newPage = [[_pdfViewCtrl GetDoc] GetPage: (int)toIndexPath.item+1];
//		
//		NSMutableArray* newBookmarks = [BookmarkUtils updateUserBookmarks:mutableBookmarkArray
//							 oldPageNumber:(unsigned int)fromIndexPath.item+1
//							 newPageNumber:(unsigned int)toIndexPath.item+1
//							  oldSDFNumber:[[pageToMove GetSDFObj] GetObjNum]
//							  newSDFNumber:[[newPage GetSDFObj] GetObjNum]];
//		
//		
//		[BookmarkUtils saveBookmarkData:newBookmarks forFileUrl:url];
//        //[[_pdfViewCtrl GetDoc] Unlock];
//    }
    
}

#pragma mark - UICollectionViewDataSource_Draggable Helper

-(BOOL)isAllThumbsExistFromIndexPath:(NSIndexPath*)fromPath toIndexPath:(NSIndexPath*)toPath
{
    if (fromPath.item < toPath.item) {
        for (int index = (int)fromPath.item; index <= toPath.item; index++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
            if (![_thumbsList.allKeys containsObject:indexPath]) {
                return NO;
            }
        }
        return YES;
    } else {
        for (int index = (int)toPath.item; index <= fromPath.item; index++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
            if (![_thumbsList.allKeys containsObject:indexPath]) {
                return NO;
            }
        }
        return YES;
    }
}
-(void)updateCellsFromIndexPath:(NSIndexPath*)fromPath toIndexPath:(NSIndexPath*)toPath
{
    // **important**
    // this method is called before the collection view update the cell position after a drag event
    // i.e. it still follows the index path before any movement
    // therefore we need to update the cell corresponding to the index path before movement
    
    if (fromPath.item < toPath.item) {
        for (int index = (int)fromPath.item; index < toPath.item; index++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
            NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:index+1 inSection:0];
            ThumbnailViewCell *cell = (ThumbnailViewCell *)[_collectionView cellForItemAtIndexPath:nextIndexPath];
            if (cell) {
                BOOL isCurrentPage = _currentPageIndex == (indexPath.item+1);
                BOOL isChecked = NO;
                [cell setPageNumber:indexPath.item+1 isCurrentPage:isCurrentPage isEditing:self.editing isChecked:isChecked];
                [cell setThumbnail:_thumbsList[indexPath] forPage:indexPath.item+1];
            }
        }
    } else {
        for (int index = (int)toPath.item; index < fromPath.item; index++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
            NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:index+1 inSection:0];
            ThumbnailViewCell *cell = (ThumbnailViewCell *)[_collectionView cellForItemAtIndexPath:indexPath];
            if (cell) {
                BOOL isCurrentPage = _currentPageIndex == (nextIndexPath.item+1);
                BOOL isChecked = NO;
                [cell setPageNumber:nextIndexPath.item+1 isCurrentPage:isCurrentPage isEditing:self.editing isChecked:isChecked];
                [cell setThumbnail:_thumbsList[nextIndexPath] forPage:nextIndexPath.item+1];
            }
        }
    }
}

-(void)clearSelection:(BOOL)uncheckCell
{
    if (_selectedList.count > 0)
    {
        if (uncheckCell) {
            for (NSIndexPath *indexPath in _selectedList) {
                ThumbnailViewCell *cell=(ThumbnailViewCell*)[_collectionView cellForItemAtIndexPath:indexPath];
                if (cell) {
                    cell.checkbox.selected = NO;
                }
            }
        }
        [_selectedList removeAllObjects];
        
        _selectedCountButton.title = [NSString stringWithFormat:@"Selected: %lu", (unsigned long)_selectedList.count];
    }
    
    _deleteButton.enabled = NO;
}

@end
