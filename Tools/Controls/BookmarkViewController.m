//
//  BookmarkViewController.m
//  Tools
//
//  Created by PDFTron on 2014-09-23.
//

#import "BookmarkViewController.h"
#import "BookmarkUtils.h"
#import "PDFViewCtrlToolsUtil.h"


@interface BookmarkTextView : UITextView

@property (nonatomic, copy) NSString* bookmarkID;

@end

@implementation BookmarkTextView

@end

@interface BookmarkViewController ()
{
    
}

-(void)setToolbarItems;
-(void)reloadBookmarksFromDisk;

@property (nonatomic, weak) PTPDFViewCtrl *pdfViewCtrl;
@property (nonatomic, copy) NSString* freshlyAddedBookmark;
@property (nonatomic, assign) BOOL newBookmarkEditing;
@property (nonatomic, strong) UITextView* bookmarkTextView;
@property (nonatomic) UIButton* addBookmarkView;

@end

#define TEXT_VIEW 10

@implementation BookmarkViewController

-(void)setAlsoAnnotations:(BOOL)alsoAnnotations
{
    _alsoAnnotations = alsoAnnotations;
    [self setToolbarItems];
}

-(void)setAlsoOutline:(BOOL)alsoOutline
{
    _alsoOutline = alsoOutline;
    [self setToolbarItems];
}

- (void) refreshToolbarItens {
    [self setToolbarItems];
}

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl *)pdfViewCtrl userId:(NSString*)userId {
    
    NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
    self.title = NSLocalizedStringFromTableInBundle(@"Bookmarks", @"PDFNet-Tools", stringBundle, @"Title of bookmarks list");
    self.userId = userId;
    
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        self.pdfViewCtrl = pdfViewCtrl;
        
        //		self.alsoAnnotations = YES;
        //		self.alsoOutline = YES;
        UIEdgeInsets oldInsets = self.tableView.contentInset;
        self.tableView.contentInset = UIEdgeInsetsMake(20, oldInsets.left, oldInsets.bottom, oldInsets.right);
    }
    
    return self;
}



- (void)reloadBookmarksFromDisk
{
    NSArray* bookmarkArray = [NSArray arrayWithArray:[BookmarkUtils bookmarkDataForDocument:self.docPath withUserId:self.userId]];
    
    NSArray* sortedBookmarks = [bookmarkArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        int first = [a[@"page-number"] intValue];
        int second = [b[@"page-number"] intValue];
        return (first > second);
    }];
    
    self.bookmarks = [NSMutableArray arrayWithArray:sortedBookmarks];
    
    [self refresh];
}

-(void)setDocPath:(NSURL *)docPath
{
    _docPath = docPath;
    
    [self reloadBookmarksFromDisk];
}

-(void)setToolbarItems
{
    NSMutableArray* otherViews = [NSMutableArray array];
    
    
    NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
    if( self.alsoOutline )
        [otherViews addObject:NSLocalizedStringFromTableInBundle(@"Outline", @"PDFNet-Tools", stringBundle, @"Outline")];
    
    if( self.alsoAnnotations )
        [otherViews addObject:NSLocalizedStringFromTableInBundle(@"Annotations", @"PDFNet-Tools", stringBundle, @"Annotations")];
    
    
    [otherViews addObject:NSLocalizedStringFromTableInBundle(@"Bookmarks", @"PDFNet-Tools", stringBundle, @"Bookmarks")];
    
    
    if( self.alsoAnnotations || self.alsoOutline	)
    {
        UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:otherViews];
        segmentedControl.frame = CGRectMake(0.0f, 0.0f, 300.0f, 35.0f);
        segmentedControl.selectedSegmentIndex = 2;
        [segmentedControl addTarget:self action:@selector(changeMode:) forControlEvents:UIControlEventValueChanged];
        
        [self setToolbarItems:@[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:@selector(add:)],
                                [[UIBarButtonItem alloc] initWithCustomView:segmentedControl],
                                [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:@selector(add:)]] animated:NO];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
    // On iPhone we need a button to dismiss the full-screen view. iPad the user can click outside the popover.
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Done", @"PDFNet-Tools", stringBundle, @"Done") style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    }
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    //	self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.toolbarHidden = NO;
    [self reloadBookmarksFromDisk];
    [self refresh];
    self.addBookmarkView = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    self.addBookmarkView.layer.borderWidth = 1.0f;
    self.addBookmarkView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
    
    self.addBookmarkView.frame = CGRectMake(0, self.navigationController.navigationBar.frame.origin.y+self.navigationController.navigationBar.frame.size.height, self.navigationController.navigationBar.frame.size.width, 50);
    self.addBookmarkView.backgroundColor = [UIColor whiteColor];
    [self.addBookmarkView setTitle:NSLocalizedStringFromTableInBundle(@"+ Add Bookmark", @"PDFNet-Tools", stringBundle, @"Add Bookmark") forState:UIControlStateNormal];
    [self.addBookmarkView addTarget:self action:@selector(addBookmark:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.view addSubview:self.addBookmarkView];
    
    self.editing = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.addBookmarkView removeFromSuperview];
    [BookmarkUtils saveBookmarkData:[NSArray arrayWithArray:self.bookmarks] forFileUrl:self.docPath withUserId:_userId];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self setEditing:NO animated:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeMode: (UISegmentedControl*)segmentedControl {
    
    NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
    if( [[segmentedControl titleForSegmentAtIndex:segmentedControl.selectedSegmentIndex] isEqualToString:NSLocalizedStringFromTableInBundle(@"Outline", @"PDFNet-Tools", stringBundle, @"Outline")] )
        [self.delegate bookmarkViewControllerSwitchToOutline:self];
    else
        [self.delegate bookmarkViewControllerSwitchToAnnotations:self];
    
}

-(void)addBookmark:(UIButton*)button
{
    int pageNum = [self.pdfViewCtrl GetCurrentPage];
    
    NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    NSString* page = NSLocalizedStringFromTableInBundle(@"Page", @"PDFNet-Tools", stringBundle, @"");
    
    
    NSString* pageNumString = [NSString stringWithFormat:@"%@ %d", page, pageNum];
    NSString* uniqueID = [NSUUID UUID].UUIDString;
    unsigned int sdfNum = [[[[self.pdfViewCtrl GetDoc] GetPage:pageNum] GetSDFObj] GetObjNum];
    self.freshlyAddedBookmark = uniqueID;
    
    [self.bookmarks addObject:[@{@"name" : pageNumString,
                                 @"page-number" : @(pageNum),
                                 @"obj-number" : @(sdfNum),
                                 @"unique-id" : uniqueID} mutableCopy]];
    
    NSArray* sortedBookmarks = [self.bookmarks sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        int first = [a[@"page-number"] intValue];
        int second = [b[@"page-number"] intValue];
        return (first > second);
    }];
    
    self.bookmarks = [NSMutableArray arrayWithArray:sortedBookmarks];
    
    [self refresh];
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    for(UITableViewCell* view in (self.tableView).visibleCells)
    {
        for(UIView* subview in (view.contentView).subviews)
        {
            if( [subview isKindOfClass:[UITextView class]] )
            {
                subview.userInteractionEnabled = editing;
            }
        }
    }
    
    if( editing == NO )
    {
        // On iPhone we need a button to dismiss the full-screen view. iPad the user can click outside the popover.
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
            self.navigationItem.rightBarButtonItem.enabled = YES;
        }
        [self.tableView endEditing:YES];
    }
    else
    {
        // On iPhone we need a button to dismiss the full-screen view. iPad the user can click outside the popover.
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
            self.navigationItem.rightBarButtonItem.enabled = NO;
        }
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    if ([text isEqualToString:@"\n"]) {
        [self setEditing:NO animated:YES];
        return NO;
    }
    return YES;
}

-(BOOL)textViewShouldBeginEditing:(UITextView*)textView
{
    BOOL retValue =  self.tableView.editing || self.newBookmarkEditing;
    self.newBookmarkEditing = NO;
    return retValue;
}

-(void)textViewDidEndEditing:(UITextView*)textView
{
    for(int i = 0; i < self.bookmarks.count; ++i) {
        if( [self.bookmarks[i][@"unique-id"] isEqualToString:((BookmarkTextView*)textView).bookmarkID] )
        {
            self.bookmarks[i][@"name"] = textView.text;
            break;
        }
    }
    
    [BookmarkUtils saveBookmarkData:[NSArray arrayWithArray:self.bookmarks] forFileUrl:self.docPath withUserId:_userId];
}

- (void)dismiss {
    if ([self.delegate respondsToSelector:@selector(bookmarkViewControllerDidCancel:)]) {
        [self.delegate bookmarkViewControllerDidCancel:self];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return (self.bookmarks).count;
}

- (void)refresh {
    [self.tableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( self.tableView.rowHeight == -1 )
        self.tableView.rowHeight = UITableViewAutomaticDimension;
    if( self.tableView.rowHeight == -1 )
        self.tableView.rowHeight = 44;
    
    return self.tableView.rowHeight;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    else
    {
        [(cell.contentView).subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    CGRect frame = CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.rowHeight);
    BookmarkTextView* tv = [[BookmarkTextView alloc] initWithFrame:frame];
    
    tv.text = [self.bookmarks[indexPath.row][@"name"] stringByTrimmingCharactersInSet:
               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    // allows bookmarks to be used for navigation but not later editing
    tv.editable = YES;
    tv.delegate = self;
    tv.tag = indexPath.row;
    tv.returnKeyType = UIReturnKeyDone;
    
    tv.bookmarkID = (NSString*)(self.bookmarks[indexPath.row][@"unique-id"]);
    
    tv.userInteractionEnabled = YES;
    
    [cell.contentView addSubview:tv];
    
    	cell.contentView.autoresizesSubviews = YES;
    
    tv.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    if(self.freshlyAddedBookmark && [(NSString*)(self.bookmarks[indexPath.row][@"unique-id"]) isEqualToString:self.freshlyAddedBookmark] )
    {
        self.newBookmarkEditing = YES;
        self.bookmarkTextView = tv;
        self.freshlyAddedBookmark = Nil;
    }
    
    cell.accessoryView = [[ UIImageView alloc ] initWithImage:[PDFViewCtrlToolsUtil toolImageNamed:@"dismiss.png"]];
    [cell.accessoryView setFrame:CGRectMake(0, 0, 20, 20)];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(deleteBookmark:)];

    [cell.accessoryView addGestureRecognizer:singleFingerTap];
    cell.accessoryView.userInteractionEnabled = YES;
    
    tv.font = [UIFont systemFontOfSize:14.0f];
    
    
    return cell;
}

-(void)deleteBookmark:(UITapGestureRecognizer *) sender
{
    
    CGPoint point = [sender locationInView:self.tableView];
    
    NSIndexPath *theIndexPath = [self.tableView indexPathForRowAtPoint:point];
    
    [self.bookmarks removeObjectAtIndex:theIndexPath.row];
    
    [self.tableView deleteRowsAtIndexPaths:@[theIndexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    		[self.bookmarks removeObjectAtIndex:indexPath.row];
    
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row == ((NSIndexPath*)tableView.indexPathsForVisibleRows.lastObject).row )
    {
        [self.bookmarkTextView becomeFirstResponder];
    }
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


//// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        // Delete the row from the data source
//
//		[self.bookmarks removeObjectAtIndex:indexPath.row];
//
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//    }
//}


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int pg = [self.bookmarks[indexPath.row][@"page-number"] intValue];
    [self.pdfViewCtrl SetCurrentPage:pg];
    
    if ([self.delegate respondsToSelector:@selector(bookmarkViewController:selectedBookmark:)]) {
        
        [self.delegate bookmarkViewController:self selectedBookmark:self.bookmarks[indexPath.row]];
    }
    
    //	if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
    [self dismiss];
    //	}
}

@end
