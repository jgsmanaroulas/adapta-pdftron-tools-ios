//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "AnnotationToolbar.h"

#import "ArrowCreate.h"
#import "EllipseCreate.h"
#import "FreeHandCreate.h"
#import "FreeTextCreate.h"
#import "LineCreate.h"
#import "RectangleCreate.h"
#import "StickyNoteCreate.h"
#import "PanTool.h"
#import "TextHighlightCreate.h"
#import "TextStrikeoutCreate.h"
#import "TextUnderlineCreate.h"
#import "DigitalSignatureTool.h"
#import "FreehandCreateToolbar.h"
#import "AnalyticsHandlerAdapter.h"
#import "Eraser.h"
#import "PDFViewCtrlToolsUtil.h"

@interface AnnotationToolbar () {
    UISegmentedControl *actions, *objects, *text, *comment;
	FreehandCreateToolbar *_freehandCreateToolbar;
	UIView* _freehandHolder;
	NSMutableArray* _buttonsArray;
	NSArray* _precedenceArray;
}

- (void)cancelTool: (UISegmentedControl*)barButton;

@end


@implementation AnnotationToolbar

//-2 Comment
//-3 Highlight
//-6 Strike
//-7 Underline
//-13 Squiggly
//-5 Signature
//-4 FreeHand
//-8 FreeText
//-9 Arrow
//-12 Line
//-10 Rectangle
//-11 Elipse
//-1 Pan

// order in which buttons should appear
typedef enum {
	e_bar_stickynote,
	e_bar_highlight,
	e_bar_strikeout,
	e_bar_underline,
	e_bar_squiggly,
	e_bar_signature,
	e_bar_freehand,
	e_bar_eraser,
	e_bar_freetext,
	e_bar_arrow,
	e_bar_line,
	e_bar_rectangle,
	e_bar_ellipse,
	e_bar_pan,
	e_bar_close,
	e_bar_last_entry,
} barButton;

@synthesize delegate = _annotationDelegate, toolManager = _toolManager;

- (UIImage *)makeImageNegative:(UIImage *)originalImage
{
    UIGraphicsBeginImageContext(originalImage.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeCopy);
    CGRect imageRect = CGRectMake(0, 0, originalImage.size.width, originalImage.size.height);
    [originalImage drawInRect:imageRect];
	
	
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeDifference);
    // translate/flip the graphics context (for transforming from CG* coords to UI* coords
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0, originalImage.size.height);
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
    //mask the image
    CGContextClipToMask(UIGraphicsGetCurrentContext(), imageRect,  originalImage.CGImage);
    CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(),[UIColor whiteColor].CGColor);
    CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, originalImage.size.width, originalImage.size.height));
    UIImage *returnImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    return returnImage;
}


- (instancetype)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	
    if (self) {
		
		_freehandCreateToolbar = [[FreehandCreateToolbar alloc] initWithFrame:CGRectMake(0.0f, -64.0f, frame.size.width, 44.0f)];
		_freehandCreateToolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		_freehandCreateToolbar.hidden = NO;
        [self addSubview:_freehandCreateToolbar];
		
		_buttonsArray = [[NSMutableArray alloc] initWithCapacity:12];
		
		UIButton* commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[commentButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_sticky_note.png"] forState:UIControlStateNormal];
		[commentButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_sticky_note_i.png"] forState:UIControlStateSelected];
		[commentButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
		commentButton.tag = e_bar_stickynote;
		[_buttonsArray addObject:commentButton];
		
		
		UIButton* sigButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[sigButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_signature.png"] forState:UIControlStateNormal];
		[sigButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_signature_i.png"] forState:UIControlStateSelected];
		[sigButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
		sigButton.tag = e_bar_signature;
		[_buttonsArray addObject:sigButton];
		
				
		UIButton* freeHandButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[freeHandButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_free_hand.png"] forState:UIControlStateNormal];
		[freeHandButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_free_hand_i.png"] forState:UIControlStateSelected];
		[freeHandButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
		freeHandButton.tag = e_bar_freehand;
		[_buttonsArray addObject:freeHandButton];
		
		
		UIButton* squareButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[squareButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_square.png"] forState:UIControlStateNormal];
		[squareButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_square_i.png"] forState:UIControlStateSelected];
		[squareButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
		squareButton.tag = e_bar_rectangle;
		[_buttonsArray addObject:squareButton];
		
		UIButton* arrowButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[arrowButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_arrow.png"] forState:UIControlStateNormal];
		[arrowButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_arrow_i.png"] forState:UIControlStateSelected];
		[arrowButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
		arrowButton.tag = e_bar_arrow;
		[_buttonsArray addObject:arrowButton];
		
		UIButton* freeTextButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[freeTextButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_free_text.png"] forState:UIControlStateNormal];
		[freeTextButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_free_text_i.png"] forState:UIControlStateSelected];
		[freeTextButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
		freeTextButton.tag = e_bar_freetext;
		[_buttonsArray addObject:freeTextButton];
		
		UIButton* highlightButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[highlightButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_highlight.png"] forState:UIControlStateNormal];
		[highlightButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_highlight_i.png"] forState:UIControlStateSelected];
		[highlightButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
		highlightButton.tag = e_bar_highlight;
		[_buttonsArray addObject:highlightButton];
		
		UIButton* underlineButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[underlineButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_underline.png"] forState:UIControlStateNormal];
		[underlineButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_underline_i.png"] forState:UIControlStateSelected];
		[underlineButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
		underlineButton.tag = e_bar_underline;
		[_buttonsArray addObject:underlineButton];
		
		UIButton* strikeoutButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[strikeoutButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_strikeout.png"] forState:UIControlStateNormal];
		[strikeoutButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_strikeout_i.png"] forState:UIControlStateSelected];
		[strikeoutButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
		strikeoutButton.tag = e_bar_strikeout;
		[_buttonsArray addObject:strikeoutButton];
		
		UIButton* eraserButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[eraserButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_eraser.png"] forState:UIControlStateNormal];
		[eraserButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_eraser_i.png"] forState:UIControlStateSelected];
		[eraserButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
		eraserButton.tag = e_bar_eraser;
		[_buttonsArray addObject:eraserButton];
		
		UIButton* lineButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[lineButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_line.png"] forState:UIControlStateNormal];
		[lineButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_line_i.png"] forState:UIControlStateSelected];
		[lineButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
		lineButton.tag = e_bar_line;
		[_buttonsArray addObject:lineButton];
		
		UIButton* ellipseButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[ellipseButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_circle.png"] forState:UIControlStateNormal];
		[ellipseButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"annotation_circle_i.png"] forState:UIControlStateSelected];
		[ellipseButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
		ellipseButton.tag = e_bar_ellipse;
		[_buttonsArray addObject:ellipseButton];
		
		UIButton* panButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[panButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"pan.png"] forState:UIControlStateNormal];
		[panButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"pan_i.png"] forState:UIControlStateSelected];
		[panButton addTarget:self action:@selector(toggleButton:) forControlEvents:UIControlEventTouchUpInside];
		panButton.tag = e_bar_pan;
		[_buttonsArray addObject:panButton];
		 
		for (UIButton* button in _buttonsArray) {
			button.frame = CGRectMake(0, 0, 44, 40);
			[button addTarget:self action:@selector(buttonTouchDown:) forControlEvents:UIControlEventTouchDown];
		}
		
		UIButton* closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
		closeButton.frame = CGRectMake(0, 0, 44, 40);
		[closeButton setImage:[PDFViewCtrlToolsUtil toolImageNamed:@"dismiss.png"] forState:UIControlStateNormal];
		[closeButton addTarget:self action:@selector(doCancel:) forControlEvents:UIControlEventTouchUpInside];
		closeButton.tag = e_bar_close;
		[_buttonsArray addObject:closeButton];
		
		_precedenceArray = @[@(e_bar_close),
							@(e_bar_pan),
							@(e_bar_stickynote),
							@(e_bar_highlight),
							@(e_bar_freehand),
							@(e_bar_signature),
							@(e_bar_strikeout),
							@(e_bar_underline),
							@(e_bar_freetext),
							@(e_bar_arrow),
							@(e_bar_eraser),
							@(e_bar_ellipse),
							@(e_bar_line),
							@(e_bar_rectangle)];
	
		[self setButtonItems];
	}

	return self;
}
	
-(void)setButtonItems
{
	UIBarButtonItem* sideSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
	sideSpaceLeft.width = 10;
	UIBarButtonItem* sideSpaceRight = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
	sideSpaceRight.width = 10;
	
	NSMutableArray* objectsForToolbar = [[NSMutableArray alloc] init];
	
	[objectsForToolbar addObject:sideSpaceLeft];
	[objectsForToolbar addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];

    // set number of toolbar items based on available space (for iPhone portrait orientation)
	NSUInteger maxPermitted = MIN([_precedenceArray count]-1,(int)([[UIScreen mainScreen] bounds].size.width / [_buttonsArray[0] frame].size.width)+1);

	int added = 0;
	
	for(int i = 0; i < e_bar_last_entry; i++)
	{
		for(int j = 0; j < maxPermitted; j++ ) {
			if( i == [_precedenceArray[j] intValue])
			{
				int tagWeWant = i;
				for (UIButton* button in _buttonsArray) {
					if( button.tag == tagWeWant )
					{
						[objectsForToolbar addObject:[[UIBarButtonItem alloc] initWithCustomView:button]];
						[objectsForToolbar addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
						added++;
						if( added >= maxPermitted )
							goto escapeLoops;
					}
				}
				
			}
		}
	}
	
escapeLoops:
	
	[objectsForToolbar addObject:sideSpaceRight];
	
	self.items = Nil;
	self.items = [NSArray arrayWithArray:[objectsForToolbar copy]];
}

-(void)rotateToOrientation:(UIInterfaceOrientation)orientation
{
	[self setButtonItems];
	[self bringSubviewToFront:_freehandCreateToolbar];
}

-(void)buttonTouchDown:(UIButton*)button
{
	button.backgroundColor = button.tintColor;
}

-(void)toggleButton:(UIButton*)button
{
	[self toggleButton:button andSetTool:YES];
}

-(void)toggleButton:(UIButton*)button andSetTool:(BOOL)setTool
{
	for (UIBarButtonItem* barButton in self.items) {
		if([barButton.customView isMemberOfClass:[UIButton class]])
		{
			((UIButton*)(barButton.customView)).backgroundColor = [UIColor clearColor];
			[((UIButton*)(barButton.customView)) setSelected:NO];
		}
	}
	
	button.backgroundColor = button.tintColor;
	button.selected = YES;
		
	

	if( setTool )
	{
		BOOL backToPan;
		
		if ([self.delegate respondsToSelector:@selector(toolShouldGoBackToPan)])
			backToPan = [self.delegate toolShouldGoBackToPan];
		else
			backToPan = YES;

		switch (button.tag) {
			case e_bar_signature:
				{
					DigitalSignatureTool* dst = (DigitalSignatureTool*)[_toolManager changeTool:[DigitalSignatureTool class]];
					
					// always false
					dst.backToPanToolAfterUse = NO;

					[[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] Signature selected"];
				}
				break;
			case e_bar_freehand:
				{
					[[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] Freehand selected"];
					
					FreeHandCreate* fhc = (FreeHandCreate*)[_toolManager changeTool:[FreeHandCreate class]];
					fhc.multistrokeMode = YES;
					fhc.delegate = self;

					fhc.backToPanToolAfterUse = backToPan;
					[self addFreehandCreateToolbar];
				}
				break;
			case e_bar_rectangle:
				{
					[[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] Rectangle selected"];
					
					RectangleCreate* rc = (RectangleCreate*)[_toolManager changeTool:[RectangleCreate class]];
					rc.backToPanToolAfterUse = backToPan;
				}
				break;
			case e_bar_arrow:
				{
					[[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] Arrow selected"];
					
					ArrowCreate* ac = (ArrowCreate*)[_toolManager changeTool:[ArrowCreate class]];
					ac.backToPanToolAfterUse = backToPan;
				}
				break;
			case e_bar_freetext:
				{
					[[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] Freetext selected"];
					
					FreeTextCreate* ftc = (FreeTextCreate*)[_toolManager changeTool:[FreeTextCreate class]];
					ftc.backToPanToolAfterUse = backToPan;
				}
				break;
			case e_bar_underline:
				{
					[[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] Underline selected"];
					
					TextUnderlineCreate* tuc = (TextUnderlineCreate*)[_toolManager changeTool:[TextUnderlineCreate class]];
					tuc.backToPanToolAfterUse = backToPan;
				}
				break;
			case e_bar_strikeout:
				{
					[[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] Strikeout selected"];
					
					TextStrikeoutCreate* tsc = (TextStrikeoutCreate*)[_toolManager changeTool:[TextStrikeoutCreate class]];
					tsc.backToPanToolAfterUse = backToPan;
				}
				break;
			case e_bar_highlight:
				{
					[[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] Highlight selected"];
					
					TextHighlightCreate* thc = (TextHighlightCreate*)[_toolManager changeTool:[TextHighlightCreate class]];
					thc.backToPanToolAfterUse = backToPan;
				}
				break;
			case e_bar_stickynote:
				{
					[[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] Sticky Note selected"];
					StickyNoteCreate* snc = (StickyNoteCreate*)[_toolManager changeTool:[StickyNoteCreate class]];
					snc.backToPanToolAfterUse = backToPan;
				}
				break;
			case e_bar_eraser:
			{
				[[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] Eraser selected"];
				Eraser* eraser = (Eraser*)[_toolManager changeTool:[Eraser class]];
				eraser.backToPanToolAfterUse = backToPan;
			}
				break;
			case e_bar_line:
			{
				[[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] Line selected"];
				LineCreate* lc = (LineCreate*)[_toolManager changeTool:[LineCreate class]];
				lc.backToPanToolAfterUse = backToPan;
			}
				break;
			case e_bar_ellipse:
			{
				[[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] Ellipse selected"];
				EllipseCreate* ec = (EllipseCreate*)[_toolManager changeTool:[EllipseCreate class]];
				ec.backToPanToolAfterUse = backToPan;
			}
				break;
			default: //e_bar_pan:
				{
					[[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] Pan selected"];
					
					PanTool* pt = (PanTool*)[_toolManager changeTool:[PanTool class]];
					pt.backToPanToolAfterUse = backToPan;
				}
				break;
		}
	}
}

- (void)doCancel: (UIBarButtonSystemItem*)barButton {
    if ([self.delegate respondsToSelector:@selector(annotationToolbarDidCancel:)]) {
        [self.delegate annotationToolbarDidCancel:self];
    }
}

- (void)doUndo: (UIBarButtonSystemItem*)barButton {
    // Stub to be filled in later
}

-(void)strokeAdded:(FreeHandCreate*)freeHandCreate
{
	[_freehandCreateToolbar disableRedoButton];
	[_freehandCreateToolbar enableUndoButton];
}

- (void)reset {
    [self cancelTool:nil];
}

- (void)setButtonForTool: (Tool*)tool {
	barButton buttonToSelect = -1;
	
	
    if ([tool isKindOfClass:[PanTool class]]) {
        buttonToSelect = e_bar_pan;
    } else if ([tool isKindOfClass:[RectangleCreate class]]) {
		buttonToSelect = e_bar_rectangle;
    } else if ([tool isKindOfClass:[FreeHandCreate class]]) {
         buttonToSelect = e_bar_freehand;
    } else if ([tool isKindOfClass:[ArrowCreate class]]) {
         buttonToSelect = e_bar_arrow;
    } else if ([tool isKindOfClass:[StickyNoteCreate class]]) {
         buttonToSelect = e_bar_stickynote;
    } else if ([tool isKindOfClass:[TextUnderlineCreate class]]) {
         buttonToSelect = e_bar_underline;
    } else if ([tool isKindOfClass:[TextStrikeoutCreate class]]) {
         buttonToSelect = e_bar_strikeout;
    } else if ([tool isKindOfClass:[TextHighlightCreate class]]) {
         buttonToSelect = e_bar_highlight;
    } else if ([tool isKindOfClass:[FreeTextCreate class]]) {
         buttonToSelect = e_bar_freetext;
	} else if ([tool isKindOfClass:[DigitalSignatureTool class]]) {
         buttonToSelect = e_bar_signature;
	} else if ([tool isKindOfClass:[Eraser class]]) {
		buttonToSelect = e_bar_eraser;
	} else if ([tool isKindOfClass:[LineCreate class]]) {
		buttonToSelect = e_bar_line;
	} else if ([tool isKindOfClass:[EllipseCreate class]]) {
		buttonToSelect = e_bar_ellipse;
	} else {
		if ([tool.defaultClass isSubclassOfClass:[PanTool class]]) {
			buttonToSelect = e_bar_pan;
		}
	}
	
	for (UIBarButtonItem* barButton in self.items) {
		if([barButton.customView isMemberOfClass:[UIButton class]])
		{
			if( ((UIButton*)(barButton.customView)).tag == buttonToSelect )
			{
				if( ((UIButton*)(barButton.customView)).selected == NO )
					[self toggleButton:(UIButton*)(barButton.customView) andSetTool:NO];
				break;
			}
		}
	}
}


- (void)cancelTool: (UISegmentedControl*)segmentedControl {
    objects.selectedSegmentIndex = -1;
    text.selectedSegmentIndex = -1;
    actions.selectedSegmentIndex = 0;
	[_toolManager changeTool:[PanTool class]];
}


-(void)addFreehandCreateToolbar
{
	[UIView animateWithDuration:0.2f animations:^(void) {
		_freehandCreateToolbar.frame = CGRectMake(0.0f, 0.0f, self.bounds.size.width, 44.0f);
		[self bringSubviewToFront:_freehandCreateToolbar];
		//[_freehandHolder bringSubviewToFront:_freehandCreateToolbar];

	} completion:^(BOOL finished) {
		
	}];
}

-(void)dismissFreehandToolbar:(UIButton*)button
{
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] freehand Save button selected"];
    
	[UIView animateWithDuration:0.2f animations:^(void) {
		_freehandCreateToolbar.frame = CGRectMake(0.0f, -64.0f, self.bounds.size.width, 44.0f);
		//[self bringSubviewToFront:_freehandCreateToolbar];
		
	} completion:^(BOOL finished) {
		[(FreeHandCreate*)_toolManager.tool commitAnnotation];
		{
			BOOL backToPan = ((Tool*)self.toolManager.tool).backToPanToolAfterUse;
			
			PanTool* pt = (PanTool*)[_toolManager changeTool:[PanTool class]];

			pt.backToPanToolAfterUse = backToPan;
		}
	}];
}

-(void)cancelFreehandToolbar:(UIButton*)button
{
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] freehand Cancel button selected"];
    
	[UIView animateWithDuration:0.2f animations:^(void) {
		_freehandCreateToolbar.frame = CGRectMake(0.0f, -64.0f, self.bounds.size.width, 44.0f);
		
	} completion:^(BOOL finished) {
		{
			BOOL backToPan = ((Tool*)self.toolManager.tool).backToPanToolAfterUse;
			
			PanTool* pt = (PanTool*)[_toolManager changeTool:[PanTool class]];
			pt.backToPanToolAfterUse = backToPan;
		}
	}];
}

-(void)undoFreetextStroke:(UIButton*)button
{
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] freehand Undo button selected"];
     
	[(FreeHandCreate*)_toolManager.tool undoStroke];
	if(![(FreeHandCreate*)_toolManager.tool canUndoStroke])
		[_freehandCreateToolbar disableUndoButton];
	if([(FreeHandCreate*)_toolManager.tool canRedoStroke])
		[_freehandCreateToolbar enableRedoButton];
}

-(void)redoFreetextStroke:(UIButton*)button
{
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Annotation Toolbar] freehand Redo button selected"];
    
	[(FreeHandCreate*)_toolManager.tool redoStroke];
	if(![(FreeHandCreate*)_toolManager.tool canRedoStroke])
		[_freehandCreateToolbar disableRedoButton];
	if([(FreeHandCreate*)_toolManager.tool canUndoStroke])
		[_freehandCreateToolbar enableUndoButton];
}

@end
