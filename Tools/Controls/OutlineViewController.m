//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "OutlineViewController.h"
#import "AnalyticsHandlerAdapter.h"
#import "PDFViewCtrlToolsUtil.h"


#define KEY_TITLE       @"title"
#define KEY_CHILDREN    @"hasChildren"
#define KEY_PAGENUM     @"pageNumber"
#define KEY_BOOKMARK    @"_bookmark"

@interface OutlineViewController () {
    PTPDFDoc *_document;
	PTPDFViewCtrl *_pdfViewCtrl;
    PTBookmark *_bookmark;
    NSMutableArray *_childrenBookmarks;
    int _selectedSegment;
	BOOL _containsBookmarks;
}

@end

@implementation OutlineViewController

-(void)setAlsoAnnotations:(BOOL)alsoAnnotations
{
	_alsoAnnotations = alsoAnnotations;
	[self setToolbarItems];
}

-(void)setAlsoBookmarks:(BOOL)alsoBookmarks
{
	_alsoBookmarks = alsoBookmarks;
	[self setToolbarItems];
}

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)pdfViewCtrl
{
    self = [self initWithPDFViewCtrl:pdfViewCtrl fromBookmark:nil];
    if (self) {
       
    }
    return self;
}

- (void)refresh
{
    [self.tableView reloadData];
}

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl *)pdfViewCtrl fromBookmark: (PTBookmark*)bookmark;
{
	self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
		_pdfViewCtrl = pdfViewCtrl;
		_document = [_pdfViewCtrl GetDoc];
		_bookmark = bookmark;
		
		self.alsoBookmarks = YES;
		self.alsoAnnotations = YES;
		
        [_document LockRead];
		
		NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
		
        if (!bookmark) {
			self.title = NSLocalizedStringFromTableInBundle(@"Outline", @"PDFNet-Tools", toolsStringBundle, @"Outline view title");
            bookmark = [_document GetFirstBookmark];
        } else {
            self.title = [bookmark GetTitle];
            bookmark = [bookmark GetFirstChild];
        }
        
        _childrenBookmarks = [[NSMutableArray alloc] initWithCapacity:8];
        PTBookmark *child = bookmark;
		_containsBookmarks = NO;

        while ([child IsValid]) {

            PTAction *action = [child GetAction];

            if (![action IsValid] || [action GetType] != e_ptGoTo || ![[action GetDest] IsValid]) { child = [child GetNext]; continue; }

			[_childrenBookmarks addObject:@{KEY_TITLE: [child GetTitle],
                                           KEY_CHILDREN: @([child HasChildren]),
                                           KEY_PAGENUM: @([[[[child GetAction] GetDest] GetPage] GetIndex]),
                                           KEY_BOOKMARK: child}];
			_containsBookmarks = YES;

			child = [child GetNext];
        }
		
        // On iPhone we need a button to dismiss the full-screen view. iPad the user can click outside the popover.
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
            
            NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
            
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Done", @"PDFNet-Tools", stringBundle, @"Done") style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
        }
        
        [_document UnlockRead];
		
    }
    return self;
}


- (void)dismiss {
    if ([self.delegate respondsToSelector:@selector(outlineViewControllerDidCancel:)]) {
        [self.delegate outlineViewControllerDidCancel:self];
    }
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.toolbarHidden = NO;
}

-(void)setToolbarItems
{
	NSMutableArray* otherViews = [[NSMutableArray alloc] init];
    
    NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
	
	[otherViews addObject:NSLocalizedStringFromTableInBundle(@"Outline", @"PDFNet-Tools", stringBundle, @"Outline")];
	
	if( self.alsoAnnotations )
        [otherViews addObject:NSLocalizedStringFromTableInBundle(@"Annotations", @"PDFNet-Tools", stringBundle, @"Annotations")];
	
	if( self.alsoBookmarks )
		[otherViews addObject:NSLocalizedStringFromTableInBundle(@"Bookmarks", @"PDFNet-Tools", stringBundle, @"Bookmarks")];
	
	if( self.alsoAnnotations || self.alsoBookmarks )
	{
		UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:otherViews];
		segmentedControl.frame = CGRectMake(0.0f, 0.0f, 300.0f, 35.0f);
		segmentedControl.selectedSegmentIndex = 0;
		[segmentedControl addTarget:self action:@selector(changeMode:) forControlEvents:UIControlEventValueChanged];
		
		[self setToolbarItems:@[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:@selector(add:)],
							   [[UIBarButtonItem alloc] initWithCustomView:segmentedControl],
							   [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:@selector(add:)]] animated:NO];
		
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	if( !_containsBookmarks )
	{
		UILabel* noBookmarksLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 330, 330)];
		
		//todo: fix how this is displayed without using a \n
		NSBundle* toolsStringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
		
		noBookmarksLabel.text =  NSLocalizedStringFromTableInBundle(@"This document does not contain an outline.", @"PDFNet-Tools", toolsStringBundle, @"");
		noBookmarksLabel.numberOfLines = 2;
		noBookmarksLabel.textAlignment = NSTextAlignmentCenter;
		UIViewAutoresizing mask = noBookmarksLabel.autoresizingMask;
		mask |= UIViewAutoresizingFlexibleLeftMargin;
		mask |= UIViewAutoresizingFlexibleRightMargin;
		mask |= UIViewAutoresizingFlexibleBottomMargin;
		noBookmarksLabel.autoresizingMask = mask;
		[self.view addSubview:noBookmarksLabel];
	}

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)changeMode: (UISegmentedControl*)segmentedControl {
    
        NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];

	if( [[segmentedControl titleForSegmentAtIndex:segmentedControl.selectedSegmentIndex] isEqualToString:NSLocalizedStringFromTableInBundle(@"Annotations", @"PDFNet-Tools", stringBundle, @"Annotations")] )
		[self.delegate outlineViewControllerSwitchToAnnotations:self];
	else
		[self.delegate outlineViewControllerSwitchToBookmarks:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _childrenBookmarks.count;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
	NSDictionary *details = _childrenBookmarks[indexPath.row];
	
	cell.textLabel.text = details[KEY_TITLE];
	
	cell.textLabel.numberOfLines = 1;
	cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0f];

	if ([details[KEY_CHILDREN] boolValue]) {
		cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
	} else {
		cell.accessoryType = UITableViewCellAccessoryNone;
	}
	
	cell.imageView.image = nil;
    
    return cell;
}

#pragma mark - Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Bookmark] Navigated by Outline List"];
    
	NSDictionary *details = _childrenBookmarks[indexPath.row];
	
	[_pdfViewCtrl SetCurrentPage:[details[@"pageNumber"] intValue]];
	
	if ([self.delegate respondsToSelector:@selector(outlineViewController:selectedBookmark:)]) {
		
		[self.delegate outlineViewController:self selectedBookmark:details];
	}
}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    PTBookmark *child = _childrenBookmarks[indexPath.row][KEY_BOOKMARK];
    
    OutlineViewController *outlineViewController = [[OutlineViewController alloc] initWithPDFViewCtrl:_pdfViewCtrl fromBookmark:child];

    outlineViewController.alsoAnnotations = _alsoAnnotations;
	outlineViewController.delegate = self.delegate;
    [self.navigationController pushViewController:outlineViewController animated:YES];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
