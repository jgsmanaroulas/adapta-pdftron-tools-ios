//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "FreehandCreateToolbar.h"
#import "PDFViewCtrlToolsUtil.h"

@interface FreehandCreateToolbar()
{
	UIBarButtonItem* _undoButton;
	UIBarButtonItem* _redoButton;
}

@end


@implementation FreehandCreateToolbar

@synthesize delegate;

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		UIBarButtonItem* fixedSpaceToSaveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
		
		if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone  )
			fixedSpaceToSaveButton.width = 15;
		else
			fixedSpaceToSaveButton.width = 30;
		
		UIBarButtonItem* fixedSpaceBetweenUndoRedo = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
		fixedSpaceBetweenUndoRedo.width = 15;
		// Initialization code
		
		_undoButton = [[UIBarButtonItem alloc] initWithImage:[PDFViewCtrlToolsUtil toolImageNamed:@"glyphicons_435_undo.png"] style:UIBarButtonItemStylePlain target:self.delegate action:@selector(undoFreetextStroke:)];
		_redoButton	= [[UIBarButtonItem alloc] initWithImage:[PDFViewCtrlToolsUtil toolImageNamed:@"glyphicons_434_redo.png"] style:UIBarButtonItemStylePlain target:self.delegate action:@selector(redoFreetextStroke:)];
		
		self.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self.delegate action:@selector(cancelFreehandToolbar:)],
					  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
					  _undoButton,
					  fixedSpaceBetweenUndoRedo,
					  _redoButton,
					  fixedSpaceToSaveButton,
					  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self.delegate action:@selector(dismissFreehandToolbar:)]];
		
		_redoButton.enabled = NO;
		
    }
    return self;
}

-(void)enableUndoButton
{
	_undoButton.enabled = YES;
}

-(void)disableUndoButton
{
	_undoButton.enabled = NO;
}

-(void)enableRedoButton
{
	_redoButton.enabled = YES;
}

-(void)disableRedoButton
{
	_redoButton.enabled = NO;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
