//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "BookmarkUtils.h"

@implementation BookmarkUtils


+ (NSString *)bookmarkFileNameFromFileURL:(NSURL *)documentUrl withUserId: (NSString*) userId
{
	NSFileManager* manager = [NSFileManager defaultManager];
	NSArray* paths = [manager URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask];
	
	NSURL* libraryDirectory = paths[0];
	
	NSString* docName = libraryDirectory.path.stringByDeletingLastPathComponent;
	docName = [documentUrl.path substringFromIndex:docName.length];
	docName = [docName stringByReplacingOccurrencesOfString:@"/" withString:@""];
	
	NSString* libraryName = [libraryDirectory.path stringByAppendingPathComponent:docName];
    libraryName = [libraryName stringByAppendingString:userId];
	return libraryName;
}


+(void)fileMovedFrom:(NSURL*)oldLocation to:(NSURL*)newLocation
{
//	NSString* oldBookmarkFile = [self bookmarkFileNameFromFileURL:oldLocation];
//    
//    if( [[NSFileManager defaultManager] fileExistsAtPath:oldBookmarkFile] )
//    {
//        NSString* newBookmarkFile = [self bookmarkFileNameFromFileURL:newLocation];
//        
//        NSError* error;
//        
//        [[NSFileManager defaultManager] moveItemAtPath:oldBookmarkFile toPath:newBookmarkFile error:&error];
//        
//        assert(!error);
//    }
	
}


+(void)saveBookmarkData:(NSArray*)bookmarkData forFileUrl:(NSURL*)documentUrl withUserId:(NSString*) userId
{
	NSError* error;
	
	NSMutableArray* propertyList;
 
	if(!bookmarkData )
		propertyList = [NSMutableArray array];
	else
		propertyList = [NSMutableArray arrayWithArray:bookmarkData];
	
	// bookmark file is an array of dictionaries
	// dictionary key value pairs are
	// @"page-number" : NSNumber numberWithInt
	// @"sdf-obj-number" : NSNumber numberWithInt
	// @"name" : NSString
	// @"unqiue-id" : NSString (a UUID string)
	
	NSData* plistData = [NSPropertyListSerialization dataWithPropertyList:propertyList
																   format:NSPropertyListBinaryFormat_v1_0
																  options:0
																	error:&error];
	
	if(!plistData)
		NSLog(@"Couldn't generate bookmark data: %@", error);


    NSString* libraryName = [self bookmarkFileNameFromFileURL:documentUrl withUserId:userId];

	assert(libraryName);
	assert([libraryName length]);
	
	BOOL success = [plistData writeToFile:libraryName options:0 error:&error];
	assert(success);
	
	
}

+(NSArray*)bookmarkDataForDocument:(NSURL*)documentUrl withUserId:(NSString*) userId
{
	NSString *libraryName;
	libraryName = [self bookmarkFileNameFromFileURL:documentUrl withUserId:userId];

	NSData* data = [NSData dataWithContentsOfFile:libraryName];
	
	if( !data.length )
	{
		return @[];
	}
	
	NSError* error;
	
	id plist = [NSPropertyListSerialization propertyListWithData:data options:NSPropertyListMutableContainersAndLeaves format:nil error:&error];
	
	return [NSArray arrayWithArray:plist];
}


+(NSMutableArray*)updateUserBookmarks:(NSMutableArray*) bookmarks oldPageNumber:(const unsigned int)oldPageNumber newPageNumber:(const unsigned int)newPageNumber oldSDFNumber:(const unsigned int)oldSDFNumber newSDFNumber:(const unsigned int)newSDFNumber
{
	if( newPageNumber != oldPageNumber )
	{
		const unsigned int updateStartingAtPageNumber = MIN(oldPageNumber, newPageNumber);
		const unsigned int updateEndingAtPageNumber = MAX(oldPageNumber, newPageNumber);
		
		int change = 0;
		
		if( newPageNumber > oldPageNumber )
		{
			change = -1;
		}
		else
		{
			change = 1;
		}
		
		// assumes bookmarks is sorted by page number
		for (NSMutableDictionary* bookmarkDict in bookmarks) {
			
			unsigned int orgPageNum = [bookmarkDict[@"page-number"] intValue];
			
			if( orgPageNum >= updateStartingAtPageNumber && orgPageNum <= updateEndingAtPageNumber )
			{
				if(orgPageNum == oldPageNumber)
				{
					bookmarkDict[@"page-number"] = [NSNumber numberWithInt:newPageNumber];
					
					//might need SDF obj updated
					if( [bookmarkDict[@"obj-number"] unsignedIntValue] == oldSDFNumber)
					{
						bookmarkDict[@"obj-number"] = @(newSDFNumber);
					}
				}
				else
				{
					bookmarkDict[@"page-number"] = @((unsigned int)((int)[bookmarkDict[@"page-number"] unsignedIntValue] + change));
				}
			}
		}
	}
	
	return bookmarks;

}


@end
