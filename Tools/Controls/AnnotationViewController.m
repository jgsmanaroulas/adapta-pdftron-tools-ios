//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "AnnotationViewController.h"
#import "AnalyticsHandlerAdapter.h"
#import "PDFViewCtrlToolsUtil.h"

#define KEY_PAGENUM     @"pageNumber"
#define KEY_TYPE        @"type"
#define KEY_SUBTYPE     @"subtype"
#define KEY_CONTENTS    @"contents"
#define KEY_RECT        @"rect"
#define KEY_OBJNUM		@"objNum"

// We need to augment the built-in annotation types (for example to distinguish b/w a line and an "arrow-head" line)
#define XE_ARROW        100000

UIImage *AnnotationImage(int type) {
    switch (type) {
        case e_ptText:
            return [PDFViewCtrlToolsUtil toolImageNamed:@"annotation_sticky_note.png"];
        case e_ptLine:
            return [PDFViewCtrlToolsUtil toolImageNamed:@"annotation_line.png"];
        case e_ptSquare:
            return [PDFViewCtrlToolsUtil toolImageNamed:@"annotation_square.png"];
        case e_ptCircle:
            return [PDFViewCtrlToolsUtil toolImageNamed:@"annotation_circle.png"];
        case e_ptPolygon:
            return [PDFViewCtrlToolsUtil toolImageNamed:@"annotation_polygon.png"];
        case e_ptUnderline:
            return [PDFViewCtrlToolsUtil toolImageNamed:@"annotation_underline.png"];
        case e_ptStrikeOut:
            return [PDFViewCtrlToolsUtil toolImageNamed:@"annotation_strikeout.png"];
        case e_ptInk:
            return [PDFViewCtrlToolsUtil toolImageNamed:@"annotation_free_hand.png"];
        case e_ptHighlight:
            return [PDFViewCtrlToolsUtil toolImageNamed:@"annotation_highlight.png"];
        case e_ptFreeText:
            return [PDFViewCtrlToolsUtil toolImageNamed:@"annotation_free_text.png"];
        case XE_ARROW:
            return [PDFViewCtrlToolsUtil toolImageNamed:@"annotation_arrow.png"];
        case e_ptSquiggly:
            return [PDFViewCtrlToolsUtil toolImageNamed:@"annotation_squiggly.png"];
        case e_ptStamp:
        case e_ptCaret:
        case e_ptPolyline:
        case e_ptRedact:
            return [PDFViewCtrlToolsUtil toolImageNamed:@"annotation_edit.png"];
    }
    
    return nil;
}


@interface AnnotationViewController () {
    PTPDFDoc *_document;
	PTPDFViewCtrl *_pdfViewCtrl;;
    int _selectedSegment;
    NSThread *_runner;
	UILabel* _noAnnotsLabel;
}

//- (id)initWithDocument: (PDFDoc*)document fromBookmark: (Bookmark*)bookmark;
- (void)beginAddingAnnotationResultsForDocument: (PTPDFDoc*)document;

// We use this only internally. When we first create a root bookmark view, we asynchronously
// begin fetching a list of all annotations. We then pass this along to each child bookmark
// view so it doesn't have to generate it itself.
@property (nonatomic, strong) NSMutableArray *annotations;

@end

@implementation AnnotationViewController

-(void)setAlsoBookmarks:(BOOL)alsoBookmarks
{
	_alsoBookmarks = alsoBookmarks;
	[self setToolbarItems];
}

-(void)setAlsoOutline:(BOOL)alsoOutline
{
	_alsoOutline = alsoOutline;
	[self setToolbarItems];
}

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl *)pdfViewCtrl {
    
	//_alsoBookmarks = YES;

	self.title = NSLocalizedStringFromTableInBundle(@"Annotations", @"PDFNet-Tools", [PDFViewCtrlToolsUtil toolsStringBundle], @"Annotations controller title");
	
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
		_pdfViewCtrl = pdfViewCtrl;
		_document = [_pdfViewCtrl GetDoc];
        _annotations = [[NSMutableArray alloc] initWithCapacity:8];
		self.alsoBookmarks = NO;
		self.alsoOutline = NO;
		
		// On iPhone we need a button to dismiss the full-screen view. iPad the user can click outside the popover.
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
			NSString* done = NSLocalizedStringFromTableInBundle(@"Done", @"PDFNet-Tools", [PDFViewCtrlToolsUtil toolsStringBundle], @"Dismiss annotation view controller");
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:done style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
        }
		

		_noAnnotsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 330, 330)];

		// todo fix word wrap
		
		_noAnnotsLabel.text = NSLocalizedStringFromTableInBundle(@"This document does not contain any annotations.",
																 @"PDFNet-Tools",
																 [PDFViewCtrlToolsUtil toolsStringBundle],
																 @"Annotation summary: no annotations in document.");
		_noAnnotsLabel.numberOfLines = 2;
		_noAnnotsLabel.textAlignment = NSTextAlignmentCenter;
		_noAnnotsLabel.hidden = YES;
		UIViewAutoresizing mask = _noAnnotsLabel.autoresizingMask;
		mask |= UIViewAutoresizingFlexibleLeftMargin;
		mask |= UIViewAutoresizingFlexibleRightMargin;
		mask |= UIViewAutoresizingFlexibleBottomMargin;
		_noAnnotsLabel.autoresizingMask = mask;
		[self.view addSubview:_noAnnotsLabel];
		
		
        [self refresh];
    }
    return self;
}

-(instancetype)initWithStyle:(UITableViewStyle)style
{
	NSAssert(false, @"You must init this class using the designated initializer initWithPDFViewCtrl");
	self = [self initWithStyle:style];
	return Nil;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
	NSAssert(false, @"You must init this class using the designated initializer initWithPDFViewCtrl");
	self = [self initWithCoder:aDecoder];
	return Nil;
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	NSAssert(false, @"You must init this class using the designated initializer initWithPDFViewCtrl");
	self = [self initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	return Nil;
}

- (void)refresh {
    if (_runner) {
        [_runner cancel];
    }
    
    [_annotations removeAllObjects];
    [self.tableView reloadData];
    _runner = [[NSThread alloc] initWithTarget:self selector:@selector(beginAddingAnnotationResultsForDocument:) object:_document];
    [_runner start];
}


- (void)addAnnotations: (NSArray*)annotationsForPage { 
                    
    [_annotations addObject:annotationsForPage];
  
	[self.tableView reloadData];
    
}

- (void)dismiss {
    if ([self.delegate respondsToSelector:@selector(annotationViewControllerDidCancel:)]) {
        [self.delegate annotationViewControllerDidCancel:self];
    }
}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
//	return UITableViewCellEditingStyleDelete;
//}

-(void)deleteAnnotation:(UITapGestureRecognizer *) sender{
    CGPoint point = [sender locationInView:self.tableView];

    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:point];

    NSDictionary *anAnnotation = _annotations[indexPath.section][indexPath.row];

    int pageNumber = [anAnnotation[@"pageNumber"] intValue];
    PTAnnot* annot;
    BOOL updated = NO;
    BOOL success = NO;
    @try
    {
        [_pdfViewCtrl DocLock:YES];
                    
        PTPDFDoc* document = [_pdfViewCtrl GetDoc];
        
        NSUInteger objNum = [anAnnotation[KEY_OBJNUM] unsignedIntegerValue];
        PTObj* ob = [[document GetSDFDoc] GetObj:(unsigned int)objNum];
        annot = [[PTAnnot alloc] initWithD:ob];
        
        PTPage* pg = [document GetPage:pageNumber];
        
        
        if ([pg IsValid] && [annot IsValid]) {
            [pg AnnotRemoveWithAnnot:annot];
            success = YES;
        }
        
        NSMutableArray* pagesOnScreen = [_pdfViewCtrl GetVisiblePages];
        
        for( NSNumber* pageNum in pagesOnScreen )
        {
            if( pageNum.intValue == pageNumber)
            {
                [_pdfViewCtrl UpdateWithAnnot:annot page_num:pageNumber];
                updated = YES;
                break;
            }
        }
        
        if( !updated && success)
            [_pdfViewCtrl Update:YES];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
        if( !updated && success)
            [_pdfViewCtrl Update:YES];
    }
    @finally {
        [_pdfViewCtrl DocUnlock];
    }

    if( success )
    {
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [_annotations[indexPath.section] removeObjectAtIndex:indexPath.row];
        if( [_annotations[indexPath.section] count] == 0 )
        {
            [_annotations removeObjectAtIndex:indexPath.section];
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
        }
        [self.tableView endUpdates];
        [_pdfViewCtrl.toolDelegate annotationRemoved:annot onPageNumber:pageNumber];
    }
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//		
////		NSDictionary *anAnnotation = _annotations[indexPath.section][indexPath.row];
////		
////		int pageNumber = [anAnnotation[@"pageNumber"] intValue];
////		PTAnnot* annot;
////		BOOL updated = NO;
////		BOOL success = NO;
////		@try
////		{
////			[_pdfViewCtrl DocLock:YES];
////				
////			PTPDFDoc* document = [_pdfViewCtrl GetDoc];
////						
////			NSUInteger objNum = [anAnnotation[KEY_OBJNUM] unsignedIntegerValue];
////			PTObj* ob = [[document GetSDFDoc] GetObj:(unsigned int)objNum];
////			annot = [[PTAnnot alloc] initWithD:ob];
////
////			PTPage* pg = [document GetPage:pageNumber];
////			
////			
////			if ([pg IsValid] && [annot IsValid]) {
////				[pg AnnotRemoveWithAnnot:annot];
////				success = YES;
////			}
////		
////			NSMutableArray* pagesOnScreen = [_pdfViewCtrl GetVisiblePages];
////			
////			for( NSNumber* pageNum in pagesOnScreen )
////			{
////				if( pageNum.intValue == pageNumber)
////				{
////					[_pdfViewCtrl UpdateWithAnnot:annot page_num:pageNumber];
////					updated = YES;
////					break;
////				}
////			}
////			
////			if( !updated && success)
////				[_pdfViewCtrl Update:YES];
////			
////		}
////		@catch (NSException *exception) {
////			NSLog(@"Exception: %@: %@",exception.name, exception.reason);
////			if( !updated && success)
////				[_pdfViewCtrl Update:YES];
////		}
////		@finally {
////			[_pdfViewCtrl DocUnlock];
////		}
////		
////		if( success )
////		{
////			[self.tableView beginUpdates];
////			[self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
////			[_annotations[indexPath.section] removeObjectAtIndex:indexPath.row];
////			if( [_annotations[indexPath.section] count] == 0 )
////			{
////				[_annotations removeObjectAtIndex:indexPath.section];
////				[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
////			}
////			[self.tableView endUpdates];
////			[_pdfViewCtrl.toolDelegate annotationRemoved:annot onPageNumber:pageNumber];
////		}
//
//		
//    }
//}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.toolbarHidden = NO;
}

-(void)setToolbarItems
{
	NSMutableArray* otherViews = [NSMutableArray array];
    
    
    NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
    
    if( self.alsoOutline )
        [otherViews addObject:NSLocalizedStringFromTableInBundle(@"Outline", @"PDFNet-Tools", stringBundle, @"Outline")];
    
    
    
    [otherViews addObject:NSLocalizedStringFromTableInBundle(@"Annotations", @"PDFNet-Tools", stringBundle, @"Annotations")];
    
    if( self.alsoBookmarks )
        [otherViews addObject:NSLocalizedStringFromTableInBundle(@"Bookmarks", @"PDFNet-Tools", stringBundle, @"Bookmarks")];

    

	
	if( self.alsoBookmarks || self.alsoOutline	)
	{
		UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:otherViews];
		segmentedControl.frame = CGRectMake(0.0f, 0.0f, 300.0f, 35.0f);
		segmentedControl.selectedSegmentIndex = 1;
		[segmentedControl addTarget:self action:@selector(changeMode:) forControlEvents:UIControlEventValueChanged];
		
		[self setToolbarItems:@[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:@selector(add:)],
							   [[UIBarButtonItem alloc] initWithCustomView:segmentedControl],
							   [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:@selector(add:)]] animated:NO];
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[self setToolbarItems];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)changeMode: (UISegmentedControl*)segmentedControl {
	
    NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];
    
	if( [[segmentedControl titleForSegmentAtIndex:segmentedControl.selectedSegmentIndex] isEqualToString:NSLocalizedStringFromTableInBundle(@"Outline", @"PDFNet-Tools", stringBundle, @"Outline")] )
		[self.delegate annotationViewControllerSwitchToOutline:self];
	else
		[self.delegate annotationViewControllerSwitchToBookmarks:self];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return _annotations.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_annotations.count) {
        return [_annotations[section] count];
    }
    return 0;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (_annotations.count) {
		NSString* localizedPage = NSLocalizedStringFromTableInBundle(@"Page", @"PDFNet-Tools", [PDFViewCtrlToolsUtil toolsStringBundle], @"Page");
        return [NSString stringWithFormat:@"%@ %@", localizedPage, [_annotations[section] lastObject][@"pageNumber"]];
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (_annotations.count && [_annotations[indexPath.section] count])
	{
        NSDictionary *details = _annotations[indexPath.section][indexPath.row];

        //cell.textLabel.text = [details objectForKey:KEY_SUBTYPE];
        cell.textLabel.text = (![details[KEY_CONTENTS]  isEqual: @""] ? details[KEY_CONTENTS] : @"Marcação sem nota");
//        cell.accessoryType = UITableViewCellAccessoryNone;

        cell.textLabel.numberOfLines = 2;
        cell.textLabel.font = [UIFont systemFontOfSize:14.0f];

        cell.imageView.image = AnnotationImage([details[KEY_TYPE] intValue]);
        
        
        cell.accessoryView = [[ UIImageView alloc ] initWithImage:[PDFViewCtrlToolsUtil toolImageNamed:@"dismiss"]];
        [cell.accessoryView setFrame:CGRectMake(0, 0, 20, 20)];
        
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(deleteAnnotation:)];
        
        [cell.accessoryView addGestureRecognizer:singleFingerTap];
        cell.accessoryView.userInteractionEnabled = YES;
    }
    
    return cell;
}


#pragma mark - Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[Bookmark] Navigated by Annotation List"];
    
	 NSDictionary *anAnnotation = _annotations[indexPath.section][indexPath.row];
	
	int pageNumber = [anAnnotation[@"pageNumber"] intValue];
	
    CGRect rect = [self PDFRectPage2CGRectScreen:(PTPDFRect*)anAnnotation[@"rect"] PageNumber:pageNumber];
	
    rect.origin.x += [_pdfViewCtrl GetHScrollPos];
    rect.origin.y += [_pdfViewCtrl GetVScrollPos];
    
    [_pdfViewCtrl SetCurrentPage:pageNumber];
	
    // Create a view to be our highlight marker
    UIView *highlight = [[UIView alloc] initWithFrame:rect];
    highlight.backgroundColor = [UIColor colorWithRed:0.4375f green:0.53125f blue:1.0f alpha:1.0f];
    [_pdfViewCtrl->ContainerView addSubview:highlight];
    
    NSTimeInterval delay = 0.0;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        delay = 0.3;
    }
	
    // Pulse the annotation. There seem to be issues with the built-in repeat and auto-reverse...
    highlight.alpha = 0.0f;
	
	dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC));
	dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		[UIView animateWithDuration:0.20f delay:0.0f options:0 animations:^(void) {
			highlight.alpha = 1.0f;
		} completion:^(BOOL finished) {
			[UIView animateWithDuration:0.20f delay:0.0f options:0 animations:^(void) {
				highlight.alpha = 0.4f;
			} completion:^(BOOL finished) {
				[UIView animateWithDuration:0.20f delay:0.0f options:0 animations:^(void) {
					highlight.alpha = 1.0f;
				} completion:^(BOOL finished) {
					[UIView animateWithDuration:0.20f delay:0.0f options:0 animations:^(void) {
						highlight.alpha = 0.0f;
					} completion:^(BOOL finished) {
						[highlight removeFromSuperview];
					}];
				}];
			}];
		}];
	});

	
	if ([self.delegate respondsToSelector:@selector(annotationViewController:selectedAnnotaion:)]) {
	   
		[self.delegate annotationViewController:self selectedAnnotaion:anAnnotation];
	}
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

// @TODO: refresh annotations when the user adds a new annotation
- (void)beginAddingAnnotationResultsForDocument: (PTPDFDoc*)document {
    @autoreleasepool {
        [document LockRead];
        
		BOOL annotFound = NO;
        PTPageIterator* pageIterator = [document GetPageIterator:1];
        int idx = 0;
        PTTextExtractor* textExtractor = [[PTTextExtractor alloc] init];
        while ([pageIterator HasNext]) {
            idx++;
            PTPage *page = [pageIterator Current];
            NSMutableArray *annotations = [NSMutableArray array];

            if ([page IsValid]) {
                int annotationCount = [page GetNumAnnots];
                for (int a = 0; a < annotationCount; a++) {
                    PTAnnot *annotation = [page GetAnnot:a];
                    if (!annotation || ![annotation IsValid]) { continue; }
                    if (!AnnotationImage([annotation GetType])) { continue; }

                    NSString* contents = @"";
                    switch ([annotation GetType]) {
                        case e_ptFreeText:
                            contents = [annotation GetContents];
                            break;
                        case e_ptLine:
                        case e_ptSquare:
                        case e_ptCircle:
                        case e_ptPolygon:
                        case e_ptText:
                        case e_ptInk:
                        {
                            PTMarkup* annot = [[PTMarkup alloc] initWithAnn:annotation];
//                            NSString* author = [annot GetTitle];
//                            if( !author )
//                                 author = @"";
//                            if( author.length > 0 )
//                            {
//                                contents = author;
//                            }
                            PTPopup* popup = [annot GetPopup];
                            if([popup IsValid]) {
								
								NSString* popupContents = [popup GetContents];
								if(popupContents)
//                                    contents = [NSString stringWithFormat:@"%@: %@",author, popupContents];
                                    contents = popupContents;
								
                            } else {
                                contents = @"";
                            }
                            
                            break;
                        }
                        case e_ptUnderline:
                        case e_ptStrikeOut:
                        case e_ptHighlight:
                        case e_ptSquiggly:
                            [textExtractor Begin:page clip_ptr:0 flags:e_ptno_ligature_exp];
                            contents = [textExtractor GetTextUnderAnnot:annotation];
                            break;
                        default:
                            break;
                    }

                    int kind = [annotation GetType];
                    
                    if ([annotation GetType] == e_ptLine) {
                        PTObj* lineSdf = [annotation GetSDFObj];
                        PTObj* lineObj = [lineSdf FindObj:@"LE"];
                        
                        if( lineObj && [lineObj IsArray])
                        {
                            unsigned long s = [lineObj Size];
                            for(unsigned long i = 0; i < s; i++)   
                            {
                                PTObj* obj = [lineObj GetAt:i];
                                if( [obj IsName] && ([[obj GetName] isEqualToString:@"OpenArrow"] || [[obj GetName] isEqualToString:@"ClosedArrow"] ) )
                                {
                                    kind = XE_ARROW;
                                    break;   
                                }   
                            }
                        }
                    }
                    unsigned int objNum = [[annotation GetSDFObj] GetObjNum];
                    
                    if( !contents )
                        contents = @"";
					
                    [annotations addObject:@{KEY_PAGENUM: @(idx), 
                                            KEY_SUBTYPE: [[[[annotation GetSDFObj] Get: @"Subtype"] Value] GetName].description,
                                            KEY_TYPE: @(kind),
                                            KEY_RECT: [annotation GetRect],
                                            KEY_CONTENTS: [contents stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]],
											KEY_OBJNUM: @(objNum)}];
                }
            }
            
            [document UnlockRead];
            
            if ([NSThread currentThread].cancelled) { return; }
            
            // Consider not waiting until done. Will be faster on dual+ core procs
            // Does obj-c runtime ensure in-order, no-barging threads against main thread.
            if (annotations.count) {
				annotFound = YES;
				_noAnnotsLabel.hidden = YES;
                [self performSelectorOnMainThread:@selector(addAnnotations:) withObject:annotations waitUntilDone:YES];
            }

            [document LockRead];
            
            [pageIterator Next];
        }

        [document UnlockRead];
		
		if( !annotFound )
			_noAnnotsLabel.hidden = NO;
	
    }
}

-(void)ConvertPagePtToScreenPtX:(CGFloat*)x Y:(CGFloat*)y PageNumber:(int)pageNumber
{
    PTPDFPoint *m_pagePt = [[PTPDFPoint alloc] initWithPx:(*x) py:(*y)];
    
    [m_pagePt setX:*x];
    [m_pagePt setY:*y];
    
    PTPDFPoint *m_screenPt = [_pdfViewCtrl ConvPagePtToScreenPt:m_pagePt page_num:pageNumber];
    
    *x = (float)[m_screenPt getX];
    *y = (float)[m_screenPt getY];
}

-(CGRect)PDFRectPage2CGRectScreen:(PTPDFRect*)r PageNumber:(int)pageNumber
{
    PTPDFPoint* pagePtA = [[PTPDFPoint alloc] init];
    PTPDFPoint* pagePtB = [[PTPDFPoint alloc] init];
    
    [pagePtA setX:[r GetX1]];
    [pagePtA setY:[r GetY2]];
    
    [pagePtB setX:[r GetX2]];
    [pagePtB setY:[r GetY1]];
    
    CGFloat paX = [pagePtA getX];
    CGFloat paY = [pagePtA getY];
    
    CGFloat pbX = [pagePtB getX];
    CGFloat pbY = [pagePtB getY];
    
    [self ConvertPagePtToScreenPtX:&paX Y:&paY PageNumber:pageNumber];
    [self ConvertPagePtToScreenPtX:&pbX Y:&pbY PageNumber:pageNumber];
    
    return CGRectMake(paX, paY, pbX-paX, pbY-paY);
}

@end
