//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "ThumbnailViewCell.h"
#import "PDFViewCtrlToolsUtil.h"

#define SHAPE_RATIO 30.0
#define LABEL_RATIO 10.0
#define CHECKBOX_RATIO 8.0

@implementation ThumbnailViewCell

@synthesize checkbox = _checkBox;
@synthesize nightMode;

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        // clear background looks nicer when drag and drop
        self.backgroundColor = [UIColor clearColor];
        [self setOpaque:NO];
        
        // page number indicator
        CGFloat labelHeight = frame.size.height / LABEL_RATIO;
        CGFloat labelWidth = frame.size.width / LABEL_RATIO * 3.0;
        _label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, labelWidth, labelHeight)];
        _label.center = CGPointMake(frame.size.width / 2.0, frame.size.height - labelHeight);
        _label.layer.cornerRadius = 8.0;
        _label.textAlignment = NSTextAlignmentCenter;
        _label.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        _label.backgroundColor = [UIColor colorWithWhite:0 alpha:.5];
        _label.textColor = [UIColor whiteColor];
        _label.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_label];
        
        // thumbnail image
        UIView* whiteView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                     0,
                                                                     self.contentView.frame.size.width,
                                                                     self.contentView.frame.size.height)];
		if( self.nightMode )
			whiteView.backgroundColor = [UIColor blackColor];
		else
			whiteView.backgroundColor = [UIColor whiteColor];
		
        UIImage* image = [self imageWithView:whiteView];
        
        _thumbnailView = [[UIImageView alloc]initWithImage:image];
        [self.contentView insertSubview:_thumbnailView belowSubview:_label];
        
        // current page indicator
        _shapeLayer = [CAShapeLayer layer];
        [self.contentView.layer addSublayer:_shapeLayer];
        
        // checkbox in edit mode
        CGFloat checkBoxMargin = self.contentView.frame.size.width / CHECKBOX_RATIO;
        _checkBox = [[UIButton alloc]initWithFrame:CGRectMake(checkBoxMargin, checkBoxMargin, self.contentView.frame.size.width / CHECKBOX_RATIO, self.contentView.frame.size.width / CHECKBOX_RATIO)];

        UIImage* normalImage = [PDFViewCtrlToolsUtil toolImageNamed:@"check_box_unchecked"];
        UIImage* checkedImage = [PDFViewCtrlToolsUtil toolImageNamed:@"check_box_checked"];
		
        [_checkBox setImage:normalImage forState:UIControlStateNormal];
        [_checkBox setImage:checkedImage forState:UIControlStateSelected];
        
        [self.contentView addSubview:_checkBox];
        _checkBox.hidden = YES;
    }
    return self;
}

-(void)setPageNumber:(NSInteger)pageNumber isCurrentPage:(BOOL)isCurrent isEditing:(BOOL)isEditing isChecked:(BOOL)isChecked
{
    
    _label.text = [NSString stringWithFormat: @"%ld", (long)pageNumber];
    
    if (isCurrent) {
        [self drawCurrentPageRect];
    } else {
        [_shapeLayer removeFromSuperlayer];
    }
    
    // white out reused cell
    if (!isEditing) {
        [_thumbnailView removeFromSuperview];
        UIView* whiteView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                     0,
                                                                     self.contentView.frame.size.width,
                                                                     self.contentView.frame.size.height)];
		if( self.nightMode )
			whiteView.backgroundColor = [UIColor blackColor];
		else
			whiteView.backgroundColor = [UIColor whiteColor];
		
        UIImage* image = [self imageWithView:whiteView];
        _thumbnailView = [[UIImageView alloc]initWithImage:image];
        [self.contentView insertSubview:_thumbnailView belowSubview:_label];
        
        _checkBox.hidden = YES;
    } else {
        _checkBox.hidden = NO;
        _checkBox.selected = isChecked;
        
    }
}

-(void)setThumbnail:(UIImage*)image forPage:(NSInteger)pageNum
{
    if(!image)
    {
        // should now always receive a thumbnail.
        assert(false);
        UIView* whiteView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                     0,
                                                                     self.contentView.frame.size.width,
                                                                     self.contentView.frame.size.height)];
		if( self.nightMode )
			whiteView.backgroundColor = [UIColor blackColor];
		else
			whiteView.backgroundColor = [UIColor whiteColor];
		
        image = [self imageWithView:whiteView];
    }
    
    [_thumbnailView removeFromSuperview];
    _thumbnailView = [[UIImageView alloc] initWithImage:image];
    _thumbnailView.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
    _thumbnailView.opaque = YES;
    _thumbnailView.tag = pageNum;
    _thumbnailView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView insertSubview:_thumbnailView belowSubview:_label];
    
    CGRect frame = _checkBox.frame;
    CGFloat checkBoxMargin = self.contentView.frame.size.width / CHECKBOX_RATIO;
    frame.origin.x = _thumbnailView.frame.origin.x + checkBoxMargin;
    frame.origin.y = _thumbnailView.frame.origin.y + checkBoxMargin;
    _checkBox.frame = frame;
    
}

-(UIImage*)imageWithView:(UIView*)view
{
	UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0f);
	[view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage* img = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return img;
}

-(void)drawPageLabel:(CGRect)rect
{
    CGFloat rwk = 0.5f;
    CGFloat rhk = 0.34f;
    CGRect drawRect = CGRectMake(rect.size.width * (1 - rwk) / 2, rect.size.height * (1 - rhk) / 2,
                                 rect.size.width * rwk, rect.size.height * rhk);
    [[UIBezierPath bezierPathWithRoundedRect:drawRect cornerRadius:5] fill];
}

-(void)drawCurrentPageRect
{
    CGFloat width = self.contentView.frame.size.width;
    CGFloat height = self.contentView.frame.size.height;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    // # Entry point
    [path moveToPoint:CGPointMake(0.0, 0.0)];
    [path addLineToPoint:CGPointMake(0.0, height)];
    [path addLineToPoint:CGPointMake(width, height)];
    [path addLineToPoint:CGPointMake(width, 0.0)];
    // # Back to entry point
    [path addLineToPoint:CGPointMake(0.0, 0.0)];
    [path addLineToPoint:CGPointMake(0.0, height)];
    
    // # Create a CAShapeLayer that uses that UIBezierPath:
    [_shapeLayer removeFromSuperlayer];
    _shapeLayer = [CAShapeLayer layer];
    _shapeLayer.path = path.CGPath;
    _shapeLayer.strokeColor = [UIApplication sharedApplication].delegate.window.tintColor.CGColor;
    CGFloat lineWidth = self.frame.size.width / SHAPE_RATIO;
    _shapeLayer.lineWidth = lineWidth;
    _shapeLayer.fillColor = [UIColor clearColor].CGColor;
    // # add shapeLayer as sublayer inside layer view
    [self.contentView.layer addSublayer:_shapeLayer];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
