//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "PanTool.h"
#import "AnnotEditTool.h"
#import "RectangleCreate.h"
#import "EllipseCreate.h"
#import "LineCreate.h"
#import "ArrowCreate.h"
#import "StickyNoteCreate.h"
#import "FreeHandCreate.h"
#import "TextSelectTool.h"
#import "Eraser.h"
#import "FormFillTool.h"
#import "FreeTextCreate.h"
#import "LineEditTool.h"
#import "DigitalSignatureTool.h"
#import "RichMediaTool.h"
#import "AnalyticsHandlerAdapter.h"
#import "TextMarkupEditTool.h"

#import "PDFViewCtrlToolsUtil.h"

@class PTPDFViewCtrl;

@implementation PanTool


- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        m_pdfViewCtrl = in_pdfViewCtrl;
    }

    return self;
}

- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    return YES;
}

-(BOOL)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if( [m_pdfViewCtrl GetDoc] == Nil )
    {
        return YES;
    }
    
    CGPoint down = [gestureRecognizer locationInView:m_pdfViewCtrl];
    
    [m_pdfViewCtrl becomeFirstResponder];
    	
    if( gestureRecognizer.state == UIGestureRecognizerStateBegan )
    {

        if( [m_moving_annotation IsValid] )
        {
            m_moving_annotation = nil;
        }

        @try
        {
            [m_pdfViewCtrl DocLockRead];
        
            m_moving_annotation = [m_pdfViewCtrl GetAnnotationAt:down.x y:down.y distanceThreshold:GET_ANNOT_AT_DISTANCE_THRESHOLD minimumLineWeight:GET_ANNOT_AT_MINIMUM_LINE_WEIGHT];
        
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@: %@",exception.name, exception.reason);
        }
        @finally {
            [m_pdfViewCtrl DocUnlockRead];
        }

        if( [m_moving_annotation IsValid] )
        {
			PTAnnotType annotType = [m_moving_annotation GetType];
            if([m_moving_annotation GetType] == e_ptWidget) {
                nextToolType = [FormFillTool class];
            }
            else if([m_moving_annotation GetType] == e_ptLine) {
                nextToolType = [LineEditTool class];
            }
			else if(annotType == e_ptHighlight || annotType == e_ptUnderline || annotType == e_ptStrikeOut || annotType ==  e_ptSquiggly) {
                nextToolType = [TextMarkupEditTool class];
            }
            else
            {
                nextToolType = [AnnotEditTool class];
            }
            
            return NO;

        }
        else
        {
            m_moving_annotation = 0;
        }
    }
    
    if( m_moving_annotation == 0 )
    {
        [m_pdfViewCtrl SetTextSelectionMode:e_ptrectangular];
        [m_pdfViewCtrl SelectX1:down.x Y1:down.y X2:1+down.x Y2:1+down.y];
        [m_pdfViewCtrl SetTextSelectionMode:e_ptstructural];
        
        PTSelection* selection = [m_pdfViewCtrl GetSelection:-1];
        
        //if over text
        if (selection == 0 || [[selection GetQuads] size] > 0)
        {  
            UIMenuController *theMenu = [UIMenuController sharedMenuController];
            [theMenu setMenuVisible:NO animated:NO];
            nextToolType = [TextSelectTool class];
            return NO;
        }
        else
        {
			if( gestureRecognizer.state == UIGestureRecognizerStateEnded )
			{
				// show creation menu
                [self becomeFirstResponder];
				[self attachInitialMenuItems];
				
				m_down = [gestureRecognizer locationInView:m_pdfViewCtrl];

				// "NO" prevents flicker when moving finger
				[self showSelectionMenu:CGRectMake(m_down.x, m_down.y, 1, 1) animated:NO];
			}
            
        }

    }

    
    return YES;

}

- (void) attachInitialMenuItems
{
    NSMutableArray* menuItems = [[NSMutableArray alloc] initWithCapacity:2];
    
    UIMenuItem* menuItem;
	
	NSBundle* stringBundle = [PDFViewCtrlToolsUtil toolsStringBundle];	
    
	menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Note", @"PDFNet-Tools", stringBundle, @"Note tool name") action:@selector(createStickeyNote)];
    [menuItems addObject:menuItem];

	//menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Signature", @"PDFNet-Tools", stringBundle, @"Signature tool name") action:@selector(createSignature)];
    //[menuItems addObject:menuItem];
	
	//menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Ink", @"PDFNet-Tools", stringBundle, @"Ink tool name") action:@selector(createFreeHand)];
    //[menuItems addObject:menuItem];
    
    //menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Eraser", @"PDFNet-Tools", stringBundle, @"Ink tool name") action:@selector(createEraser)];
    //[menuItems addObject:menuItem];
	
//    menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Text", @"PDFNet-Tools", stringBundle, @"Text tool name") action:@selector(createFreeText)];
//    [menuItems addObject:menuItem];
	
	//menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Arrow", @"PDFNet-Tools", stringBundle, @"Arrow tool name") action:@selector(createArrow)];
    //[menuItems addObject:menuItem];


	//menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Line", @"PDFNet-Tools", stringBundle, @"Line tool name") action:@selector(createLine)];
    //[menuItems addObject:menuItem];
	
    //menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Rectangle", @"PDFNet-Tools", stringBundle, @"Rectangle tool name") action:@selector(createRectangle)];
    //[menuItems addObject:menuItem];
    
    //menuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Ellipse", @"PDFNet-Tools", stringBundle, @"Ellipse tool name") action:@selector(createEllipse)];
    //[menuItems addObject:menuItem];
    
    
    UIMenuController *theMenu = [UIMenuController sharedMenuController];
    theMenu.menuItems = menuItems;
    
}

-(void)createRectangle
{
	// the next tool will adopt the backToPanToolAfterUse setting of the current tool.
	// tools activated through the quick menu should never always go back to the pan tool.
	((Tool*)m_pdfViewCtrl.toolDelegate.tool).backToPanToolAfterUse = YES;
    nextToolType = [RectangleCreate class];
	[m_pdfViewCtrl postCustomEvent:Nil];
    
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] Rectangle selected"];
}

-(void)createEllipse
{
	// the next tool will adopt the backToPanToolAfterUse setting of the current tool.
	// tools activated through the quick menu should never always go back to the pan tool.
	((Tool*)m_pdfViewCtrl.toolDelegate.tool).backToPanToolAfterUse = YES;
    nextToolType = [EllipseCreate class];
	[m_pdfViewCtrl postCustomEvent:Nil];
    
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] Ellipse selected"];
}

-(void)createLine
{
	// the next tool will adopt the backToPanToolAfterUse setting of the current tool.
	// tools activated through the quick menu should never always go back to the pan tool.
	((Tool*)m_pdfViewCtrl.toolDelegate.tool).backToPanToolAfterUse = YES;
    nextToolType = [LineCreate class];
	[m_pdfViewCtrl postCustomEvent:Nil];
    
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] Line selected"];
}

-(void)createArrow
{
	// the next tool will adopt the backToPanToolAfterUse setting of the current tool.
	// tools activated through the quick menu should never always go back to the pan tool.
	((Tool*)m_pdfViewCtrl.toolDelegate.tool).backToPanToolAfterUse = YES;
    nextToolType = [ArrowCreate class];
	[m_pdfViewCtrl postCustomEvent:Nil];
    
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] Arrow selected"];
}

-(void)createStickeyNote
{
	// the next tool will adopt the backToPanToolAfterUse setting of the current tool.
	// tools activated through the quick menu should never always go back to the pan tool.
	((Tool*)m_pdfViewCtrl.toolDelegate.tool).backToPanToolAfterUse = YES;
    nextToolType = [StickyNoteCreate class];
	[m_pdfViewCtrl postCustomEvent:Nil];
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] StickyNote selected"];
}

-(void)createFreeHand
{
	// the next tool will adopt the backToPanToolAfterUse setting of the current tool.
	// tools activated through the quick menu should never always go back to the pan tool.
	((Tool*)m_pdfViewCtrl.toolDelegate.tool).backToPanToolAfterUse = YES;
    nextToolType = [FreeHandCreate class];
	[m_pdfViewCtrl postCustomEvent:Nil];
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] FreeHand selected"];
}

//-(void)createEraser
//{
//    nextToolType = [Eraser class];
//	[m_pdfViewCtrl postCustomEvent:Nil];
//    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] Eraser selected"];
//}

-(void)createFreeText
{
	// the next tool will adopt the backToPanToolAfterUse setting of the current tool.
	// tools activated through the quick menu should never always go back to the pan tool.
	((Tool*)m_pdfViewCtrl.toolDelegate.tool).backToPanToolAfterUse = YES;
    nextToolType = [FreeTextCreate class];
	[m_pdfViewCtrl postCustomEvent:@"Start"];
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] FreeText selected"];
}

-(void)createSignature
{
	// the next tool will adopt the backToPanToolAfterUse setting of the current tool.
	// tools activated through the quick menu should never always go back to the pan tool.
	((Tool*)m_pdfViewCtrl.toolDelegate.tool).backToPanToolAfterUse = YES;
	nextToolType = [DigitalSignatureTool class];
	[m_pdfViewCtrl postCustomEvent:Nil];
    
    [[AnalyticsHandlerAdapter getInstance] sendCustomEventWithTag:@"[QuickMenu Tool] Signature selected"];
}

- (BOOL)onCustomEvent:(id)userData
{
    if( nextToolType /*== [DigitalSignatureTool class]*/ )
    {
        return NO;
    }
    else
        return YES;
}


- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.backgroundColor = [UIColor clearColor];
    
    self.frame = CGRectMake(0, 0, 0, 0);
    
    if( nextToolType )
        return NO;
    
    return YES;
}

- (BOOL)handleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    if( [m_pdfViewCtrl GetDoc] == Nil )
    {
        return YES;
    }
    
    CGPoint down = [gestureRecognizer locationInView:m_pdfViewCtrl];
    
    [m_pdfViewCtrl becomeFirstResponder];
    
    int pn = [m_pdfViewCtrl GetPageNumberFromScreenPt:down.x y:down.y];
    
    if( pn < 1 )
    {
        return YES;
    }

    if( m_moving_annotation )
    {
        m_moving_annotation = nil;
    }

    PTLinkInfo* linkInfo;
    @try
    {
        [m_pdfViewCtrl DocLockRead];

        m_moving_annotation = [m_pdfViewCtrl GetAnnotationAt:down.x y:down.y distanceThreshold:GET_ANNOT_AT_DISTANCE_THRESHOLD minimumLineWeight:GET_ANNOT_AT_MINIMUM_LINE_WEIGHT];

        if( ![m_moving_annotation IsValid] )
            linkInfo = [m_pdfViewCtrl GetLinkAt:down.x y:down.y];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }

    if( [m_moving_annotation IsValid] )
    {
		PTAnnotType annotType = [m_moving_annotation GetType];
        if([m_moving_annotation GetType] == e_ptWidget) {
            nextToolType = [FormFillTool class];
        }
        else if ([m_moving_annotation GetType] == e_ptLine)
        {
            nextToolType = [LineEditTool class];
        }
		else if( [m_moving_annotation GetType] == e_ptRichMedia)
		{
			nextToolType = [RichMediaTool class];
		}
		else if(annotType == e_ptHighlight || annotType == e_ptUnderline || annotType == e_ptStrikeOut || annotType ==  e_ptSquiggly) {
			nextToolType = [TextMarkupEditTool class];
		}
        else
            nextToolType = [AnnotEditTool class];
        
        return NO;
    }
    
    if( [linkInfo getUrl].length > 0 )
    {
        nextToolType = [AnnotEditTool class];
        return NO;
    }


    return YES;
}


@end
