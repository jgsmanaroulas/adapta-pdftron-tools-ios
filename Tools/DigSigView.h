//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

@interface DigSigView : UIView
{
	//NSMutableArray* m_dig_sig_points;
	CGPoint m_startPoint;
    CGPoint m_endPoint;
	CGPoint m_currentPoint;
	CGContextRef m_context;
	int m_numberOfPoints;
	int m_leftMost, m_rightMost, m_topMost, m_bottomMost;
}
@property (assign, nonatomic) CGRect m_boundingRect;
@property (strong, nonatomic) NSMutableArray* m_dig_sig_points;
@property (strong, nonatomic) UIColor* strokeColor;
@property (assign, nonatomic) CGFloat strokeThickness;

-(instancetype)initWithFrame:(CGRect)frame withColour:(UIColor*)color withStrokeThickness:(CGFloat)thickness NS_DESIGNATED_INITIALIZER;

@end
