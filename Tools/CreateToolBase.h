//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "Tool.h"

/**
 * An abstract class that many of the annotation-creating tools derive from.
 */
@interface CreateToolBase : Tool {
    CGPoint startPoint;
    CGPoint endPoint;
    Class pdfNetAnnotType;
    CGRect drawArea;
    int pageNumber;
    NSUInteger m_num_touches;
	UIColor* _strokeColor;
	UIColor* _fillColor;
	double _opacity;
	double _thickness;
}

-(double)setupContextFromDefaultAnnot:(CGContextRef)currentContext;
-(void)setPropertiesFromDefaultAnnotation: (PTAnnot *) annotation;
-(CGPoint)boundToPageScreenPoint:(CGPoint)touchPoint withThicknessCorrection:(CGFloat)thickness;
-(CGPoint)boundToPageScreenPoint:(CGPoint)touchPoint
                         withPaddingLeft:(CGFloat)left
                                   right:(CGFloat)right
                                  bottom:(CGFloat)bottom
                                     top:(CGFloat)top;
@end
