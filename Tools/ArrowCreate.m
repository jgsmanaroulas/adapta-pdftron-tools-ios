//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "ArrowCreate.h"
#import "ColorDefaults.h"

@implementation ArrowCreate

- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        pdfNetAnnotType = [PTLineAnnot class];
        mCos = cos(3.1415926/6);
        mSin = sin(3.1415926/6);
        mArrowLength = 10;
    }
    
    return self;
}

-(PTAnnotType)annotType
{
	return e_ptLine;
}

- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch = touches.allObjects[0];
    
	CGPoint touchPoint = [touch locationInView:m_pdfViewCtrl];
	
	endPoint = [super boundToPageScreenPoint:touchPoint withThicknessCorrection:0];
	
	if( _thickness < -1 )
	{
		_thickness = [ColorDefaults defaultBorderThicknessForAnnotType:e_ptLine];
		_thickness = _thickness*[m_pdfViewCtrl GetZoom];
	}
    
    mArrowLength = 10*_thickness/2;
    
    // max size of control in both directions
    double width = m_pdfViewCtrl.frame.size.width;
    double height = m_pdfViewCtrl.frame.size.height;
    
    self.frame = CGRectMake([m_pdfViewCtrl GetHScrollPos], [m_pdfViewCtrl GetVScrollPos], width, height);
    
    return YES;
}

- (void)drawRect:(CGRect)rect
{
    if( pageNumber >= 1 && !self.allowScrolling )
    {
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        
        [super setupContextFromDefaultAnnot:currentContext];
        
        CGRect myRect = CGContextGetClipBoundingBox(currentContext);
        
        CGPoint firstSmall, secondSmall;
        
        CGContextBeginPath (currentContext);

        double dx = startPoint.x - endPoint.x;
        double dy = startPoint.y - endPoint.y;
        double len = dx*dx+dy*dy;
        
        if( len > 0 )
        {
            len = sqrt(len);
            dx /= len;
            dy /= len;
            
            double dx1 = dx * mCos - dy * mSin;
            double dy1 = dy * mCos + dx * mSin;
            
            firstSmall = CGPointMake(startPoint.x - mArrowLength*dx1, startPoint.y - mArrowLength*dy1);
            
            double dx2 = dx * mCos + dy * mSin;
            double dy2 = dy * mCos - dx * mSin;
            
            secondSmall = CGPointMake(startPoint.x - mArrowLength*dx2, startPoint.y - mArrowLength*dy2);
            

            // end of small line
            CGContextMoveToPoint(currentContext, firstSmall.x, firstSmall.y);
            
            // tip of arrow
            CGContextAddLineToPoint(currentContext, startPoint.x, startPoint.y);
            
            // end of second small line
            CGContextAddLineToPoint(currentContext, secondSmall.x, secondSmall.y);
            
            // tip of arrow
            CGContextMoveToPoint(currentContext, startPoint.x, startPoint.y);
            
            // base of long arrow line
            CGContextAddLineToPoint(currentContext, endPoint.x, endPoint.y);
            
            
            
        }

        CGContextStrokePath(currentContext);
        
        CGContextClipToRect(currentContext, myRect);
    }
    
    [super drawRect:rect];
}

@end
