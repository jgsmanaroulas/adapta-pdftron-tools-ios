//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "Tool.h"
#import "MagnifierView.h"

@class SelectionBar;

@interface TextSelectTool : Tool {
    SelectionBar* m_moving_selection_bar;
    
    CGPoint selectionStart;
    CGPoint selectionEnd;
    int selectionStartPage;
    int selectionEndPage;
    
    TrnMagnifierView* loupe;
    
    SelectionBar* leftBar;
    SelectionBar* rightBar;
    
    BOOL swapBars;
    
    CGPoint selectionStartCorner, selectionEndCorner;
    
    NSMutableArray* selectionLayers;
    
    BOOL selectionOnScreen;
    
    BOOL tapOK;
    
    
    CGPoint lastCurrStartCorner;
    CGPoint lastCurrEndCorner;
    
}

-(void)onLayoutChanged;
-(BOOL)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer;
-(void)DrawSelectionQuads:(NSMutableArray*)quads WithLines:(BOOL)lines;
-(void)selectionBarMoved:(SelectionBar*) bar withTouches:(NSSet *) touches withEvent: (UIEvent *) event;
-(void)createTextMarkupAnnot:(PTAnnotType)annotType;
-(void)createTextMarkupAnnot:(PTAnnotType)annotType withColor:(PTColorPt*)color withComponents:(int)components withOpacity:(double)opacity;
-(void)CopySelectedTextToPasteboard;
-(void)longPressTextSelectAt: (CGPoint) down WithRecognizer: (UILongPressGestureRecognizer *) gestureRecognizer;
-(NSMutableArray*)MakeSelection;

-(void)ClearSelectionBars;
-(void)ClearSelectionOnly;
-(void)selectionBarUp:(SelectionBar*) bar withTouches:(NSSet *) touches withEvent: (UIEvent *) event;
-(void)ShowMenuController;
-(NSMutableArray*)GetQuadsFromPage:(int)page1 ToPage:(int)page2;
-(NSString*)GetSelectedTextFromPage:(int)page1 ToPage:(int)page2;
- (void)DrawSelectionBars:(NSMutableArray*)selection;
@end
