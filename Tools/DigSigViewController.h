//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>
@class DigSigView;
@class DigitalSignatureTool;
@protocol DigSigViewControllerDelegate;

@interface DigSigViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
	DigSigView* m_digSigView;

}

@property (nonatomic, assign) CGFloat strokeThickness;
@property (nonatomic, strong) UIColor* strokeColor;
@property (nonatomic, assign) BOOL allowDigitalSigning;

-(void)resetDigSigView;
-(UIImage*)correctForRotation:(UIImage*)src;

/**
 * An object that conforms to the DigSigViewControllerDelegate protocol.
 *
 */
@property (nonatomic, weak) id<DigSigViewControllerDelegate> delegate;

@property UIImage* m_image;

@end


@protocol DigSigViewControllerDelegate<NSObject>

@required
-(void)saveAppearanceWithPath:(NSMutableArray*)points fromCanvasSize:(CGSize)canvasSize;
-(void)saveAppearanceWithUIImage:(UIImage*)image;
-(void)closeSignatureDialog;
-(void)signAndSave;
@end
