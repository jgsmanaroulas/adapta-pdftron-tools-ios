//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import "Tool.h"
#import "AnnotEditTool.h"
#import "DigSigViewController.h"
#import "FloatingSigViewController.h"

@class TRNStampManager;
//@class DigSigViewController;
//@class FloatingSigViewController;
//@protocol FloatingSigViewControllerProtocol;

typedef enum {
    e_signature_digitally_signed,
    e_signature_path_appearance_only,
    e_signature_image_apearance_only,
	e_signature_empty
} SignatureState;

typedef enum {
	e_digital_mode,
	e_floating_mode
} ToolMode;


@interface DigitalSignatureTool : Tool<DigSigViewControllerDelegate, FloatingSigViewControllerDelegate>
{
	DigSigViewController* m_digSigViewController;
	FloatingSigViewController* m_floatingSigViewController;

}

@property (nonatomic, assign) CGFloat strokeThickness;
@property (nonatomic, strong) UIColor* strokeColor;
@property (nonatomic, assign) BOOL allowDigitalSigning;

-(void)showFloatingSignatureViewController;

/**
 *
 * @param points - An array of NSValue objects that hold CGPoints, representing
 * points that are used to construct the signature path. CGPointZero denotes
 * the beginging of a new picewise linear curve.
 *
 */
-(void)saveAppearanceWithPath:(NSMutableArray*)points fromCanvasSize:(CGSize)canvasSize;

-(void)normalizePoints:(NSMutableArray*)points onCanvasSize:(CGSize)canvasSize;

-(void)closeSignatureDialog;

-(void)signAndSave;

-(void)deleteAppearance;

@end
