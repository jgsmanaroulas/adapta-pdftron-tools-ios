//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------


#import "TextMarkupCreate.h"

/**
    Creates a text strikeout annotation.
 */
@interface TextStrikeoutCreate : TextMarkupCreate

@end
