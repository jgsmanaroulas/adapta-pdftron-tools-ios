//
//  Copyright 2011-12 PDFTron Systems Inc All rights reserved.
//

#import "CreateToolBase.h"
#import "PanTool.h"
#import "ArrowCreate.h"
#import "ColorDefaults.h"
#import "AnnotEditTool.h"
#import "LineEditTool.h"
#import "TextMarkupEditTool.h"
#import "StickyNoteCreate.h"

@implementation CreateToolBase


- (instancetype)initWithPDFViewCtrl:(PTPDFViewCtrl*)in_pdfViewCtrl
{
    
    self = [super initWithPDFViewCtrl:in_pdfViewCtrl];
    if (self) {
        [m_pdfViewCtrl setScrollEnabled:NO];
        self.opaque = NO;
    }
    
    return self;
}

-(PTAnnotType)annotType
{
	assert(false);
	return -1;
}

+(BOOL)createsAnnotation
{
	return YES;
}

-(BOOL)handleTap:(UITapGestureRecognizer *)sender
{
    // don't create a tiny annotation
    if( self.backToPanToolAfterUse)
    {
        nextToolType = [PanTool class];
        return NO;
    }
    else
    {
		Tool* editTool = [[PanTool alloc] initWithPDFViewCtrl:m_pdfViewCtrl];
		
		while(![editTool handleTap:sender])
		{
			editTool = [editTool getNewTool];
		}
		
		if([editTool isKindOfClass:[AnnotEditTool class]] || [editTool isKindOfClass:[TextMarkupEditTool class]])
		{
			nextToolType = [editTool class];
			self.defaultClass = [self class];
			return NO;
		}
		
        return YES;
    }
}

- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	// keeps tool on top after a two-finger page change in
	// non-continous page view modes.
    [self.superview bringSubviewToFront:self];
	
    UITouch *touch = touches.allObjects[0];
    
    m_num_touches = touches.count;
    
    endPoint = startPoint = [touch locationInView:m_pdfViewCtrl];
    
    pageNumber = [m_pdfViewCtrl GetPageNumberFromScreenPt:startPoint.x y:startPoint.y];
    
    if( pageNumber <= 0 )
    {
        return YES;
    }
    
    self.hidden = NO;
    
    self.backgroundColor = [UIColor clearColor];
    
    self.frame = CGRectMake([m_pdfViewCtrl GetHScrollPos], [m_pdfViewCtrl GetVScrollPos], 0, 0);
    
    PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
    
    @try
    {
        [m_pdfViewCtrl DocLockRead];
    
        PTPage* pg = [doc GetPage:pageNumber];
        
        PTPDFRect* pageRect = [pg GetCropBox];
        
        CGRect cropBox = [self PDFRectPage2CGRectScreen:pageRect PageNumber:pageNumber];
        
        CGRect cropBoxContainer = CGRectMake(cropBox.origin.x+[m_pdfViewCtrl GetHScrollPos], cropBox.origin.y+[m_pdfViewCtrl GetVScrollPos], cropBox.size.width, cropBox.size.height);
        
		CGContextRef context = UIGraphicsGetCurrentContext();
		
		if( context )
			CGContextClipToRect(context, cropBoxContainer);
    
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }
    
    
    return YES;
}



- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{

    UITouch *touch = touches.allObjects[0];
    
    m_num_touches = touches.count;
    
    endPoint = [touch locationInView:m_pdfViewCtrl];
	
	CGPoint touchPoint = [touch locationInView:m_pdfViewCtrl];
	
	endPoint = [self boundToPageScreenPoint:touchPoint withThicknessCorrection:0];
    
    CGPoint origin = CGPointMake(MIN(startPoint.x, endPoint.x), MIN(startPoint.y, endPoint.y));
    
    double drawWidth = MAX(0,fabs(endPoint.x-startPoint.x));
    double drawHeight = MAX(0,fabs(endPoint.y-startPoint.y));

    // used by rectangle and ellipse
    drawArea = CGRectMake(origin.x, origin.y, drawWidth, drawHeight);
    
    double width = m_pdfViewCtrl.frame.size.width;
    double height = m_pdfViewCtrl.frame.size.height;
    
    self.frame = CGRectMake([m_pdfViewCtrl GetHScrollPos], [m_pdfViewCtrl GetVScrollPos], width, height);

    return YES;
}

NSString* NSStringFromPTPDFRect(PTPDFRect* ptpdfrect)
{
    return [[NSString stringWithFormat:@"{{%f, %f}, {%f, %f}}; {%f, %f}", [ptpdfrect GetX1], [ptpdfrect GetY1], [ptpdfrect GetX2], [ptpdfrect GetY2], [ptpdfrect Width], [ptpdfrect Height]] copy];
}

- (BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{

    if( self.allowScrolling || (fabs(startPoint.x-endPoint.x) <= 3 && fabs(startPoint.y-endPoint.y) <= 3 && pdfNetAnnotType != [PTText class]) )
    {
        if( self.backToPanToolAfterUse)
        {
            nextToolType = [PanTool class];
            return NO;
        }
        else
        {
            return YES;
        }
    }
	
	if( pdfNetAnnotType != [PTText class] )
		[self keepToolAppearanceOnScreen];

	
    m_num_touches = touches.count;
    
    PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
    PTAnnot* annotation;

    @try
    {
        [m_pdfViewCtrl DocLock:YES];
    
        self.hidden = YES;

        @try
        {
            
            if (pageNumber < 1)
            {
                nextToolType = self.defaultClass;
                
                return NO;
            }
            
            PTPage* pg = [doc GetPage:pageNumber];
			
			assert([pg IsValid]);
            
            [self ConvertScreenPtToPagePtX:&startPoint.x Y:&startPoint.y PageNumber:pageNumber];
            [self ConvertScreenPtToPagePtX:&endPoint.x Y:&endPoint.y PageNumber:pageNumber];

            PTPDFRect* myRect;
            
            if( pdfNetAnnotType != [PTText class] ) // everything other than sticky
            {
                
                myRect = [[PTPDFRect alloc] initWithX1:startPoint.x y1:startPoint.y x2:endPoint.x y2:endPoint.y];
				
                annotation = [pdfNetAnnotType Create:(PTSDFDoc*)doc pos:myRect];
                
                if ([self isKindOfClass:[ArrowCreate class]])
                {
                    PTObj* lineSdf = [annotation GetSDFObj];
                    PTObj* le = [lineSdf PutArray:@"LE"];
                    [le PushBackName:@"OpenArrow"];
                    [le PushBackName:@"None"];
                }
                
                [self setPropertiesFromDefaultAnnotation: annotation];
				
				m_moving_annotation = annotation;
                m_annot_page_number = pageNumber;
                
            }
            else //sticky
            {
                NSString* str = [[NSString alloc] init];
				
				CGRect rect = self.frame;
				
				if( rect.size.width == 0 )
					rect.size.width = 1;
				if( rect.size.height == 0 )
					rect.size.height = 1;
				
        

				
                myRect = [[PTPDFRect alloc] initWithX1:endPoint.x y1:endPoint.y x2:endPoint.x+1 y2:endPoint.y+1];
                [myRect Normalize];

				annotation = [pdfNetAnnotType CreateTextWithRect:(PTSDFDoc*)doc pos:myRect contents:str];
                
                [((PTText*)annotation) SetTextIconType: e_ptComment];
                PTColorPt* colorPoint = [[PTColorPt alloc] initWithX:1.0 y:1.0 z:0.0 w:0.0];
                [((PTText*)annotation) SetColor:colorPoint numcomp:3];

//				// sample code for a custom sticky note icon. Also need to eliminate subsequent calls to RefreshAppearance
//				// which will regenerate the standard icon
//				PTElementWriter* writer = [[PTElementWriter alloc] init];
//				PTElementBuilder* builder = [[PTElementBuilder alloc] init];
//				
//				[writer WriterBeginWithSDFDoc:[doc GetSDFDoc] compress:YES];
//				PTImage* img = [PTImage Create:[doc GetSDFDoc] filename:[[NSBundle mainBundle] pathForResource:@"butterfly" ofType:@"png"]];
//				int w = [img GetImageWidth], h = [img GetImageHeight];
//				PTElement* img_element = [builder CreateImageWithCornerAndScale:img x:0 y:0 hscale:w vscale:h];
//				[writer WritePlacedElement:img_element];
//				
//				PTObj* appearance_stream = [writer End];
//
//				[appearance_stream PutRect:@"BBox" x1:0 y1:0 x2:w y2:h];
//				
//				annotation = [PTText CreateTextWithRect:(PTSDFDoc*)doc pos:myRect contents:@"Hello World"];
//				[annotation SetAppearance:appearance_stream annot_state:e_ptnormal app_state:0];
//				[pg AnnotPushBack:annotation];

				
                m_moving_annotation = annotation;
                m_annot_page_number = pageNumber;
                
                
            }
            
            
            [annotation RefreshAppearance];

			if( self.annotationAuthor && self.annotationAuthor.length > 0 && [annotation isKindOfClass:[PTMarkup class]]	)
			{
				[(PTMarkup*)annotation SetTitle:self.annotationAuthor];
			}
			
            [pg AnnotPushBack:annotation];
            
            if( pdfNetAnnotType == [PTText class] )
                [self editSelectedAnnotationNote];
            
        }
        @catch(NSException * e)
        {
            // continue
        }

        
    
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlock];
    }

    
    if( pageNumber > 0 )
        [m_pdfViewCtrl UpdateWithAnnot:annotation page_num:pageNumber];
	
	[self annotationAdded:annotation onPageNumber:pageNumber];
	
	if( pdfNetAnnotType == [PTText class] )
	{
		// required to make it appear upright in rotated documents
		[annotation RefreshAppearance];
	}
    
	m_moving_annotation = annotation;

	if( pdfNetAnnotType == [PTText class] )
	{
		nextToolType = [PanTool class]; // will pop up note edit window
		return YES;
	}
	else if( pdfNetAnnotType == [PTLineAnnot class] )
		nextToolType = [LineEditTool class];
	else
		nextToolType = [AnnotEditTool class];
	
	if( self.backToPanToolAfterUse && pdfNetAnnotType != [PTText class])
		self.defaultClass  = [PanTool class];
	else
		self.defaultClass = [self class];
	
	return NO;
}

-(CGPoint)boundToPageScreenPoint:(CGPoint)touchPoint
                         withPaddingLeft:(CGFloat)left
                                   right:(CGFloat)right
                                  bottom:(CGFloat)bottom
                                     top:(CGFloat)top
{
    CGFloat xt1, yt1;
    CGFloat xt2, yt2;
    
    @try
    {
        [m_pdfViewCtrl DocLockRead];
        
        PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
        
        PTPage* page = [doc GetPage:pageNumber];
        
        xt1 = [[page GetCropBox] GetX1];
        yt1 = [[page GetCropBox] GetY1];
        
        xt2 = [[page GetCropBox] GetX2];
        yt2 = [[page GetCropBox] GetY2];



        
        PTRotate r = [[m_moving_annotation GetPage] GetRotation];
        if( r == e_pt90 || r == e_pt270 )
        {
            // because the convert methods to not consider page rotation
            [self swapA:&xt2 B:&yt2];
        }
        
        [self ConvertPagePtToScreenPtX:&xt1 Y:&yt1 PageNumber:pageNumber];
        [self ConvertPagePtToScreenPtX:&xt2 Y:&yt2 PageNumber:pageNumber];
        
        
        
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }
    
    CGFloat minX = MIN(xt1, xt2)+left;
    CGFloat maxX = MAX(xt1, xt2)-right;
    CGFloat minY = MIN(yt1, yt2)+top;
    CGFloat maxY = MAX(yt1, yt2)-bottom;
    

    if( touchPoint.x < minX )
        touchPoint.x = minX;
    else if( touchPoint.x > maxX)
        touchPoint.x = maxX;
    
    if( touchPoint.y < minY )
        touchPoint.y = minY;
    else if( touchPoint.y > maxY)
        touchPoint.y = maxY;
    
    NSLog(@"Touch point %@", NSStringFromCGPoint(touchPoint));
    return CGPointMake(touchPoint.x, touchPoint.y);
}


-(CGPoint)boundToPageScreenPoint:(CGPoint)touchPoint withThicknessCorrection:(CGFloat)thickness
{
    return [self boundToPageScreenPoint:touchPoint
                           withPaddingLeft:thickness
                                     right:thickness
                                    bottom:thickness
                                       top:thickness];
}

- (void) setPropertiesFromDefaultAnnotation: (PTAnnot *) annotation  {
    
    @try
    {
        [m_pdfViewCtrl DocLockRead];
		
		PTAnnotType annotType = [annotation GetType];

        // stroke colour
		PTColorPt* strokeColor = [ColorDefaults defaultColorPtForAnnotType:annotType attribute:ATTRIBUTE_STROKE_COLOR colorPostProcessMode:e_ptpostprocess_none];
		int compNums = [ColorDefaults numCompsInColorPtForAnnotType:annotType attribute:ATTRIBUTE_STROKE_COLOR];
        [annotation SetColor:strokeColor numcomp:compNums];
        
        // thickness
		double width = [ColorDefaults defaultBorderThicknessForAnnotType:annotType];
		PTBorderStyle* bs = [[PTBorderStyle alloc] initWithS:e_ptsolid b_width:width b_hr:0 b_vr:0];
		
        [annotation SetBorderStyle:bs oldStyleOnly:NO];
        
        if ([annotation isKindOfClass:[PTMarkup class]]) {
            
			PTColorPt* fillColor = [ColorDefaults defaultColorPtForAnnotType:annotType attribute:ATTRIBUTE_FILL_COLOR colorPostProcessMode:e_ptpostprocess_none];
			int compNums = [ColorDefaults numCompsInColorPtForAnnotType:annotType attribute:ATTRIBUTE_FILL_COLOR];
			[(PTMarkup*)annotation SetInteriorColor:fillColor CompNum:compNums];
            
            // opacity
			double opacity = [ColorDefaults defaultOpacityForAnnotType:annotType];
            [(PTMarkup*)annotation SetOpacity:opacity];
        }
    
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }

}

- (BOOL)onTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    return [self onTouchesEnded:touches withEvent:event];
}

-(void)setFrame:(CGRect)frame
{
    super.frame = frame;
    [self setNeedsDisplay];
}

-(double)setupContextFromDefaultAnnot:(CGContextRef)currentContext
{
    @try
    {
		PTPDFDoc* doc = [m_pdfViewCtrl GetDoc];
		
		PTPage* pg = [doc GetPage:pageNumber];
		
		PTPDFRect* pageRect = [pg GetCropBox];
		
		CGRect cropBox = [self PDFRectPage2CGRectScreen:pageRect PageNumber:pageNumber];
		
		if( currentContext )
			CGContextClipToRect(currentContext, cropBox);

        [m_pdfViewCtrl DocLockRead];
		
		PTColorPostProcessMode mode = [m_pdfViewCtrl GetColorPostProcessMode];
		
    
		PTAnnotType annotType = [self annotType];
        
		if(! _strokeColor )
		{
			_strokeColor = [ColorDefaults defaultColorForAnnotType:annotType attribute:ATTRIBUTE_STROKE_COLOR colorPostProcessMode:mode];
		
			_fillColor = [ColorDefaults defaultColorForAnnotType:annotType attribute:ATTRIBUTE_FILL_COLOR colorPostProcessMode:mode];
			
			 _opacity = [ColorDefaults defaultOpacityForAnnotType:annotType];
			
			_thickness = [ColorDefaults defaultBorderThicknessForAnnotType:annotType];
			
			_thickness *= [m_pdfViewCtrl GetZoom];
		}
		
		CGContextSetLineWidth(currentContext, _thickness);
		CGContextSetLineCap(currentContext, kCGLineCapRound);
		CGContextSetLineJoin(currentContext, kCGLineJoinRound);
		CGContextSetStrokeColorWithColor(currentContext, _strokeColor.CGColor);
		CGContextSetFillColorWithColor(currentContext, _fillColor.CGColor);
		CGContextSetAlpha(currentContext, _opacity);
		
		
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@: %@",exception.name, exception.reason);
    }
    @finally {
        [m_pdfViewCtrl DocUnlockRead];
    }
    
    return _thickness;
}

@end
