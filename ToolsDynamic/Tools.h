//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

//! Project version number for Tools.
FOUNDATION_EXPORT double ToolsVersionNumber;

//! Project version string for Tools.
FOUNDATION_EXPORT const unsigned char ToolsVersionString[];


#import "UICollectionView+Draggable.h"
#import "UICollectionViewDataSource_Draggable.h"
#import "UICollectionViewLayout_Warpable.h"
#import "LSCollectionViewHelper.h"
#import "LSCollectionViewLayoutHelper.h"
#import "DraggableCollectionViewFlowLayout.h"
#import "AnalyticsHandlerAdapter.h"
#import "AnnotationToolbar.h"
#import "AnnotationViewController.h"
#import "BookmarkViewController.h"
#import "FreehandCreateToolbar.h"
#import "OutlineViewController.h"
#import "ThumbnailSliderViewController.h"
#import "ThumbnailsViewController.h"
#import "ThumbnailViewCell.h"
#import "BookmarkUtils.h"
#import "PDFViewCtrlToolsUtil.h"
#import "ToolManager.h"
#import "AnnotEditTool.h"
#import "FormFillTool.h"
#import "LineEditTool.h"
#import "ArrowCreate.h"
#import "ChoiceFormViewController.h"
#import "ColorDefaults.h"
#import "ColorSwatchesView.h"
#import "ColorSwatchViewController.h"
#import "CreateToolBase.h"
#import "EllipseCreate.h"
#import "FreeHandCreate.h"
#import "LineCreate.h"
#import "MagnifierView.h"
#import "NoFadeTiledLayer.h"
#import "NoteEditController.h"
#import "PanTool.h"
#import "RectangleCreate.h"
#import "ResizeWidgetView.h"
#import "RichMediaTool.h"
#import "SelectionBar.h"
#import "SelectionRectContainerView.h"
#import "SelectionRectView.h"
#import "StickyNoteCreate.h"
#import "FreeTextCreate.h"
#import "TextSelectTool.h"
#import "TextMarkupEditTool.h"
#import "TextMarkupCreate.h"
#import "TextHighlightCreate.h"
#import "TextSquigglyCreate.h"
#import "TextUnderlineCreate.h"
#import "TextStrikeoutCreate.h"
#import "Tool.h"
#import "DigitalSignatureTool.h"
#import "TRNStampManager.h"
#import "DigSigViewController.h"
#import "FloatingSigViewController.h"
#import "DigSigView.h"
#import "Eraser.h"
